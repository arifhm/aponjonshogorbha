package com.humaclab.aponjon;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT. Enable "keep" sections if you want to edit. 
/**
 * Entity mapped to table WEIGHT.
 */
public class Weight {

    private Long id;
    private Long id_server;
    private String user_id;
    private Integer weight;
    private Double weightGain;
    private java.util.Date date;
    private Boolean isSync;

    public Weight() {
    }

    public Weight(Long id) {
        this.id = id;
    }

    public Weight(Long id, Long id_server, String user_id, Integer weight, Double weightGain, java.util.Date date, Boolean isSync) {
        this.id = id;
        this.id_server = id_server;
        this.user_id = user_id;
        this.weight = weight;
        this.weightGain = weightGain;
        this.date = date;
        this.isSync = isSync;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId_server() {
        return id_server;
    }

    public void setId_server(Long id_server) {
        this.id_server = id_server;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public Double getWeightGain() {
        return weightGain;
    }

    public void setWeightGain(Double weightGain) {
        this.weightGain = weightGain;
    }

    public java.util.Date getDate() {
        return date;
    }

    public void setDate(java.util.Date date) {
        this.date = date;
    }

    public Boolean getIsSync() {
        return isSync;
    }

    public void setIsSync(Boolean isSync) {
        this.isSync = isSync;
    }

}
