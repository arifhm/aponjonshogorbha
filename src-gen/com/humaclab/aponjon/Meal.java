package com.humaclab.aponjon;

import java.util.List;
import com.humaclab.aponjon.DaoSession;
import de.greenrobot.dao.DaoException;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT. Enable "keep" sections if you want to edit. 
/**
 * Entity mapped to table MEAL.
 */
public class Meal {

    private Long id;
    private String meal_name;
    private Long id_server;
    private Float meal_cal_set_curb;
    private Float meal_cal_set_protien;
    private Float meal_cal_set_fat;
    private Float meal_cal_set_total;
    private Float req_cal_curb;
    private Float req_cal_protien;
    private Float req_cal_fat;
    private Float req_cal_total;
    private java.util.Date creation_date;
    private Boolean is_synced;

    /** Used to resolve relations */
    private transient DaoSession daoSession;

    /** Used for active entity operations. */
    private transient MealDao myDao;

    private List<Meal_Food> mealFoods;

    public Meal() {
    }

    public Meal(Long id) {
        this.id = id;
    }

    public Meal(Long id, String meal_name, Long id_server, Float meal_cal_set_curb, Float meal_cal_set_protien, Float meal_cal_set_fat, Float meal_cal_set_total, Float req_cal_curb, Float req_cal_protien, Float req_cal_fat, Float req_cal_total, java.util.Date creation_date, Boolean is_synced) {
        this.id = id;
        this.meal_name = meal_name;
        this.id_server = id_server;
        this.meal_cal_set_curb = meal_cal_set_curb;
        this.meal_cal_set_protien = meal_cal_set_protien;
        this.meal_cal_set_fat = meal_cal_set_fat;
        this.meal_cal_set_total = meal_cal_set_total;
        this.req_cal_curb = req_cal_curb;
        this.req_cal_protien = req_cal_protien;
        this.req_cal_fat = req_cal_fat;
        this.req_cal_total = req_cal_total;
        this.creation_date = creation_date;
        this.is_synced = is_synced;
    }

    /** called by internal mechanisms, do not call yourself. */
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getMealDao() : null;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMeal_name() {
        return meal_name;
    }

    public void setMeal_name(String meal_name) {
        this.meal_name = meal_name;
    }

    public Long getId_server() {
        return id_server;
    }

    public void setId_server(Long id_server) {
        this.id_server = id_server;
    }

    public Float getMeal_cal_set_curb() {
        return meal_cal_set_curb;
    }

    public void setMeal_cal_set_curb(Float meal_cal_set_curb) {
        this.meal_cal_set_curb = meal_cal_set_curb;
    }

    public Float getMeal_cal_set_protien() {
        return meal_cal_set_protien;
    }

    public void setMeal_cal_set_protien(Float meal_cal_set_protien) {
        this.meal_cal_set_protien = meal_cal_set_protien;
    }

    public Float getMeal_cal_set_fat() {
        return meal_cal_set_fat;
    }

    public void setMeal_cal_set_fat(Float meal_cal_set_fat) {
        this.meal_cal_set_fat = meal_cal_set_fat;
    }

    public Float getMeal_cal_set_total() {
        return meal_cal_set_total;
    }

    public void setMeal_cal_set_total(Float meal_cal_set_total) {
        this.meal_cal_set_total = meal_cal_set_total;
    }

    public Float getReq_cal_curb() {
        return req_cal_curb;
    }

    public void setReq_cal_curb(Float req_cal_curb) {
        this.req_cal_curb = req_cal_curb;
    }

    public Float getReq_cal_protien() {
        return req_cal_protien;
    }

    public void setReq_cal_protien(Float req_cal_protien) {
        this.req_cal_protien = req_cal_protien;
    }

    public Float getReq_cal_fat() {
        return req_cal_fat;
    }

    public void setReq_cal_fat(Float req_cal_fat) {
        this.req_cal_fat = req_cal_fat;
    }

    public Float getReq_cal_total() {
        return req_cal_total;
    }

    public void setReq_cal_total(Float req_cal_total) {
        this.req_cal_total = req_cal_total;
    }

    public java.util.Date getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(java.util.Date creation_date) {
        this.creation_date = creation_date;
    }

    public Boolean getIs_synced() {
        return is_synced;
    }

    public void setIs_synced(Boolean is_synced) {
        this.is_synced = is_synced;
    }

    /** To-many relationship, resolved on first access (and after reset). Changes to to-many relations are not persisted, make changes to the target entity. */
    public List<Meal_Food> getMealFoods() {
        if (mealFoods == null) {
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            Meal_FoodDao targetDao = daoSession.getMeal_FoodDao();
            List<Meal_Food> mealFoodsNew = targetDao._queryMeal_MealFoods(id);
            synchronized (this) {
                if(mealFoods == null) {
                    mealFoods = mealFoodsNew;
                }
            }
        }
        return mealFoods;
    }

    /** Resets a to-many relationship, making the next get call to query for a fresh result. */
    public synchronized void resetMealFoods() {
        mealFoods = null;
    }

    /** Convenient call for {@link AbstractDao#delete(Object)}. Entity must attached to an entity context. */
    public void delete() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }    
        myDao.delete(this);
    }

    /** Convenient call for {@link AbstractDao#update(Object)}. Entity must attached to an entity context. */
    public void update() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }    
        myDao.update(this);
    }

    /** Convenient call for {@link AbstractDao#refresh(Object)}. Entity must attached to an entity context. */
    public void refresh() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }    
        myDao.refresh(this);
    }

}
