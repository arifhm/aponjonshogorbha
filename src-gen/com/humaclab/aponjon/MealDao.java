package com.humaclab.aponjon;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import de.greenrobot.dao.AbstractDao;
import de.greenrobot.dao.Property;
import de.greenrobot.dao.internal.DaoConfig;

import com.humaclab.aponjon.Meal;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.
/** 
 * DAO for table MEAL.
*/
public class MealDao extends AbstractDao<Meal, Long> {

    public static final String TABLENAME = "MEAL";

    /**
     * Properties of entity Meal.<br/>
     * Can be used for QueryBuilder and for referencing column names.
    */
    public static class Properties {
        public final static Property Id = new Property(0, Long.class, "id", true, "_id");
        public final static Property Meal_name = new Property(1, String.class, "meal_name", false, "MEAL_NAME");
        public final static Property Id_server = new Property(2, Long.class, "id_server", false, "ID_SERVER");
        public final static Property Meal_cal_set_curb = new Property(3, Float.class, "meal_cal_set_curb", false, "MEAL_CAL_SET_CURB");
        public final static Property Meal_cal_set_protien = new Property(4, Float.class, "meal_cal_set_protien", false, "MEAL_CAL_SET_PROTIEN");
        public final static Property Meal_cal_set_fat = new Property(5, Float.class, "meal_cal_set_fat", false, "MEAL_CAL_SET_FAT");
        public final static Property Meal_cal_set_total = new Property(6, Float.class, "meal_cal_set_total", false, "MEAL_CAL_SET_TOTAL");
        public final static Property Req_cal_curb = new Property(7, Float.class, "req_cal_curb", false, "REQ_CAL_CURB");
        public final static Property Req_cal_protien = new Property(8, Float.class, "req_cal_protien", false, "REQ_CAL_PROTIEN");
        public final static Property Req_cal_fat = new Property(9, Float.class, "req_cal_fat", false, "REQ_CAL_FAT");
        public final static Property Req_cal_total = new Property(10, Float.class, "req_cal_total", false, "REQ_CAL_TOTAL");
        public final static Property Creation_date = new Property(11, java.util.Date.class, "creation_date", false, "CREATION_DATE");
        public final static Property Is_synced = new Property(12, Boolean.class, "is_synced", false, "IS_SYNCED");
    };

    private DaoSession daoSession;


    public MealDao(DaoConfig config) {
        super(config);
    }
    
    public MealDao(DaoConfig config, DaoSession daoSession) {
        super(config, daoSession);
        this.daoSession = daoSession;
    }

    /** Creates the underlying database table. */
    public static void createTable(SQLiteDatabase db, boolean ifNotExists) {
        String constraint = ifNotExists? "IF NOT EXISTS ": "";
        db.execSQL("CREATE TABLE " + constraint + "'MEAL' (" + //
                "'_id' INTEGER PRIMARY KEY AUTOINCREMENT ," + // 0: id
                "'MEAL_NAME' TEXT," + // 1: meal_name
                "'ID_SERVER' INTEGER," + // 2: id_server
                "'MEAL_CAL_SET_CURB' REAL," + // 3: meal_cal_set_curb
                "'MEAL_CAL_SET_PROTIEN' REAL," + // 4: meal_cal_set_protien
                "'MEAL_CAL_SET_FAT' REAL," + // 5: meal_cal_set_fat
                "'MEAL_CAL_SET_TOTAL' REAL," + // 6: meal_cal_set_total
                "'REQ_CAL_CURB' REAL," + // 7: req_cal_curb
                "'REQ_CAL_PROTIEN' REAL," + // 8: req_cal_protien
                "'REQ_CAL_FAT' REAL," + // 9: req_cal_fat
                "'REQ_CAL_TOTAL' REAL," + // 10: req_cal_total
                "'CREATION_DATE' INTEGER," + // 11: creation_date
                "'IS_SYNCED' INTEGER);"); // 12: is_synced
    }

    /** Drops the underlying database table. */
    public static void dropTable(SQLiteDatabase db, boolean ifExists) {
        String sql = "DROP TABLE " + (ifExists ? "IF EXISTS " : "") + "'MEAL'";
        db.execSQL(sql);
    }

    /** @inheritdoc */
    @Override
    protected void bindValues(SQLiteStatement stmt, Meal entity) {
        stmt.clearBindings();
 
        Long id = entity.getId();
        if (id != null) {
            stmt.bindLong(1, id);
        }
 
        String meal_name = entity.getMeal_name();
        if (meal_name != null) {
            stmt.bindString(2, meal_name);
        }
 
        Long id_server = entity.getId_server();
        if (id_server != null) {
            stmt.bindLong(3, id_server);
        }
 
        Float meal_cal_set_curb = entity.getMeal_cal_set_curb();
        if (meal_cal_set_curb != null) {
            stmt.bindDouble(4, meal_cal_set_curb);
        }
 
        Float meal_cal_set_protien = entity.getMeal_cal_set_protien();
        if (meal_cal_set_protien != null) {
            stmt.bindDouble(5, meal_cal_set_protien);
        }
 
        Float meal_cal_set_fat = entity.getMeal_cal_set_fat();
        if (meal_cal_set_fat != null) {
            stmt.bindDouble(6, meal_cal_set_fat);
        }
 
        Float meal_cal_set_total = entity.getMeal_cal_set_total();
        if (meal_cal_set_total != null) {
            stmt.bindDouble(7, meal_cal_set_total);
        }
 
        Float req_cal_curb = entity.getReq_cal_curb();
        if (req_cal_curb != null) {
            stmt.bindDouble(8, req_cal_curb);
        }
 
        Float req_cal_protien = entity.getReq_cal_protien();
        if (req_cal_protien != null) {
            stmt.bindDouble(9, req_cal_protien);
        }
 
        Float req_cal_fat = entity.getReq_cal_fat();
        if (req_cal_fat != null) {
            stmt.bindDouble(10, req_cal_fat);
        }
 
        Float req_cal_total = entity.getReq_cal_total();
        if (req_cal_total != null) {
            stmt.bindDouble(11, req_cal_total);
        }
 
        java.util.Date creation_date = entity.getCreation_date();
        if (creation_date != null) {
            stmt.bindLong(12, creation_date.getTime());
        }
 
        Boolean is_synced = entity.getIs_synced();
        if (is_synced != null) {
            stmt.bindLong(13, is_synced ? 1l: 0l);
        }
    }

    @Override
    protected void attachEntity(Meal entity) {
        super.attachEntity(entity);
        entity.__setDaoSession(daoSession);
    }

    /** @inheritdoc */
    @Override
    public Long readKey(Cursor cursor, int offset) {
        return cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0);
    }    

    /** @inheritdoc */
    @Override
    public Meal readEntity(Cursor cursor, int offset) {
        Meal entity = new Meal( //
            cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0), // id
            cursor.isNull(offset + 1) ? null : cursor.getString(offset + 1), // meal_name
            cursor.isNull(offset + 2) ? null : cursor.getLong(offset + 2), // id_server
            cursor.isNull(offset + 3) ? null : cursor.getFloat(offset + 3), // meal_cal_set_curb
            cursor.isNull(offset + 4) ? null : cursor.getFloat(offset + 4), // meal_cal_set_protien
            cursor.isNull(offset + 5) ? null : cursor.getFloat(offset + 5), // meal_cal_set_fat
            cursor.isNull(offset + 6) ? null : cursor.getFloat(offset + 6), // meal_cal_set_total
            cursor.isNull(offset + 7) ? null : cursor.getFloat(offset + 7), // req_cal_curb
            cursor.isNull(offset + 8) ? null : cursor.getFloat(offset + 8), // req_cal_protien
            cursor.isNull(offset + 9) ? null : cursor.getFloat(offset + 9), // req_cal_fat
            cursor.isNull(offset + 10) ? null : cursor.getFloat(offset + 10), // req_cal_total
            cursor.isNull(offset + 11) ? null : new java.util.Date(cursor.getLong(offset + 11)), // creation_date
            cursor.isNull(offset + 12) ? null : cursor.getShort(offset + 12) != 0 // is_synced
        );
        return entity;
    }
     
    /** @inheritdoc */
    @Override
    public void readEntity(Cursor cursor, Meal entity, int offset) {
        entity.setId(cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0));
        entity.setMeal_name(cursor.isNull(offset + 1) ? null : cursor.getString(offset + 1));
        entity.setId_server(cursor.isNull(offset + 2) ? null : cursor.getLong(offset + 2));
        entity.setMeal_cal_set_curb(cursor.isNull(offset + 3) ? null : cursor.getFloat(offset + 3));
        entity.setMeal_cal_set_protien(cursor.isNull(offset + 4) ? null : cursor.getFloat(offset + 4));
        entity.setMeal_cal_set_fat(cursor.isNull(offset + 5) ? null : cursor.getFloat(offset + 5));
        entity.setMeal_cal_set_total(cursor.isNull(offset + 6) ? null : cursor.getFloat(offset + 6));
        entity.setReq_cal_curb(cursor.isNull(offset + 7) ? null : cursor.getFloat(offset + 7));
        entity.setReq_cal_protien(cursor.isNull(offset + 8) ? null : cursor.getFloat(offset + 8));
        entity.setReq_cal_fat(cursor.isNull(offset + 9) ? null : cursor.getFloat(offset + 9));
        entity.setReq_cal_total(cursor.isNull(offset + 10) ? null : cursor.getFloat(offset + 10));
        entity.setCreation_date(cursor.isNull(offset + 11) ? null : new java.util.Date(cursor.getLong(offset + 11)));
        entity.setIs_synced(cursor.isNull(offset + 12) ? null : cursor.getShort(offset + 12) != 0);
     }
    
    /** @inheritdoc */
    @Override
    protected Long updateKeyAfterInsert(Meal entity, long rowId) {
        entity.setId(rowId);
        return rowId;
    }
    
    /** @inheritdoc */
    @Override
    public Long getKey(Meal entity) {
        if(entity != null) {
            return entity.getId();
        } else {
            return null;
        }
    }

    /** @inheritdoc */
    @Override    
    protected boolean isEntityUpdateable() {
        return true;
    }
    
}
