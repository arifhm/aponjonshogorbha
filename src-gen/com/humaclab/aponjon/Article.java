package com.humaclab.aponjon;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT. Enable "keep" sections if you want to edit. 
/**
 * Entity mapped to table ARTICLE.
 */
public class Article {

    private Long id;
    private Long ariticle_id;
    private String category;
    private String text_comment;
    private String img_link_server;
    private String img_link_local;
    private Integer week_id;
    private String weblink;
    private Boolean show_enabled;

    public Article() {
    }

    public Article(Long id) {
        this.id = id;
    }

    public Article(Long id, Long ariticle_id, String category, String text_comment, String img_link_server, String img_link_local, Integer week_id, String weblink, Boolean show_enabled) {
        this.id = id;
        this.ariticle_id = ariticle_id;
        this.category = category;
        this.text_comment = text_comment;
        this.img_link_server = img_link_server;
        this.img_link_local = img_link_local;
        this.week_id = week_id;
        this.weblink = weblink;
        this.show_enabled = show_enabled;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAriticle_id() {
        return ariticle_id;
    }

    public void setAriticle_id(Long ariticle_id) {
        this.ariticle_id = ariticle_id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getText_comment() {
        return text_comment;
    }

    public void setText_comment(String text_comment) {
        this.text_comment = text_comment;
    }

    public String getImg_link_server() {
        return img_link_server;
    }

    public void setImg_link_server(String img_link_server) {
        this.img_link_server = img_link_server;
    }

    public String getImg_link_local() {
        return img_link_local;
    }

    public void setImg_link_local(String img_link_local) {
        this.img_link_local = img_link_local;
    }

    public Integer getWeek_id() {
        return week_id;
    }

    public void setWeek_id(Integer week_id) {
        this.week_id = week_id;
    }

    public String getWeblink() {
        return weblink;
    }

    public void setWeblink(String weblink) {
        this.weblink = weblink;
    }

    public Boolean getShow_enabled() {
        return show_enabled;
    }

    public void setShow_enabled(Boolean show_enabled) {
        this.show_enabled = show_enabled;
    }

}
