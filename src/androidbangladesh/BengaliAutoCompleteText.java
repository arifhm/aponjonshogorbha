package androidbangladesh;

import android.content.Context;
import android.graphics.Typeface;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import com.omicronlab.avro.PhoneticParser;
import com.omicronlab.avro.PhoneticXmlLoader;

public class BengaliAutoCompleteText extends AutoCompleteTextView{

	public BengaliAutoCompleteText(Context context, AttributeSet attrs) {
		super(context, attrs);
//		Typeface tf = Typeface.createFromAsset(context.getAssets(), "solaimanlipinormal.ttf");
		setTypeface(tf);
		avro = PhoneticParser.getInstance();    
		avro.setLoader(new PhoneticXmlLoader(context));
		tw = new TextWatcher() {
			public void afterTextChanged(Editable s){
				try{
				setSelection(text.length());
				}catch(Exception e){}
			}
			public void  beforeTextChanged(CharSequence s, int start, int count, int after){
				// you can check for enter key here

			}
			public void  onTextChanged (CharSequence s, int start, int before,int count) {
				try{
					if(before == 0){
						in_text += s.toString().charAt(start);
					}else{
						in_text = in_text.substring(0, in_text.length()-1);
					}
					if(!in_text.equals(null)){
						out_text = avro.parse(in_text);
						removeTextChangedListener(tw);
						setText(text = out_text);
						//setText(text = BengaliUnicodeString.getBengaliUTF(out_text));
						addTextChangedListener(tw);
					}
				}catch(Exception e){}		
			} 
		};

		addTextChangedListener(tw);
	}





	String in_text = "";
	//String eng_token="";
	String out_text;
	PhoneticParser avro;
	TextWatcher tw;
	//TextView tv;
	String text;
	//String temp[];
	Context ctx;
	Typeface tf;

	public String getInputText(){
		return in_text;
	}
}