package com.dnet.mama.taskmanager;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.dnet.mama.diary.CF;
import com.fortysevendeg.swipelistview.BaseSwipeListViewListener;
import com.fortysevendeg.swipelistview.SwipeListView;
import com.humaclab.aponjon.DaoSession;
import com.humaclab.aponjon.Task;
import com.humaclab.aponjon.TaskType;
import com.humaclab.aponjon.TaskTypeDao;
import com.humaclab.aponjonshogorbha.AponjonApplication;
import com.humaclab.aponjonshogorbha.fetch.app.data.ContentUpdate;
import com.humaclab.aponjonshogorbha.fragments.dialogs.DatePickerFragment;
import com.humaclab.aponjonshogorbha.fragments.dialogs.TimePickerFragment;
import com.bd.aponjon.pregnancy.R;

public class AllTaskFragment extends Fragment implements OnClickListener {

	
	
	SwipeListView listView;
	List<Task> listData;
	List<TaskType> taskTypes;
	List<TaskType> customTaskTypes;
	TextView txtMonthLebel;
	
	private int REQUEST_DATE = 0;
	private int REQUEST_TIME = 1;
	private static final String DIALOG_DATE = "date";
	boolean thisMonthTask, allTask, dailyTask;
	Date taskDateTime;
	
	// change
	
	public static AllTaskFragment getInstance(String filter) {
		AllTaskFragment fr = new AllTaskFragment();
		Bundle bundle = new Bundle();
		bundle.putSerializable(TaskManagerFragment.FILTER, filter);
		fr.setArguments(bundle);
		
		return fr;
	}
	
	public static AllTaskFragment getInstance(String filter, Date date) {
		AllTaskFragment fr = new AllTaskFragment();		
		Bundle bundle = new Bundle();
		bundle.putSerializable(TaskManagerFragment.FILTER, filter);
		bundle.putSerializable("date", date);
		fr.setArguments(bundle);
		
		return fr;
	}
	
	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);		
		if (thisMonthTask) {
			((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("এই মাসের করণীয়");
		} else if (allTask) {
			((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("সব করণীয়");
		} else if(dailyTask) {
			((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("দিনের করণীয়");
		}
//		((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("");
	}
	@Override
	@Nullable
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		
			View v = inflater.inflate(R.layout.activity_all_task, container, false);
			v.findViewById(R.id.btnAddTask).setOnClickListener(this);
		
			txtMonthLebel=(TextView) v.findViewById(R.id.textView1);
			
		listView = (SwipeListView) v.findViewById(R.id.listView_all_task);
		listView.setSwipeListViewListener(new BaseSwipeListViewListener() {
            @Override
            public void onOpened(int position, boolean toRight) {
            }

            @Override
            public void onClosed(int position, boolean fromRight) {
            }

            @Override
            public void onListChanged() {
            }

            @Override
            public void onMove(int position, float x) {
            }

            @Override
            public void onStartOpen(int position, int action, boolean right) {
                Log.d("swipe", String.format("onStartOpen %d - action %d", position, action));
            }

            @Override
            public void onStartClose(int position, boolean right) {
                Log.d("swipe", String.format("onStartClose %d", position));
            }

            @Override
            public void onClickFrontView(int position) {
                Log.d("swipe", String.format("onClickFrontView %d", position));
                
             
                showDetailsMsg(listData.get(position));
               // listView.openAnimate(position); //when you touch front view it will open
              
             
            }

            @Override
            public void onClickBackView(int position) {
                Log.d("swipe", String.format("onClickBackView %d", position));
                
                listView.closeAnimate(position);//when you touch back view it will close
            }

            @Override
            public void onDismiss(int[] reverseSortedPositions) {
            	
            }

        });
		
	/*	listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				// TODO Auto-generated method stub
				showDetailsMsg(listData.get(position));
				
			}
		});*/
		
		
		
		//listView.setOnTouchListener(new SwipeListViewTouchListener(listView, R.id.front, R.id.back));
		
		//setting as your requirement 
		listView.setSwipeMode(SwipeListView.SWIPE_MODE_BOTH); // there are five swiping modes
		//listView.setSwipeActionLeft(SwipeListView.SWIPE_ACTION_DISMISS); //there are four swipe actions 
		listView.setSwipeActionRight(SwipeListView.SWIPE_ACTION_REVEAL);
		listView.setOffsetLeft(convertDpToPixel(200)); // left side offset
		listView.setOffsetRight(convertDpToPixel(200f)); // right side offset
		listView.setAnimationTime(500); // Animation time
		listView.setSwipeOpenOnLongPress(true); // enable or disable SwipeOpenOnLongPress
		listView.setSwipeOpenOnLongPress(true);
		
		
		
		
		Bundle bundle = this.getArguments();
		String filterType = (String) bundle.getSerializable(TaskManagerFragment.FILTER);
		long taskTypeId = bundle.getLong("taskTypeId"); 
		Date date =  (Date)bundle.getSerializable("date");
		
		ContentUpdate cu = new ContentUpdate(getActivity());
//		if(filter==null || filter.equals(FILTER_ALL)){
//			
//			listData = cu.getTaskList();
//			
//		}else if(filter.equals(FILTER_DAY)){
//			
//			Date date=(Date)bundle.getSerializable("date1");
//			listData = cu.getTaskList(date);
//			
//		} else if(filter.equals(FILTER_MONTH)){
//			
//			Date dateFrom=(Date)bundle.getSerializable("date1");
//			Date dateTo=(Date)bundle.getSerializable("date2");
//			listData = cu.getTaskListBetween(dateFrom, dateTo);
//			
//		} 
		
		//listData = cu.getTaskList(taskTypeId);
		if(filterType.equals(TaskManagerFragment.FILTER_DAY)){			
			txtMonthLebel.setText(CF.dateConvertToBangla(date));
			listData=cu.getThisDayTaskList(date);
			dailyTask = true;
			
		}else if(filterType.equals(TaskManagerFragment.FILTER_MONTH)){
			txtMonthLebel.setText(CF.getBanglaMonth(date)+" মাসের সব করনীয়");
			listData=cu.getThisMonthTaskList(taskTypeId, date);
			thisMonthTask = true;
			
		}else if(filterType.equals(TaskManagerFragment.FILTER_ALL)){
			txtMonthLebel.setText("সব করনীয়");
			listData=cu.getTaskList(taskTypeId);
			allTask = true;
		}
		
		
		
		

		// item mapping
		/*String[] from = new String[] { "title", "time", "desc" };
		int[] to = new int[] { R.id.textViewTitle, R.id.textViewtime,
				R.id.textViewDesc };

		// prepare the list of all records
		List<HashMap<String, String>> dataList = new ArrayList<HashMap<String, String>>();
		for (int i = 0; i < listData.size(); i++) {
			Task task = listData.get(i);
			HashMap<String, String> map = new HashMap<String, String>();

			SimpleDateFormat sdfDate = new SimpleDateFormat("dd MMM");// dd/MM/yyyy
			String strDate = sdfDate.format(task.getTask_date());
			map.put("time", strDate);
			map.put("title", task.getTitle());
			map.put("desc", task.getDescription());
			dataList.add(map);
		}*/

		// fill in the grid_item layout
		//SimpleAdapter adapter = new SimpleAdapter(getActivity(), dataList,R.layout.item_task_list, from, to);
		TaskAdapter adapter=new TaskAdapter(getActivity(), R.layout.item_task_list, listData);
		listView.setAdapter(adapter);
		
		return v;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (v.getId() == R.id.btnAddTask) {

			addTaskPopup();
		}
	}
	

	
	AlertDialog alertDialog=null;
	private void showDetailsMsg(Task task) {

		AlertDialog.Builder builder=new AlertDialog.Builder(getActivity())
		.setTitle(task.getTitle())
		.setMessage(Html.fromHtml( task.getDescription()))
		.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) { 
				
			}
		})
		.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) { 
				alertDialog.cancel();
			}
		})
		.setIcon(R.drawable.ic_launcher);

		alertDialog=builder.create();

		alertDialog.show();
	}
	
	void addTaskPopup(){
    	AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
    	// ...Irrelevant code for customizing the buttons and title
    	LayoutInflater inflater = getActivity().getLayoutInflater();
    	final View dialogView = inflater.inflate(R.layout.layout_add_task, null);
    	dialogBuilder.setView(dialogView);
    	final AlertDialog alertDialog = dialogBuilder.create();
    	
    	alertDialog.getWindow().setSoftInputMode( WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
    	alertDialog.show();
    	
    	//spinner
    	 AponjonApplication mApp = AponjonApplication.getInstance();
    	 DaoSession mDaoSession = mApp.getDaoSession();
         TaskTypeDao taskTypeDao = mDaoSession.getTaskTypeDao();
         taskTypes = taskTypeDao.queryBuilder().list();//where(TaskTypeDao.Properties.CatId.eq(1)).list();
         customTaskTypes = taskTypeDao.queryBuilder().where(TaskTypeDao.Properties.CatId.eq(1)).list();
         List<String> catNames = new ArrayList<>();
         List<String> customCatNames = new ArrayList<>();
         
         //remove duplicate
         for(TaskType tasktype : taskTypes) {
         	boolean isAddTaskname=true;
         	for (int i = 0; i < catNames.size(); i++) {
         		if(catNames.get(i).equals(tasktype.getName())){
         			isAddTaskname=false;
         			break;
         		}
 			}
         	
         	if(isAddTaskname){
         		catNames.add(tasktype.getName());
         	}
         	
         }
         
         for(TaskType customType : customTaskTypes) {
        	 customCatNames.add(customType.getName());
         }
         // add a blank position for init value
         customCatNames.add(0,"");
         
         
         
         CharSequence []Titles = customCatNames.toArray(new CharSequence[customCatNames.size()]);
         Spinner spinner=((Spinner)dialogView.findViewById(R.id.spinnerTAskCat));
         ArrayAdapter<CharSequence> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, Titles);
         spinner.setAdapter(adapter);
         
         taskDateTime = new Date(System.currentTimeMillis());
    	
    	//task date editTextTaskDate
         editTextTaskDate=(TextView) dialogView.findViewById(R.id.editTextTaskDate);
         editTextTaskDate.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				FragmentManager fm = getActivity().getSupportFragmentManager();
				DatePickerFragment dialog = DatePickerFragment.newInstance(taskDateTime, "Set Date");
				dialog.setTargetFragment(AllTaskFragment.this, REQUEST_DATE);
				dialog.show(fm, DIALOG_DATE);   
				
			}
		});
         
        //task time
         editTextTaskTime = (TextView) dialogView.findViewById(R.id.editTextTaskTime);
         editTextTaskTime.setOnClickListener(new View.OnClickListener() {
 			
 			@Override
 			public void onClick(View v) {
 				FragmentManager fm = getActivity().getSupportFragmentManager();
 				TimePickerFragment dialog = TimePickerFragment.newInstance(taskDateTime);
 				dialog.setTargetFragment(AllTaskFragment.this, REQUEST_TIME);
 				dialog.show(fm, DIALOG_DATE);   
 				
 			}
 		});
    	
    	dialogView.findViewById(R.id.buttonSaveTask).setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				ContentUpdate cu=new ContentUpdate(getActivity());
				//id, 
				String title = ((EditText)dialogView.findViewById(R.id.editTextTaskTile)).getText().toString();
				String description =  ((EditText)dialogView.findViewById(R.id.editTextTaskDesc)).getText().toString();
				int position = ((Spinner)dialogView.findViewById(R.id.spinnerTAskCat)).getSelectedItemPosition();
				if(position==0){
					Toast.makeText(getActivity(), "Set Task Type", Toast.LENGTH_SHORT).show();
					return;
				}else{
					position -=1;
				}
				
				long typeId = customTaskTypes.get(position).getId();
				cu.saveTask(null, title, description, typeId, taskDateTime);
				Toast.makeText(getActivity(), "Task Added", Toast.LENGTH_SHORT).show();
				alertDialog.dismiss();
			}
		});
    	
    	
    	
    }
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if(requestCode == REQUEST_DATE) {	    	
			Date date = (Date)data.getSerializableExtra(DatePickerFragment.EXTRA_DATE);
			updateDate(date);			
		}else if(requestCode == REQUEST_TIME) {	    	
			Date date = (Date)data.getSerializableExtra(TimePickerFragment.EXTRA_TIME);
			updateTime(date);			
		}
	}
	
	TextView editTextTaskDate;
	public void updateDate(Date date) {
		taskDateTime = date;
		SimpleDateFormat format = new SimpleDateFormat();
		format.applyPattern("yyyy-MM-dd");
		editTextTaskDate.setText(format.format(date));
	}	
	
	
	TextView editTextTaskTime;
	public void updateTime(Date date) {
		taskDateTime = date;
		SimpleDateFormat format = new SimpleDateFormat();
		format.applyPattern("hh-mm a");
		editTextTaskTime.setText(format.format(date));
	}
	
	
	
	 public int convertDpToPixel(float dp) {
	        DisplayMetrics metrics = getResources().getDisplayMetrics();
	        float px = dp * (metrics.densityDpi / 160f);
	        return (int) px;
	    }

}
