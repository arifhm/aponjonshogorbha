package com.dnet.mama.taskmanager;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.humaclab.aponjon.DaoSession;
import com.humaclab.aponjon.Task;
import com.humaclab.aponjon.TaskType;
import com.humaclab.aponjon.TaskTypeDao;
import com.humaclab.aponjonshogorbha.AponjonApplication;
import com.humaclab.aponjonshogorbha.Constants;
import com.humaclab.aponjonshogorbha.SessionManager;
import com.bd.aponjon.pregnancy.R;
import com.humaclab.aponjonshogorbha.fetch.app.data.ContentUpdate;

import com.humaclab.aponjonshogorbha.fragments.dialogs.TaskCompletePrompt;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import androidbangladesh.BengaliEditText;

public class TaskAdapter extends ArrayAdapter<Task> {
	SessionManager sessionMgr;
	SharedPreferences prefs;
	List<Task>   data; 
	Context context;
	int layoutResID;
	Date lmpDate;

	AppCompatActivity ac;
	private List<TaskType> customTaskTypes;

	public TaskAdapter(Context context, int layoutResourceId,List<Task> data) {
		super(context, layoutResourceId, data);

		this.data=data;
		this.context=context;
		this.layoutResID=layoutResourceId;
		sessionMgr = new SessionManager(context);
		prefs = sessionMgr.getPrefs();

		ac=((AppCompatActivity)this.context);

		SimpleDateFormat format = new SimpleDateFormat();
		format.applyPattern("yyyy-MM-dd");
		String lmp = prefs.getString(Constants.UserData.USER_LMP, null);
		try {
			lmpDate = format.parse(lmp);
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		NewsHolder holder = null;
		View row = convertView;

		if(row == null) {
			LayoutInflater inflater = ((Activity)context).getLayoutInflater();
			row = inflater.inflate(layoutResID, parent, false);

			holder = new NewsHolder();

			holder.textViewTitle = (TextView)row.findViewById(R.id.textViewTitle);
			holder.textViewtime = (TextView)row.findViewById(R.id.textViewtime);
			holder.textViewDesc = (TextView)row.findViewById(R.id.textViewDesc);
			holder.checkBox1 = (CheckBox)row.findViewById(R.id.checkBox1);
			holder.imageViewTaskType=(ImageView)row.findViewById(R.id.imageViewTaskType);
			holder.button1=(ImageView)row.findViewById(R.id.swipe_button1);
			holder.button2=(ImageView)row.findViewById(R.id.swipe_button2);
			row.setTag(holder);
		} else {
			holder = (NewsHolder)row.getTag();
		}

		final Task itemdata = data.get(position);
		if(itemdata.getCatId() == 2) {
			holder.button2.setVisibility(View.GONE);
		} else {
			holder.button2.setVisibility(View.VISIBLE);
		}
		
		holder.textViewTitle.setText(itemdata.getTitle());

		String strDate="";
		SimpleDateFormat sdfDate = new SimpleDateFormat("dd/MM/yyyy");// dd/MM/yyyy
		if(itemdata.getTask_date() !=null){
			strDate = sdfDate.format(itemdata.getTask_date());
		} else {
			int day = itemdata.getDayNo();
			Calendar cal = Calendar.getInstance();
			cal.setTime(lmpDate);
			cal.add(Calendar.DATE, day-1);

			strDate = sdfDate.format(cal.getTime());
		}

		holder.textViewtime.setText(strDate);
		holder.textViewDesc.setText(Html.fromHtml(itemdata.getDescription()));
		holder.checkBox1.setChecked(itemdata.getIsCompleted());
		//		if(itemdata.getIsCompleted()){
		//			holder.checkBox1.setChecked(true);
		//		}else{
		//			holder.checkBox1.setChecked(false);
		//		}
		//		
		//		if(itemdata.getIsCompleted() != null) {
		//			holder.checkBox1.setChecked(itemdata.getIsCompleted());
		//		}


		if(itemdata.getTypeId()==TaskManagerFragment.TASK_USER_VAC)
		{holder.imageViewTaskType.setImageResource(R.drawable.ic_vac);}
		else if(itemdata.getTypeId()==TaskManagerFragment.TASK_USER_MED)
		{holder.imageViewTaskType.setImageResource(R.drawable.ic_med);}
		else if(itemdata.getTypeId()==TaskManagerFragment.TASK_USER_EXC)
		{holder.imageViewTaskType.setImageResource(R.drawable.ic_exc);}
		else if(itemdata.getTypeId()==TaskManagerFragment.TASK_USER_OTHERS)
		{holder.imageViewTaskType.setImageResource(R.drawable.ic_med);}


		holder.checkBox1.setOnClickListener(new View.OnClickListener() {			
			@Override
			public void onClick(View v) {
				final View view = v;
				boolean isChecked = ((CheckBox) view).isChecked();

				if(isChecked) {
					TaskCompletePrompt promptComplete = TaskCompletePrompt.getInstance("Complete");

					promptComplete.setOnClickListener(new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int resultCode) {
//							Toast.makeText(ac, ""+resultCode, Toast.LENGTH_LONG).show();

							if(resultCode == 1) {
								((CheckBox) view).setChecked(true);
								itemdata.setIsCompleted(true);
								itemdata.setIsSync(false);
								completeTask(itemdata);
								dialog.dismiss();
							} else if (resultCode == 2) {
								((CheckBox) view).setChecked(false);
								dialog.dismiss();
							} else dialog.dismiss();
						}
					});

					Fragment fragmentByTag = ac.getSupportFragmentManager().findFragmentByTag(TaskCompletePrompt.TAG);
					if(fragmentByTag == null) {
						promptComplete.show(ac.getSupportFragmentManager(), TaskCompletePrompt.TAG);
					}
				} else {
					TaskCompletePrompt promptComplete = TaskCompletePrompt.getInstance("inComplete");

					promptComplete.setOnClickListener(new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int resultCode) {
//							Toast.makeText(ac, ""+resultCode, Toast.LENGTH_LONG).show();

							if(resultCode == 1) {
								((CheckBox) view).setChecked(false);
								itemdata.setIsCompleted(false);
								itemdata.setIsSync(false);
								completeTask(itemdata);
								dialog.dismiss();
							} else if (resultCode == 2) {
								((CheckBox) view).setChecked(true);
								dialog.dismiss();
							} else dialog.dismiss();
						}
					});

					Fragment fragmentByTag = ac.getSupportFragmentManager().findFragmentByTag(TaskCompletePrompt.TAG);
					if(fragmentByTag == null) {
						promptComplete.show(ac.getSupportFragmentManager(), TaskCompletePrompt.TAG);
					}

				}
			}
		});

		holder.button1.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				
				final View view = v;
				TaskCompletePrompt promptComplete = TaskCompletePrompt.getInstance("Complete");

				promptComplete.setOnClickListener(new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int resultCode) {
//						Toast.makeText(ac, ""+resultCode, Toast.LENGTH_LONG).show();
						ViewGroup topMostParent = (ViewGroup) view.getParent().getParent();
						ViewGroup topSiblingParent = (ViewGroup) topMostParent.findViewById(R.id.front);
						CheckBox checkbox = (CheckBox) topSiblingParent.findViewById(R.id.checkBox1);
						
						if(resultCode==1) {	
							checkbox.setChecked(true);
							itemdata.setIsCompleted(true);
							itemdata.setIsSync(false);
							completeTask(itemdata);
							dialog.dismiss();
						}
						if(resultCode==2) {
							checkbox.setChecked(false);
							itemdata.setIsCompleted(false);
							itemdata.setIsSync(false);
							completeTask(itemdata);
							dialog.dismiss();
						} else dialog.dismiss();
					}
				});

				Fragment fragmentByTag = ac.getSupportFragmentManager().findFragmentByTag(TaskCompletePrompt.TAG);
				if(fragmentByTag == null) {
					promptComplete.show(ac.getSupportFragmentManager(), TaskCompletePrompt.TAG);
				}

			}
		});

		holder.button2.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				if(itemdata.getCatId()==2){
					return;
				}


				editTaskPopup(itemdata);
			}
		});




		return row;

	}

	void editTaskPopup(final Task task){
		AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ac);
		// ...Irrelevant code for customizing the buttons and title
		LayoutInflater inflater = ac.getLayoutInflater();
		final View dialogView = inflater.inflate(R.layout.layout_add_task, null);
		dialogBuilder.setView(dialogView);
		final AlertDialog alertDialog = dialogBuilder.create();
		
		AponjonApplication mApp = AponjonApplication.getInstance();
   	 	DaoSession mDaoSession = mApp.getDaoSession();
        TaskTypeDao taskTypeDao = mDaoSession.getTaskTypeDao();
        
        customTaskTypes = taskTypeDao.queryBuilder().where(TaskTypeDao.Properties.CatId.eq(1)).list();
        List<String> customCatNames = new ArrayList<>();
        
        customCatNames.add(0,"");
        int currentIndex = 0;
        for(int i = 0; i<customTaskTypes.size(); i++) {
        	customCatNames.add(customTaskTypes.get(i).getName());
        	if(customTaskTypes.get(i).getId() == task.getTypeId() ) {
        		currentIndex = i+1;
        	}
        }
        // add a blank position for init value
       
        
        CharSequence []Titles = customCatNames.toArray(new CharSequence[customCatNames.size()]);
        Spinner spinner=((Spinner)dialogView.findViewById(R.id.spinnerTAskCat));
        ArrayAdapter<CharSequence> adapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item, Titles);
        spinner.setAdapter(adapter);

		//assign data
		((BengaliEditText)dialogView.findViewById(R.id.editTextTaskTile)).setText(task.getTitle());
		((BengaliEditText)dialogView.findViewById(R.id.editTextTaskDesc)).setText(task.getDescription());
		spinner.setSelection(currentIndex);

		Date date = task.getTask_date();
		if(date != null) {
			((TextView) dialogView.findViewById(R.id.editTextTaskDate)).setText(new SimpleDateFormat("dd/MM/yyyy").format(date));
			((TextView) dialogView.findViewById(R.id.editTextTaskTime)).setText(new SimpleDateFormat("hh:mm").format(date));
		}

		alertDialog.show();

		dialogView.findViewById(R.id.buttonSaveTask).setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				//id, 
				String title = ((BengaliEditText)dialogView.findViewById(R.id.editTextTaskTile)).getText().toString();
				String description =  ((BengaliEditText)dialogView.findViewById(R.id.editTextTaskDesc)).getText().toString();
				int position = ((Spinner)dialogView.findViewById(R.id.spinnerTAskCat)).getSelectedItemPosition();
				if(position!=0) position -=1;


				String stDate= ((TextView)dialogView.findViewById(R.id.editTextTaskDate)).getText().toString();
				String stTIme = ((TextView)dialogView.findViewById(R.id.editTextTaskTime)).getText().toString();

				Date updatetdTaskDate=null;
				SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh-mm a");
				try {
					updatetdTaskDate=dateFormat.parse(stDate+ " "+stTIme);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				task.setTitle(title);
				task.setDescription(description);
				task.setTypeId(customTaskTypes.get(position).getId());
				task.setTask_date(updatetdTaskDate);

				updateTask(task);

				//Toast.makeText(ac, "Task updated", Toast.LENGTH_SHORT).show();
				//alertDialog.dismiss();
			}
		});
	}

	void completeTask(Task task){

		ContentUpdate cu = new ContentUpdate(context);
		cu.updateTask(task);

		if(task.getIsCompleted())
		{
			Toast.makeText(context, "Complete",Toast.LENGTH_SHORT).show();
		}
		else
		{
			Toast.makeText(context, "not Complete",Toast.LENGTH_SHORT).show();
		}
	}

	void updateTask(Task task){
		task.setIsCompleted(true);
		ContentUpdate cu = new ContentUpdate(context);
		cu.updateTask(task);
		Toast.makeText(context, "Task Updated",Toast.LENGTH_SHORT).show();
	}



	static class NewsHolder{

		TextView textViewTitle;
		TextView textViewtime;
		TextView textViewDesc;
		CheckBox checkBox1;
		ImageView imageViewTaskType;
		ImageView button1;
		ImageView button2;
	}



}




