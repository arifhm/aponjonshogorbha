package com.dnet.mama.taskmanager;

import java.util.Date;

import com.humaclab.aponjonshogorbha.activities.DrawerFragmentActivity;

import android.os.Bundle;
import android.support.v4.app.Fragment;

public class TaskManagerTab extends DrawerFragmentActivity {

	@Override
	protected Fragment createFragment() {
		String curFilter= getIntent().getStringExtra(TaskManagerFragment.FILTER);
				
		if(curFilter.equals(TaskManagerFragment.FILTER_DAY)  || curFilter.equals(TaskManagerFragment.FILTER_MONTH)) {			
			Date date=(Date) getIntent().getSerializableExtra("date");
			return  TaskManagerTabFragment.getInstance(date, curFilter);			
		}else{
			return  TaskManagerTabFragment.getInstance(curFilter);
		}
				
	}
}