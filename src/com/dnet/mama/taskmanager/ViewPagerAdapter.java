package com.dnet.mama.taskmanager;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.humaclab.aponjonshogorbha.AponjonApplication;
import com.humaclab.aponjon.*;
import com.bd.aponjon.pregnancy.R;
import com.omicronlab.avro.phonetic.Data;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.text.SpannableString;
import android.text.style.ImageSpan;
 
/**
 * Created by hp1 on 21-01-2015.
 */
public class ViewPagerAdapter extends FragmentStatePagerAdapter {
	
	AponjonApplication mApp;
	DaoSession mDaoSession;
	List<String> mTypeIds;
	List<TaskType> taskTypes;
    CharSequence Titles[]; // This will Store the Titles of the Tabs which are Going to be passed when ViewPagerAdapter is created
    int NumbOfTabs; // Store the number of tabs, this will also be passed when the ViewPagerAdapter is created
    String filterType;
    Date date;
 
    // Build a Constructor and assign the passed Values to appropriate values in the class
    public ViewPagerAdapter(FragmentManager fm, String filterType, Date date) {
        super(fm);
        
        mApp = AponjonApplication.getInstance();
        mDaoSession = mApp.getDaoSession();
        TaskTypeDao taskTypeDao = mDaoSession.getTaskTypeDao();
        taskTypes = taskTypeDao.queryBuilder().list();
        List<String> catNames = new ArrayList<>();
        for(TaskType tasktype : taskTypes) {
        	boolean isAddTaskname=true;
        	for (int i = 0; i < catNames.size(); i++) {
        		if(catNames.get(i).equals(tasktype.getName())){
        			isAddTaskname=false;
        			break;
        		}
			}
        	
        	if(isAddTaskname){
        		catNames.add(tasktype.getName());
        	}
        	
        }
        this.Titles = catNames.toArray(new CharSequence[catNames.size()]);
        this.NumbOfTabs = Titles.length;
        this.filterType = filterType;
        this.date = date;
 
    }
 
    //This method return the fragment for the every position in the View Pager
    @Override
    public Fragment getItem(int position) {
    	
    	TaskType taskType = taskTypes.get(position); 
    	long id = taskType.getId();
    	
    	Bundle args=new Bundle(); 
        args.putSerializable("date", date);
        args.putSerializable(TaskManagerFragment.FILTER, filterType);
        args.putLong("taskTypeId", id);
       
        
        AllTaskFragment tab1 = new AllTaskFragment();
        tab1.setArguments(args);
        
        return tab1;
 
    }
 
    // This method return the titles for the Tabs in the Tab Strip
 
    @Override
    public CharSequence getPageTitle(int position) {
        return Titles[position];
    }
    
  
 
    // This method return the Number of tabs for the tabs Strip
 
    @Override
    public int getCount() {
        return NumbOfTabs;
    }
}