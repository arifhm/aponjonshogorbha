package com.dnet.mama.taskmanager;

import java.util.Date;

import com.bd.aponjon.pregnancy.R;
import com.dnet.mama.diary.DetailsFeelingFragment;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class TaskManagerTabFragment extends Fragment {
	
	// Declaring Your View and Variables	 
    Toolbar toolbar;
    ViewPager pager;
    ViewPagerAdapter adapter;
    SlidingTabLayout tabs;
    
    
	@Override
	@Nullable
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		
		View v = inflater.inflate(R.layout.activity_task_tab, container, false);
		
		String filterType = (String)getArguments().getSerializable(TaskManagerFragment.FILTER);
		Date date=null;
		if(filterType.equals(TaskManagerFragment.FILTER_DAY) || filterType.equals(TaskManagerFragment.FILTER_MONTH)) {
			date =(Date) getArguments().getSerializable("date");
		}
		
		// Creating The ViewPagerAdapter and Passing Fragment Manager, Titles fot the Tabs and Number Of Tabs.
        adapter =  new ViewPagerAdapter(getActivity().getSupportFragmentManager(), filterType, date);
 
        // Assigning ViewPager View and setting the adapter
        pager = (ViewPager) v.findViewById(R.id.pager);
        pager.setAdapter(adapter);
 
        // Assiging the Sliding Tab Layout View
        tabs = (SlidingTabLayout) v.findViewById(R.id.tabs);
        //tabs.setCustomTabView(R.layout.tab_layout_icon, R.id.tabTextView);
        tabs.setDistributeEvenly(true); // To make the Tabs Fixed set this true, This makes the tabs Space Evenly in Available width
 
        
        
        //set tab icon
        int[] iconResourceArray = { R.drawable.ic_tab_chekup,
        		R.drawable.ic_tab_hashpatal,
        		R.drawable.ic_tab_vac,
        		R.drawable.ic_tab_norachora,
        		R.drawable.ic_tab_taka_jomano,
        		R.drawable.ic_tab_hashpatal,
        		R.drawable.ic_tab_roktodan,
        		R.drawable.ic_tab_oushudh,
        		R.drawable.ic_tab_bayam};
        tabs.setIconResourceArray(iconResourceArray);
        
        // Setting Custom Color for the Scroll bar indicator of the Tab View
        tabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.tabsScrollColor);
            }
        });
 
        // Setting the ViewPager For the SlidingTabsLayout
        tabs.setViewPager(pager);
		
		return v;
	}
	
	public static TaskManagerTabFragment getInstance(Date date, String filterType ) {
		TaskManagerTabFragment fr = new TaskManagerTabFragment();
		Bundle bundle = new Bundle();
		bundle.putSerializable(TaskManagerFragment.FILTER, filterType);
		bundle.putSerializable("date", date);
		fr.setArguments(bundle);

		return fr;
	}
	
	public static TaskManagerTabFragment getInstance( String filterType ) {
		TaskManagerTabFragment fr = new TaskManagerTabFragment();
		Bundle bundle = new Bundle();
		bundle.putSerializable(TaskManagerFragment.FILTER, filterType);
		fr.setArguments(bundle);

		return fr;
	}
}
