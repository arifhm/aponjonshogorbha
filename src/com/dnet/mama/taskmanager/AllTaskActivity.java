package com.dnet.mama.taskmanager;

import java.util.Date;

import android.support.v4.app.Fragment;

import com.humaclab.aponjonshogorbha.activities.DrawerFragmentActivity;

public class AllTaskActivity extends DrawerFragmentActivity {

	@Override
	protected Fragment createFragment() {		
		String filter = getIntent().getExtras().getString(TaskManagerFragment.FILTER);
		if(filter.equals(TaskManagerFragment.FILTER_DAY)) {
			Date date = (Date) getIntent().getSerializableExtra("date");			
			return AllTaskFragment.getInstance(filter, date);
		} else {
			return AllTaskFragment.getInstance(filter);
		}		
	}
}
