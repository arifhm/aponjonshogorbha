package com.dnet.mama.taskmanager;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.humaclab.aponjon.DaoSession;
import com.humaclab.aponjon.Task;
import com.humaclab.aponjon.TaskType;
import com.humaclab.aponjon.TaskTypeDao;
import com.humaclab.aponjonshogorbha.AponjonApplication;
import com.humaclab.aponjonshogorbha.Constants;
import com.humaclab.aponjonshogorbha.fetch.app.data.ContentUpdate;

import com.humaclab.aponjonshogorbha.fragments.dialogs.DatePickerFragment;
import com.humaclab.aponjonshogorbha.fragments.dialogs.DemoModeDialog;
import com.humaclab.aponjonshogorbha.fragments.dialogs.TimePickerFragment;
import com.roomorama.caldroid.CaldroidFragment;
import com.roomorama.caldroid.CaldroidListener;

import com.bd.aponjon.pregnancy.R;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.CalendarView;
import android.widget.CalendarView.OnDateChangeListener;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import java.text.DateFormatSymbols;

public class TaskManagerFragment extends Fragment implements View.OnClickListener {

	final static int TASK_SYS=0;
	final static int TASK_USER_VAC=1;
	final static int TASK_USER_MED=2;
	final static int TASK_USER_EXC=3;
	final static int TASK_USER_OTHERS=4;

	public static final String FILTER="filter";
	static final String FILTER_DAY="day";
	public static final String FILTER_MONTH="month";
	static final String FILTER_ALL="all";

	private int REQUEST_DATE = 0;
	private int REQUEST_TIME = 1;
	private static final String DIALOG_DATE = "date";

	private CaldroidFragment caldroidFragment;
	private List<Task> tasksList;

	TextView textViewMonYr;
	Date curDate;
	List<TaskType> taskTypes;
	List<TaskType> customTaskTypes;
	//	int mSelectedMonth;

	Date mCurCalViewDate;
	Date taskDateTime;


	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("কবে কি করবো");
	}
	@Override
	@Nullable
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.activity_taskman, container, false);
		//			initializeCalendar(v);
		caldroidFragment = new CaldroidFragment();
		// If Activity is created after rotation
		if (savedInstanceState != null) {
			caldroidFragment.restoreStatesFromKey(savedInstanceState,
					"CALDROID_SAVED_STATE");
		}
		// If activity is created from fresh
		else {
			Bundle args = new Bundle();
			Calendar cal = Calendar.getInstance();
			args.putInt(CaldroidFragment.MONTH, cal.get(Calendar.MONTH) + 1);
			args.putInt(CaldroidFragment.YEAR, cal.get(Calendar.YEAR));
			args.putBoolean(CaldroidFragment.ENABLE_SWIPE, true);
			args.putBoolean(CaldroidFragment.SIX_WEEKS_IN_CALENDAR, true);

			//set custom theme
			args.putInt(CaldroidFragment.THEME_RESOURCE, com.caldroid.R.style.CaldroidAponjonApp);

			// Uncomment this to customize startDayOfWeek
			args.putInt(CaldroidFragment.START_DAY_OF_WEEK,CaldroidFragment.SATURDAY); // Tuesday

			//setCustomResourceForDates();

			//add custom event

			ContentUpdate cu= new ContentUpdate(getActivity());

			tasksList = cu.getAllTaskDateList();
						 
			caldroidFragment.setArguments(args);
		}

		//			setCustomResourceForDates();


		// Attach to the activity
		FragmentTransaction t = getActivity().getSupportFragmentManager().beginTransaction();
		t.replace(R.id.calendar1, caldroidFragment);
		t.commit();

		// Setup listener
		final CaldroidListener listener = new CaldroidListener() {

			@Override
			public void onSelectDate(Date date, View view) {					
				//					textViewMonYr.setText(convertToMonthYear(date));
				//Toast.makeText(getApplicationContext(),day + "/" + month + "/" + year, Toast.LENGTH_LONG).show();
				if(Constants.is_demo) {
					DemoModeDialog dialog = new DemoModeDialog(getActivity());
					dialog.show();
				} else {
					Intent intent =new Intent(getActivity(), AllTaskActivity.class);
					intent.putExtra(FILTER, FILTER_DAY);
					intent.putExtra("date", date);
					getActivity().startActivity(intent);
				}
			}

			@Override
			public void onChangeMonth(int month, int year) {
				Calendar cal = Calendar.getInstance();
				cal.set(year, month-1, 1);
				mCurCalViewDate = cal.getTime();
				//					mCurMonth = new DateFormatSymbols().getMonths()[month-1]+ " "+year;
				//					textViewMonYr.setText(my);
			}

			@Override
			public void onLongClickDate(Date date, View view) {
			}

			@Override
			public void onCaldroidViewCreated() {
			}

		};

		// Setup Caldroid
		caldroidFragment.setCaldroidListener(listener);


		v.findViewById(R.id.buttonAllTask).setOnClickListener(this);
		v.findViewById(R.id.buttonMonthTask).setOnClickListener(this);
		//v.findViewById(R.id.rlAddTask).setOnClickListener(this);
		v.findViewById(R.id.btnAddTask).setOnClickListener(this);


		curDate=new Date();
		//			textViewMonYr=(TextView) v.findViewById(R.id.textViewMonYr);
		//			textViewMonYr.setText(convertToMonthYear(curDate));

		return v;				
	}

	private void setCustomResourceForDates(Date date) {
		//		Calendar cal = Calendar.getInstance();
		//
		//		// Min date is last 7 days
		//		cal.add(Calendar.DATE, -18);
		//		Date blueDate = cal.getTime();
		//
		//		// Max date is next 7 days
		//		cal = Calendar.getInstance();
		//		cal.add(Calendar.DATE, 16);
		//		Date greenDate = cal.getTime();

		if (caldroidFragment != null) {
			caldroidFragment.setBackgroundResourceForDate(R.color.ColorPrimary, date);
			caldroidFragment.setTextColorForDate(R.color.white,  date);
			//caldroidFragment.setBackgroundResourceForDate(R.color.ColorPrimaryTransparent,greenDate);
			//			caldroidFragment.setTextColorForDate(R.color.white, blueDate);
			//caldroidFragment.setTextColorForDate(R.color.white, greenDate);
		}
	}

	/**
	 * Save current states of the Caldroid here
	 */
	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);

		if (caldroidFragment != null) {
			caldroidFragment.saveStatesToKey(outState, "CALDROID_SAVED_STATE");
		}

		//		if (dialogCaldroidFragment != null) {
		//			dialogCaldroidFragment.saveStatesToKey(outState,
		//					"DIALOG_CALDROID_SAVED_STATE");
		//		}
	}

	@Override
	public void onClick(View v) {

		//		FragmentManager fm = getActivity().getSupportFragmentManager();
		//		Fragment fragment = fm.findFragmentById(R.id.nav_contentframe);
		//
		//		Bundle bundle = new Bundle();

		switch (v.getId()) {
		case R.id.buttonAllTask :
			if(Constants.is_demo) {
				DemoModeDialog dialog = new DemoModeDialog(getActivity());
				dialog.show();
			} else {
				Intent i = new Intent(getActivity(), TaskManagerTab.class);
				i.putExtra(FILTER, FILTER_ALL);
				startActivity(i);
			}
			break;

		case R.id.buttonMonthTask :
			if(Constants.is_demo) {
				DemoModeDialog dialog = new DemoModeDialog(getActivity());
				dialog.show();
			} else {
				Intent intent =new Intent(getActivity(), TaskManagerTab.class);
				intent.putExtra(FILTER, FILTER_MONTH);
				intent.putExtra("date", mCurCalViewDate);
				getActivity().startActivity(intent);
			}		

			break;

		case R.id.rlAddTask:
			if(Constants.is_demo) {
				DemoModeDialog dialog = new DemoModeDialog(getActivity());
				dialog.show();
			} else {
				addTaskPopup();
			}
			break;
		case R.id.btnAddTask:		
			if(Constants.is_demo) {
				DemoModeDialog dialog = new DemoModeDialog(getActivity());
				dialog.show();
			} else {
				addTaskPopup();
			}
			break;


		default:
			break;
		}

		//
		//		if(fragment == null) {
		//			fm.beginTransaction().add(R.id.nav_contentframe, fragment).commit();
		//		} else {
		//			fm.beginTransaction().replace(R.id.nav_contentframe, fragment).commit();
		//		}

	}



	void addTaskPopup(){
		AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
		// ...Irrelevant code for customizing the buttons and title
		LayoutInflater inflater = getActivity().getLayoutInflater();
		final View dialogView = inflater.inflate(R.layout.layout_add_task, null);
		dialogBuilder.setView(dialogView);
		final AlertDialog alertDialog = dialogBuilder.create();

		alertDialog.getWindow().setSoftInputMode( WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
		alertDialog.show();

		//spinner
		AponjonApplication mApp = AponjonApplication.getInstance();
		DaoSession mDaoSession = mApp.getDaoSession();
		TaskTypeDao taskTypeDao = mDaoSession.getTaskTypeDao();
		taskTypes = taskTypeDao.queryBuilder().where(TaskTypeDao.Properties.CatId.eq(1)).list();
		customTaskTypes = taskTypeDao.queryBuilder().where(TaskTypeDao.Properties.CatId.eq(1)).list();
		List<String> catNames = new ArrayList<>();
		List<String> customCatNames = new ArrayList<>();

		//remove duplicate
		for(TaskType tasktype : taskTypes) {
			boolean isAddTaskname=true;
			for (int i = 0; i < catNames.size(); i++) {
				if(catNames.get(i).equals(tasktype.getName())){
					isAddTaskname=false;
					break;
				}
			}

			if(isAddTaskname){
				catNames.add(tasktype.getName());
			}

		}


		for(TaskType customType : customTaskTypes) {
			customCatNames.add(customType.getName());
		}
		// add a blank position for init value
		customCatNames.add(0,"");

		CharSequence []Titles = customCatNames.toArray(new CharSequence[customCatNames.size()]);
		Spinner spinner=((Spinner)dialogView.findViewById(R.id.spinnerTAskCat));
		ArrayAdapter<CharSequence> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, Titles);
		spinner.setAdapter(adapter);

		taskDateTime = new Date(System.currentTimeMillis());

		//task date editTextTaskDate
		editTextTaskDate=(TextView) dialogView.findViewById(R.id.editTextTaskDate);
		editTextTaskDate.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				FragmentManager fm = getActivity().getSupportFragmentManager();
				DatePickerFragment dialog = DatePickerFragment.newInstance(taskDateTime, "Set Date");
				dialog.setTargetFragment(TaskManagerFragment.this, REQUEST_DATE);
				dialog.show(fm, DIALOG_DATE);   
			}
		});

		//task time
		editTextTaskTime = (TextView) dialogView.findViewById(R.id.editTextTaskTime);
		editTextTaskTime.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				FragmentManager fm = getActivity().getSupportFragmentManager();
				TimePickerFragment dialog = TimePickerFragment.newInstance(taskDateTime);
				dialog.setTargetFragment(TaskManagerFragment.this, REQUEST_TIME);
				dialog.show(fm, DIALOG_DATE);   
			}
		});

		dialogView.findViewById(R.id.buttonSaveTask).setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				ContentUpdate cu=new ContentUpdate(getActivity());
				//id, 
				String title = ((EditText)dialogView.findViewById(R.id.editTextTaskTile)).getText().toString();
				String description =  ((EditText)dialogView.findViewById(R.id.editTextTaskDesc)).getText().toString();
				int position = ((Spinner)dialogView.findViewById(R.id.spinnerTAskCat)).getSelectedItemPosition();
				if(position==0){
					Toast.makeText(getActivity(), "Set Task Type", Toast.LENGTH_SHORT).show();
					return;
				}else{
					position -=1;
				}

				long typeId = customTaskTypes.get(position).getId();

				cu.saveTask(null, title, description, typeId, taskDateTime);
				setCustomResourceForDates(taskDateTime);
				caldroidFragment.refreshView();
				Toast.makeText(getActivity(), "Task Added", Toast.LENGTH_SHORT).show();
				alertDialog.dismiss();
			}
		});



	}





	private Date ConvertToDate(String dateString){
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		Date convertedDate = new Date();
		try {
			convertedDate = dateFormat.parse(dateString);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return convertedDate;
	}

	private String convertToMonthYear(Date date){

		SimpleDateFormat sdfDate = new SimpleDateFormat("MMMM yyyy");
		return sdfDate.format(date);

	}

	private String convertToMonthYear(int month, int year){


		SimpleDateFormat sdfDate = new SimpleDateFormat("MMMM yyyy");

		//sdfDate.parse(string)
		return "";

	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if(requestCode == REQUEST_DATE) {	    	
			Date date = (Date)data.getSerializableExtra(DatePickerFragment.EXTRA_DATE);
			updateDate(date);			
		}else if(requestCode == REQUEST_TIME) {	    	
			Date date = (Date)data.getSerializableExtra(TimePickerFragment.EXTRA_TIME);
			updateTime(date);			
		}
	}

	TextView editTextTaskDate;
	public void updateDate(Date date) {
		taskDateTime = date;

		SimpleDateFormat format = new SimpleDateFormat();
		format.applyPattern("yyyy-MM-dd");
		editTextTaskDate.setText(format.format(date));
	}	


	TextView editTextTaskTime;
	public void updateTime(Date date) {
		taskDateTime = date;
		SimpleDateFormat format = new SimpleDateFormat();
		format.applyPattern("hh-mm a");
		editTextTaskTime.setText(format.format(date));
	}	
	
	@Override
	public void onResume() {
		if(tasksList != null && tasksList.size()>0){
			for (int i = 0; i < tasksList.size(); i++) {
				Task task =tasksList.get(i);
				if(task.getTask_date() != null && caldroidFragment != null){
					caldroidFragment.setBackgroundResourceForDate(R.color.ColorPrimary, task.getTask_date());
					caldroidFragment.setTextColorForDate(R.color.white,  task.getTask_date());
				}						
			} 
		}
		super.onResume();
	}


}
