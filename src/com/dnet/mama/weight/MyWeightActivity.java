package com.dnet.mama.weight;

import android.support.v4.app.Fragment;

import com.humaclab.aponjonshogorbha.activities.DrawerFragmentActivity;

public class MyWeightActivity extends DrawerFragmentActivity {

	@Override
	protected Fragment createFragment() {
		return new MyWeightFragment();
	}

}
