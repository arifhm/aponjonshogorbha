package com.dnet.mama.weight;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import android.app.AlertDialog;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import bengali.language.support.BengaliUnicodeString;

import com.bd.aponjon.pregnancy.R;
import com.dnet.mama.diary.CF;
import com.humaclab.aponjon.Weight;
import com.humaclab.aponjonshogorbha.Constants;
import com.humaclab.aponjonshogorbha.SessionManager;
import com.humaclab.aponjonshogorbha.fetch.app.data.ContentUpdate;
import com.jjoe64.graphview.DefaultLabelFormatter;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.LegendRenderer;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;



public class MyWeightFragment extends Fragment implements View.OnClickListener{

	Spinner spinnerCurWeight, spinnerHeightFeet, spinnerHeightInch, spinnerPPWeight;
	boolean hasHwa;
	SessionManager mSessionMgr;
	SharedPreferences mPrefs;
	TextView mLabelWeight;
	ViewGroup mLyoutHeight;

	static final String WEIGHT_CAT_UNDER="under";//< 18.5
	static final String WEIGHT_CAT_NORMAL="normal";//>= 18.5 and < 25
	static final String WEIGHT_CAT_OVER="over";//>= 25 and < 30
	static final String WEIGHT_CAT_OBESE="obese";//> 30
	String myWeightCat="";
	double myBMI;

	double BMIStandard1=18.5;
	double BMIStandard2=25;
	double BMIStandard3=30;

	double underUper[]={0, 3, 18};
	double underUperSD[]={
			0,
			0.2308,
			0.4615,
			0.6923,
			0.9231,
			1.1538,
			1.3846,
			1.6154,
			1.8462,
			2.0769,
			2.3077,
			2.5385,
			2.7692,
			3.0000,
			3.5556,
			4.1111,
			4.6667,
			5.2222,
			5.7778,
			6.3333,
			6.8889,
			7.4444,
			8.0000,
			8.5556,
			9.1111,
			9.6667,
			10.2222,
			10.7778,
			11.3333,
			11.8889,
			12.4444,
			13.0000,
			13.5556,
			14.1111,
			14.6667,
			15.2222,
			15.7778,
			16.3333,
			16.8889,
			17.4444,
			18.0000
	};
	double underLower[]={0, 1, 12.5};
	double underLowerSD[]={
			0,		
			0.0769,
			0.1538,
			0.2308,
			0.3077,
			0.3846,
			0.4615,
			0.5385,
			0.6154,
			0.6923,
			0.7692,
			0.8462,
			0.9231,
			1.0000,
			1.4259,
			1.8519,
			2.2778,
			2.7037,
			3.1296,
			3.5556,
			3.9815,
			4.4074,
			4.8333,
			5.2593,
			5.6852,
			6.1111,
			6.5370,
			6.9630,
			7.3889,
			7.8148,
			8.2407,
			8.6667,
			9.0926,
			9.5185,
			9.9444,
			10.3704,
			10.7963,
			11.2222,
			11.6481,
			12.0741,
			12.5000};


	double normalLower[]={0, 1, 11.5};
	double normalLowerSD[]={
			0,
			0.0769,
			0.1538,
			0.2308,
			0.3077,
			0.3846,
			0.4615,
			0.5385,
			0.6154,
			0.6923,
			0.7692,
			0.8462,
			0.9231,
			1.0000,
			1.3889,
			1.7778,
			2.1667,
			2.5556,
			2.9444,
			3.3333,
			3.7222,
			4.1111,
			4.5000,
			4.8889,
			5.2778,
			5.6667,
			6.0556,
			6.4444,
			6.8333,
			7.2222,
			7.6111,
			8.0000,
			8.3889,
			8.7778,
			9.1667,
			9.5556,
			9.9444,
			10.3333,
			10.7222,
			11.1111,
			11.5000,
	};
	double normalUper[]={0, 3, 16};
	double normalUperSD[]={
			0,
			0.2308,
			0.4615,
			0.6923,
			0.9231,
			1.1538,
			1.3846,
			1.6154,
			1.8462,
			2.0769,
			2.3077,
			2.5385,
			2.7692,
			3.0000,
			3.4815,
			3.9630,
			4.4444,
			4.9259,
			5.4074,
			5.8889,
			6.3704,
			6.8519,
			7.3333,
			7.8148,
			8.2963,
			8.7778,
			9.2593,
			9.7407,
			10.2222,
			10.7037,
			11.1852,
			11.6667,
			12.1481,
			12.6296,
			13.1111,
			13.5926,
			14.0741,
			14.5556,
			15.0370,
			15.5185,
			16.0000
	};


	double overLower[]={0, 1, 7};
	double overLowerSD[]={
			0,
			0.0769,
			0.1538,
			0.2308,
			0.3077,
			0.3846,
			0.4615,
			0.5385,
			0.6154,
			0.6923,
			0.7692,
			0.8462,
			0.9231,
			1.0000,
			1.2222,
			1.4444,
			1.6667,
			1.8889,
			2.1111,
			2.3333,
			2.5556,
			2.7778,
			3.0000,
			3.2222,
			3.4444,
			3.6667,
			3.8889,
			4.1111,
			4.3333,
			4.5556,
			4.7778,
			5.0000,
			5.2222,
			5.4444,
			5.6667,
			5.8889,
			6.1111,
			6.3333,
			6.5556,
			6.7778,
			7.0000
	};
	double overUper[]={0, 3, 11.5};
	double overUperSD[]={
			0,
			0.2308,
			0.4615,
			0.6923,
			0.9231,
			1.1538,
			1.3846,
			1.6154,
			1.8462,
			2.0769,
			2.3077,
			2.5385,
			2.7692,
			3.0000,
			3.3148,
			3.6296,
			3.9444,
			4.2593,
			4.5741,
			4.8889,
			5.2037,
			5.5185,
			5.8333,
			6.1481,
			6.4630,
			6.7778,
			7.0926,
			7.4074,
			7.7222,
			8.0370,
			8.3519,
			8.6667,
			8.9815,
			9.2963,
			9.6111,
			9.9259,
			10.2407,
			10.5556,
			10.8704,
			11.1852,
			11.5000
	};

	double obeseLower[]={0, 0.5, 5};
	double obeseLowerSD[]={
			0,
			0.0385,
			0.0769,
			0.1154,
			0.1538,
			0.1923,
			0.2308,
			0.2692,
			0.3077,
			0.3462,
			0.3846,
			0.4231,
			0.4615,
			0.5000,
			0.6667,
			0.8333,
			1.0000,
			1.1667,
			1.3333,
			1.5000,
			1.6667,
			1.8333,
			2.0000,
			2.1667,
			2.3333,
			2.5000,
			2.6667,
			2.8333,
			3.0000,
			3.1667,
			3.3333,
			3.5000,
			3.6667,
			3.8333,
			4.0000,
			4.1667,
			4.3333,
			4.5000,
			4.6667,
			4.8333,
			5.0000
	};
	double obeseUper[]={0, 2, 9};
	double obeseUperSD[]={
			0,
			0.1538,
			0.3077,
			0.4615,
			0.6154,
			0.7692,
			0.9231,
			1.0769,
			1.2308,
			1.3846,
			1.5385,
			1.6923,
			1.8462,
			2.0000,
			2.2593,
			2.5185,
			2.7778,
			3.0370,
			3.2963,
			3.5556,
			3.8148,
			4.0741,
			4.3333,
			4.5926,
			4.8519,
			5.1111,
			5.3704,
			5.6296,
			5.8889,
			6.1481,
			6.4074,
			6.6667,
			6.9259,
			7.1852,
			7.4444,
			7.7037,
			7.9630,
			8.2222,
			8.4815,
			8.7407,
			9.0000
	};

	int weeks[]={0, 13, 40};

	double prePregHeight, prePregWeight;

	double currentLowerBMI, currentUpperBMI;
	int myCurrentWeek;

	LineGraphSeries<DataPoint> uperLineSerise, lowerLineSerise, currentSerise;

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("আমার ওজন");
	}


	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

		View v = inflater.inflate(R.layout.activity_my_weight, container, false);

		v.findViewById(R.id.buttonCalculateWeight).setOnClickListener(this);

		spinnerCurWeight=(Spinner) v.findViewById(R.id.spinnerCurWeight);
		spinnerHeightFeet=(Spinner) v.findViewById(R.id.spinnerHeightFeet);
		spinnerHeightInch=(Spinner) v.findViewById(R.id.spinnerHeightInch);
		spinnerPPWeight=(Spinner) v.findViewById(R.id.spinnerPPWeight);
		mLyoutHeight = (ViewGroup) v.findViewById(R.id.relativeLayout1);

		mLabelWeight = (TextView) v.findViewById(R.id.tv_label_pp_weight);

		mSessionMgr = new SessionManager(getActivity());
		mPrefs = mSessionMgr.getPrefs();
		hasHwa = mPrefs.getBoolean(Constants.UserData.HAS_HWA, false);

		if(hasHwa) {
			mLyoutHeight.setVisibility(View.GONE);
			mLabelWeight.setVisibility(View.VISIBLE);

			String weight = BengaliUnicodeString.convertToBengaliText(getActivity(), mPrefs.getInt(Constants.UserData.USER_WEIGHT, 0)+"")+ " কেজি";
			mLabelWeight.setText(weight);
			prePregWeight = (double) mPrefs.getInt(Constants.UserData.USER_WEIGHT, 0);
			float height = mPrefs.getFloat(Constants.UserData.USER_HEIGHT, 0f);
			int inch = (int) Math.round((height * 0.393701));
			prePregHeight = CF.inchToMeter(inch);
		}

		return v;
	}





	@Override
	public void onClick(View v) {
		if(v.getId() == R.id.buttonCalculateWeight){
			showAlert();
		}
	}

	double calculateBMI(double weightKG, double heightMeter){
		myBMI=weightKG/(heightMeter*heightMeter);

		return CF.roundDouble(myBMI);
	}

	public String getweightCat(double bmi){

		if(bmi<BMIStandard1){
			myWeightCat= WEIGHT_CAT_UNDER;

		}else if(bmi>=BMIStandard1 && bmi< BMIStandard2){
			myWeightCat= WEIGHT_CAT_NORMAL;

		}else if(bmi>=BMIStandard2 && bmi< BMIStandard3){
			myWeightCat= WEIGHT_CAT_OVER;

		}else if(bmi>BMIStandard3){
			myWeightCat= WEIGHT_CAT_OBESE;

		} 

		return myWeightCat;
	}

	void processGraphStandard(int curWeek){

		String stWeightCat=getweightCat(calculateBMI(prePregWeight, prePregHeight));

		if(stWeightCat.equals(WEIGHT_CAT_UNDER)){

			getLowerLineSerise(weeks, underLower);
			getUperLineSerise(weeks, underUper);

			setCurrentUperAndLowerBMI(underLowerSD, underUperSD, curWeek);

		}else  if(stWeightCat.equals(WEIGHT_CAT_NORMAL)){

			getLowerLineSerise(weeks, normalLower);
			getUperLineSerise(weeks, normalUper);

			setCurrentUperAndLowerBMI(normalLowerSD, normalUperSD, curWeek);

		}else if(stWeightCat.equals(WEIGHT_CAT_OVER)){

			getLowerLineSerise(weeks, overLower);
			getUperLineSerise(weeks, overUper);

			setCurrentUperAndLowerBMI(overLowerSD, overUperSD, curWeek);

		}else if(stWeightCat.equals(WEIGHT_CAT_OBESE)){

			getLowerLineSerise(weeks, obeseLower);
			getUperLineSerise(weeks, obeseUper);

			setCurrentUperAndLowerBMI(obeseLowerSD, obeseUperSD, curWeek);

		}
	}

	void setCurrentUperAndLowerBMI(double lower[], double uper[], int curWeek){

		currentLowerBMI =  lower[curWeek];
		currentUpperBMI = uper[curWeek];

		/* if(curWeek>13){

 		   currentLowerBMI = findY(weeks[0], lower[0], weeks[1], lower[1], curWeek);
 		   currentUpperBMI = findY(weeks[0], uper[0], weeks[1], uper[1], curWeek);

 	   }else{

 		  currentLowerBMI = findY(weeks[1], lower[1], weeks[2], lower[2], curWeek);
		   currentUpperBMI = findY(weeks[1], uper[1], weeks[2], uper[2], curWeek);
 	   }*/

	}

	double findY(double startX, double startY, double endX, double endY, double curX){

		double m = (endY-startY)/(endX-startX);
		double y = (m*curX) - ( m * startX)+ startY;
		return y;
	}


	LineGraphSeries<DataPoint>  getUperLineSerise(int weekarrs[], double seriseArr[]){

		uperLineSerise =  new LineGraphSeries<DataPoint>(new DataPoint[] {
				new DataPoint(weekarrs[0], seriseArr[0]),
				new DataPoint(weekarrs[1], seriseArr[1]),
				new DataPoint(weekarrs[2], seriseArr[2])
		});

		return uperLineSerise;
	}

	LineGraphSeries<DataPoint>  getLowerLineSerise(int weekarrs[], double seriseArr[]){

		lowerLineSerise =  new LineGraphSeries<DataPoint>(new DataPoint[] {
				new DataPoint(weekarrs[0], seriseArr[0]),
				new DataPoint(weekarrs[1], seriseArr[1]),
				new DataPoint(weekarrs[2], seriseArr[2])
		});

		return lowerLineSerise;
	}

	private void processCurrentWeight(double weightGain, int week) {
		// TODO Auto-generated method stub



		currentSerise  = new LineGraphSeries<DataPoint>(new DataPoint[] {
				new DataPoint(week, weightGain),
				new DataPoint(week+.1, weightGain)
		});
		currentSerise.setColor(Color.parseColor("#ED3E75"));
		currentSerise.setDrawDataPoints(true);
		currentSerise.setDataPointsRadius(5);
		currentSerise.setThickness(8);

		/*// custom paint to make a dotted line
 		    	Paint paint = new Paint();
 		    	paint.setStyle(Paint.Style.STROKE);
 		    	paint.setStrokeWidth(10);
 		    	paint.setPathEffect(new DashPathEffect(new float[]{8, 5}, 0))*/

 		
 	}
    
   
    
    void setWeightMsg(TextView textView,double weightGain ){
  	   
    	 if(weightGain==currentLowerBMI ||weightGain == currentUpperBMI || weightGain >= currentLowerBMI && weightGain <= currentUpperBMI){
   			  
   			  textView.setText(R.string.str_weight_msg_normal );
   			  
   		  }else if(weightGain < currentLowerBMI ){
    		   
    			  textView.setText(R.string.str_weight_msg_below_normal );
    			  
    		  }else if(weightGain > currentUpperBMI ){
    			 
    			  textView.setText(R.string.str_weight_msg_upper_normal );
    		  }
       }
    
  
   

     void showAlert(){
     	AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
     	// ...Irrelevant code for customizing the buttons and title
     	LayoutInflater inflater = getActivity().getLayoutInflater();
     	View dialogView = inflater.inflate(R.layout.layout_alert_weight_calculator, null);
     	dialogBuilder.setView(dialogView);
     	double wg= (spinnerCurWeight.getSelectedItemPosition()+weightSpinnerOffset)- prePregWeight;
     	
     	

     	
 		myCurrentWeek= getCurrentWeek(new Date());
 		//graph
     	GraphView graph = (GraphView) dialogView.findViewById(R.id.graph);
     	
     	//graph legend visibility and position and text size
     	graph.getLegendRenderer().setVisible(true);
     	graph.getLegendRenderer().setTextSize(16);
     	graph.getLegendRenderer().setAlign(LegendRenderer.LegendAlign.TOP);
     	graph.getLegendRenderer().setBackgroundColor(Color.WHITE);
     	
     	graph.getGridLabelRenderer().setLabelFormatter(new DefaultLabelFormatter() {
     	    @Override
     	    public String formatLabel(double value, boolean isValueX) {
     	    if (isValueX) {
     	        // show normal x values
     	        return BengaliUnicodeString.convertToBengaliText(getActivity(), super.formatLabel(value, isValueX));
     	    } else {
     	        // show current weight by adding pre pregnancy weight with Y value
     	    	 return BengaliUnicodeString.convertToBengaliText(getActivity(), super.formatLabel((value+prePregWeight), isValueX));
     	    }
     	    }
     	});
     	
     	processGraphStandard(myCurrentWeek);
     	
     	uperLineSerise.setTitle("ঊর্ধ্বসীমা");
     	uperLineSerise.setColor(Color.parseColor("#0A4B7C"));
     	graph.addSeries(uperLineSerise);
     	
     	lowerLineSerise.setTitle("নিম্নসীমা");
     	lowerLineSerise.setColor(Color.parseColor("#9DB1CC"));
     	graph.addSeries(lowerLineSerise);
 		
     	/*processCurrentWeight(wg,  myCurrentWeek);
     	currentSerise.setTitle("আপনার ওজন");
     	graph.addSeries(currentSerise);*/
     	
     	ContentUpdate cu= new ContentUpdate(getActivity());
     	List<Weight> list = cu.getWeightList();
     	
     	if(list != null || list.size() > 0){
     		DataPoint[] dataPoint=new DataPoint[list.size()+1];
     		for (int i = 0; i < list.size(); i++) {
//>>>>>>> 6f34c844eeda58e612c2e20b082da31afe82c24d
				Weight weight = list.get(i);

				dataPoint[i]=new DataPoint(getCurrentWeek(weight.getDate()), weight.getWeight()- prePregWeight);
				/*currentSerise  = new LineGraphSeries<DataPoint>(new DataPoint[] {
		 		        new DataPoint(getCurrentWeek(weight.getDate()), weight.getWeightGain()),
		 		        new DataPoint(getCurrentWeek(weight.getDate())+.1, weight.getWeightGain())
		 		    	});
		 		    	currentSerise.setColor(Color.parseColor("#EeeE75"));
		 		    	currentSerise.setDrawDataPoints(true);
		 		    	currentSerise.setDataPointsRadius(2);
		 		    	currentSerise.setThickness(4);*/



			}
     		
     		dataPoint[list.size()]=new DataPoint(myCurrentWeek, wg);

			currentSerise=new LineGraphSeries<DataPoint>(dataPoint);
			currentSerise.setColor(Color.parseColor("#ED3E25"));
			currentSerise.setDrawDataPoints(true);
			currentSerise.setDataPointsRadius(5);
			currentSerise.setThickness(5);

			currentSerise.setTitle("ওজন রেখা");
			graph.addSeries(currentSerise);

		}


		/*    	currentSerise  = new LineGraphSeries<DataPoint>(new DataPoint[] {
 		        new DataPoint(myCurrentWeek, currentLowerBMI),
 		        new DataPoint(myCurrentWeek+.1, currentLowerBMI)
 		    	});
 		    	currentSerise.setColor(Color.parseColor("#EeeE75"));
 		    	currentSerise.setDrawDataPoints(true);
 		    	currentSerise.setDataPointsRadius(5);
 		    	currentSerise.setThickness(8);

 		 graph.addSeries(currentSerise);

 		 currentSerise  = new LineGraphSeries<DataPoint>(new DataPoint[] {
 			        new DataPoint(myCurrentWeek, currentUpperBMI),
 			        new DataPoint(myCurrentWeek+.1, currentUpperBMI)
 			    	});
 			    	currentSerise.setColor(Color.parseColor("#EFFE75"));
 			    	currentSerise.setDrawDataPoints(true);
 			    	currentSerise.setDataPointsRadius(5);
 			    	currentSerise.setThickness(8);

 			 graph.addSeries(currentSerise);*/

		//Toast.makeText(getActivity(), ""+h, Toast.LENGTH_SHORT).show();
		TextView textViewWeightMsg =(TextView)dialogView.findViewById(R.id.textViewWeightMsg);
		setWeightMsg(textViewWeightMsg, wg);



		final AlertDialog alertDialog = dialogBuilder.create();

		Button btn = (Button) dialogView.findViewById(R.id.button1);
		btn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				alertDialog.dismiss();
				/*Intent intent =new Intent(MyWeightActivity.this, MyAllWeightActivity.class);
 				startActivity(intent);*/

				saveWeight();
			}
		});
		//btn.setVisibility(View.INVISIBLE);


		alertDialog.show();
	}


	public int  getCurrentWeek(Date date) {
		SessionManager sessionMgr= new SessionManager(getActivity());
		SharedPreferences prefs= sessionMgr.getPrefs();

		SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
		String date1 = prefs.getString(Constants.UserData.USER_LMP, null);
		Date lmpDate = null;
		try {
			lmpDate = format1.parse(date1);
		} catch (ParseException e) {

			e.printStackTrace();
		}
		
 		Date d1 = lmpDate;
 		Date d2 = date;
 		long diffWeeks=0;

 		try {
 			//in milliseconds
 			long diff = d2.getTime() - d1.getTime();	
 			 diffWeeks = diff / (7 * 24 * 60 * 60 * 1000);
 		} catch (Exception e) {
 			e.printStackTrace();
 		}
 		
 		return (int) diffWeeks;
 	}
     
     
 	
     		
     
    
 	int weightSpinnerOffset=39;
 	void saveWeight(){
 		
     	ContentUpdate cu= new ContentUpdate(getActivity());
     	int w=spinnerCurWeight.getSelectedItemPosition()+weightSpinnerOffset;
     	double wg = (spinnerCurWeight.getSelectedItemPosition()+weightSpinnerOffset)- prePregWeight;
     	
     	
     	cu.saveWeight(w, wg, new Date());
     	
     	Toast.makeText(getActivity(), "Weight saved", Toast.LENGTH_SHORT).show();
     	getActivity().finish();
     }
}
