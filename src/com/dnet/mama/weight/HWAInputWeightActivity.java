package com.dnet.mama.weight;

import android.support.v4.app.Fragment;

import com.humaclab.aponjonshogorbha.activities.DrawerFragmentActivity;
import com.humaclab.aponjonshogorbha.fragments.kikhabo.HWAInputFrgment;

public class HWAInputWeightActivity extends DrawerFragmentActivity {

	@Override
	protected Fragment createFragment() {
		return new HWAInputWeightFrgment();
	}

}
