package com.dnet.mama.weight;



import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import bengali.language.support.BengaliUnicodeString;

import com.dnet.mama.diary.CF;
import com.dnet.mama.movecounter.MoveCounterDetailsActivity;
import com.humaclab.aponjon.Weight;
import com.humaclab.aponjonshogorbha.fetch.app.data.ContentUpdate;

import com.bd.aponjon.pregnancy.R;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.SimpleAdapter;

public class MyAllWeightFragment extends Fragment {
	
	ListView listView;
	List<Weight> listdata;
	
	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("আমার সকল ওজন");
	}

	
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

		View v = inflater.inflate(R.layout.activity_all_feeling, container, false);
		
		listView=(ListView) v.findViewById(R.id.listView_all_feeelings);
		
		

		listdata= new ContentUpdate(getActivity()).getWeightList();
		
		// item mapping
		String[] from = new String[] { "date", "weight" };
		int[] to = new int[] { R.id.textViewListItemWeightDate, R.id.textViewListItemWeight };
				

				// prepare the list of all records
				List<HashMap<String, String>> dataList = new ArrayList<HashMap<String, String>>();
				for (int i = 0; i < listdata.size(); i++) {
					Weight weight = listdata.get(i);
					HashMap<String, String> map = new HashMap<String, String>();

					SimpleDateFormat sdfDate = new SimpleDateFormat("dd");// dd/MM/yyyy
					String strDate = BengaliUnicodeString.convertToBengaliText(getActivity(),  sdfDate.format(weight.getDate()) );
					map.put(from[0], strDate+" "+CF.getBanglaMonth(weight.getDate()));
					map.put(from[1], BengaliUnicodeString.convertToBengaliText(getActivity(),  weight.getWeight()+" কেজি") );
					dataList.add(map);
				}

				
				SimpleAdapter adapter = new SimpleAdapter(getActivity(), dataList,R.layout.item_all_weight, from, to);
		
				listView.setAdapter(adapter);

		return v;
	}
	
	

}
