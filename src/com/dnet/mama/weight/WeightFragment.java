package com.dnet.mama.weight;


import com.dnet.mama.diary.DiaryActivity;
import com.dnet.mama.diary.DiaryActivityDemo;
import com.humaclab.aponjonshogorbha.Constants;
import com.humaclab.aponjonshogorbha.SessionManager;
import com.bd.aponjon.pregnancy.R;
import com.humaclab.aponjonshogorbha.activities.kikhabo.HWAInputActivity;
import com.humaclab.aponjonshogorbha.fragments.dialogs.DemoModeDialog;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;


public class WeightFragment extends Fragment implements View.OnClickListener{
	SessionManager mSessionMgr;
	SharedPreferences mPrefs;
	boolean hasHwa;
	
	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("ওজন ঠিক বাড়ছে?");
	}

    
    @Override
	@Nullable
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.activity_weight, container, false);

		v.findViewById(R.id.ivMyWeight).setOnClickListener(this);
        v.findViewById(R.id.ivMyallWeight).setOnClickListener(this);
        v.findViewById(R.id.ivothersWeight).setOnClickListener(this);
        
		mSessionMgr = new SessionManager(getActivity());
		mPrefs = mSessionMgr.getPrefs();
		hasHwa = mPrefs.getBoolean(Constants.UserData.HAS_HWA, false);

		return v;
	}
    
    @Override
    public void onClick(View v) {
		
    	if(v.getId() == R.id.ivMyWeight){
    		if(Constants.is_demo){
    			DemoModeDialog dialog = new DemoModeDialog(getActivity());
				dialog.show();
			} else if(hasHwa) { 
				startActivity(new Intent(getActivity(), MyWeightActivity.class));
			} else startActivity(new Intent(getActivity(), HWAInputWeightActivity.class));
    	} else if(v.getId() == R.id.ivMyallWeight){
    		if(Constants.is_demo){
    			DemoModeDialog dialog = new DemoModeDialog(getActivity());
				dialog.show();
			} else startActivity(new Intent(getActivity(), MyAllWeightActivity.class));
    	} else if(v.getId() == R.id.ivothersWeight){
    		if(Constants.is_demo){
    			DemoModeDialog dialog = new DemoModeDialog(getActivity());
				dialog.show();
			} else startActivity(new Intent(getActivity(), OthersWeightActivity.class));
    	}
    	
    }
}
