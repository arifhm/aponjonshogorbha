package com.dnet.mama.movecounter;

import java.util.Date;

import android.support.v4.app.Fragment;

import com.humaclab.aponjonshogorbha.activities.DrawerFragmentActivity;

public class MoveCounterDetailsActivity extends DrawerFragmentActivity {

	@Override
	protected Fragment createFragment() {
		Date date = (Date) getIntent().getSerializableExtra("Date");
		int count = getIntent().getIntExtra("MoveCount", 0);
		return MoveCounterDetailsFragment.getInstance(date, count);
	}

}
