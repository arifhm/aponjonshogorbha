package com.dnet.mama.movecounter;



import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.humaclab.aponjon.Move;
import com.humaclab.aponjonshogorbha.fetch.app.data.ContentUpdate;

import com.bd.aponjon.pregnancy.R;



public class AllCountedMoveFragment extends Fragment {


	ListView listView;
	List<Move> Listdata;
	
	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("সোনামনি কবার নড়ল");
	}
	
	@Override
	@Nullable
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

		View v = inflater.inflate(R.layout.activity_all_feeling, container, false);

		ContentUpdate update=new ContentUpdate(getActivity());
		Listdata=update.getMoveCounterList();

		listView=(ListView)v.findViewById(R.id.listView_all_feeelings);
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int pos,
					long arg3) {
				Move move=Listdata.get(pos);

				Intent i = new Intent(getActivity(), MoveCounterDetailsActivity.class);
				i.putExtra("Date", move.getDate());
				i.putExtra("MoveCount", move.getMoveCount());
				startActivity(i);		
			}
		});

		ArrayAdapter<Move> adapter = new ArrayAdapter<Move>(getActivity(), R.layout.item_all_moves, R.id.textViewListItemMove, Listdata);
		listView.setAdapter(adapter);

		return v;
	}

}
