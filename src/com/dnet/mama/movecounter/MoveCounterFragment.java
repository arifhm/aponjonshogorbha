package com.dnet.mama.movecounter;


import java.util.Date;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import bengali.language.support.BengaliUnicodeString;

import com.humaclab.aponjonshogorbha.fetch.app.data.ContentUpdate;

import com.bd.aponjon.pregnancy.R;


public class MoveCounterFragment extends Fragment  implements View.OnClickListener{

	static private int totalCount=0;

	static TextView textViewShowCount, textViewTimeRemain;
	final int timeInms=12*60*60*1000;

	static CountDownTimer countDownTimer;
	static int toogleBtnState=0;

	ImageView imageViewMCStart;

	//parent view
	View pv;

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("সোনামনি কবার নড়ল");
	}

	@Override
	@Nullable
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

		View v = inflater.inflate(R.layout.activity_movecounter, container, false);
		pv=v;

		textViewShowCount=(TextView) v.findViewById(R.id.textViewShowCount);
		textViewTimeRemain=(TextView) v.findViewById(R.id.textViewTimeRemain);

		v.findViewById(R.id.imageViewCountPlus).setOnClickListener(this);
		v.findViewById(R.id.imageViewCountMinus).setOnClickListener(this);
		v.findViewById(R.id.buttonMoveCountSave).setOnClickListener(this);
		v.findViewById(R.id.imageViewMoveList).setOnClickListener(this);



		imageViewMCStart = (ImageView) v.findViewById(R.id.imageViewMCStart);
		imageViewMCStart.setImageResource(toogleBtnState==0?R.drawable.ic_start:R.drawable.ic_reset);
		imageViewMCStart.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				if (toogleBtnState == 0) {
					imageViewMCStart.setImageResource(R.drawable.ic_reset);
					toogleBtnState = 1;
					startTimer();

				} else if (toogleBtnState == 1) {


					if(countDownTimer!=null){


						showRestartAlert();
					}
				}
			}
		});
		// v.findViewById(R.id.buttonMoveCountSave).setEnabled(false);

		if(countDownTimer != null){
			textViewShowCount.setText(totalCount+ " বার");
		}

		return v;

	}

	void startTimer(){

		countDownTimer= new CountDownTimer(timeInms, 1000) {

			public void onTick(long millisUntilFinished) {
				textViewTimeRemain.setText(convertSecondsToHMmSs(millisUntilFinished/1000));
			}

			public void onFinish() {
				//textViewTimeRemain.setText("done!");
				pv.findViewById(R.id.buttonMoveCountSave).setEnabled(true);
				showfinishAlert();
			}
		};

		countDownTimer.start();
	}



	String convertSecondsToHMmSs(long seconds) {
//		long s = seconds % 60;
//		long m = (seconds / 60) % 60;
//		long h = (seconds / (60 * 60)) % 24;
//
//		String second = BengaliUnicodeString.convertToBengaliText(getActivity(), String.format("%02d", s) );
//		String minute = BengaliUnicodeString.convertToBengaliText(getActivity(), String.format("%02d", m) );
//		String hour = BengaliUnicodeString.convertToBengaliText(getActivity(), String.format("%d", h) );

		  
		long leftSeconds=(timeInms/1000)-seconds;
		long s = leftSeconds % 60;
		long m = (leftSeconds / 60) % 60;
		long h = (leftSeconds / (60 * 60)) % 24;
		
		String second = BengaliUnicodeString.convertToBengaliText(getActivity(), String.format("%02d", s) );
		String minute = BengaliUnicodeString.convertToBengaliText(getActivity(), String.format("%02d", m) );
		String hour = BengaliUnicodeString.convertToBengaliText(getActivity(), String.format("%d", h) );
		 
		String time = hour + ":" + minute + ":" + second;

		saveMsg = BengaliUnicodeString.convertToBengaliText(getActivity(), "আপনি "+hour+" ঘন্টা "+minute+" মিনিটে মোট "+totalCount+" বার নড়াচড়া গুণেছেন। সেইভ করতে চান?");

		return time;
	}

	@Override
	public void onClick(View v) {

		if(v.getId() == R.id.imageViewCountPlus){
			if(countDownTimer!=null){
				totalCount++;
				textViewShowCount.setText(BengaliUnicodeString.convertToBengaliText(getActivity(), totalCount+ " বার"));
			}

		}else if(v.getId() == R.id.imageViewCountMinus){
			if(totalCount==0) {
				Toast.makeText(getActivity(), "No move", Toast.LENGTH_SHORT).show();	
				return;
			}


			totalCount--;
			textViewShowCount.setText(totalCount+ " বার");
		}else if(v.getId() == R.id.buttonMoveCountSave){
			if(totalCount==0) {
				Toast.makeText(getActivity(), "No move to save", Toast.LENGTH_SHORT).show();	
				return;
			}

			showfinishAlert();

		}else if(v.getId() == R.id.imageViewMoveList){


			startActivity(new Intent(getActivity(), AllCountedMoveActivity.class));			
		}


	}

	private void saveMove(){
		ContentUpdate contentUpdate= new ContentUpdate(getActivity());
		contentUpdate.saveMove(totalCount, new Date());
		totalCount=0;
		textViewShowCount.setText(totalCount+ " বার");
		textViewTimeRemain.setText("00:00");
		Toast.makeText(getActivity(), "নড়াচড়া সেইভ । ", Toast.LENGTH_SHORT).show();
		getActivity().finish();

	}

	AlertDialog alertDialog=null;
	private void showRestartAlert() {

		AlertDialog.Builder builder=new AlertDialog.Builder(getActivity())
		.setTitle("Restart Timer")
		.setMessage("Are you sure you want to restart timer?")
		.setPositiveButton("হ্যাঁ", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) { 
				imageViewMCStart.setImageResource(R.drawable.ic_start);
				toogleBtnState = 0;
				countDownTimer.cancel();
				totalCount=0;
				textViewShowCount.setText(totalCount+ " বার");
				textViewTimeRemain.setText("00:00");

			}
		})
		.setNegativeButton("না", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) { 
				alertDialog.cancel();
			}
		})
		.setIcon(R.drawable.ic_launcher);

		alertDialog=builder.create();

		alertDialog.show();
	}

	String saveMsg="";
	private void showfinishAlert() {



		AlertDialog.Builder builder=new AlertDialog.Builder(getActivity())
		.setTitle("সোনামনি কবার নড়ল")
		.setMessage(saveMsg)
		.setPositiveButton("হ্যাঁ", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) { 
				saveMove();
				countDownTimer.cancel();
			}
		})
		.setNegativeButton("না", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) { 
				alertDialog.cancel();
			}
		})
		.setIcon(R.drawable.ic_launcher);

		alertDialog=builder.create();

		alertDialog.show();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();

		//noinspection SimplifiableIfStatement
		if (id == R.id.action_settings) {
			return true;
		}

		return super.onOptionsItemSelected(item);
	}
}
