package com.dnet.mama.movecounter;

import android.support.v4.app.Fragment;

import com.humaclab.aponjonshogorbha.activities.DrawerFragmentActivity;

public class MoveCounterActivity extends DrawerFragmentActivity {

	@Override
	protected Fragment createFragment() {
		return new MoveCounterFragment();
	}

}
