package com.dnet.mama.movecounter;

import android.support.v4.app.Fragment;

import com.humaclab.aponjonshogorbha.activities.DrawerFragmentActivity;

public class AllCountedMoveActivity extends DrawerFragmentActivity {

	@Override
	protected Fragment createFragment() {
		return new AllCountedMoveFragment();
	}

}
