package com.dnet.mama.movecounter;

import java.text.SimpleDateFormat;
import java.util.Date;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import bengali.language.support.BengaliUnicodeString;

import com.bd.aponjon.pregnancy.R;
import com.dnet.mama.diary.CF;

public class MoveCounterDetailsFragment extends Fragment {
	
	
	public static MoveCounterDetailsFragment getInstance(Date date, int count) {
		MoveCounterDetailsFragment fr = new MoveCounterDetailsFragment();
		Bundle args = new Bundle();
		args.putSerializable("Date", date);
		args.putSerializable("MoveCount", count);
		fr.setArguments(args);
		
		return fr;		
	}
	
	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("সোনামনি কবার নড়ল");
	}
	
	@Override
	@Nullable
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

		View v = inflater.inflate(R.layout.move_counted_details, container,
				false);
		
		Bundle bundle=getArguments();

		Date date = (Date) bundle.getSerializable("Date");
		int count = (Integer) bundle.getSerializable("MoveCount");

		TextView textViewMoveCount = (TextView) v.findViewById(R.id.textViewMoveCount);
		textViewMoveCount.setText(BengaliUnicodeString.convertToBengaliText(getActivity(), "" + count));

		
		TextView textViewMoveDate = (TextView) v.findViewById(R.id.textViewMoveDate);		
		textViewMoveDate.setText(BengaliUnicodeString.convertToBengaliText(getActivity(), CF.dateToBanglaDate(date)));

		
		TextView textViewMoveTime = (TextView) v.findViewById(R.id.textViewMoveTime);
		textViewMoveTime.setText(CF.dateToBanglaTime(date));

		return v;
	}
}
