package com.dnet.mama.diary;

import android.support.v4.app.Fragment;

import com.humaclab.aponjonshogorbha.activities.DrawerFragmentActivityDemo;

public class DiaryActivityDemo extends DrawerFragmentActivityDemo {

	@Override
	protected Fragment createFragment() {
		return new DiaryFragment();
	}

}
