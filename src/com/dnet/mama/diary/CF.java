package com.dnet.mama.diary;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import com.humaclab.aponjonshogorbha.Constants;
import com.humaclab.aponjonshogorbha.SessionManager;

import android.content.Context;
import android.content.SharedPreferences;
import bengali.language.support.BengaliUnicodeString;

public class CF {
	static Context mCtx;
	static private SharedPreferences mPrefs;
	static SessionManager sessionManager;
	public CF(Context ctx) {
		mCtx = ctx;
		sessionManager =new SessionManager(mCtx);
		mPrefs=sessionManager.getPrefs();
	}
	public static String dateToBanglaTimeDayYear(Date date) {

		String pre = "";

		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int mm = cal.get(Calendar.MINUTE);
		int timeOfDay = cal.get(Calendar.HOUR_OF_DAY);
		int dayOfMonth = cal.get(Calendar.DAY_OF_MONTH);
		int monthOfYear = cal.get(Calendar.MONTH);
		int year = cal.get(Calendar.YEAR);

		// সকাল বিকাল দুপুর সন্ধ্যা রাত

		if (timeOfDay >= 5 && timeOfDay < 11) {
			// Morning
			pre = "সকাল "; 
		} else if (timeOfDay >= 11 && timeOfDay < 15) {
			// Afternoon
			pre = "দুপুর " ;
		} else if (timeOfDay >= 15 && timeOfDay < 18) {
			// Afternoon
			pre = "বিকাল " ;
		} else if (timeOfDay >= 18 && timeOfDay < 20) {
			// Evening
			pre = "সন্ধ্যা " ;
		} else if ((timeOfDay >= 20 && timeOfDay < 24)
				|| (timeOfDay >= 0 && timeOfDay < 5)) {
			// Night
			pre = "রাত " ;
		}
		
		String r =  BengaliUnicodeString.convertToBengaliText(mCtx, ""+(timeOfDay%12)) + ":" 
					+BengaliUnicodeString.convertToBengaliText(mCtx, ""+mm)  + "টা, " 
					+BengaliUnicodeString.convertToBengaliText(mCtx, ""+dayOfMonth)  + " "
					+ getBanglaMonth(monthOfYear) + " " 
					+BengaliUnicodeString.convertToBengaliText(mCtx, ""+year) ;

		return pre+r;
	}
	
	public static String dateToBanglaDate(Date date) {

		String r = "";

		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int mm = cal.get(Calendar.MINUTE);
		int timeOfDay = cal.get(Calendar.HOUR_OF_DAY);
		int dayOfMonth = cal.get(Calendar.DAY_OF_MONTH);
		int monthOfYear = cal.get(Calendar.MONTH);
		int year = cal.get(Calendar.YEAR);

		r = dayOfMonth + " "+ getBanglaMonth(monthOfYear) ;

		return BengaliUnicodeString.convertToBengaliText(mCtx, r);
	}
	
	public static String dateToBanglaTime(Date date) {

		String pre = "";
		

		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int mm = cal.get(Calendar.MINUTE);
		int timeOfDay = cal.get(Calendar.HOUR_OF_DAY);
		int dayOfMonth = cal.get(Calendar.DAY_OF_MONTH);
		int monthOfYear = cal.get(Calendar.MONTH);
		int year = cal.get(Calendar.YEAR);

		// সকাল বিকাল দুপুর সন্ধ্যা রাত

		if (timeOfDay >= 5 && timeOfDay < 11) {
			// Morning
			pre = "সকাল "; 
		} else if (timeOfDay >= 11 && timeOfDay < 15) {
			// Afternoon
			pre = "দুপুর " ;
		} else if (timeOfDay >= 15 && timeOfDay < 18) {
			// Afternoon
			pre = "বিকাল " ;
		} else if (timeOfDay >= 18 && timeOfDay < 20) {
			// Evening
			pre = "সন্ধ্যা " ;
		} else if ((timeOfDay >= 20 && timeOfDay < 24)
				|| (timeOfDay >= 0 && timeOfDay < 5)) {
			// Night
			pre = "রাত " ;
		}
		
		String r= BengaliUnicodeString.convertToBengaliText(mCtx, ""+timeOfDay%12) + ":" + BengaliUnicodeString.convertToBengaliText(mCtx, ""+mm) + "টা " ;

		return pre+r;
	}

	public static ArrayList<String> months = new ArrayList<String>() {
		{
			add("জানুয়ারী");
			add("ফেব্রুয়ারী");
			add("মার্চ");
			add("এপ্রিল");
			add("মে");
			add("জুন");
			add("জুলাই");
			add("অগাস্ট");
			add("সেপ্টেম্বর");
			add("অক্টোবর");
			add("নভেম্বর");
			add("ডিসেম্বর");
		}
	};

	public static String getBanglaMonth(int month) {

		return months.get(month);
	}
	
	public static String getBanglaMonth(Date date) {
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);

		return months.get(cal.get(Calendar.MONTH));
	}
	
	public static String dateConvertToBangla(Date date) {
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int dayofMonth = cal.get(Calendar.DAY_OF_MONTH);
		int year = cal.get(Calendar.YEAR);
		
		String str= dayofMonth+" "+months.get(cal.get(Calendar.MONTH))+", "+ year ;
		
		return BengaliUnicodeString.convertToBengaliText(mCtx, str);
	}

	public static ArrayList<String> getBanglaMonthList() {

		return months;
	}
	
	public static double inchToMeter(int inch){
	 	    double inchConversion=0.0254;
	 	   return roundDouble(inch * inchConversion);
	    }
	    
	public static    double roundDouble(double number){
	 	   number = Math.round(number * 100);
	 	   number = number/100;
	 	   return number;
	    }
	
	public static int getCurrentDay(Date date) {
		SimpleDateFormat format = new SimpleDateFormat();
		format.applyPattern("yyyy-MM-dd");
		
		Date lmp = null;
		int dayPassed = 0;
		try {
			lmp = format.parse(mPrefs.getString(Constants.UserData.USER_LMP, null));
			
			dayPassed = (int)((date.getTime() - lmp.getTime()) / (1000*60*60*24l))+1;
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return dayPassed;
	}
	
	public static int getCurrentWeek(Date date) {
		SimpleDateFormat format = new SimpleDateFormat();
		format.applyPattern("yyyy-MM-dd");
		
		Date lmp = null;
		int dayPassed = 0;
		try {
			lmp = format.parse(mPrefs.getString(Constants.UserData.USER_LMP, null));
			
			dayPassed = (int)((date.getTime() - lmp.getTime()) / (1000*60*60*24l))+1;
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return dayPassed/7;
	}
}
