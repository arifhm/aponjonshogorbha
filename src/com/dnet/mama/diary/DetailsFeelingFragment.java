package com.dnet.mama.diary;



import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import bengali.language.support.BengaliUnicodeString;

import com.humaclab.aponjon.DaoSession;
import com.humaclab.aponjon.Diary;
import com.humaclab.aponjon.DiaryDao;
import com.humaclab.aponjonshogorbha.AponjonApplication;
import com.bd.aponjon.pregnancy.R;

public class DetailsFeelingFragment extends Fragment{
	AponjonApplication mApp;
	ImageView mTodayImage;
	String mPhotoPath;

	public static DetailsFeelingFragment getInstance(long id) {
		DetailsFeelingFragment fr = new DetailsFeelingFragment();
		Bundle bundle = new Bundle();
		bundle.putSerializable("DiaryId", id);
		fr.setArguments(bundle);

		return fr;
	}
	
	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("আমার অনুভূতি");
	}

	@Override
	@Nullable
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.activity_feeling_details, container, false);
		
		mTodayImage = (ImageView) v.findViewById(R.id.imageView1);
		
		mApp = AponjonApplication.getInstance();
		DaoSession dSession = mApp.getDaoSession();
		DiaryDao diaryDao = dSession.getDiaryDao();

		long id = (Long) getArguments().getSerializable("DiaryId");
		Diary diary = diaryDao.load(id);

		if(diary != null){
			((TextView)v.findViewById(R.id.textViewFDDate)).setText( diary.toString());
			((TextView)v.findViewById(R.id.textViewFDNote)).setText(diary.getNote());
			
			if(diary.getPhotoPath() != null) {
				mPhotoPath = diary.getPhotoPath();
				
				File imgFile = new File(mPhotoPath);
				if(imgFile!=null && imgFile.exists()) {
					// Get the dimensions of the View
					
//					int targetW = mTodayImage.getWidth();
//					int targetH = mTodayImage.getHeight();
//
//					// Get the dimensions of the bitmap
//					BitmapFactory.Options bmOptions = new BitmapFactory.Options();
//					bmOptions.inJustDecodeBounds = true;
//					BitmapFactory.decodeFile(mPhotoPath, bmOptions);
//					int photoW = bmOptions.outWidth;
//					int photoH = bmOptions.outHeight;
//
//					// Determine how much to scale down the image
//					int scaleFactor = Math.min(photoW/targetW, photoH/targetH);
//
//					// Decode the image file into a Bitmap sized to fill the View
//					bmOptions.inJustDecodeBounds = false;
//					bmOptions.inSampleSize = scaleFactor << 1;
//					bmOptions.inPurgeable = true;
					
					Bitmap bitmap = BitmapFactory.decodeFile(mPhotoPath);
					
					ExifInterface exif = null;
					try {
						exif = new ExifInterface(mPhotoPath);
						
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}  
					String orientString = exif.getAttribute(ExifInterface.TAG_ORIENTATION);
					int orientation = orientString != null ? Integer.parseInt(orientString) :  ExifInterface.ORIENTATION_NORMAL;

					int rotationAngle = 0;
					if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
						rotationAngle = 90;
					}
					if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
						rotationAngle = 180;
					}
					if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
						rotationAngle = 270;
					}
					Matrix mtx = new Matrix();
					mtx.postRotate(rotationAngle);
					
					// Rotating Bitmap
					Bitmap rotatedBMP = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), mtx, true);

					if (rotatedBMP != bitmap)
						bitmap.recycle();

					mTodayImage.setImageBitmap(rotatedBMP);
				}
//				Bitmap bitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
//				Matrix mtx = new Matrix();
//				mtx.postRotate(270);
//				// Rotating Bitmap
//				Bitmap rotatedBMP = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), mtx, true);
//				mTodayImage.setImageBitmap(rotatedBMP);
			} else {
				mTodayImage.setImageResource(R.drawable.pic_profile_demo);
			}
			
			if(diary.getRg0() == null) {
				((ViewGroup) v.findViewById(R.id.radioLayout_01)).setVisibility(View.GONE);
			} else if(diary.getRg0()){
				((RadioButton)v.findViewById(R.id.d_radio1a)).setChecked(true);
			} else if(!diary.getRg0()){
				((RadioButton) v.findViewById(R.id.d_radio1b)).setChecked(true);
			} 
			
			if (diary.getRg1() == null) {
				((ViewGroup) v.findViewById(R.id.radioLayout_02)).setVisibility(View.GONE);
			} else if(diary.getRg1()){
				((RadioButton)v.findViewById(R.id.d_radio2a)).setChecked(true);
			} else if(!diary.getRg1()){
				((RadioButton) v.findViewById(R.id.d_radio2b)).setChecked(true);
			} 
			
			if (diary.getRg2() == null) {
				((ViewGroup) v.findViewById(R.id.radioLayout_03)).setVisibility(View.GONE);
			} else if(diary.getRg2()){
				((RadioButton)v.findViewById(R.id.d_radio3a)).setChecked(true);
			} else if(!diary.getRg2()){
				((RadioButton) v.findViewById(R.id.d_radio3b)).setChecked(true);
			} 
			
			
			if (diary.getRg3() == null) {
				((ViewGroup) v.findViewById(R.id.radioLayout_04)).setVisibility(View.GONE);
			} else if(diary.getRg3()){
				((RadioButton)v.findViewById(R.id.d_radio4a)).setChecked(true);
			} else if(!diary.getRg3()){
				((RadioButton) v.findViewById(R.id.d_radio4b)).setChecked(true);
			} 
			
			if (diary.getRg4() == null) {
				((ViewGroup) v.findViewById(R.id.radioLayout_05)).setVisibility(View.GONE);
			} else if(diary.getRg4()){
				((RadioButton)v.findViewById(R.id.d_radio5a)).setChecked(true);
			} else if(!diary.getRg4()){
				((RadioButton) v.findViewById(R.id.d_radio5b)).setChecked(true);
			} 
			
			if (diary.getRg5() == null) {
				((ViewGroup) v.findViewById(R.id.radioLayout_06)).setVisibility(View.GONE);
			} else if(diary.getRg5()){
				((RadioButton)v.findViewById(R.id.d_radio6a)).setChecked(true);
			} else if(!diary.getRg5()){
				((RadioButton) v.findViewById(R.id.d_radio6b)).setChecked(true);
			}
			
			
			if (diary.getRg6() == null) {
				((ViewGroup) v.findViewById(R.id.radioLayout_07)).setVisibility(View.GONE);
			} else if(diary.getRg6()){
				((RadioButton)v.findViewById(R.id.d_radio7a)).setChecked(true);
			} else if(!diary.getRg6()){
				((RadioButton) v.findViewById(R.id.d_radio7b)).setChecked(true);
			}
			
			if (diary.getRg7() == null) {
				((ViewGroup) v.findViewById(R.id.radioLayout_08)).setVisibility(View.GONE);
			} else if(diary.getRg7()){
				((RadioButton)v.findViewById(R.id.d_radio8a)).setChecked(true);
			} else if(!diary.getRg7()){
				((RadioButton) v.findViewById(R.id.d_radio8b)).setChecked(true);
			}
			
			if (diary.getRg8() == null) {
				((ViewGroup) v.findViewById(R.id.radioLayout_09)).setVisibility(View.GONE);
			} else if(diary.getRg8()){
				((RadioButton)v.findViewById(R.id.d_radio9a)).setChecked(true);
			} else if(!diary.getRg8()){
				((RadioButton) v.findViewById(R.id.d_radio9b)).setChecked(true);
			}
			
			if (diary.getRg9() == null) {
				((ViewGroup) v.findViewById(R.id.radioLayout_10)).setVisibility(View.GONE);
			} else if(diary.getRg9()){
				((RadioButton)v.findViewById(R.id.d_radio10a)).setChecked(true);
			} else if(!diary.getRg9()){
				((RadioButton) v.findViewById(R.id.d_radio10b)).setChecked(true);
			}
		}



		return v;
	}

	private void loadImageFromStorage(String path)
	{

		/*try {
	        File f=new File(path, "profile.jpg");
	        Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f));
	            ImageView img=(ImageView)v.findViewById(R.id.imageView1);
	        img.setImageBitmap(b);
	    } 
	    catch (FileNotFoundException e) 
	    {
	        e.printStackTrace();
	    }
		 */
	}




}

