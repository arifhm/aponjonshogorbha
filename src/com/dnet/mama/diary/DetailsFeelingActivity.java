package com.dnet.mama.diary;

import android.support.v4.app.Fragment;

import com.humaclab.aponjon.Diary;
import com.humaclab.aponjonshogorbha.activities.DrawerFragmentActivity;

public class DetailsFeelingActivity extends DrawerFragmentActivity {
	@Override
	protected Fragment createFragment() {
//		Diary diary = (Diary) getIntent().getSerializableExtra("Diary");
		long id = getIntent().getLongExtra("DiaryId", 0l);
		return DetailsFeelingFragment.getInstance(id);
	}
}
