package com.dnet.mama.diary;

import java.text.SimpleDateFormat;
import java.util.List;

import bengali.language.support.BengaliUnicodeString;

import com.humaclab.aponjon.Diary;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;


public class DiaryAdapter extends ArrayAdapter<Diary>{

    Context context; 
    int layoutResourceId;   
    int textViewId;
    List<Diary> listData;
    
   
    
    public DiaryAdapter(Context context, int layoutResourceId, int textViewId, List<Diary> listData) {
        super(context, layoutResourceId, textViewId, listData);
        this.context = context;
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.textViewId = textViewId;
        this.listData = listData;
    }
    
    
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        DiaryHolder holder = null;
        
        if(row == null)
        {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            
            holder = new DiaryHolder();
            holder.txtTitle = (TextView)row.findViewById(textViewId);
            
            row.setTag(holder);
        }
        else
        {
            holder = (DiaryHolder)row.getTag();
        }
        
        Diary diary= listData.get(position);
        holder.txtTitle.setText(diary.toString());
       // holder.txtTitle.setText(diary.toString());

        
        return row;
    }
    
    static class DiaryHolder
    {       
        TextView txtTitle;
    }
}
