package com.dnet.mama.diary;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.humaclab.aponjon.*;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Toast;

import com.humaclab.aponjonshogorbha.AponjonApplication;
import com.humaclab.aponjonshogorbha.ImageUploadNoteTask;
import com.humaclab.aponjonshogorbha.ImageUploadTask;
import com.humaclab.aponjonshogorbha.fetch.app.data.ContentUpdate;
import com.humaclab.aponjonshogorbha.fragments.dialogs.CaptureSrcPrompt;

import com.bd.aponjon.pregnancy.R;

public class TodaysFeelingFragment extends Fragment implements
View.OnClickListener {

	private AponjonApplication mApp;
	private DaoSession mDaoSession;
//	private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 100;
	private int REQUEST_TAKE_PHOTO = 1;
	private int PICK_IMAGE_REQUEST = 2;
	private Uri fileUri;
	Boolean rg1, rg2, rg3, rg4, rg5, rg6, rg7, rg8, rg9, rg10;
	boolean rba1 = false, rba2= false, rba3 = false, rba4=false, rba5=false, rba6=false, rba7=false, rba8=false, rba9=false, rba10=false;
	boolean rbb1 = false, rbb2= false, rbb3 = false, rbb4=false, rbb5=false, rbb6=false, rbb7=false, rbb8=false, rbb9=false, rbb10=false;
	java.util.Date date;
	String note = "";
	String mDayFeelingPhotoPath;
	private ImageView mTodayImgView;
	private Bitmap mBitmap;
	long mNoteId = 0;

	View pv;

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("দিনটা কেমন যাচ্ছে");
	}

	@Override
	@Nullable
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		pv = inflater.inflate(R.layout.activity_todaysfeeling, container,
				false);

		//		getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

		mTodayImgView =(ImageView) pv.findViewById(R.id.imageView);
		mApp = AponjonApplication.getInstance();
		mDaoSession = mApp.getDaoSession();

		pv.findViewById(R.id.buttonSaveFeeling).setOnClickListener(
				new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						saveDairy();
					}
				});

		pv.findViewById(R.id.imageView).setOnClickListener(
				new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						initPhotoCapture();
//						Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//						// Ensure that there's a camera activity to handle the intent
//						if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
//							// Create the File where the photo should go
//							File photoFile = null;
//							try {
//								photoFile = createImageFile();
//							} catch (IOException ex) {
//								// Error occurred while creating the File
//							}
//							// Continue only if the File was successfully created
//							if (photoFile != null) {
//								takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
//										Uri.fromFile(photoFile));
//								startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
//							}
//						}
					}
				});

		pv.findViewById(R.id.radio1a).setOnClickListener(this);
		pv.findViewById(R.id.radio1b).setOnClickListener(this);
		pv.findViewById(R.id.radio2a).setOnClickListener(this);
		pv.findViewById(R.id.radio2b).setOnClickListener(this);
		pv.findViewById(R.id.radio3a).setOnClickListener(this);
		pv.findViewById(R.id.radio3b).setOnClickListener(this);
		pv.findViewById(R.id.radio4a).setOnClickListener(this);
		pv.findViewById(R.id.radio4b).setOnClickListener(this);
		pv.findViewById(R.id.radio5a).setOnClickListener(this);
		pv.findViewById(R.id.radio5b).setOnClickListener(this);
		pv.findViewById(R.id.radio6a).setOnClickListener(this);
		pv.findViewById(R.id.radio6b).setOnClickListener(this);
		pv.findViewById(R.id.radio7a).setOnClickListener(this);
		pv.findViewById(R.id.radio7b).setOnClickListener(this);
		pv.findViewById(R.id.radio8a).setOnClickListener(this);
		pv.findViewById(R.id.radio8b).setOnClickListener(this);
		pv.findViewById(R.id.radio9a).setOnClickListener(this);
		pv.findViewById(R.id.radio9b).setOnClickListener(this);
		pv.findViewById(R.id.radio10a).setOnClickListener(this);
		pv.findViewById(R.id.radio10b).setOnClickListener(this);

		return pv;
	}

	private File createImageFile() throws IOException {
		// Create an image file name
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
		String imageFileName = "JPEG_" + timeStamp + "_";
		String storageDir = Environment.getExternalStorageDirectory() + "/picUpload";
		File dir = new File(storageDir);
		if (!dir.exists())
			dir.mkdir();

		File image = new File(storageDir + "/" + imageFileName + ".jpg");

		// Save a file: path for use with ACTION_VIEW intents
		mDayFeelingPhotoPath = image.getAbsolutePath();

		return image;
	}


	void saveDairy() {

		note = ((EditText) pv.findViewById(R.id.editTextNote)).getText().toString();

		if ( TextUtils.isEmpty(note) 
				&& (!((RadioButton) pv.findViewById(R.id.radio1a)).isChecked() && !((RadioButton) pv.findViewById(R.id.radio1b)).isChecked())
				&& (!((RadioButton) pv.findViewById(R.id.radio2a)).isChecked() && !((RadioButton) pv.findViewById(R.id.radio2b)).isChecked())
				&& (!((RadioButton) pv.findViewById(R.id.radio3a)).isChecked() && !((RadioButton) pv.findViewById(R.id.radio3b)).isChecked())
				&& (!((RadioButton) pv.findViewById(R.id.radio4a)).isChecked() && !((RadioButton) pv.findViewById(R.id.radio4b)).isChecked())
				&& (!((RadioButton) pv.findViewById(R.id.radio5a)).isChecked() && !((RadioButton) pv.findViewById(R.id.radio5b)).isChecked())
				&& (!((RadioButton) pv.findViewById(R.id.radio6a)).isChecked() && !((RadioButton) pv.findViewById(R.id.radio6b)).isChecked())
				&& (!((RadioButton) pv.findViewById(R.id.radio7a)).isChecked() && !((RadioButton) pv.findViewById(R.id.radio7b)).isChecked())
				&& (!((RadioButton) pv.findViewById(R.id.radio8a)).isChecked() && !((RadioButton) pv.findViewById(R.id.radio8b)).isChecked())
				&& (!((RadioButton) pv.findViewById(R.id.radio9a)).isChecked() && !((RadioButton) pv.findViewById(R.id.radio9b)).isChecked())
				&& (!((RadioButton) pv.findViewById(R.id.radio10a)).isChecked() && !((RadioButton) pv.findViewById(R.id.radio10b)).isChecked())) {

			Toast.makeText(getActivity(), "Please Answer a question",
					Toast.LENGTH_SHORT).show();			
			return;
		}

		ContentUpdate cu = new ContentUpdate(getActivity());
		date = new Date();

		//photo 
		boolean isSync=false;


		if(rba1 == false && rbb1 ==false) {
			rg1 = null;
		} else if (rba1 == true) {
			rg1 = true;
		} else if (rbb1 == true) {
			rg1 = false;
		}

		if(rba2 == false && rbb2 ==false) {
			rg2 = null;
		} else if (rba2 == true) {
			rg2 = true;
		} else if (rbb2 == true) {
			rg2 = false;
		}

		if(rba3 == false && rbb3 ==false) {
			rg3 = null;
		} else if (rba3 == true) {
			rg3 = true;
		} else if (rbb3 == true) {
			rg3 = false;
		}

		if(rba4 == false && rbb4 ==false) {
			rg4 = null;
		} else if (rba4 == true) {
			rg4 = true;
		} else if (rbb4 == true) {
			rg4 = false;
		}

		if(rba5 == false && rbb5 ==false) {
			rg5 = null;
		} else if (rba5 == true) {
			rg5 = true;
		} else if (rbb5 == true) {
			rg5 = false;
		}

		if(rba6 == false && rbb6 ==false) {
			rg6 = null;
		} else if (rba6 == true) {
			rg6 = true;
		} else if (rbb6 == true) {
			rg6 = false;
		}

		if(rba7 == false && rbb7 ==false) {
			rg7 = null;
		} else if (rba7 == true) {
			rg7 = true;
		} else if (rbb7 == true) {
			rg7 = false;
		}

		if(rba8 == false && rbb8 ==false) {
			rg8 = null;
		} else if (rba8 == true) {
			rg8 = true;
		} else if (rbb8 == true) {
			rg8 = false;
		}

		if(rba9 == false && rbb9 ==false) {
			rg9 = null;
		} else if (rba9 == true) {
			rg9 = true;
		} else if (rbb9 == true) {
			rg9 = false;
		}

		if(rba10 == false && rbb10 ==false) {
			rg10 = null;
		} else if (rba10 == true) {
			rg10 = true;
		} else if (rbb10 == true) {
			rg10 = false;
		}

		mNoteId = cu.saveDiary(note, rg1, rg2, rg3, rg4, rg5, rg6, rg7, rg8, rg9, rg10, mDayFeelingPhotoPath, null,
				date, isSync);
		Toast.makeText(getActivity(), "Diary saved", Toast.LENGTH_SHORT).show();
		getActivity().finish();
	}


	public void onRadioButtonClicked(View view) {
		// Is the button now checked?
		boolean checked = ((RadioButton) view).isChecked();

		// Check which radio button was clicked
		switch (view.getId()) {
		case R.id.radio1a:
			if (checked)
				rba1 = true;
			break;
		case R.id.radio1b:
			if (checked)
				rbb1 = true;
			break;


		case R.id.radio2a:
			if (checked)
				rba2 = true;
			break;
		case R.id.radio2b:
			if (checked)
				rbb2 = true;
			break;

		case R.id.radio3a:
			if (checked)
				rba3 = true;
			break;
		case R.id.radio3b:
			if (checked)
				rbb3 = true;
			break;

		case R.id.radio4a:
			if (checked)
				rba4 = true;
			break;
		case R.id.radio4b:
			if (checked)
				rbb4 = true;
			break;

		case R.id.radio5a:
			if (checked)
				rba5 = true;
			break;
		case R.id.radio5b:
			if (checked)
				rbb5 = true;
			break;

		case R.id.radio6a:
			if (checked)
				rba6 = true;
			break;
		case R.id.radio6b:
			if (checked)
				rbb6 = true;
			break;

		case R.id.radio7a:
			if (checked)
				rba7 = true;
			break;
		case R.id.radio7b:
			if (checked)
				rbb7 = true;
			break;

		case R.id.radio8a:
			if (checked)
				rba8 = true;
			break;
		case R.id.radio8b:
			if (checked)
				rbb8 = true;
			break;

		case R.id.radio9a:
			if (checked)
				rba9 = true;
			break;
		case R.id.radio9b:
			if (checked)
				rbb9 = true;
			break;

		case R.id.radio10a:
			if (checked)
				rba10 = true;
			break;
		case R.id.radio10b:
			if (checked)
				rbb10 = true;
			break;
		}
	}
	
	
	public void initPhotoCapture() {
		CaptureSrcPrompt srcPrompt = CaptureSrcPrompt.getInstance();

		srcPrompt.setOnClickListener(new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int resultCode) {
				if(resultCode == 0) {
					dispatchTakePictureIntent();									
				} else if (resultCode == 1 ) {
					//					Toast.makeText(getActivity(), "Gallery", Toast.LENGTH_SHORT).show();
					Intent intent = new Intent();
					// Show only images, no videos or anything else				
					intent.setType("image/*");
					intent.setAction(Intent.ACTION_GET_CONTENT);
					// Always show the chooser (if there are multiple options available)
					startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);

				} else if (resultCode == 2 ) {
					dialog.dismiss();
				}
				dialog.dismiss();
			}
		});

		Fragment fragmentByTag = getActivity().getSupportFragmentManager().findFragmentByTag(CaptureSrcPrompt.D_TAG);//avoid opening twice dialog  
		if (fragmentByTag == null)  
			srcPrompt.show(getActivity().getSupportFragmentManager(), CaptureSrcPrompt.D_TAG);
	}
	
	private void dispatchTakePictureIntent() {
		Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		
		if(getActivity().getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FRONT)) {
			takePictureIntent.putExtra("android.intent.extras.CAMERA_FACING", Camera.CameraInfo.CAMERA_FACING_FRONT);
		}		
		
		// Ensure that there's a camera activity to handle the intent
		if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
			// Create the File where the photo should go
			File photoFile = null;
			try {
				photoFile = createImageFile();
			} catch (IOException ex) {
				// Error occurred while creating the File
			}
			// Continue only if the File was successfully created
			if (photoFile != null) {
				takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
						Uri.fromFile(photoFile));
				startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
			}
		}
	}

	public void onActivityResult(int requestCode, int resultCode, Intent data) 
	{ 
//		if (requestCode == REQUEST_TAKE_PHOTO) { 
//			setPic();
////			Bitmap photo = (Bitmap) data.getExtras().get("data");
////			ImageView img =(ImageView)
////					pv.findViewById(R.id.imageView);
////			img.setScaleType(ImageView.ScaleType.CENTER_CROP);
////			img.setImageBitmap(photo);
////			saveToInternalSorage(photo);			
//		}
		
		if (requestCode==PICK_IMAGE_REQUEST && resultCode == Activity.RESULT_OK && data != null && data.getData() != null) {

			Uri uri = data.getData();

			try {
				Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
				
				File file = createImageFile();
				FileOutputStream fos = null;
	             try {
	                 fos = new FileOutputStream(file);
	                 bitmap.compress(Bitmap.CompressFormat.PNG,70, fos);

	                 fos.flush();
	                 fos.close();
	              //   MediaStore.Images.Media.insertImage(getContentResolver(), b, "Screen", "screen");
	             }catch (FileNotFoundException e) {
	                 e.printStackTrace();
	             } catch (Exception e) {
	                 e.printStackTrace();
	             }
	             setPic();
//				mProfileImg.setImageBitmap(bitmap);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else if (requestCode == REQUEST_TAKE_PHOTO && resultCode == Activity.RESULT_OK) {
			setPic();
		}
	}
	
	private void setPic() {
		// Get the dimensions of the View
		int targetW = mTodayImgView.getWidth();
		int targetH = mTodayImgView.getHeight();

		// Get the dimensions of the bitmap
		BitmapFactory.Options bmOptions = new BitmapFactory.Options();
		bmOptions.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(mDayFeelingPhotoPath, bmOptions);
		int photoW = bmOptions.outWidth;
		int photoH = bmOptions.outHeight;

		// Determine how much to scale down the image
		int scaleFactor = Math.min(photoW/targetW, photoH/targetH);

		// Decode the image file into a Bitmap sized to fill the View
		bmOptions.inJustDecodeBounds = false;
		bmOptions.inSampleSize = scaleFactor << 1;
//		bmOptions.inPurgeable = true;

		Bitmap bitmap = BitmapFactory.decodeFile(mDayFeelingPhotoPath, bmOptions);
		
		ExifInterface exif = null;
		try {
			exif = new ExifInterface(mDayFeelingPhotoPath);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  
		String orientString = exif.getAttribute(ExifInterface.TAG_ORIENTATION);
		int orientation = orientString != null ? Integer.parseInt(orientString) :  ExifInterface.ORIENTATION_NORMAL;

		int rotationAngle = 0;
		if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
			rotationAngle = 90;
		}
		if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
			rotationAngle = 180;
		}
		if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
			rotationAngle = 270;
		}
		Matrix mtx = new Matrix();
		mtx.postRotate(rotationAngle);
		
		// Rotating Bitmap
		Bitmap rotatedBMP = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), mtx, true);

		if (rotatedBMP != bitmap)
			bitmap.recycle();
		
		mTodayImgView.setScaleType(ImageView.ScaleType.CENTER_CROP);
		mTodayImgView.setImageBitmap(rotatedBMP);
		mBitmap = rotatedBMP;
		
		//photo upload
		if(mBitmap != null && mNoteId!= 0) {
			try {
//				sendPhoto(mBitmap);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	private void sendPhoto(Bitmap bitmap) {
		new ImageUploadNoteTask(mDayFeelingPhotoPath, getActivity(), mNoteId).execute(bitmap);
	}


//	private String saveToInternalSorage(Bitmap bitmapImage) {
//		ContextWrapper cw = new ContextWrapper(getActivity());
//		// path to /data/data/yourapp/app_data/imageDir
//		File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
//		// Create imageDir
//		File mypath = new File(directory, "profile.jpg");
//
//		FileOutputStream fos = null;
//		try {
//			fos = new FileOutputStream(mypath);
//
//			// Use the compress method on the BitMap object to write image to
//			// the OutputStream
//			bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
//			fos.close();
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		return directory.getAbsolutePath();
//	}

	@Override
	public void onClick(View v) {
		onRadioButtonClicked(v);
	}

}
