package com.dnet.mama.diary;

import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.humaclab.aponjon.Diary;
import com.humaclab.aponjonshogorbha.fetch.app.data.ContentUpdate;

import com.bd.aponjon.pregnancy.R;

public class AllFeelingFragment extends Fragment {

	ListView listView;
	List<Diary> listData;
	
	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("আমার সকল অনুভূতি");
	}
	
	@Override
	@Nullable
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.activity_all_feeling, container, false);
		
		ContentUpdate cu=new ContentUpdate(getActivity());
		listData=cu.getDiaryList();
		listView=(ListView)v.findViewById(R.id.listView_all_feeelings);
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int pos,
					long arg3) {
				
				Intent i = new Intent(getActivity(), DetailsFeelingActivity.class);
				i.putExtra("DiaryId", listData.get(pos).getId());
				startActivity(i);								
			}
		});
		
		 DiaryAdapter adapter = new DiaryAdapter(getActivity(), R.layout.item_all_feelings, R.id.textViewListItemFeelings, listData);
		  listView.setAdapter(adapter);
		  
		  return v;
	}

}

