package com.dnet.mama.diary;

import android.support.v4.app.Fragment;

import com.humaclab.aponjonshogorbha.activities.DrawerFragmentActivity;

public class TodaysFeelingActivity extends DrawerFragmentActivity {

	@Override
	protected Fragment createFragment() {
		return new TodaysFeelingFragment();
	}

}
