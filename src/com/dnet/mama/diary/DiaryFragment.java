package com.dnet.mama.diary;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dnet.mama.movecounter.MoveCounterActivity;
import com.humaclab.aponjonshogorbha.Constants;
import com.bd.aponjon.pregnancy.R;
import com.humaclab.aponjonshogorbha.fragments.dialogs.DemoModeDialog;


public class DiaryFragment extends Fragment implements View.OnClickListener{
	
	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("আমার ডায়েরি");
	}
	
	@Override
	@Nullable
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.activity_diary, container, false);

		v.findViewById(R.id.ivTodaysFeeling).setOnClickListener(this);
		v.findViewById(R.id.ivallfeeling).setOnClickListener(this);

		return v;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.ivTodaysFeeling :
			if(Constants.is_demo){
				DemoModeDialog dialog = new DemoModeDialog(getActivity());
				dialog.show();
			} else startActivity(new Intent(getActivity(), TodaysFeelingActivity.class));
			break;
		case R.id.ivallfeeling : 
			if(Constants.is_demo){
				DemoModeDialog dialog = new DemoModeDialog(getActivity());
				dialog.show();
			} else startActivity(new Intent(getActivity(), AllFeelingActivity.class));
			break;
		default:
			break;
		}
	}

}
