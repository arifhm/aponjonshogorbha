package com.dnet.mama.diary;

import android.support.v4.app.Fragment;

import com.humaclab.aponjonshogorbha.activities.DrawerFragmentActivity;

public class DiaryActivity extends DrawerFragmentActivity {

	@Override
	protected Fragment createFragment() {
		return new DiaryFragment();
	}
}
