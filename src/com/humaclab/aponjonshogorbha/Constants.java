package com.humaclab.aponjonshogorbha;

import java.util.ArrayList;
import java.util.List;

public final class Constants {
	public Constants() {}

	public static abstract class AppData {
		// SharedPreference Mode
		public static final int PREF_MODE_PRIVATE = 0;
		// Sharedpref file name
		public static final String PREF_NAME = "AponjonAppPref";
		//boolean is login
		public static final String IS_LOGIN = "IsLoggedIn";

		public static final String IS_FIRST_TIME = "FirstTimeInApp";
		// Base_image_link
		public static final String KEY_BASE_IMG_LINK = "base_img_link";
		// last_sync_date
		public static final String KEY_LAST_SYNC_DATE = "last_sync_date";
	}

	public static abstract class UserData {
		// Current lmp
		public static final String USER_LMP = "user_lmp";		
		// Current week
		public static final String USER_CURRENT_WEEK = "user_curr_week";
		// User id 
		public static final String USER_ID = "user_id";	     
		// User name 
		public static final String USER_NAME = "user_name";    
		// Email address 
		public static final String USER_EMAIL = "user_email";	
		// Phone
		public static final String USER_PHONE = "user_phone"; 
		// Token 
		public static final String USER_TOKEN = "user_token";
		// current_user_img_path
		public static final String USER_IMG_SD_PATH = "profile_img_sd_path";
		// current_user_img_path
		public static final String USER_IMG_LINK = "profile_img_link";
		// current_user_img_path
		public static final String USER_HEIGHT = "user_height";
		// current_user_img_path
		public static final String USER_WEIGHT = "user_weight";
		// current_user_img_path
		public static final String USER_AGE = "user_age";
		//hwa boolean
		public static final String HAS_HWA = "user_has_hwa";
		//AponjonRegStatus
		public static final String IS_APONJON_REG = "is_aponjon_registered_user";
		
		//minReqCurb
		public static final String MIN_REQ_CURB = "min_req_curb";
		//minReqProtien 
		public static final String MIN_REQ_PROTIEN = "min_req_protien";
		//minReqFat
		public static final String MIN_REQ_FAT = "min_req_fat";
		//minReqTotal
		public static final String MIN_REQ_TOTAL = "min_req_total";
		//Trimester Specific Required total calorie
		public static final String REQ_TOTAL = "req_total_per";
	}
	public static final String BACKGROUND_SERVICE_BATTERY_CONTROL = "background_service_battery_controll";
	public static boolean is_demo = false;
//	public static boolean is_aponjon_grahok = false;

	public static List<String>makeSearchArray(){
		List<String> search_array = new ArrayList<String>();
		search_array.add("আমার আমি");
		search_array.add("আমার সোনামনি");
		search_array.add("শিশু কবে জন্মাবে");
		search_array.add("কবে কি করব");
		search_array.add("কি খাবো");
		search_array.add("আমার জগৎ");
		search_array.add("আপনজনে নিবন্ধন");
		search_array.add("অ্যাপ সম্পর্কে");
		search_array.add("এই মাসের সব করণীয়");
		search_array.add("সব করনীয়");
		search_array.add("নমুনা খাদ্য তালিকা");
		search_array.add("নিজে খাদ্য তালিকা তৈরী করি");
		search_array.add("আমার খাদ্য তালিকা");
		search_array.add("আমার ডায়েরি");
		search_array.add("সোনামনি কবার নড়লো");
		search_array.add("ওজন ঠিক বাড়ছে?");
		search_array.add("দিনটা কেমন যাচ্ছে");
		search_array.add("আমার সকল অনুভুতি");
		search_array.add("আমার ওজন");
		search_array.add("আমার সকল ওজন");
		search_array.add("অন্য কারো ওজন");
		search_array.add("আপনজন কি ও কেন");

		return search_array;
	}

	public static List<String>makeSearchArray2(){
		List<String> search_array = new ArrayList<String>();
		search_array.add("আমার আমি");
		search_array.add("আমার সোনামনি");
		search_array.add("শিশু কবে জন্মাবে");
		search_array.add("কবে কি করব");
		search_array.add("কি খাবো");
		search_array.add("আমার জগৎ");
		search_array.add("ডাক্তার লাইন");
		search_array.add("অ্যাপ সম্পর্কে");
		search_array.add("এই মাসের সব করণীয়");
		search_array.add("সব করনীয়");
		search_array.add("নমুনা খাদ্য তালিকা");
		search_array.add("নিজে খাদ্য তালিকা তৈরী করি");
		search_array.add("আমার খাদ্য তালিকা");
		search_array.add("আমার ডায়েরি");
		search_array.add("সোনামনি কবার নড়লো");
		search_array.add("ওজন ঠিক বাড়ছে?");
		search_array.add("দিনটা কেমন যাচ্ছে");
		search_array.add("আমার সকল অনুভুতি");
		search_array.add("আমার ওজন");
		search_array.add("আমার সকল ওজন");
		search_array.add("অন্য কারো ওজন");
		search_array.add("আপনজন কি ও কেন");

		return search_array;
	}
}