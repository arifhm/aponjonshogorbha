package com.humaclab.aponjonshogorbha;


import com.bd.aponjon.pregnancy.R;
import com.humaclab.aponjonshogorbha.activities.DashboardActivity;
import com.humaclab.aponjonshogorbha.activities.EulaActivity;
import com.humaclab.aponjonshogorbha.activities.StartActivity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;

public class SplashActivity extends Activity {
	private static final int SPLASH_TIME = 3 * 1000;// 3 seconds
	private SessionManager mSession;
	private SharedPreferences mPrefs;
	private boolean mFirstTime;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_splash);
		
		mSession = new SessionManager(this);
		mPrefs = mSession.pref;
		
		mFirstTime = mPrefs.getBoolean(Constants.AppData.IS_FIRST_TIME, true);
		
		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() {
				
				if(mFirstTime) {
					startActivity(new Intent(SplashActivity.this, EulaActivity.class));
					finish();
				} else if(mSession.isLoggedIn()) {
					Intent i = new Intent(SplashActivity.this, DashboardActivity.class);
					i.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);  
					i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		            // Add new Flag to start new Activity
//		            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		            startActivity(i);
					finish();
				} else {
					Intent i = new Intent(SplashActivity.this, StartActivity.class);
					startActivity(i);
					finish();
				}
				
//				Intent intent = new Intent(SplashActivity.this,
//						StartActivity.class);
//				startActivity(intent);
//
//				SplashActivity.this.finish();
//
//				overridePendingTransition(R.anim.appear, R.anim.disappear);

			}
		}, SPLASH_TIME);

		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
			} 
		}, SPLASH_TIME);

	}


	@Override
	public void onBackPressed() {
		this.finish();
		super.onBackPressed();
	}    
}
