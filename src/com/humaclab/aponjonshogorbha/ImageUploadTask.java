package com.humaclab.aponjonshogorbha;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import com.humaclab.aponjon.DaoSession;
import com.humaclab.aponjon.TaskTypeDao.Properties;
import com.humaclab.aponjon.User;
import com.humaclab.aponjon.UserDao;
import com.humaclab.aponjonshogorbha.MultipartEntity;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.InputStreamBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;


import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;

public class ImageUploadTask extends AsyncTask<Bitmap, Void, String> {
	SessionManager mSessionMgr;
	SharedPreferences mPrefs;
	Editor mEditor;
	String mToken;
	String mFilepath;
	File mFile;
	AponjonApplication mApp;
	DaoSession mDaoSession;
	
	public ImageUploadTask(String path, Context ctx) {
		mFilepath = path;
		mSessionMgr = new SessionManager(ctx);
		mPrefs = mSessionMgr.getPrefs();
		mEditor = mSessionMgr.getEditor();
		mToken = mPrefs.getString(Constants.UserData.USER_TOKEN, null);
		mApp = AponjonApplication.getInstance();
		mDaoSession = mApp.getDaoSession();
		Log.d("ImageUpload", "fdfdlfldf");
	}
	
		
		protected String doInBackground(Bitmap... bitmaps) {
			String link = null;
			if (bitmaps[0] == null)
				return null;
//			setProgress(0);
			
			Bitmap bitmap = bitmaps[0];
			ByteArrayOutputStream stream = new ByteArrayOutputStream();
			bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream); // convert Bitmap to ByteArrayOutputStream
			InputStream in = new ByteArrayInputStream(stream.toByteArray()); // convert ByteArrayOutputStream to ByteArrayInputStream
			
//			ContentBody body = new InputStreamBody(in, System.currentTimeMillis() + ".jpg");
			
			
			DefaultHttpClient httpclient = new DefaultHttpClient();
			try {
				HttpPost httppost = new HttpPost(
						AppConfig.URL_PROFILE_PHOTO_UPLOAD); // server
				//httppost.addHeader("Content-Type", "multipart/form-data");
				
//				StringBody tokenBody = new StringBody(mToken, "text/plain", null); 
//				FileBody imgBody = new FileBody(new File(mFilepath), "image/png");
				
				MultipartEntity reqEntity = new MultipartEntity();

//				reqEntity.addPart("photo", System.currentTimeMillis() + ".jpg", in, "image/png"); 
//				//reqEntity.isSetFirst = false;
//				//reqEntity.addPart("photo", new File(mFilepath));
//				reqEntity.addPart("token", mToken);
				
				
//				reqEntity.addPart("photo",
//						 System.currentTimeMillis() + ".jpg", in);
				
				reqEntity.addPart("data[extra]", "");
				reqEntity.addPart("data[token]", mToken);
				reqEntity.addPart("photo", System.currentTimeMillis() + ".jpg", in);
				
				httppost.setEntity(reqEntity);
				

//				Log.i(TAG, "request " + httppost.getRequestLine());
				HttpResponse response = null;
				try {
					response = httpclient.execute(httppost);
					String responseString = EntityUtils.toString(response.getEntity());
					/**
					 * 	{	"request":[],
					 * 		"result":{
					 * 					"profile_photo":"http:\/\/www.humaclab.com\/development\/aponjonshogorva\/resource\/profile_photo\/562f0bf5a8bb6_1445923829.jpg"
					 * 				 },
					 * 		"message":"Photo uploaded successfully.",
					 * 		"status_code":200
					 * 	}
					 */
					
					try {
						JSONObject responseObj = new JSONObject(responseString);
						JSONObject resultObj = responseObj.getJSONObject("result");
						link = resultObj.getString("profile_photo");
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					Log.d("PhotoResponse", "CatchResponse: " + responseString);
				} catch (ClientProtocolException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
//				try {
//					if (response != null)
//						Log.i(TAG, "response " + response.getStatusLine().toString());
//				} finally {
//
//				}
			} finally {

			}

			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			if (stream != null) {
				try {
					stream.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			return link;
		}
		
//		@Override
//		protected void onProgressUpdate(Void... values) {
//			super.onProgressUpdate(values);
//		}
		
		@Override
		protected void onPostExecute(String link) {
			UserDao userDao = mDaoSession.getUserDao();
			User user = userDao.queryBuilder().where(UserDao.Properties.Token.eq(mToken)).unique();
			user.setPhotoLink(link);
			userDao.update(user);
			mEditor.putString(Constants.UserData.USER_IMG_LINK, link);
			mEditor.commit();
		}
		
//		protected File getFile(Bitmap bitMap) {
//			//create a file to write bitmap data
//			File f = new File(context.getCacheDir(), filename);
//			f.createNewFile();
//
//			//Convert bitmap to byte array
//			Bitmap bitmap = bitMap;
//			ByteArrayOutputStream bos = new ByteArrayOutputStream();
//			bitmap.compress(CompressFormat.PNG, 0 /*ignored for PNG*/, bos);
//			byte[] bitmapdata = bos.toByteArray();
//
//			//write the bytes in file
//			FileOutputStream fos = new FileOutputStream(f);
//			fos.write(bitmapdata);
//			fos.flush();
//			fos.close();
//		} 
		
}
