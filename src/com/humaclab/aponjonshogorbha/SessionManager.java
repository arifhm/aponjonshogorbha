 package com.humaclab.aponjonshogorbha;

import java.util.HashMap;

import com.humaclab.aponjonshogorbha.activities.StartActivity;
import com.humaclab.aponjonshogorbha.userdata.ClearUserData;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class SessionManager {
	// Shared Preferences
	public SharedPreferences pref;

	// Editor for Shared preferences
	Editor editor;

	// Context
	Context _context;


	// Constructor
	public SessionManager(Context context){
		this._context = context;
		pref = _context.getSharedPreferences(Constants.AppData.PREF_NAME, Constants.AppData.PREF_MODE_PRIVATE);
		editor = pref.edit();
	}
	
	public SharedPreferences getPrefs() {
		return pref;	
	}
	public Editor getEditor() {
		return editor;
	} 
	

	/**
	 * Create login session
	 * */
	public void createLoginSession(String id, String name, String email, String phone, String lmp, 
			int currWeek, String token, String height, String weight, String age, String photoPath, String photoUrl) {

		// Storing login value as TRUE
		editor.putBoolean(Constants.AppData.IS_LOGIN, true);
		// Storing name in pref
		editor.putString(Constants.UserData.USER_ID, id);
		// Storing name in pref
		editor.putString(Constants.UserData.USER_NAME, name);
		// Storing email in pref
		editor.putString(Constants.UserData.USER_EMAIL, email);
		// String phone in pref
		editor.putString(Constants.UserData.USER_PHONE, phone);
		// Storing lmp in pref
		editor.putString(Constants.UserData.USER_LMP, lmp);
		// Storing currWeek in prefs
		editor.putInt(Constants.UserData.USER_CURRENT_WEEK, currWeek);
		// Storing token in pref
		editor.putString(Constants.UserData.USER_TOKEN, token);
		
		String oldPath = pref.getString(Constants.UserData.USER_IMG_SD_PATH, null);
		if(photoUrl!=null) {
			editor.putString(Constants.UserData.USER_IMG_LINK, photoUrl);
		}
		
		if(photoPath==null || photoPath.equals("") != true) {
			editor.putString(Constants.UserData.USER_IMG_SD_PATH, photoPath);
		}
		
		// Storing HWA
		int flagValue = 0;
		if(height != null) {
			float lHeight = 0f;
			try {
				lHeight = Float.parseFloat(height);
				flagValue++;
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}
			editor.putFloat(Constants.UserData.USER_HEIGHT, lHeight);
		}		
		if(weight != null) {
			int lWeight = 0;
			try {
//				lWeight = Integer.parseInt(weight);
				lWeight = (int) Float.parseFloat(weight);
				flagValue++;
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}
			editor.putInt(Constants.UserData.USER_WEIGHT, lWeight);
		}
		if(age != null) {
			int lAge = 0;
			try {
				lAge = Integer.parseInt(age);
				flagValue++;
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}
			editor.putInt(Constants.UserData.USER_AGE, lAge);
		}
		if(flagValue == 3) {
			editor.putBoolean(Constants.UserData.HAS_HWA, true);
		}
		// commit changes
		editor.commit();
	}   
	
	/**
	 * SET_PREFFERENCES OF HWA
	 */
	public void setHWAtoPreff(float height, int weight, int age) {
		editor.putFloat(Constants.UserData.USER_HEIGHT, height);
		editor.putInt(Constants.UserData.USER_WEIGHT, weight);
		editor.putInt(Constants.UserData.USER_AGE, age);
		editor.putBoolean(Constants.UserData.HAS_HWA, true);
		editor.commit();
	}
	

	/**
	 * Check login method wil check user login status
	 * If false it will redirect user to login page
	 * Else won't do anything
	 * */
//	public void checkLogin() {
//		// Check login status
//		if(!this.isLoggedIn()){
//			// user is not logged in redirect him to Login Activity
//			Intent i = new Intent(_context, StartActivity.class);
//			// Closing all the Activities
//			i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//
//			// Add new Flag to start new Activity
//			i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//
//			// Staring Login Activity
//			_context.startActivity(i);
//		}
//	}


//	/**
//	 * Get stored session data
//	 * */
//	public HashMap<String, String> getUserDetails(){
//		HashMap<String, String> user = new HashMap<String, String>();
//		// user id
//		user.put(Constants.UserData.USER_ID, pref.getString(Constants.UserData.USER_ID, null));
//		// user name
//		user.put(Constants.UserData.USER_NAME, pref.getString(Constants.UserData.USER_NAME, null));
//		// user email id
//		user.put(Constants.UserData.USER_EMAIL, pref.getString(Constants.UserData.USER_EMAIL, null));
//		// user phone
//		user.put(Constants.UserData.USER_LMP, pref.getString(Constants.UserData.USER_LMP, null));
//		// user 
//		user.put(Constants.UserData.USER_PHONE, pref.getString(Constants.UserData.USER_PHONE, null));
//		// current Week
//		user.put(Constants.UserData.USER_CURRENT_WEEK, pref.getString(Constants.UserData.USER_CURRENT_WEEK, null));
//		// current Week
//		user.put(Constants.UserData.USER_HEIGHT, pref.getString(Constants.UserData.USER_HEIGHT, null));
//		// current Week
//		user.put(Constants.UserData.USER_WEIGHT, pref.getString(Constants.UserData.USER_WEIGHT, null));
//		// current Week
//		user.put(Constants.UserData.USER_AGE, pref.getString(Constants.UserData.USER_AGE, null));
//		// user token
//		user.put(Constants.UserData.USER_TOKEN, pref.getString(Constants.UserData.USER_TOKEN, null));
//
//		// return user
//		return user;
//	}

	/**
	 * Clear session details
	 * */
	public void logoutUser(){
		// Clearing all data from Shared Preferences
		editor.clear();
		editor.commit();
		
		ClearUserData clearObj = new ClearUserData();
		clearObj.clearData();
		
		Intent broadcastIntent = new Intent();
		broadcastIntent.setAction("com.humaclab.aponjon.ACTION_LOGOUT");
		_context.sendBroadcast(broadcastIntent);
		
		// After logout redirect user to Loing Activity
		Intent i = new Intent(_context, StartActivity.class);
		// Closing all the Activities
		i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		// Add new Flag to start new Activity
		i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		// Staring Login Activity
		
		
		_context.startActivity(i);
	}

	/**
	 * Quick check for login
	 * **/
	// Get Login State
	public boolean isLoggedIn(){
		return pref.getBoolean(Constants.AppData.IS_LOGIN, false);
	}
}
