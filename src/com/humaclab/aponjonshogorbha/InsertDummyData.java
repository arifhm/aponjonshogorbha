package com.humaclab.aponjonshogorbha;

import java.util.List;

import com.humaclab.aponjon.DaoMaster;
import com.humaclab.aponjon.DaoSession;
import com.humaclab.aponjon.Food;
import com.humaclab.aponjon.FoodDao;
import com.humaclab.aponjon.Food_Category;
import com.humaclab.aponjon.Food_CategoryDao;
import com.humaclab.aponjon.DaoMaster.DevOpenHelper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;

public class InsertDummyData extends AsyncTask<Void, Void, Void>{
	Context mCtx;
	
	public InsertDummyData(Context ctx) {
		mCtx = ctx;
	}

	@Override
	protected Void doInBackground(Void... params) {
		insertData();
		return null;
	}
	
	void insertData() {
		//Process userObj to save in database
		DevOpenHelper helper = new DaoMaster.DevOpenHelper(mCtx, "aponjon-db", null);
		SQLiteDatabase db = helper.getWritableDatabase();
		DaoMaster daomaster = new DaoMaster(db);
		DaoSession session = daomaster.newSession();
		/*
		 * REMOVE DUMMY BLOCK START
		 * For Testing Purpose only
		 */

		Food_CategoryDao categoryDao = session.getFood_CategoryDao();
		List<Food_Category> categoryList = categoryDao.queryBuilder().list();
		if(categoryList.isEmpty()) {
			FoodDao foodDao = session.getFoodDao();

			Food_Category cat1 = new Food_Category(null, Long.parseLong("1"), "শর্করা জাতীয় খাবার", false);
			categoryDao.insert(cat1);
//			foodDao.insert(new Food(null, Long.parseLong("101"), cat1.getId(), "১ টি রুটি", "টি", Float.parseFloat("1"), Float.parseFloat("71.88"), Float.parseFloat("9.00"), Float.parseFloat("3.24"), Float.parseFloat("59.67")));
//			foodDao.insert(new Food(null, Long.parseLong("102"), cat1.getId(), "১ টি নান রুটি", "টি", Float.parseFloat("1"), Float.parseFloat("145.525"), Float.parseFloat("19.24"), Float.parseFloat("25.425"), Float.parseFloat("100.86")));
//			foodDao.insert(new Food(null, Long.parseLong("103"), cat1.getId(), "১ স্লাইস পাউরুটি", "স্লাইস", Float.parseFloat("1"), Float.parseFloat("47.526"), Float.parseFloat("8.528"), Float.parseFloat("3.87"), Float.parseFloat("35.128")));
//			foodDao.insert(new Food(null, Long.parseLong("104"), cat1.getId(), "১ কাপ মুড়ি", "কাপ", Float.parseFloat("1"), Float.parseFloat("71.88"), Float.parseFloat("9.00"), Float.parseFloat("3.24"), Float.parseFloat("59.67")));
//			foodDao.insert(new Food(null, Long.parseLong("105"), cat1.getId(), "১ কাপ খই", "কাপ", Float.parseFloat("1"), Float.parseFloat("71.88"), Float.parseFloat("9.00"), Float.parseFloat("3.24"), Float.parseFloat("59.67")));
//			foodDao.insert(new Food(null, Long.parseLong("106"), cat1.getId(), "আধ কাপ শুকনো চিড়া", "কাপ", Float.parseFloat("0.5"), Float.parseFloat("142.40"), Float.parseFloat("10.44"), Float.parseFloat("3.24"), Float.parseFloat("59.67")));
//			foodDao.insert(new Food(null, Long.parseLong("107"), cat1.getId(), "৪ টি মিষ্টি বিসকুট (১০ গ্রাম ওজনের)", "টি", Float.parseFloat("6"), Float.parseFloat("71.88"), Float.parseFloat("9.00"), Float.parseFloat("3.24"), Float.parseFloat("59.67")));					
//			foodDao.insert(new Food(null, Long.parseLong("108"), cat1.getId(), "৪ টি ক্র্যাকার বিসকুট (১০ গ্রাম ওজনের)", "টি", Float.parseFloat("1.00"), Float.parseFloat("71.88"), Float.parseFloat("9.00"), Float.parseFloat("3.24"), Float.parseFloat("59.67")));										
//			foodDao.insert(new Food(null, Long.parseLong("109"), cat1.getId(), "১ কাপ মিষ্টি আলু সিদ্ধ", "কাপ", Float.parseFloat("1.00"), Float.parseFloat("71.88"), Float.parseFloat("9.00"), Float.parseFloat("3.24"), Float.parseFloat("59.67")));
//			foodDao.insert(new Food(null, Long.parseLong("110"), cat1.getId(), "আধ কাপ আলু সিদ্ধ", "কাপ", Float.parseFloat("1.00"), Float.parseFloat("71.88"), Float.parseFloat("9.00"), Float.parseFloat("3.24"), Float.parseFloat("59.67")));
//			foodDao.insert(new Food(null, Long.parseLong("111"), cat1.getId(), "আধ কাপ আলু ভাজি", "কাপ", Float.parseFloat("1.00"), Float.parseFloat("71.88"), Float.parseFloat("9.00"), Float.parseFloat("3.24"), Float.parseFloat("59.67")));				
//			foodDao.insert(new Food(null, Long.parseLong("112"), cat1.getId(), "১ চামচ (৫ মি.লি.) মধু", "চামচ", Float.parseFloat("1.00"), Float.parseFloat("71.88"), Float.parseFloat("9.00"), Float.parseFloat("3.24"), Float.parseFloat("59.67")));					
//			foodDao.insert(new Food(null, Long.parseLong("113"), cat1.getId(), "১ কাপ দুধ চা", "কাপ", Float.parseFloat("1.00"), Float.parseFloat("71.88"), Float.parseFloat("9.00"), Float.parseFloat("3.24"), Float.parseFloat("59.67")));
//			foodDao.insert(new Food(null, Long.parseLong("114"), cat1.getId(), "১ কাপ কফি (দুধ ও চিনি সহ)", "কাপ", Float.parseFloat("1.00"), Float.parseFloat("71.88"), Float.parseFloat("9.00"), Float.parseFloat("3.24"), Float.parseFloat("59.67")));
//			foodDao.insert(new Food(null, Long.parseLong("115"), cat1.getId(), "১ কাপ লিকার চা", "কাপ", Float.parseFloat("1.00"), Float.parseFloat("71.88"), Float.parseFloat("9.00"), Float.parseFloat("3.24"), Float.parseFloat("59.67")));
//			foodDao.insert(new Food(null, Long.parseLong("116"), cat1.getId(), "১ কাপ ভাত", "কাপ", Float.parseFloat("1.00"), Float.parseFloat("71.88"), Float.parseFloat("9.00"), Float.parseFloat("3.24"), Float.parseFloat("59.67")));
//			foodDao.insert(new Food(null, Long.parseLong("117"), cat1.getId(), "১ কাপ খিচুড়ি", "কাপ", Float.parseFloat("1.00"), Float.parseFloat("71.88"), Float.parseFloat("9.00"), Float.parseFloat("3.24"), Float.parseFloat("59.67")));
//			foodDao.insert(new Food(null, Long.parseLong("118"), cat1.getId(), "১ কাপ পোলাও", "কাপ", Float.parseFloat("1.00"), Float.parseFloat("71.88"), Float.parseFloat("9.00"), Float.parseFloat("3.24"), Float.parseFloat("59.67")));
//			foodDao.insert(new Food(null, Long.parseLong("119"), cat1.getId(), "১ কাপ পায়েস", "কাপ", Float.parseFloat("1.00"), Float.parseFloat("71.88"), Float.parseFloat("9.00"), Float.parseFloat("3.24"), Float.parseFloat("59.67")));


			Food_Category cat2 = new Food_Category(null, Long.parseLong("2"), "আমিষ জাতীয় খাবার", false);
			categoryDao.insert(cat2);

//			foodDao.insert(new Food(null, Long.parseLong("120"), cat2.getId(), "১ টি মুরগির ডিম (সিদ্ধ)", "টি", Float.parseFloat("1"), Float.parseFloat("71.88"), Float.parseFloat("9.00"), Float.parseFloat("3.24"), Float.parseFloat("59.67")));
//			foodDao.insert(new Food(null, Long.parseLong("121"), cat2.getId(), "১ টি মুরগির ডিম (পোচ)", "টি", Float.parseFloat("1"), Float.parseFloat("71.88"), Float.parseFloat("9.00"), Float.parseFloat("3.24"), Float.parseFloat("59.67")));
//			foodDao.insert(new Food(null, Long.parseLong("122"), cat2.getId(), "১ টি মুরগির ডিম (ভাজি)", "টি", Float.parseFloat("1"), Float.parseFloat("71.88"), Float.parseFloat("9.00"), Float.parseFloat("3.24"), Float.parseFloat("59.67")));
//			foodDao.insert(new Food(null, Long.parseLong("123"), cat2.getId(), "১ টুকরো মাছ (কাঁটা ছাড়া ৩০ গ্রাম) (তরকারী)", "টুকরো", Float.parseFloat("1"), Float.parseFloat("71.88"), Float.parseFloat("9.00"), Float.parseFloat("3.24"), Float.parseFloat("59.67")));
//			foodDao.insert(new Food(null, Long.parseLong("124"), cat2.getId(), "১ টুকরো মাছ (কাঁটা ছাড়া ৩০ গ্রাম) (ভাজা)", "টুকরো", Float.parseFloat("1"), Float.parseFloat("71.88"), Float.parseFloat("9.00"), Float.parseFloat("3.24"), Float.parseFloat("59.67")));
//			foodDao.insert(new Food(null, Long.parseLong("125"), cat2.getId(), "১ টুকরো মুরগির মাংস (হাড় ছাড়া ৩০ গ্রাম)", "টুকরো", Float.parseFloat("1"), Float.parseFloat("71.88"), Float.parseFloat("9.00"), Float.parseFloat("3.24"), Float.parseFloat("59.67")));
//			foodDao.insert(new Food(null, Long.parseLong("126"), cat2.getId(), "১ টুকরো গরুর মাংস (হাড় ছাড়া ৩০ গ্রাম)", "টুকরো", Float.parseFloat("1"), Float.parseFloat("71.88"), Float.parseFloat("9.00"), Float.parseFloat("3.24"), Float.parseFloat("59.67")));
//			foodDao.insert(new Food(null, Long.parseLong("127"), cat2.getId(), "১ টুকরো খাসির মাংস (হাড় ছাড়া ৩০ গ্রাম)", "টুকরো", Float.parseFloat("1"), Float.parseFloat("71.88"), Float.parseFloat("9.00"), Float.parseFloat("3.24"), Float.parseFloat("59.67")));
//			foodDao.insert(new Food(null, Long.parseLong("128"), cat2.getId(), "১ কাপ চিংড়ি (খোলস ছাড়া) (তরকারি)", "কাপ", Float.parseFloat("1"), Float.parseFloat("71.88"), Float.parseFloat("9.00"), Float.parseFloat("3.24"), Float.parseFloat("59.67")));
//			foodDao.insert(new Food(null, Long.parseLong("129"), cat2.getId(), "১ কাপ চিংড়ি (খোলস ছাড়া) (ভাজা)", "কাপ", Float.parseFloat("1"), Float.parseFloat("71.88"), Float.parseFloat("9.00"), Float.parseFloat("3.24"), Float.parseFloat("59.67")));
//			foodDao.insert(new Food(null, Long.parseLong("130"), cat2.getId(), "১ কাপ মসুর ডাল", "কাপ", Float.parseFloat("1"), Float.parseFloat("71.88"), Float.parseFloat("9.00"), Float.parseFloat("3.24"), Float.parseFloat("59.67")));
//			foodDao.insert(new Food(null, Long.parseLong("131"), cat2.getId(), "১ কাপ গরুর দুধ (পূর্ণ ননী যুক্ত)", "কাপ", Float.parseFloat("1"), Float.parseFloat("71.88"), Float.parseFloat("9.00"), Float.parseFloat("3.24"), Float.parseFloat("59.67")));
//			foodDao.insert(new Food(null, Long.parseLong("132"), cat2.getId(), "১ কাপ গরুর দুধ (ননী ছাড়া)", "কাপ", Float.parseFloat("1"), Float.parseFloat("71.88"), Float.parseFloat("9.00"), Float.parseFloat("3.24"), Float.parseFloat("59.67")));
//			foodDao.insert(new Food(null, Long.parseLong("133"), cat2.getId(), "১ কাপ মিষ্টি দই", "কাপ", Float.parseFloat("1"), Float.parseFloat("71.88"), Float.parseFloat("9.00"), Float.parseFloat("3.24"), Float.parseFloat("59.67")));

			Food_Category cat3 = new Food_Category(null, Long.parseLong("3"), "তেল ও চর্বি জাতীয় খাবার", false);
			categoryDao.insert(cat3);

//			foodDao.insert(new Food(null, Long.parseLong("134"), cat3.getId(), "১ চামচ (৫ মি. লি.) মাখন", "চামচ", Float.parseFloat("1"), Float.parseFloat("71.88"), Float.parseFloat("9.00"), Float.parseFloat("3.24"), Float.parseFloat("59.67")));
//			foodDao.insert(new Food(null, Long.parseLong("135"), cat3.getId(), "১ চামচ (৫ মি. লি.) পনির", "চামচ", Float.parseFloat("1"), Float.parseFloat("71.88"), Float.parseFloat("9.00"), Float.parseFloat("3.24"), Float.parseFloat("59.67")));
//			foodDao.insert(new Food(null, Long.parseLong("136"), cat3.getId(), "১ চামচ (৫ মি. লি.) মেয়নেজ", "চামচ", Float.parseFloat("1"), Float.parseFloat("71.88"), Float.parseFloat("9.00"), Float.parseFloat("3.24"), Float.parseFloat("59.67")));
//			foodDao.insert(new Food(null, Long.parseLong("137"), cat3.getId(), "১ চামচ (৫ মি. লি.) ঘি", "চামচ", Float.parseFloat("1"), Float.parseFloat("71.88"), Float.parseFloat("9.00"), Float.parseFloat("3.24"), Float.parseFloat("59.67")));
//			foodDao.insert(new Food(null, Long.parseLong("138"), cat3.getId(), "১ চামচ (৫ মি. লি.) সরিষার তেল", "চামচ", Float.parseFloat("1"), Float.parseFloat("71.88"), Float.parseFloat("9.00"), Float.parseFloat("3.24"), Float.parseFloat("59.67")));
//			foodDao.insert(new Food(null, Long.parseLong("139"), cat3.getId(), "২৫ গ্রাম চিনাবাদাম", "গ্রাম", Float.parseFloat("1"), Float.parseFloat("71.88"), Float.parseFloat("9.00"), Float.parseFloat("3.24"), Float.parseFloat("59.67")));
//			foodDao.insert(new Food(null, Long.parseLong("140"), cat3.getId(), "১ চামচ (৫ মি. লি.) তিল", "চামচ", Float.parseFloat("1"), Float.parseFloat("71.88"), Float.parseFloat("9.00"), Float.parseFloat("3.24"), Float.parseFloat("59.67")));
//			foodDao.insert(new Food(null, Long.parseLong("141"), cat3.getId(), "১ চামচ (৫ মি. লি.) সরিষার দানা", "চামচ", Float.parseFloat("1"), Float.parseFloat("71.88"), Float.parseFloat("9.00"), Float.parseFloat("3.24"), Float.parseFloat("59.67")));
//			foodDao.insert(new Food(null, Long.parseLong("142"), cat3.getId(), "আধ কাপ নারকেলের টুকরা", "কাপ", Float.parseFloat("1"), Float.parseFloat("71.88"), Float.parseFloat("9.00"), Float.parseFloat("3.24"), Float.parseFloat("59.67")));
//			foodDao.insert(new Food(null, Long.parseLong("143"), cat3.getId(), "আধ কাপ পেস্তা", "কাপ", Float.parseFloat("1"), Float.parseFloat("71.88"), Float.parseFloat("9.00"), Float.parseFloat("3.24"), Float.parseFloat("59.67")));
//			foodDao.insert(new Food(null, Long.parseLong("144"), cat3.getId(), "আধ কাপ মিষ্টি কুমড়ার বিচি", "কাপ", Float.parseFloat("1"), Float.parseFloat("71.88"), Float.parseFloat("9.00"), Float.parseFloat("3.24"), Float.parseFloat("59.67")));

			Food_Category cat4 = new Food_Category(null, Long.parseLong("4"), "শাক-সব্জি", false);
			categoryDao.insert(cat4);

//			foodDao.insert(new Food(null, Long.parseLong("145"), cat4.getId(), "১ কাপ শাক", "কাপ", Float.parseFloat("1.00"), Float.parseFloat("71.88"), Float.parseFloat("9.00"), Float.parseFloat("3.24"), Float.parseFloat("59.67")));
//			foodDao.insert(new Food(null, Long.parseLong("146"), cat4.getId(), "১ কাপ সব্জি সিদ্ধ", "কাপ", Float.parseFloat("1.00"), Float.parseFloat("71.88"), Float.parseFloat("9.00"), Float.parseFloat("3.24"), Float.parseFloat("59.67")));

			Food_Category cat5 = new Food_Category(null, Long.parseLong("5"), "ফল", false);
			categoryDao.insert(cat5);

//			foodDao.insert(new Food(null, Long.parseLong("147"), cat5.getId(), "১ টি পাকা সাগর কলা (৬ ইঞ্চি লম্বা)", "টি", Float.parseFloat("1.00"), Float.parseFloat("71.88"), Float.parseFloat("9.00"), Float.parseFloat("3.24"), Float.parseFloat("59.67")));
//			foodDao.insert(new Food(null, Long.parseLong("148"), cat5.getId(), "১ কাপ স্লাইস করা পাকা পেপে ", "কাপ", Float.parseFloat("1.00"), Float.parseFloat("71.88"), Float.parseFloat("9.00"), Float.parseFloat("3.24"), Float.parseFloat("59.67")));
//			foodDao.insert(new Food(null, Long.parseLong("149"), cat5.getId(), "১ টি আপেল (খোসা সহ) (আড়াই ইঞ্চি)", "টি", Float.parseFloat("1.00"), Float.parseFloat("71.88"), Float.parseFloat("9.00"), Float.parseFloat("3.24"), Float.parseFloat("59.67")));
//			foodDao.insert(new Food(null, Long.parseLong("150"), cat5.getId(), "১ কাপ খোসা ছাড়া স্লাইস করা আপেল", "কাপ", Float.parseFloat("1.00"), Float.parseFloat("71.88"), Float.parseFloat("9.00"), Float.parseFloat("3.24"), Float.parseFloat("59.67")));
//			foodDao.insert(new Food(null, Long.parseLong("151"), cat5.getId(), "১ কাপ টুকরো করে কাটা আম", "কাপ", Float.parseFloat("1.00"), Float.parseFloat("71.88"), Float.parseFloat("9.00"), Float.parseFloat("3.24"), Float.parseFloat("59.67")));
//			foodDao.insert(new Food(null, Long.parseLong("152"), cat5.getId(), "১ কাপ বিচি ছাড়া কাঠাল", "কাপ", Float.parseFloat("1.00"), Float.parseFloat("71.88"), Float.parseFloat("9.00"), Float.parseFloat("3.24"), Float.parseFloat("59.67")));
//			foodDao.insert(new Food(null, Long.parseLong("153"), cat5.getId(), "১ কাপ স্লাইস করা পেয়ারা", "কাপ", Float.parseFloat("1.00"), Float.parseFloat("71.88"), Float.parseFloat("9.00"), Float.parseFloat("3.24"), Float.parseFloat("59.67")));
//			foodDao.insert(new Food(null, Long.parseLong("154"), cat5.getId(), "১ টি মাল্টা (আড়াই ইঞ্চি)", "টি", Float.parseFloat("1.00"), Float.parseFloat("71.88"), Float.parseFloat("9.00"), Float.parseFloat("3.24"), Float.parseFloat("59.67")));
//			foodDao.insert(new Food(null, Long.parseLong("155"), cat5.getId(), "১ টি কমলা (আড়াই ইঞ্চি)", "টি", Float.parseFloat("1.00"), Float.parseFloat("71.88"), Float.parseFloat("9.00"), Float.parseFloat("3.24"), Float.parseFloat("59.67")));
//			foodDao.insert(new Food(null, Long.parseLong("156"), cat5.getId(), "১ কাপ আঙ্গুর", "কাপ", Float.parseFloat("1.00"), Float.parseFloat("71.88"), Float.parseFloat("9.00"), Float.parseFloat("3.24"), Float.parseFloat("59.67")));
//			foodDao.insert(new Food(null, Long.parseLong("157"), cat5.getId(), "১ কাপ জাম্বুরার দানা", "কাপ", Float.parseFloat("1.00"), Float.parseFloat("71.88"), Float.parseFloat("9.00"), Float.parseFloat("3.24"), Float.parseFloat("59.67")));
//			foodDao.insert(new Food(null, Long.parseLong("158"), cat5.getId(), "১ কাপ টুকরো করে কাটা আনারস", "কাপ", Float.parseFloat("1.00"), Float.parseFloat("71.88"), Float.parseFloat("9.00"), Float.parseFloat("3.24"), Float.parseFloat("59.67")));
//			foodDao.insert(new Food(null, Long.parseLong("159"), cat5.getId(), "১ কাপ টুকরো করে কাটা তরমুজ", "কাপ", Float.parseFloat("1.00"), Float.parseFloat("71.88"), Float.parseFloat("9.00"), Float.parseFloat("3.24"), Float.parseFloat("59.67")));
//			foodDao.insert(new Food(null, Long.parseLong("160"), cat5.getId(), "১ টি নাশপাতি (আড়াই ইঞ্চি)", "টি", Float.parseFloat("1.00"), Float.parseFloat("71.88"), Float.parseFloat("9.00"), Float.parseFloat("3.24"), Float.parseFloat("59.67")));
//			foodDao.insert(new Food(null, Long.parseLong("161"), cat5.getId(), "৪ টি খেজুর", "টি", Float.parseFloat("1.00"), Float.parseFloat("71.88"), Float.parseFloat("9.00"), Float.parseFloat("3.24"), Float.parseFloat("59.67")));
//			foodDao.insert(new Food(null, Long.parseLong("162"), cat5.getId(), "৪ টি খোরমা", "টি", Float.parseFloat("1.00"), Float.parseFloat("71.88"), Float.parseFloat("9.00"), Float.parseFloat("3.24"), Float.parseFloat("59.67")));
//			foodDao.insert(new Food(null, Long.parseLong("163"), cat5.getId(), "৪ টি লিচু", "টি", Float.parseFloat("1.00"), Float.parseFloat("71.88"), Float.parseFloat("9.00"), Float.parseFloat("3.24"), Float.parseFloat("59.67")));
//			foodDao.insert(new Food(null, Long.parseLong("164"), cat5.getId(), "১ কাপ বিচি ছাড়া বড়ই", "কাপ", Float.parseFloat("1.00"), Float.parseFloat("71.88"), Float.parseFloat("9.00"), Float.parseFloat("3.24"), Float.parseFloat("59.67")));
//			foodDao.insert(new Food(null, Long.parseLong("165"), cat5.getId(), "১ কাপ বেদানা", "কাপ", Float.parseFloat("1.00"), Float.parseFloat("71.88"), Float.parseFloat("9.00"), Float.parseFloat("3.24"), Float.parseFloat("59.67")));
//			foodDao.insert(new Food(null, Long.parseLong("166"), cat5.getId(), "১ টি আতাফল (৩ ইঞ্চি)", "টি", Float.parseFloat("1.00"), Float.parseFloat("71.88"), Float.parseFloat("9.00"), Float.parseFloat("3.24"), Float.parseFloat("59.67")));
//			foodDao.insert(new Food(null, Long.parseLong("167"), cat5.getId(), "১ টি কামরাঙ্গা (৩ ইঞ্চি লম্বা)", "টি", Float.parseFloat("1.00"), Float.parseFloat("71.88"), Float.parseFloat("9.00"), Float.parseFloat("3.24"), Float.parseFloat("59.67")));
//			foodDao.insert(new Food(null, Long.parseLong("168"), cat5.getId(), "১ কাপ পাকা তেতুলের শাঁস", "কাপ", Float.parseFloat("1.00"), Float.parseFloat("71.88"), Float.parseFloat("9.00"), Float.parseFloat("3.24"), Float.parseFloat("59.67")));

			Food_Category cat6 = new Food_Category(null, Long.parseLong("6"), "পানি", false);
			categoryDao.insert(cat6);

//			foodDao.insert(new Food(null, Long.parseLong("168"), cat6.getId(), "৮ - ১২ গ্লাস পানি", "গ্লাস", Float.parseFloat("1.00"), Float.parseFloat("71.88"), Float.parseFloat("9.00"), Float.parseFloat("3.24"), Float.parseFloat("59.67")));

		}
		/*
		 * REMOVE DUMMY BLOCK END
		 * For Testing Purpose only
		 */
	}
	 
}
