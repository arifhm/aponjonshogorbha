package com.humaclab.aponjonshogorbha.util;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class DatabaseQueue {
    private static ExecutorService mExecutor = null;
    public static void initialize() {
        if(mExecutor != null) {
            mExecutor.shutdown();
        }
        mExecutor = Executors.newFixedThreadPool(1);
    }

    public static void execute(Runnable task) {
        mExecutor.execute(task);
    }

    public static void shutdown() {
        if(mExecutor != null && mExecutor.isShutdown() == false) {
            mExecutor.shutdown();
        }
    }
}
