package com.humaclab.aponjonshogorbha.util;

import android.os.Looper;

public class MainThreadHelper {
    public static void execute(Runnable runnable) {
        android.os.Handler handler = new android.os.Handler(Looper.getMainLooper());
        handler.post(runnable);
    }
}
