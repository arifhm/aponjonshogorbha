package com.humaclab.aponjonshogorbha.activities;

import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.widget.FrameLayout;

import com.humaclab.aponjonshogorbha.Constants;
import com.bd.aponjon.pregnancy.R;
import com.humaclab.aponjonshogorbha.SessionManager;
import com.humaclab.aponjonshogorbha.adapters.NavListAdapter;

import com.humaclab.aponjonshogorbha.fragments.dialogs.DemoModeDialog;
/**
 * 
 * @author anam@humacLab
 *
 */
public abstract class DrawerFragmentActivityDemo extends AppCompatActivity {
	protected abstract Fragment createFragment();

	private Toolbar mToolbar;
	private DrawerLayout mDrawerLayout;
	private RecyclerView mRecycleView;
	private LayoutManager mMgrLyaout;
	private RecyclerView.Adapter mAdapter;
	private ActionBarDrawerToggle mDrawerToggle;

	private FrameLayout mContentFrame;
	private SessionManager mSession;

	private static final String PREFERENCES_FILE = "aponjon_app_settings";
	private static final String PREF_USER_LEARNED_DRAWER = "navigation_drawer_learned";
	private static final String STATE_SELECTED_POSITION = "selected_navigation_drawer_position";

	//	private boolean mUserLearnedDrawer;
	private boolean mFromSavedInstanceState;
	private int mCurrentSelectedPosition;

	private String[] mNavTitles;
	private int[] mIcons = {R.drawable.ic_amar_ami, R.drawable.ic_amar_sonamoni, R.drawable.ic_shishu_kobe_jonmabe, R.drawable.ic_kobe_ki_korbo,
			R.drawable.ic_ki_khabo, R.drawable.ic_amar_jogot, R.drawable.ic_aponjon_nibondhon, R.drawable.ic_app_somporke};
	private String mProfileName;
	private Bitmap mProfileDrawable;
	private int mEditProfileDrawable;

	private FragmentManager mFragManager;
	private Fragment mFragment;

	@Override
	public void onCreate(Bundle savedInstanceState) {        
		requestWindowFeature(Window.FEATURE_NO_TITLE);       
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_activity_main_demo);
		mSession = new SessionManager(this);
		Constants.is_demo = true;
		mNavTitles = getResources().getStringArray(R.array.arr_nav_lit_items_demo);
		mProfileName = "ডেমো";
		mProfileDrawable =  BitmapFactory.decodeResource(getResources(), R.drawable.ic_nav_header_demo_ppic);
		mEditProfileDrawable = R.drawable.ic_edit_pic_main;

		setUpToolbar();

		mDrawerLayout = (DrawerLayout) findViewById(R.id.nav_drawer);

		//		mUserLearnedDrawer = Boolean.valueOf(readSharedSetting(this, PREF_USER_LEARNED_DRAWER, "false"));
		if (savedInstanceState != null) {
			mCurrentSelectedPosition = savedInstanceState.getInt(STATE_SELECTED_POSITION);
			mFromSavedInstanceState = true;
		}

		//		setUpNavDrawer();

		mRecycleView = (RecyclerView) findViewById(R.id.nav_list_view);
		mRecycleView.setHasFixedSize(true);
		mAdapter = new NavListAdapter(mNavTitles, mIcons, mProfileName, mProfileDrawable, mEditProfileDrawable);
		mRecycleView.setAdapter(mAdapter);

		mMgrLyaout = new LinearLayoutManager(this);
		mRecycleView.setLayoutManager(mMgrLyaout);

		mContentFrame = (FrameLayout) findViewById(R.id.nav_contentframe);

		mFragManager = getSupportFragmentManager();
		mFragment = mFragManager.findFragmentById(R.id.nav_contentframe);
		if(mFragment == null) {
			mFragment = createFragment();
			mFragManager.beginTransaction().add(R.id.nav_contentframe, mFragment).commit();
		}


		((NavListAdapter)mAdapter).setOnRecyclerItemClickListener(new NavListAdapter.RecycleAdapterCallback() {

			@Override
			public void onRecycleItemClicked(int position) {
				//Toast.makeText(DrawerFragmentActivity.this, ""+position, Toast.LENGTH_LONG).show();
				setScreenView(position);
			}
		});

		if(mFragment == null) {
			mFragManager.beginTransaction().replace(R.id.nav_contentframe, mFragment).commit();
		}
	}
	DemoModeDialog dialog;
	private void setScreenView(int pos) {
		mDrawerLayout.closeDrawer(GravityCompat.END);
		boolean currHome = false;
		if(this instanceof DashboardActivityDemo) {
			currHome = true;
		}
//		backPressCount = 0;

		switch (pos) {
		case 0 :
			mCurrentSelectedPosition = 0;
			dialog = new DemoModeDialog(DrawerFragmentActivityDemo.this);
			dialog.show();

			break;
		case 1 :
			mCurrentSelectedPosition = 1;
			startActivity(new Intent(DrawerFragmentActivityDemo.this, AmarAmiActivityDemo.class));
			if(!currHome) finish();			
			break;
		case 2:
			mCurrentSelectedPosition = 2;
			startActivity(new Intent(DrawerFragmentActivityDemo.this, AmarShonamoniActivityDemo.class));
			if(!currHome) finish();
			break;
		case 3 :
			mCurrentSelectedPosition = 3;
			startActivity(new Intent(DrawerFragmentActivityDemo.this, ShishuKobeJonmabeActivityDemo.class));
			if(!currHome) finish();
			break;
		case 4:
			mCurrentSelectedPosition = 4;
			startActivity(new Intent(DrawerFragmentActivityDemo.this, TaskManagerActivityDemo.class));
			if(!currHome) finish();
			break;
		case 5:
			mCurrentSelectedPosition = 5;
			startActivity(new Intent(DrawerFragmentActivityDemo.this, KiKhaboActivityDemo.class));
			if(!currHome) finish();
			break;
		case 6:
			mCurrentSelectedPosition = 6;
			startActivity(new Intent(DrawerFragmentActivityDemo.this, AmarJogotActivityDemo.class));
			if(!currHome) finish();
			finish();
			break;
		case 7:
			mCurrentSelectedPosition = 7;
			startActivity(new Intent(DrawerFragmentActivityDemo.this, AponjonNibondhonActivityDemo.class));
			if(!currHome) finish();
			break;
		case 8:
			mCurrentSelectedPosition = 8;
			startActivity(new Intent(DrawerFragmentActivityDemo.this, AboutAppActivityDemo.class));
			if(!currHome) finish();
			break;
		default:
			break;
		}		
	}




	private void setUpToolbar() {
		mToolbar = (Toolbar) findViewById(R.id.tool_bar);
		if (mToolbar != null) {
			setSupportActionBar(mToolbar);
		}
	}

	//	private void setUpNavDrawer() {
	//		//		if (mToolbar != null) {
	//		//			getSupportActionBar().setDisplayHomeAsUpEnabled(false);
	//		//			mToolbar.setNavigationIcon(R.drawable.ic_menu_stack);
	//		//			mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
	//		//				@Override
	//		//				public void onClick(View v) {
	//		//					mDrawerLayout.openDrawer(GravityCompat.END);
	//		//				}
	//		//			});
	//		//		}
	//
	////		if (!mUserLearnedDrawer) {
	////			mDrawerLayout.openDrawer(GravityCompat.END);
	////			mUserLearnedDrawer = true;
	////			saveSharedSetting(this, PREF_USER_LEARNED_DRAWER, "true");
	////		}
	//
	//	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt(STATE_SELECTED_POSITION, mCurrentSelectedPosition);
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		mCurrentSelectedPosition = savedInstanceState.getInt(STATE_SELECTED_POSITION, 0);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case R.id.action_settings:
			mDrawerLayout.openDrawer(GravityCompat.END);
			return true;
		case R.id.action_notification:
			dialog = new DemoModeDialog(DrawerFragmentActivityDemo.this);
			dialog.show();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

//	public static int backPressCount = 0;
//	@Override
//	public void onBackPressed() {
//
//		if(mDrawerLayout.isDrawerOpen(GravityCompat.END)){
//			mDrawerLayout.closeDrawer(GravityCompat.END);
//			backPressCount = 0;
//		}else{
//
//			if(mFragment == null) {
//				mFragment = new DashboardFragment();
//				mFragManager.beginTransaction().add(R.id.nav_contentframe, mFragment).commit();
//			} else {
//				mFragManager.beginTransaction().replace(R.id.nav_contentframe, mFragment).commit();
//			}
//			backPressCount ++;
//			if(backPressCount == 2)
//				Toast.makeText(DrawerFragmentActivityDemo.this, "Tap back once more to exit", Toast.LENGTH_LONG).show();
//			else if(backPressCount > 2) 
//				super.onBackPressed();
//		}
//
//	}
	
	
//	public AutoCompleteItemClicked onSearchItemClicked = new AutoCompleteItemClicked() {
//
//		@Override
//		public void onItemClicked(String str) {
//			List<String> searchNames = Constants.makeSearchArray();
//			if(str != null) {
//				for(int i = 0; i<searchNames.size(); i++) {
//					if(searchNames.get(i).equalsIgnoreCase(str)) {
//						if(i>7) {
//							setScreenView(i+2);
//						} else {
//							setScreenView(i+1);
//						}
//					}
//				}
//			}
//
//		}
//	};
	
	public static void saveSharedSetting(Context ctx, String settingName, String settingValue) {
		SharedPreferences sharedPref = ctx.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPref.edit();
		editor.putString(settingName, settingValue);
		editor.commit();
	}

	public static String readSharedSetting(Context ctx, String settingName, String defaultValue) {
		SharedPreferences sharedPref = ctx.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);
		return sharedPref.getString(settingName, defaultValue);
	}
	@Override
	protected void onResume() {
		super.onResume();
//		backPressCount = 0;
	}
}
