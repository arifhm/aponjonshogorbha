package com.humaclab.aponjonshogorbha.activities;

import com.humaclab.aponjonshogorbha.fragments.AmarJogotFragment;

import android.support.v4.app.Fragment;

public class AmarJogotActivityDemo extends DrawerFragmentActivityDemo {

	@Override
	protected Fragment createFragment() {
		return new AmarJogotFragment();
	}

}
