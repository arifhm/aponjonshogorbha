package com.humaclab.aponjonshogorbha.activities;

import com.humaclab.aponjonshogorbha.fragments.AmarAmiFragment;

import android.support.v4.app.Fragment;

public class AmarAmiActivityDemo extends DrawerFragmentActivityDemo {

	@Override
	protected Fragment createFragment() {
		return new AmarAmiFragment();
	}

}
