package com.humaclab.aponjonshogorbha.activities;

import com.humaclab.aponjonshogorbha.fragments.AboutAppFragment;

import android.support.v4.app.Fragment;

public class AboutAppActivityDemo extends DrawerFragmentActivityDemo {

	@Override
	protected Fragment createFragment() {
		return new AboutAppFragment();
	}

}
