package com.humaclab.aponjonshogorbha.activities;

import com.dnet.mama.taskmanager.TaskManagerFragment;

import android.support.v4.app.Fragment;

public class TaskManagerActivityDemo extends DrawerFragmentActivityDemo {

	@Override
	protected Fragment createFragment() {
		return new TaskManagerFragment();
	}
	
}
