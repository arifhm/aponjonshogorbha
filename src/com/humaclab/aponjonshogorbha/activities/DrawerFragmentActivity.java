package com.humaclab.aponjonshogorbha.activities;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Toast;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.SlidingDrawer;
import android.widget.SlidingDrawer.OnDrawerCloseListener;
import android.widget.SlidingDrawer.OnDrawerOpenListener;
import android.widget.TextView;

import com.humaclab.aponjon.*;
import com.humaclab.aponjon.DaoMaster.DevOpenHelper;

import com.dnet.mama.diary.AllFeelingActivity;
import com.dnet.mama.diary.DiaryActivity;
import com.dnet.mama.diary.TodaysFeelingActivity;
import com.dnet.mama.movecounter.MoveCounterActivity;
import com.dnet.mama.taskmanager.AllTaskActivity;
import com.dnet.mama.taskmanager.TaskManagerFragment;
import com.dnet.mama.taskmanager.TaskManagerTab;
import com.dnet.mama.weight.MyAllWeightActivity;
import com.dnet.mama.weight.MyWeightActivity;
import com.dnet.mama.weight.OthersWeightActivity;
import com.dnet.mama.weight.WeightActivity;
import com.humaclab.aponjonshogorbha.Constants;
import com.bd.aponjon.pregnancy.R;
import com.humaclab.aponjonshogorbha.SessionManager;
import com.humaclab.aponjonshogorbha.activities.kikhabo.AmarKhaddoTalikaActivity;
import com.humaclab.aponjonshogorbha.activities.kikhabo.KhaddoTalikaToiriActivity;
import com.humaclab.aponjonshogorbha.activities.kikhabo.NomunaKhaddoActivity;
import com.humaclab.aponjonshogorbha.adapters.NavListAdapter;
import com.humaclab.aponjonshogorbha.adapters.notification.SlidingDrawerNotificationAdapter;
import com.humaclab.aponjonshogorbha.factory.DaoFactory;
import com.humaclab.aponjonshogorbha.fragments.DashboardFragment;
import com.humaclab.aponjonshogorbha.fragments.DashboardFragment.AutoCompleteItemClicked;
import com.squareup.picasso.Picasso;
/**
 * 
 * @author anam@humacLab
 *
 */
public abstract class DrawerFragmentActivity extends AppCompatActivity implements OnDrawerOpenListener, OnDrawerCloseListener{
	protected abstract Fragment createFragment();

	private DaoSession mDaoSession;
	private Toolbar mToolbar;
	private DrawerLayout mDrawerLayout;
	private RecyclerView mRecycleView;
	private RecyclerView mNotifTodayRecyclerView;
	private RecyclerView mNotifWeekRecyclerView;
	private LayoutManager mMgrLyaout;
	private RecyclerView.Adapter mAdapter;
	private ActionBarDrawerToggle mDrawerToggle;
	private SlidingDrawer mDrawerSlider;
	private RelativeLayout.LayoutParams mParamZeroHeight;
	private RelativeLayout.LayoutParams mParamNonZeroHeight;

	private SharedPreferences mPrefs;
	private boolean mAponjonReg;
	private boolean mIsDrawerOpen;

	private FrameLayout mContentFrame;
	private SessionManager mSession;

	private static final String PREFERENCES_FILE = "aponjon_app_settings";
	private static final String PREF_USER_LEARNED_DRAWER = "navigation_drawer_learned";
	private static final String STATE_SELECTED_POSITION = "selected_navigation_drawer_position";

	//	private boolean mUserLearnedDrawer;
	private boolean mFromSavedInstanceState;
	private int mCurrentSelectedPosition;

	private String[] mNavTitles;
	private int[] mNonRegIcons = {R.drawable.ic_amar_ami, R.drawable.ic_amar_sonamoni, R.drawable.ic_shishu_kobe_jonmabe, R.drawable.ic_kobe_ki_korbo,
			R.drawable.ic_ki_khabo, R.drawable.ic_amar_jogot, R.drawable.ic_aponjon_nibondhon, R.drawable.ic_app_somporke, R.drawable.ic_logout};
	private int[] mRegIcons = {R.drawable.ic_amar_ami, R.drawable.ic_amar_sonamoni, R.drawable.ic_shishu_kobe_jonmabe, R.drawable.ic_kobe_ki_korbo,
			R.drawable.ic_ki_khabo, R.drawable.ic_amar_jogot, R.drawable.ic_doctor_line_white, R.drawable.ic_app_somporke, R.drawable.ic_logout};

	private String mProfileName;
	private Bitmap mProfileDrawable;
	private int mEditProfileDrawable;
	private String mCurrentPhotoPath;
	private File profImgFile;

	private FragmentManager mFragManager;
	private Fragment mFragment;

	TextView mTextShow1;
	TextView mTextShow2;
	TextView mTextShow3;
	ViewGroup mHorzLine;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);       
		setContentView(R.layout.layout_activity_main);

		mSession = new SessionManager(this);
		mPrefs = mSession.getPrefs();

		DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, "aponjon-db", null);
		SQLiteDatabase db = helper.getWritableDatabase();
		DaoMaster daomaster = new DaoMaster(db);
		mDaoSession = daomaster.newSession();
		
		
		mTextShow1 = (TextView) findViewById(R.id.noti_no_task1);
		mTextShow2 = (TextView) findViewById(R.id.noti_no_task2);
		mTextShow3 = (TextView) findViewById(R.id.noti_no_task3);
		mHorzLine = (ViewGroup) findViewById(R.id.layout_horz_line);

		mAponjonReg = mPrefs.getBoolean(Constants.UserData.IS_APONJON_REG, false);
		Constants.is_demo = false;

		if(mAponjonReg) {
			mNavTitles = getResources().getStringArray(R.array.arr_nav_lit_items2);
		} else {
			mNavTitles = getResources().getStringArray(R.array.arr_nav_lit_items1);
		}

		mProfileName = mPrefs.getString(Constants.UserData.USER_NAME, "");

		mCurrentPhotoPath = mPrefs.getString(Constants.UserData.USER_IMG_SD_PATH, null);
		
		IntentFilter logoutFilter = new IntentFilter();
		logoutFilter.addAction("com.humaclab.aponjon.ACTION_LOGOUT");
		
		registerReceiver(new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				finish();
			}			
		}, logoutFilter);


		if(mCurrentPhotoPath != null) {
			profImgFile = new File(mCurrentPhotoPath);
		}
		
		if(profImgFile != null && profImgFile.exists()) {
			Bitmap bitmap = BitmapFactory.decodeFile(profImgFile.getAbsolutePath());
			Matrix mtx = new Matrix();
			mtx.postRotate(270);
			// Rotating Bitmap
			Bitmap rotatedBMP = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), mtx, true);
			mProfileDrawable = rotatedBMP;
		} else {
			mProfileDrawable = BitmapFactory.decodeResource(getResources(), R.drawable.ic_nav_header_demo_ppic);
		}
		
		mCurrentPhotoPath = mPrefs.getString(Constants.UserData.USER_IMG_SD_PATH, null);
		if(mCurrentPhotoPath!=null) {
			profImgFile = new File(mCurrentPhotoPath);
		}
		
		if(profImgFile!=null && profImgFile.exists()) {
			Bitmap bitmap = BitmapFactory.decodeFile(profImgFile.getAbsolutePath());
			Matrix mtx = new Matrix();
			mtx.postRotate(270);
			// Rotating Bitmap
			Bitmap rotatedBMP = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), mtx, true);
			mProfileDrawable = rotatedBMP;
		} else {
//			String photoLink = mPrefs.getString(Constants.UserData.USER_IMG_LINK, null);
//			if(photoLink!=null) {
//				try {
//					mProfileDrawable = Picasso.with(this).load(photoLink).get();
//				} catch (IOException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//			}
			mProfileDrawable = BitmapFactory.decodeResource(getResources(), R.drawable.ic_nav_header_demo_ppic);
		}

		mEditProfileDrawable = R.drawable.ic_edit_pic_main;

		setUpToolbar();


		mDrawerLayout = (DrawerLayout) findViewById(R.id.nav_drawer);

		//		mUserLearnedDrawer = Boolean.valueOf(readSharedSetting(this, PREF_USER_LEARNED_DRAWER, "false"));
		if (savedInstanceState != null) {
			mCurrentSelectedPosition = savedInstanceState.getInt(STATE_SELECTED_POSITION);
			mFromSavedInstanceState = true;
		}

		//		setUpNavDrawer();

		mRecycleView = (RecyclerView) findViewById(R.id.nav_list_view);
		mRecycleView.setHasFixedSize(true);
		mMgrLyaout = new LinearLayoutManager(this);
		mRecycleView.setLayoutManager(mMgrLyaout);
		if(mAponjonReg) {
			mAdapter = new NavListAdapter(mNavTitles, mRegIcons, mProfileName, mProfileDrawable, mEditProfileDrawable);
		} else {
			mAdapter = new NavListAdapter(mNavTitles, mNonRegIcons, mProfileName, mProfileDrawable, mEditProfileDrawable);
		}		
		mRecycleView.setAdapter(mAdapter);

		mNotifTodayRecyclerView = (RecyclerView) findViewById(R.id.recyclerView_notification_today);
		mNotifTodayRecyclerView.setHasFixedSize(false);
		mNotifTodayRecyclerView.setLayoutManager(new LinearLayoutManager(this));
		mNotifTodayRecyclerView.setItemAnimator(new DefaultItemAnimator());
//		mNotifWeekRecyclerView = (RecyclerView) findViewById(R.id.recyclerView_notification_week);
//		mNotifWeekRecyclerView.setHasFixedSize(false);
//		mNotifWeekRecyclerView.setLayoutManager(new LinearLayoutManager(this));
//		mNotifWeekRecyclerView.setItemAnimator(new DefaultItemAnimator());
		
		// SlidingDownList Tasks
		TaskDao taskDao = DaoFactory.getTaskDao();		
		List<Task> todayTasks = taskDao.queryBuilder().whereOr(TaskDao.Properties.DayNo.eq(getUserCurrentDay()), TaskDao.Properties.Task_date.eq(new Date(System.currentTimeMillis()))).list();
		Calendar calFirstDayOfweek = Calendar.getInstance();
		calFirstDayOfweek.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);
		Calendar calLastDayOfweek = Calendar.getInstance();
		calLastDayOfweek.set(Calendar.DAY_OF_WEEK, Calendar.FRIDAY);
//		List<Task> weekTasks = taskDao.queryBuilder().whereOr(TaskDao.Properties.WeekNo.eq(mPrefs.getInt(Constants.UserData.USER_CURRENT_WEEK, 0)),
//				TaskDao.Properties.Task_date.between(calFirstDayOfweek.getTime(), calLastDayOfweek.getTime())).list();
		List<Task> weekTasks = taskDao.queryBuilder().where(TaskDao.Properties.Task_date.between(calFirstDayOfweek.getTime(), calLastDayOfweek.getTime())).list();

		if(todayTasks.isEmpty() && weekTasks.isEmpty()) {
			mTextShow1.setVisibility(View.VISIBLE);
			mHorzLine.setVisibility(View.VISIBLE);
//			mNotifTodayRecyclerView.setVisibility(View.GONE);
//			mNotifWeekRecyclerView.setVisibility(View.GONE);
		} else if(weekTasks.isEmpty()) {
			mTextShow3.setVisibility(View.VISIBLE);
			mHorzLine.setVisibility(View.VISIBLE);
//			mNotifWeekRecyclerView.setVisibility(View.GONE);
			SlidingDrawerNotificationAdapter adapterTodayTask = new SlidingDrawerNotificationAdapter(this, todayTasks, null, "আজকের দিনের করণীয়", null);
			mNotifTodayRecyclerView.setAdapter(adapterTodayTask);
		} else if(todayTasks.isEmpty()) {
			mTextShow2.setVisibility(View.VISIBLE);
			mHorzLine.setVisibility(View.VISIBLE);
//			mNotifTodayRecyclerView.setVisibility(View.GONE);
			SlidingDrawerNotificationAdapter adapterWeekTask = new SlidingDrawerNotificationAdapter(this, null,weekTasks, null, "এই সপ্তাহের করণীয়");
			mNotifTodayRecyclerView.setAdapter(adapterWeekTask);
		} else {
			mTextShow1.setVisibility(View.GONE);
			mTextShow2.setVisibility(View.GONE);
			mTextShow3.setVisibility(View.GONE);
			mHorzLine.setVisibility(View.GONE);
//			mNotifTodayRecyclerView.setVisibility(View.VISIBLE);
			SlidingDrawerNotificationAdapter adapterTodayTask = new SlidingDrawerNotificationAdapter(this, todayTasks, weekTasks, "আজকের দিনের করণীয়", "এই সপ্তাহের করণীয়");
			mNotifTodayRecyclerView.setAdapter(adapterTodayTask);
//			mNotifWeekRecyclerView.setVisibility(View.VISIBLE);
//			SlidingDrawerNotificationAdapter adapterWeekTask = new SlidingDrawerNotificationAdapter(this, weekTasks, "এই সপ্তাহের করণীয়");
//			mNotifWeekRecyclerView.setAdapter(adapterWeekTask);
		}


		mContentFrame = (FrameLayout) findViewById(R.id.nav_contentframe);

		mFragManager = getSupportFragmentManager();
		mFragment = mFragManager.findFragmentById(R.id.nav_contentframe);
		if(mFragment == null) {
			mFragment = createFragment();
			if(mFragment instanceof DashboardFragment) {
				((DashboardFragment)mFragment).setOnAutoCompleteItemClickListener(onSearchItemClicked);
			}			
			mFragManager.beginTransaction().add(R.id.nav_contentframe, mFragment).commit();
		}


		((NavListAdapter)mAdapter).setOnRecyclerItemClickListener(new NavListAdapter.RecycleAdapterCallback() {

			@Override
			public void onRecycleItemClicked(int position) {
				//Toast.makeText(DrawerFragmentActivity.this, ""+position, Toast.LENGTH_LONG).show();
				setScreenView(position);
			}
		});

		if(mFragment == null) {
			mFragManager.beginTransaction().replace(R.id.nav_contentframe, mFragment).commit();
		}

		mParamZeroHeight = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, 0);
		mParamNonZeroHeight = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, 60);
		mDrawerSlider = (SlidingDrawer) findViewById(R.id.view_sliding_panel);
		mDrawerSlider.setOnDrawerOpenListener(this);
		mDrawerSlider.setOnDrawerCloseListener(this);
	}

	@Override
	public void onDrawerClosed() {
		mIsDrawerOpen = false;
		mDrawerSlider.getHandle().setLayoutParams(mParamZeroHeight);
	}

	@Override
	public void onDrawerOpened() {
		mIsDrawerOpen = true;
		mDrawerSlider.getHandle().setLayoutParams(mParamNonZeroHeight);
	}

	private void setScreenView(int pos) {
		mDrawerLayout.closeDrawer(GravityCompat.END);
		//		backPressCount = 0;
		boolean currHome = false;
		if(this instanceof DashboardActivity) {
			currHome = true;
		}
		switch (pos) {
		case 0 :
			//                    Snackbar.make(mContentFrame, "Item One", Snackbar.LENGTH_SHORT).show();
			mCurrentSelectedPosition = 0;
			startActivity(new Intent(DrawerFragmentActivity.this, EditProfileActivity.class));
			if(!currHome) finish();			
			break;
		case 1 :
			//                    Snackbar.make(mContentFrame, "Item One", Snackbar.LENGTH_SHORT).show();
			mCurrentSelectedPosition = 1;
			startActivity(new Intent(DrawerFragmentActivity.this, AmarAmiActivity.class));
			if(!currHome) finish();
			break;
		case 2:
			mCurrentSelectedPosition = 2;
			startActivity(new Intent(DrawerFragmentActivity.this, AmarShonamoniActivity.class));
			if(!currHome) finish();
			break;
		case 3 :
			mCurrentSelectedPosition = 3;
			startActivity(new Intent(DrawerFragmentActivity.this, ShishuKobeJonmabeActivity.class));
			if(!currHome) finish();
			break;
		case 4:
			mCurrentSelectedPosition = 4;
			startActivity(new Intent(DrawerFragmentActivity.this, TaskManagerActivity.class));
			if(!currHome) finish();
			break;
		case 5:
			mCurrentSelectedPosition = 5;
			startActivity(new Intent(DrawerFragmentActivity.this, KiKhaboActivity.class));
			if(!currHome) finish();
			break;
		case 6:
			mCurrentSelectedPosition = 6;
			startActivity(new Intent(DrawerFragmentActivity.this, AmarJogotActivity.class));
			finish();
			break;
		case 7:
			mCurrentSelectedPosition = 7;
			if(mAponjonReg) {
				startActivity(new Intent(DrawerFragmentActivity.this, DoctorLineActivity.class));
				if(!currHome) finish();
			} else 
				startActivity(new Intent(DrawerFragmentActivity.this, AponjonNibondhonActivity.class));
			if(!currHome) finish();
			break;
		case 8:
			mCurrentSelectedPosition = 8;
			startActivity(new Intent(DrawerFragmentActivity.this, AboutAppActivity.class));
			if(!currHome) finish();
			break;
		case 9:
			mCurrentSelectedPosition = 9;
			mSession.logoutUser();
			finish();
			break;
		case 10:
			Intent intent =new Intent(DrawerFragmentActivity.this, TaskManagerTab.class);
			intent.putExtra(TaskManagerFragment.FILTER, TaskManagerFragment.FILTER_MONTH);
			//			intent.putExtra("CurrentMonth", currentMontValue);
			startActivity(intent);
			if(!currHome) finish();
			break;
		case 11:
			startActivity(new Intent(DrawerFragmentActivity.this, AllTaskActivity.class));
			if(!currHome) finish();
			break;
		case 12:
			startActivity(new Intent(DrawerFragmentActivity.this, NomunaKhaddoActivity.class));
			if(!currHome) finish();
			break;
		case 13:
			startActivity(new Intent(DrawerFragmentActivity.this, KhaddoTalikaToiriActivity.class));
			if(!currHome) finish();
			break;
		case 14:
			startActivity(new Intent(DrawerFragmentActivity.this, AmarKhaddoTalikaActivity.class));
			if(!currHome) finish();
			break;
		case 15:
			startActivity(new Intent(DrawerFragmentActivity.this, DiaryActivity.class));
			if(!currHome) finish();
			break;
		case 16:
			startActivity(new Intent(DrawerFragmentActivity.this, MoveCounterActivity.class));
			if(!currHome) finish();
			break;
		case 17:
			startActivity(new Intent(DrawerFragmentActivity.this, WeightActivity.class));
			if(!currHome) finish();
			break;
		case 18:
			startActivity(new Intent(DrawerFragmentActivity.this, TodaysFeelingActivity.class));
			if(!currHome) finish();
			break;
		case 19:
			startActivity(new Intent(DrawerFragmentActivity.this, AllFeelingActivity.class));
			if(!currHome) finish();
			break;
		case 20:
			startActivity(new Intent(DrawerFragmentActivity.this, MyWeightActivity.class));
			if(!currHome) finish();
			break;
		case 21:
			startActivity(new Intent(DrawerFragmentActivity.this, MyAllWeightActivity.class));
			if(!currHome) finish();
			break;
		case 22:
			startActivity(new Intent(DrawerFragmentActivity.this, OthersWeightActivity.class));
			if(!currHome) finish();
			break;
		case 23:
			startActivity(new Intent(DrawerFragmentActivity.this, AponjonKiKenoActivity.class));
			if(!currHome) finish();
		default:
			break;
		}
	}


	private void setUpToolbar() {
		mToolbar = (Toolbar) findViewById(R.id.tool_bar);
		if (mToolbar != null) {
			setSupportActionBar(mToolbar);
		}
	}

	//	private void setUpNavDrawer() {
	//
	////		if (!mUserLearnedDrawer) {
	////			mDrawerLayout.openDrawer(GravityCompat.END);
	////			mUserLearnedDrawer = true;
	////			saveSharedSetting(this, PREF_USER_LEARNED_DRAWER, "true");
	////		}
	//	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt(STATE_SELECTED_POSITION, mCurrentSelectedPosition);
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		mCurrentSelectedPosition = savedInstanceState.getInt(STATE_SELECTED_POSITION, 0);
		//        Menu menu = mNavigationView.getMenu();
		//        menu.getItem(mCurrentSelectedPosition).setChecked(true);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case R.id.action_settings:
			mDrawerLayout.openDrawer(GravityCompat.END);
			return true;
		case R.id.action_notification:
			mDrawerSlider.animateOpen();
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	//	public static int backPressCount = 0;
	//	@Override
	//	public void onBackPressed() {
	//
	//		if(mDrawerLayout.isDrawerOpen(GravityCompat.END)){
	//			mDrawerLayout.closeDrawer(GravityCompat.END);
	//			backPressCount = 0;
	//		}else{
	//			if(mFragment instanceof DashboardFragment){
	//				mFragManager.beginTransaction().replace(R.id.nav_contentframe, mFragment).commit();
	//			}else{
	//				super.onBackPressed();
	//			}
	////			if(mFragment == null) {
	////				mFragment = new DashboardFragment();
	////				((DashboardFragment)mFragment).setOnAutoCompleteItemClickListener(onSearchItemClicked);
	////				mFragManager.beginTransaction().add(R.id.nav_contentframe, mFragment).commit();
	////			} else {
	////
	////			}
	//			backPressCount ++;
	//			if(backPressCount == 1)
	//				Toast.makeText(DrawerFragmentActivity.this, "Tap back once more to exit", Toast.LENGTH_LONG).show();
	//			else if(backPressCount > 1) 
	//				super.onBackPressed();
	//		}
	//
	//	}

	public AutoCompleteItemClicked onSearchItemClicked = new AutoCompleteItemClicked() {

		@Override
		public void onItemClicked(String str) {
			List<String> searchNames = Constants.makeSearchArray();
			if(str != null) {
				for(int i = 0; i<searchNames.size(); i++) {
					if(searchNames.get(i).equalsIgnoreCase(str)) {
						if(i>7) {
							setScreenView(i+2);
						} else {
							setScreenView(i+1);
						}
					}
				}
			}

		}
	};

	public static void saveSharedSetting(Context ctx, String settingName, String settingValue) {
		SharedPreferences sharedPref = ctx.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPref.edit();
		editor.putString(settingName, settingValue);
		editor.commit();
	}

	public static String readSharedSetting(Context ctx, String settingName, String defaultValue) {
		SharedPreferences sharedPref = ctx.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);
		return sharedPref.getString(settingName, defaultValue);
	}
	@Override
	protected void onResume() {
		super.onResume();
		
		TaskDao taskDao = DaoFactory.getTaskDao();
		List<Task> todayTasks = taskDao.queryBuilder().whereOr(TaskDao.Properties.DayNo.eq(getUserCurrentDay()), TaskDao.Properties.Task_date.eq(new Date(System.currentTimeMillis()))).list();
		Calendar calFirstDayOfweek = Calendar.getInstance();
		calFirstDayOfweek.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);
		Calendar calLastDayOfweek = Calendar.getInstance();
		calLastDayOfweek.set(Calendar.DAY_OF_WEEK, Calendar.FRIDAY);
		List<Task> weekTasks = taskDao.queryBuilder().whereOr(TaskDao.Properties.WeekNo.eq(mPrefs.getInt(Constants.UserData.USER_CURRENT_WEEK, 0)),
				TaskDao.Properties.Task_date.between(calFirstDayOfweek.getTime(), calLastDayOfweek.getTime())).list();
		
		if(todayTasks.isEmpty() && weekTasks.isEmpty()) {
			mTextShow1.setVisibility(View.VISIBLE);
			mHorzLine.setVisibility(View.VISIBLE);
//			mNotifTodayRecyclerView.setVisibility(View.GONE);
//			mNotifWeekRecyclerView.setVisibility(View.GONE);
		} else if(weekTasks.isEmpty()) {
			mTextShow3.setVisibility(View.VISIBLE);
			mHorzLine.setVisibility(View.VISIBLE);
//			mNotifWeekRecyclerView.setVisibility(View.GONE);
			SlidingDrawerNotificationAdapter adapterTodayTask = new SlidingDrawerNotificationAdapter(this, todayTasks, null, "আজকের দিনের করণীয়", null);
			mNotifTodayRecyclerView.swapAdapter(adapterTodayTask, false);
		} else if(todayTasks.isEmpty()) {
			mTextShow2.setVisibility(View.VISIBLE);
			mHorzLine.setVisibility(View.VISIBLE);
//			mNotifTodayRecyclerView.setVisibility(View.GONE);
			SlidingDrawerNotificationAdapter adapterWeekTask = new SlidingDrawerNotificationAdapter(this, null,weekTasks, null, "এই সপ্তাহের করণীয়");
			mNotifTodayRecyclerView.setAdapter(adapterWeekTask);
		} else {
			mTextShow1.setVisibility(View.GONE);
			mTextShow2.setVisibility(View.GONE);
			mTextShow3.setVisibility(View.GONE);
			mHorzLine.setVisibility(View.GONE);
//			mNotifTodayRecyclerView.setVisibility(View.VISIBLE);
			SlidingDrawerNotificationAdapter adapterTodayTask = new SlidingDrawerNotificationAdapter(this, todayTasks, weekTasks, "আজকের দিনের করণীয়", "এই সপ্তাহের করণীয়");
			mNotifTodayRecyclerView.swapAdapter(adapterTodayTask, false);
//			mNotifWeekRecyclerView.setVisibility(View.VISIBLE);
//			SlidingDrawerNotificationAdapter adapterWeekTask = new SlidingDrawerNotificationAdapter(this, weekTasks, "এই সপ্তাহের করণীয়");
//			mNotifWeekRecyclerView.setAdapter(adapterWeekTask);
		}
		
//		if(todayTasks.isEmpty() && weekTasks.isEmpty()) {
//			mTextShow1.setVisibility(View.VISIBLE);
//			mHorzLine.setVisibility(View.VISIBLE);
//			mNotifTodayRecyclerView.setVisibility(View.GONE);
//			mNotifWeekRecyclerView.setVisibility(View.GONE);
//		} else if(weekTasks.isEmpty()) {
//			mTextShow3.setVisibility(View.VISIBLE);
//			mHorzLine.setVisibility(View.VISIBLE);
//			mNotifWeekRecyclerView.setVisibility(View.GONE);
//			SlidingDrawerNotificationAdapter adapterTodayTask = new SlidingDrawerNotificationAdapter(this, todayTasks, "আজকের দিনের করণীয়");
//			mNotifTodayRecyclerView.swapAdapter(adapterTodayTask, true);
//		} else if(todayTasks.isEmpty()) {
//			mTextShow2.setVisibility(View.VISIBLE);
//			mHorzLine.setVisibility(View.VISIBLE);
//			mNotifTodayRecyclerView.setVisibility(View.GONE);
//			SlidingDrawerNotificationAdapter adapterWeekTask = new SlidingDrawerNotificationAdapter(this, weekTasks, "এই সপ্তাহের করণীয়");
//			mNotifWeekRecyclerView.swapAdapter(adapterWeekTask, true);
//		} else {
//			mTextShow1.setVisibility(View.GONE);
//			mTextShow2.setVisibility(View.GONE);
//			mTextShow3.setVisibility(View.GONE);
//			mHorzLine.setVisibility(View.GONE);
//			mNotifTodayRecyclerView.setVisibility(View.VISIBLE);	
//			SlidingDrawerNotificationAdapter adapterTodayTask = new SlidingDrawerNotificationAdapter(this, todayTasks, "আজকের দিনের করণীয়");
//			mNotifTodayRecyclerView.setAdapter(adapterTodayTask);
//			mNotifWeekRecyclerView.setVisibility(View.VISIBLE);
//			SlidingDrawerNotificationAdapter adapterWeekTask = new SlidingDrawerNotificationAdapter(this, weekTasks, "এই সপ্তাহের করণীয়");
//			mNotifWeekRecyclerView.setAdapter(adapterWeekTask);
//		}
	}

	boolean doubleBackToExitPressedOnce = false;

	@Override
	public void onBackPressed() {
		if((mFragment instanceof DashboardFragment) && !mDrawerLayout.isDrawerOpen(GravityCompat.END)) {
			if (doubleBackToExitPressedOnce) {
				super.onBackPressed();
				return;
			}

			this.doubleBackToExitPressedOnce = true;
			Toast.makeText(this, "আরো একবার Back চাপুন", Toast.LENGTH_SHORT).show();

			new Handler().postDelayed(new Runnable() {

				@Override
				public void run() {
					doubleBackToExitPressedOnce=false;                       
				}
			}, 2000);
		} else if(mDrawerLayout.isDrawerOpen(GravityCompat.END)) {
			mDrawerLayout.closeDrawer(GravityCompat.END);
		} else {
			super.onBackPressed();
			return;
		}
	}

	public int getUserCurrentDay() {
		SimpleDateFormat format = new SimpleDateFormat();
		format.applyPattern("yyyy-MM-dd");

		Date lmp = null;
		Date curDate = null;
		int dayPassed = 0;
		try {
			lmp = format.parse(mPrefs.getString(Constants.UserData.USER_LMP, null));
			curDate = new Date(System.currentTimeMillis());
			dayPassed = (int) ((curDate.getTime() - (lmp.getTime()-1000*60*60*24l)) / (1000*60*60*24l));

		} catch (ParseException e) {
			e.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
		}
		return dayPassed;
	}
}
