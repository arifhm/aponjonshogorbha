package com.humaclab.aponjonshogorbha.activities;

import android.os.Handler;
import android.support.v4.app.Fragment;
import android.widget.Toast;

import com.humaclab.aponjonshogorbha.fragments.DashboardFragment;

public class DashboardActivity extends DrawerFragmentActivity {

	@Override
	protected Fragment createFragment() {
		return new DashboardFragment();
	} 
	
}
