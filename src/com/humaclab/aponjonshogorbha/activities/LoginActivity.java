package com.humaclab.aponjonshogorbha.activities;

import com.humaclab.aponjonshogorbha.SingleFragmentActivity;
import com.humaclab.aponjonshogorbha.fragments.LoginFragment;

import android.support.v4.app.Fragment;

public class LoginActivity extends SingleFragmentActivity {

	@Override
	protected Fragment createFragment() {
		return new LoginFragment();
	}

}
