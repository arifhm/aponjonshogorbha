package com.humaclab.aponjonshogorbha.activities;

import android.support.v4.app.Fragment;

import com.humaclab.aponjonshogorbha.fragments.ShishuKobeJonmabeFragment;

public class ShishuKobeJonmabeActivity extends DrawerFragmentActivity {

	@Override
	protected Fragment createFragment() {
		return new ShishuKobeJonmabeFragment();
	}

}
