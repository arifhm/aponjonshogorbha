package com.humaclab.aponjonshogorbha.activities;

import com.humaclab.aponjonshogorbha.fragments.AmarShonamoniFragment;

import android.support.v4.app.Fragment;

public class AmarShonamoniActivity extends DrawerFragmentActivity {

	@Override
	protected Fragment createFragment() {
		return new AmarShonamoniFragment();
	}

}
