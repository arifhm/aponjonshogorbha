package com.humaclab.aponjonshogorbha.activities;

import com.humaclab.aponjonshogorbha.SingleFragmentActivity;
import com.humaclab.aponjonshogorbha.fragments.RegisterFragment;

import android.support.v4.app.Fragment;

public class RegisterActivity extends SingleFragmentActivity {

	@Override
	protected Fragment createFragment() {
		return RegisterFragment.newInstance();
	}

}
