package com.humaclab.aponjonshogorbha.activities;

import com.humaclab.aponjonshogorbha.fragments.nibondhon.AponjonKiKenoFragment;

import android.support.v4.app.Fragment;

public class AponjonKiKenoActivity extends DrawerFragmentActivity {

	@Override
	protected Fragment createFragment() {
		return new AponjonKiKenoFragment();
	}
	
}
