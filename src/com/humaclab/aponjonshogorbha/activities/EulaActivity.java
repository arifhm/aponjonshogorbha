package com.humaclab.aponjonshogorbha.activities;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;

import com.humaclab.aponjonshogorbha.Constants;
import com.bd.aponjon.pregnancy.R;
import com.humaclab.aponjonshogorbha.SessionManager;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebSettings.LayoutAlgorithm;

public class EulaActivity extends Activity {

//	WebView mView;
//	Button mAgreeButton;
//	Button mNoButton;
	SessionManager mSession;
	SharedPreferences mPrefs;
	Editor mEditor;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_eula_webview);

//		mView = (WebView) findViewById(R.id.webView_eula);
//		mAgreeButton = (Button) findViewById(R.id.view_btn_agree_yes);
//		mNoButton = (Button) findViewById(R.id.view_btn_agree_no);
		mSession = new SessionManager(this);
		mPrefs = mSession.getPrefs();
		mEditor = mSession.getEditor();

//		setupWebView();
		showDetailConfirmDialog();

//		mAgreeButton.setOnClickListener(new View.OnClickListener() {		
//			@Override
//			public void onClick(View v) {
//				
//				mEditor.putBoolean(Constants.AppData.IS_FIRST_TIME, false);
//				mEditor.commit();
//				startActivity(new Intent(EulaActivity.this, StartActivity.class));
//				finish();
//
//			}
//		});
//		mNoButton.setOnClickListener(new View.OnClickListener() {		
//			@Override
//			public void onClick(View v) {
//				finish();
//			}
//		});

	}
	
	
	public void showDetailConfirmDialog(){
		AlertDialog.Builder alert = new AlertDialog.Builder(this); 
		alert.setTitle("মোবাইল অ্যাপ্লিকেশন ব্যবহারের শর্তাবলী");

		WebView htmlWebView = new WebView(this);
		
		WebSettings webSetting = htmlWebView.getSettings();
		webSetting.setLayoutAlgorithm(LayoutAlgorithm.SINGLE_COLUMN);
				
        String htmlFilename = "eula.html";
        AssetManager mgr = getAssets();
        try {
            InputStream in = mgr.open(htmlFilename, AssetManager.ACCESS_BUFFER);
            String htmlContentInStringFormat = StreamToString(in);
            in.close();
            htmlWebView.loadDataWithBaseURL(null, htmlContentInStringFormat, "text/html", "utf-8", null);
 
        } catch (IOException e) {
            e.printStackTrace();
        }
				
		alert.setView(htmlWebView);
		alert.setPositiveButton("ঠিক আছে", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int id) {
				mEditor.putBoolean(Constants.AppData.IS_FIRST_TIME, false);
				mEditor.commit();
				startActivity(new Intent(EulaActivity.this, StartActivity.class));
				finish();
				dialog.dismiss();
			}
		});
		alert.setNegativeButton("বাতিল", new DialogInterface.OnClickListener() {			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				finish();
			}
		});
		alert.show();
	}
	
	public static String StreamToString(InputStream in) throws IOException {
        if(in == null) {
            return "";
        }
        Writer writer = new StringWriter();
        char[] buffer = new char[1024];
        try {
            Reader reader = new BufferedReader(new InputStreamReader(in, "UTF-8"));
            int n;
            while ((n = reader.read(buffer)) != -1) {
                writer.write(buffer, 0, n);
            }
        } finally {
        }
        return writer.toString();
    }

//	public void setupWebView(){		
//		WebSettings webSetting = mView.getSettings();
//		webSetting.setLayoutAlgorithm(LayoutAlgorithm.SINGLE_COLUMN);
//
//		String htmlFilename = "eula.html";
//		AssetManager mgr = getAssets();
//		try {
//			InputStream in = mgr.open(htmlFilename, AssetManager.ACCESS_BUFFER);
//			String htmlContentInStringFormat = StreamToString(in);
//			in.close();
//			mView.loadDataWithBaseURL(null, htmlContentInStringFormat, "text/html", "utf-8", null);
//
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//	}
//
//	public static String StreamToString(InputStream in) throws IOException {
//		if(in == null) {
//			return "";
//		}
//		Writer writer = new StringWriter();
//		char[] buffer = new char[1024];
//		try {
//			Reader reader = new BufferedReader(new InputStreamReader(in, "UTF-8"));
//			int n;
//			while ((n = reader.read(buffer)) != -1) {
//				writer.write(buffer, 0, n);
//			}
//		} finally {
//		}
//		return writer.toString();
//	}	

}
