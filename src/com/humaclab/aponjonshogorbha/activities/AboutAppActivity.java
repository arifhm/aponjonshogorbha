package com.humaclab.aponjonshogorbha.activities;

import com.humaclab.aponjonshogorbha.fragments.AboutAppFragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;

public class AboutAppActivity extends DrawerFragmentActivity {

	@Override
	protected Fragment createFragment() {
		return new AboutAppFragment();
	}
	
}
