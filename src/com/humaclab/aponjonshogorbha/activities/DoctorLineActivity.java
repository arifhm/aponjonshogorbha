package com.humaclab.aponjonshogorbha.activities;

import com.humaclab.aponjonshogorbha.fragments.DoctorLineFragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;

public class DoctorLineActivity extends DrawerFragmentActivity {

	@Override
	protected Fragment createFragment() {
		return new DoctorLineFragment();
	}
	
}
