package com.humaclab.aponjonshogorbha.activities;

import com.humaclab.aponjonshogorbha.fragments.DoctorLineFragment;

import android.support.v4.app.Fragment;

public class DoctorLineActivityDemo extends DrawerFragmentActivityDemo {

	@Override
	protected Fragment createFragment() {
		return new DoctorLineFragment();
	}

}
