package com.humaclab.aponjonshogorbha.activities.kikhabo;

import android.support.v4.app.Fragment;

import com.humaclab.aponjonshogorbha.activities.DrawerFragmentActivityDemo;
import com.humaclab.aponjonshogorbha.fragments.kikhabo.NomunaKhaddoFragment;

public class NomunaKhaddoActivityDemo extends DrawerFragmentActivityDemo {

	@Override
	protected Fragment createFragment() {
		return new NomunaKhaddoFragment();
	}

}
