package com.humaclab.aponjonshogorbha.activities.kikhabo;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.humaclab.aponjonshogorbha.activities.DrawerFragmentActivity;
import com.humaclab.aponjonshogorbha.fragments.kikhabo.AmarKhaddoTalikaDetailsFragment;

public class AmarKhaddoTalikaDetailsActivity extends DrawerFragmentActivity {

	@Override
	protected Fragment createFragment() {
		Bundle bundle = getIntent().getExtras();
		long id = bundle.getLong(AmarKhaddoTalikaDetailsFragment.MEAL_ID);
		String mealName = bundle.getString(AmarKhaddoTalikaDetailsFragment.MEAL_NAME);
		return AmarKhaddoTalikaDetailsFragment.getInstance(id, mealName);
	}
}
