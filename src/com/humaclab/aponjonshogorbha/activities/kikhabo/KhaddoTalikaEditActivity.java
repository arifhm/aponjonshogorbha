package com.humaclab.aponjonshogorbha.activities.kikhabo;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.humaclab.aponjonshogorbha.activities.DrawerFragmentActivity;
import com.humaclab.aponjonshogorbha.fragments.kikhabo.AmarKhaddoTalikaDetailsFragment;
import com.humaclab.aponjonshogorbha.fragments.kikhabo.KhaddoTalikaEditFragment;

public class KhaddoTalikaEditActivity extends DrawerFragmentActivity {

	@Override
	protected Fragment createFragment() {
		Bundle bundle = getIntent().getExtras();
		long id = bundle.getLong(AmarKhaddoTalikaDetailsFragment.MEAL_ID);
		return KhaddoTalikaEditFragment.getInstance(id);
	}

}
