package com.humaclab.aponjonshogorbha.activities.kikhabo;

import android.support.v4.app.Fragment;

import com.humaclab.aponjonshogorbha.activities.DrawerFragmentActivity;
import com.humaclab.aponjonshogorbha.fragments.kikhabo.NomunaKhaddoFragment;

public class NomunaKhaddoActivity extends DrawerFragmentActivity {

	@Override
	protected Fragment createFragment() {
		return new NomunaKhaddoFragment();
	}

}
