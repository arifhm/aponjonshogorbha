package com.humaclab.aponjonshogorbha.activities.kikhabo;

import android.support.v4.app.Fragment;

import com.humaclab.aponjonshogorbha.activities.DrawerFragmentActivity;
import com.humaclab.aponjonshogorbha.fragments.kikhabo.HWAInputFrgment;

public class HWAInputActivity extends DrawerFragmentActivity {

	@Override
	protected Fragment createFragment() {
		return new HWAInputFrgment();
	}

}
