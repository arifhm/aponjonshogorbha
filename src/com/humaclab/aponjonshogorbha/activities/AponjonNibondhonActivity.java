package com.humaclab.aponjonshogorbha.activities;

import com.humaclab.aponjonshogorbha.fragments.AponjonNibondhonFragment;

import android.support.v4.app.Fragment;

public class AponjonNibondhonActivity extends DrawerFragmentActivity {

	@Override
	protected Fragment createFragment() {
		return new AponjonNibondhonFragment();
	}
	
}
