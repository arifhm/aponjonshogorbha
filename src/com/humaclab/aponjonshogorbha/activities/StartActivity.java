package com.humaclab.aponjonshogorbha.activities;

import com.bd.aponjon.pregnancy.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class StartActivity extends Activity {
	ImageView mImgViewNewAcnt;
	ImageView mImgViewLogin;
	ImageView mImgViewDemo;
	public static StartActivity mInstance;
	public static StartActivity getInstance(){
		return mInstance;
	}
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_start);
		mInstance = this;
		mImgViewNewAcnt = (ImageView) findViewById(R.id.btn_new_acnt);
		mImgViewLogin = (ImageView) findViewById(R.id.btn_login);
		mImgViewDemo = (ImageView) findViewById(R.id.btn_demo);
		
		mImgViewNewAcnt.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				startActivity(new Intent(StartActivity.this, RegisterActivity.class));
				finish();
			}
		});
		
		mImgViewLogin.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				startActivity(new Intent(StartActivity.this, LoginActivity.class));
				finish();
			}
		});
		
		mImgViewDemo.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				startActivity(new Intent(StartActivity.this, DashboardActivityDemo.class));
			}
		});
	}
}