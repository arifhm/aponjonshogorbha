package com.humaclab.aponjonshogorbha.activities;

import com.humaclab.aponjonshogorbha.fragments.EditProfileFragment;

import android.support.v4.app.Fragment;

public class EditProfileActivity extends DrawerFragmentActivity {

	@Override
	protected Fragment createFragment() {
		return new EditProfileFragment();
	}

}
