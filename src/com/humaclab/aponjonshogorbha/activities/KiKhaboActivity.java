package com.humaclab.aponjonshogorbha.activities;

import com.humaclab.aponjonshogorbha.fragments.KiKhaboFragment;

import android.support.v4.app.Fragment;

public class KiKhaboActivity extends DrawerFragmentActivity {

	@Override
	protected Fragment createFragment() {
		return new KiKhaboFragment();
	}

}
