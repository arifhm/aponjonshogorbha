package com.humaclab.aponjonshogorbha.activities;

import com.humaclab.aponjonshogorbha.fragments.AmarJogotFragment;

import android.support.v4.app.Fragment;

public class AmarJogotActivity extends DrawerFragmentActivity {

	@Override
	protected Fragment createFragment() {
		return new AmarJogotFragment();
	}

}
