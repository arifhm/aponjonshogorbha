package com.humaclab.aponjonshogorbha.activities;

import com.humaclab.aponjonshogorbha.fragments.DashboardFragment;

import android.support.v4.app.Fragment;

public class DashboardActivityDemo extends DrawerFragmentActivityDemo {

	@Override
	protected Fragment createFragment() {
		return new DashboardFragment();
	}

}
