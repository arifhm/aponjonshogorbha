package com.humaclab.aponjonshogorbha.fragments.dialogs;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import com.bd.aponjon.pregnancy.R;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;
import android.widget.TimePicker;

public class TimePickerFragment extends DialogFragment {
	public static final String EXTRA_TIME = "time";

	Date mDate;

	public static TimePickerFragment newInstance(Date date) {
		Bundle args = new Bundle();
		args.putSerializable(EXTRA_TIME, date);

		TimePickerFragment fragment = new TimePickerFragment();
		fragment.setArguments(args);

		return fragment;
	}

	private void sendResult(int resultCode) {
		if (getTargetFragment() == null)
			return;

		Intent i = new Intent();
		i.putExtra(EXTRA_TIME, mDate);

		getTargetFragment().onActivityResult(getTargetRequestCode(),
				resultCode, i);
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		mDate = (Date) getArguments().getSerializable(EXTRA_TIME);

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(mDate);
		final int year = calendar.get(Calendar.YEAR);
		final int month = calendar.get(Calendar.MONTH);
		final int day = calendar.get(Calendar.DAY_OF_MONTH);

		final int hour = calendar.get(Calendar.HOUR);
		final int minute = calendar.get(Calendar.MINUTE);

		

		TimePickerDialog timePickerDialog = new TimePickerDialog(
				getActivity(), android.R.style.Theme_Holo_Light_Dialog,
				new TimePickerDialog.OnTimeSetListener() {

					@Override
					public void onTimeSet(TimePicker view, int hourOfDay,
							int minute) {
						mDate = new GregorianCalendar(year, month, day, hourOfDay, minute).getTime();
						
						sendResult(Activity.RESULT_OK);
					}
				}, hour, minute, false);
		timePickerDialog.setTitle("Set Time");

		/*DatePickerDialog dialog = new DatePickerDialog(getActivity(),
				android.R.style.Theme_Holo_Light_Dialog,
				new DatePickerDialog.OnDateSetListener() {

					@Override
					public void onDateSet(DatePicker view, int year, int month,
							int day) {
						mDate = new GregorianCalendar(year, month, day)
								.getTime();
						sendResult(Activity.RESULT_OK);
					}
				}, year, month, day);

		dialog.setTitle(R.string.str_last_menstrual_date);*/

		return timePickerDialog;
	}
}
