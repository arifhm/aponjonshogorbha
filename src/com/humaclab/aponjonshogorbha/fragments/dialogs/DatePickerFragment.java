package com.humaclab.aponjonshogorbha.fragments.dialogs;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import com.bd.aponjon.pregnancy.R;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;

public class DatePickerFragment extends DialogFragment {
	public static final String EXTRA_DATE = "last_menstrual.DATE";
	public static final String Title = "title";

	Date mDate;

	public static DatePickerFragment newInstance(Date date) {
		Bundle args = new Bundle();
		args.putSerializable(EXTRA_DATE, date);

		DatePickerFragment fragment = new DatePickerFragment();
		fragment.setArguments(args);

		return fragment;
	}
	
	public static DatePickerFragment newInstance(Date date, String dialogTitle ) {
		Bundle args = new Bundle();
		args.putSerializable(EXTRA_DATE, date);
		args.putSerializable(Title, dialogTitle);

		DatePickerFragment fragment = new DatePickerFragment();
		fragment.setArguments(args);

		return fragment;
	}

	private void sendResult(int resultCode) {
		if (getTargetFragment() == null) 
			return;

		Intent i = new Intent();
		i.putExtra(EXTRA_DATE, mDate);

		getTargetFragment()
		.onActivityResult(getTargetRequestCode(), resultCode, i);
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		mDate = (Date)getArguments().getSerializable(EXTRA_DATE);
		String dialogTitle = (String) getArguments().getSerializable(Title);

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(mDate);
		int year = calendar.get(Calendar.YEAR);
		int month = calendar.get(Calendar.MONTH);
		int day = calendar.get(Calendar.DAY_OF_MONTH);

		//        View v = getActivity().getLayoutInflater()
				//            .inflate(R.layout.dialog_date, null);
		//
		//        DatePicker datePicker = (DatePicker)v.findViewById(R.id.dialog_date_datePicker);
		//        datePicker.init(year, month, day, new OnDateChangedListener() {
		//            public void onDateChanged(DatePicker view, int year, int month, int day) {
		//                mDate = new GregorianCalendar(year, month, day).getTime();
		//
		//                // update argument to preserve selected value on rotation
		//                getArguments().putSerializable(EXTRA_DATE, mDate);
		//            }
		//        });

//		return new AlertDialog.Builder(getActivity())
//		.setView(v)
//		.setTitle(R.string.str_last_menstrual_date)
//		.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
//			public void onClick(DialogInterface dialog, int which) {
//				sendResult(Activity.RESULT_OK);
//			}
//		})
//		.create();
		
		DatePickerDialog dialog = new DatePickerDialog(getActivity(), android.R.style.Theme_Holo_Light_Dialog, new DatePickerDialog.OnDateSetListener() {
			
			@Override
			public void onDateSet(DatePicker view, int year, int month,
					int day) {
				mDate = new GregorianCalendar(year, month, day).getTime();
				sendResult(Activity.RESULT_OK);
			}
		}, year, month, day);
		
		if(dialogTitle == null)
		dialog.setTitle(R.string.str_last_menstrual_date);
		else
			dialog.setTitle(dialogTitle);	
		
		return dialog;
	}
}
