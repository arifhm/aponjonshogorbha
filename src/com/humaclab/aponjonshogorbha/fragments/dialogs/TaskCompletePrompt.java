package com.humaclab.aponjonshogorbha.fragments.dialogs;

import com.bd.aponjon.pregnancy.R;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class TaskCompletePrompt extends DialogFragment {
	
	private DialogInterface.OnClickListener onItemClickListener;
	public static final String TAG = TaskCompletePrompt.class.getSimpleName();
	public static final String COMPLETE_INCOMPLETE_FLAG = "Flag";
		
//	private String editTextString;
	private TextView displayText;
	private Button buttonYes;
	private Button buttonNo;
	private TextView viewCancel;
	
	public static TaskCompletePrompt getInstance(String arg) {  
		TaskCompletePrompt f = new TaskCompletePrompt();
		Bundle bundle = new Bundle();
		bundle.putSerializable(COMPLETE_INCOMPLETE_FLAG, arg);
		f.setArguments(bundle);

		return f;  
	}
	
	public void setOnClickListener(DialogInterface.OnClickListener onItemClickListener) {  
		this.onItemClickListener = onItemClickListener;  
	}
	
//	public onClickEditTextResponseCallBack mOnClickSaveInterface;
//	public interface onClickEditTextResponseCallBack{
//		void onSaveClicked(String txt);
//	}
//	public void setOnClickEditTextResponseCallBack(onClickEditTextResponseCallBack  onSaveCallBack){
//		mOnClickSaveInterface = onSaveCallBack;
//	} 
	
	@Override
	@NonNull
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		
		final Dialog dialog = new Dialog(getActivity());  
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); 
		dialog.requestWindowFeature(Window.PROGRESS_VISIBILITY_OFF);
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

		LayoutInflater inflater = getActivity().getLayoutInflater();
		View v = inflater.inflate(R.layout.layout_fragment_dialog_complete_task, null);

		displayText = (TextView) v.findViewById(R.id.textView1);
		buttonYes = (Button) v.findViewById(R.id.buttonYes);
		buttonNo = (Button) v.findViewById(R.id.buttonNo);
		viewCancel = (TextView) v.findViewById(R.id.view_cancel_dialog);
		
		String flag = (String) getArguments().getSerializable(COMPLETE_INCOMPLETE_FLAG);
		if(flag.equals("Complete")) {
			displayText.setText("আপনি কি কাজটি সম্পন্ন করেছেন?");
		} else {
			displayText.setText("আপনি কি কাজটি অসম্পন্ন বিবেচনা করছেন?");
		}
		
		if(onItemClickListener != null) {
			buttonYes.setOnClickListener(new View.OnClickListener() {				
				@Override
				public void onClick(View v) {
//					if(mOnClickSaveInterface != null) {
//						mOnClickSaveInterface.onSaveClicked(editTextString);
//					}
					onItemClickListener.onClick(dialog, 1);
				}
			});
			
			buttonNo.setOnClickListener(new View.OnClickListener() {				
				@Override
				public void onClick(View v) {
					onItemClickListener.onClick(dialog, 2);
				}
			});
			
			viewCancel.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					onItemClickListener.onClick(dialog, 3);
				}
			});
		}
		
		dialog.setContentView(v);
		
		return dialog;
	}
}
