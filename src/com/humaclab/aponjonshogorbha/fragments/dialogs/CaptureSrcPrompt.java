package com.humaclab.aponjonshogorbha.fragments.dialogs;

import com.bd.aponjon.pregnancy.R;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

public class CaptureSrcPrompt extends DialogFragment {

	private DialogInterface.OnClickListener onItemClickListener;
	public static final String D_TAG = CaptureSrcPrompt.class.getSimpleName();
	
	private ViewGroup cameraLayout;
	private ViewGroup galleryLayout;
	private TextView cancel;

	public static CaptureSrcPrompt getInstance() {  
		CaptureSrcPrompt f = new CaptureSrcPrompt();  
		return f;  
	}

	public void setOnClickListener(DialogInterface.OnClickListener onItemClickListener) {  
		this.onItemClickListener = onItemClickListener;  
	}

	@Override
	@NonNull
	public Dialog onCreateDialog(Bundle savedInstanceState) {

		final Dialog dialog = new Dialog(getActivity());  
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); 
		dialog.requestWindowFeature(Window.PROGRESS_VISIBILITY_OFF);
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

		LayoutInflater inflater = getActivity().getLayoutInflater();
		View v = inflater.inflate(R.layout.layout_fragment_dialog_capture_source_prompt, null);

		cameraLayout = (ViewGroup) v.findViewById(R.id.btn_view_camera);		
		galleryLayout =  (ViewGroup) v.findViewById(R.id.btn_view_gallery);
		cancel = (TextView) v.findViewById(R.id.btn_view_dismiss);

		if (onItemClickListener != null) {  
			cameraLayout.setOnClickListener(new View.OnClickListener() {  
				@Override  
				public void onClick(View v) {  
					onItemClickListener.onClick(dialog, 0);  
				}  
			});
			galleryLayout.setOnClickListener(new View.OnClickListener() {  
				@Override  
				public void onClick(View v) {  
					onItemClickListener.onClick(dialog, 1);  
				}  
			});
			cancel.setOnClickListener(new View.OnClickListener() {  
				@Override  
				public void onClick(View v) {  
					onItemClickListener.onClick(dialog, 2);  
				}  
			});
		}  
		
		dialog.setContentView(v);

		return dialog;
	}


}
