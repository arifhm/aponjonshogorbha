package com.humaclab.aponjonshogorbha.fragments.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;

import com.bd.aponjon.pregnancy.R;

public class PromptLmpDateDialog extends Dialog{

	public PromptLmpDateDialog(Context context) {
		super(context);
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.layout_dialog_lmp_date_alert);
		getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
		View v = findViewById(R.id.btn_view_dismiss);
		v.setOnClickListener(dialogDismissListener);
		
	}

	public android.view.View.OnClickListener dialogDismissListener = new View.OnClickListener() {

		@Override
		public void onClick(View v) {
			dismiss();
		}
	};
}