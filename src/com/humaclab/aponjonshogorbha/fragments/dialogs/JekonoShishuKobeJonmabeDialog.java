package com.humaclab.aponjonshogorbha.fragments.dialogs;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import bengali.language.support.BengaliUnicodeString;

import com.bd.aponjon.pregnancy.R;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

public class JekonoShishuKobeJonmabeDialog extends DialogFragment {

	private DialogInterface.OnClickListener onItemClickListener;
	public static final String D_TAG = JekonoShishuKobeJonmabeDialog.class.getSimpleName();
	
	private TextView cancel;
	private TextView date;
	String dateString;

	public static JekonoShishuKobeJonmabeDialog getInstance(int d, int m, int y) {  
		Bundle bundle = new Bundle();
		
		bundle.putSerializable("date", d);
		bundle.putSerializable("month", m);
		bundle.putSerializable("year", y);
		
		JekonoShishuKobeJonmabeDialog f = new JekonoShishuKobeJonmabeDialog();
		f.setArguments(bundle);
		
		return f;  
	}

	public void setOnClickListener(DialogInterface.OnClickListener onItemClickListener) {  
		this.onItemClickListener = onItemClickListener;  
	}
	
	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		int day = (Integer) getArguments().getSerializable("date");
		int month = (Integer) getArguments().getSerializable("month");
		int year = (Integer) getArguments().getSerializable("year");
		
		Date date = getDate(year, month, day);
		
//		SimpleDateFormat format = new SimpleDateFormat();
//		format.applyPattern("dd/MM/yyyy");
		
		DateFormat format = DateFormat.getDateInstance(DateFormat.LONG, Locale.UK);
		
		dateString = format.format(date);
	}

	@Override
	@NonNull
	public Dialog onCreateDialog(Bundle savedInstanceState) {

		final Dialog dialog = new Dialog(getActivity());  
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); 
		dialog.requestWindowFeature(Window.PROGRESS_VISIBILITY_OFF);
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

		LayoutInflater inflater = getActivity().getLayoutInflater();
		View v = inflater.inflate(R.layout.layout_fragment_dialog_show_date_of_birth, null);
	
		cancel = (TextView) v.findViewById(R.id.btn_view_dismiss);
		date = (TextView) v.findViewById(R.id.view_birtday_display);
		
		date.setText("শিশু জন্মাবার তারিখ\n" + BengaliUnicodeString.convertToBengaliText(getActivity(), getMonthString(dateString)));
//		date.setText(dateString);

		if (onItemClickListener != null) {  
			cancel.setOnClickListener(new View.OnClickListener() {  
				@Override  
				public void onClick(View v) {  
					onItemClickListener.onClick(dialog, 0);  
				}  
			});
		}  
		
		dialog.setContentView(v);

		return dialog;
	}
	
	public static Date getDate(int year, int month, int day) {
        Calendar cal = Calendar.getInstance();
        
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, month);
        cal.set(Calendar.DAY_OF_MONTH, day);
        cal.setTime(cal.getTime());
        cal.add(Calendar.DATE, 280);     
        
        return cal.getTime();
    }
	
	public String getMonthString(String nonFormatteddate) {
		String formattedString = null;
		String replacableChar = null;
		StringBuilder buffer = new StringBuilder();
		Pattern pattern = Pattern.compile("\\D");
		
		Matcher matcher = pattern.matcher(nonFormatteddate);
		while (matcher.find()) {
			buffer.append(matcher.group());
		}
		replacableChar = buffer.toString().replace(" ", "");
		String replaceChar = getReplaceChar(replacableChar);
		formattedString = nonFormatteddate.replace(replacableChar, replaceChar);
		
		return formattedString;
	}
	
	public String getReplaceChar(String s) {
		List<String> monthsBn = new ArrayList<String>();
		monthsBn.add("জানুয়ারী");
		monthsBn.add("ফেব্রুয়ারী");
		monthsBn.add("মার্চ");
		monthsBn.add("এপ্রিল");
		monthsBn.add("মে");
		monthsBn.add("জুন");
		monthsBn.add("জুলাই");
		monthsBn.add("অগাস্ট");
		monthsBn.add("সেপ্টেম্বর");
		monthsBn.add("অক্টোবর");
		monthsBn.add("নভেম্বর");
		monthsBn.add("ডিসেম্বর");
		
		List<String> monthsEn = new ArrayList<String>();
		
		monthsEn.add("January");
		monthsEn.add("February");
		monthsEn.add("March");
		monthsEn.add("April");
		monthsEn.add("May");
		monthsEn.add("June");
		monthsEn.add("July");
		monthsEn.add("August");
		monthsEn.add("September");
		monthsEn.add("October");
		monthsEn.add("November");
		monthsEn.add("December");
		
		for(int i = 0; i<monthsBn.size(); i++) {
			if(monthsEn.get(i).equalsIgnoreCase(s)) {
				return  monthsBn.get(i);
			}
		}
		return null;
	}
}
