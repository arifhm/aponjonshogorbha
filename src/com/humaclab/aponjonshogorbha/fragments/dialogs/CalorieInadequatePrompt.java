package com.humaclab.aponjonshogorbha.fragments.dialogs;

import com.bd.aponjon.pregnancy.R;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

public class CalorieInadequatePrompt extends DialogFragment {
	
	private DialogInterface.OnClickListener onItemClickListener;
	public static final String CALORIE_INADEQUATE_TAG = CalorieInadequatePrompt.class.getSimpleName();
	
	private TextView cancel;
	
	public static CalorieInadequatePrompt getInstance() {  
		CalorieInadequatePrompt f = new CalorieInadequatePrompt();  
		return f;  
	}
	
	public void setOnClickListener(DialogInterface.OnClickListener onItemClickListener) {  
		this.onItemClickListener = onItemClickListener;  
	}
	
	@Override
	@NonNull
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		
		final Dialog dialog = new Dialog(getActivity());  
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); 
		dialog.requestWindowFeature(Window.PROGRESS_VISIBILITY_OFF);
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

		LayoutInflater inflater = getActivity().getLayoutInflater();
		View v = inflater.inflate(R.layout.layout_fragment_dialog_inadequate_food_prompt, null);
		
		cancel = (TextView) v.findViewById(R.id.btn_view_inadequate_dialog_dismiss);
		
		if(onItemClickListener != null) {
			cancel.setOnClickListener(new View.OnClickListener() {				
				@Override
				public void onClick(View v) {
					onItemClickListener.onClick(dialog, 0);
				}
			});
		}
		
		dialog.setContentView(v);
		
		return dialog;
	}
}
