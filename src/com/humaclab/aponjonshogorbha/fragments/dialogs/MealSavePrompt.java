package com.humaclab.aponjonshogorbha.fragments.dialogs;

import com.bd.aponjon.pregnancy.R;
import com.humaclab.aponjonshogorbha.fragments.kikhabo.AmarKhaddoTalikaDetailsFragment;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import androidbangladesh.BengaliAutoCompleteText;
import androidbangladesh.BengaliEditText;

public class MealSavePrompt extends DialogFragment {
	
	private DialogInterface.OnClickListener onItemClickListener;
	public static final String MEAL_SAVE_TAG = MealSavePrompt.class.getSimpleName();
	
	
	private TextView cancel;
	private TextView save;
	private BengaliEditText edit;
	private String editTextString;
	
	public static MealSavePrompt getInstance(String name) {  
		MealSavePrompt f = new MealSavePrompt();  
		Bundle bundle = new Bundle();
		bundle.putSerializable(AmarKhaddoTalikaDetailsFragment.MEAL_NAME, name);
		f.setArguments(bundle);
		
		return f;  
	}
	
	public void setOnClickListener(DialogInterface.OnClickListener onItemClickListener) {  
		this.onItemClickListener = onItemClickListener;  
	}
	
	public onClickEditTextResponseCallBack mOnClickSaveInterface;
	public interface onClickEditTextResponseCallBack{
		void onSaveClicked(String txt);
	}
	public void setOnClickEditTextResponseCallBack(onClickEditTextResponseCallBack  onSaveCallBack){
		mOnClickSaveInterface = onSaveCallBack;
	} 
	
	@Override
	@NonNull
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		
		final Dialog dialog = new Dialog(getActivity());  
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); 
		dialog.requestWindowFeature(Window.PROGRESS_VISIBILITY_OFF);
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

		LayoutInflater inflater = getActivity().getLayoutInflater();
		View v = inflater.inflate(R.layout.layout_fragment_dialog_save_meal, null);

		String mealName = (String) getArguments().getSerializable(AmarKhaddoTalikaDetailsFragment.MEAL_NAME);
		
		cancel = (TextView) v.findViewById(R.id.view_btn_dialog_save_meal_dismiss);
		save =  (TextView) v.findViewById(R.id.view_btn_save);
		edit = (BengaliEditText) v.findViewById(R.id.editview_input_meal_name);
		edit.setText(mealName);
		
//		edit.addTextChangedListener(new TextWatcher() {
//			@Override
//			public void onTextChanged(CharSequence s, int start, int before, int count) {
//				// TODO Auto-generated method stub
//			}
//			@Override
//			public void beforeTextChanged(CharSequence s, int start, int count,
//					int after) {				
//			}
//			
//			@Override
//			public void afterTextChanged(Editable s) {
//				editTextString = s.toString();
//			}
//		});
		
				
		if(onItemClickListener != null) {
			save.setOnClickListener(new View.OnClickListener() {				
				@Override
				public void onClick(View v) {
					if(mOnClickSaveInterface != null && edit.getText().toString() != null) {
						mOnClickSaveInterface.onSaveClicked(edit.getText().toString());
						onItemClickListener.onClick(dialog, 1);
					} else Toast.makeText(getActivity(), "sldfldfd", Toast.LENGTH_SHORT).show();								
				}
			});
			
			cancel.setOnClickListener(new View.OnClickListener() {				
				@Override
				public void onClick(View v) {
					onItemClickListener.onClick(dialog, 2);
				}
			});
		}
		
		dialog.setContentView(v);
		
		return dialog;
	}
}
