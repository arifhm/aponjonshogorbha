package com.humaclab.aponjonshogorbha.fragments;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences.Editor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import androidbangladesh.BengaliAutoCompleteText;

import com.humaclab.aponjon.Article;
import com.humaclab.aponjon.ArticleDao;
import com.humaclab.aponjon.DaoMaster;
import com.humaclab.aponjon.DaoMaster.DevOpenHelper;
import com.humaclab.aponjon.DaoSession;
import com.humaclab.aponjon.User;
import com.humaclab.aponjon.UserDao;
import com.humaclab.aponjon.services.BackgroundService;
import com.humaclab.aponjonshogorbha.AppConfig;
import com.humaclab.aponjonshogorbha.AponjonApplication;
import com.humaclab.aponjonshogorbha.Constants;
import com.humaclab.aponjonshogorbha.ImageUploadTask;
import com.bd.aponjon.pregnancy.R;
import com.humaclab.aponjonshogorbha.SessionManager;
import com.humaclab.aponjonshogorbha.activities.DashboardActivity;
import com.humaclab.aponjonshogorbha.activities.StartActivity;

import com.humaclab.aponjonshogorbha.fragments.dialogs.CaptureSrcPrompt;
import com.humaclab.aponjonshogorbha.fragments.dialogs.DatePickerFragment;
import com.humaclab.aponjonshogorbha.fragments.dialogs.PromptLmpDateDialog;
import com.humaclab.aponjonshogorbha.userdata.download.CheckRegistered;
import com.humaclab.aponjonshogorbha.userdata.download.DownloadUserAllDataTask;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import de.hdodenhof.circleimageview.CircleImageView;

public class RegisterFragment extends Fragment {
	private int REQUEST_TAKE_PHOTO = 1;
	private int PICK_IMAGE_REQUEST = 2;
	String mCurrentPhotoPath;
	SessionManager mSession;
	Editor editor;
	AponjonApplication mApp;

	String mToken;
	String mLmp;

	BengaliAutoCompleteText mEditTextName;
	EditText mEditTextEmail;
	EditText mEditTextPassword1;
	EditText mEditTextPassword2;
	EditText mEditTextPhone;

	TextView mDateButton;
	Button mRegisterButton;	
	ImageView mImgViewBack;
	TextView mTextTitle;
	CircleImageView mProfileImg;

	String mName;
	String mEmail;
	String mPassword1;
	String mPassword2;
	String mPhone;
	Date mMenstrualDate;
	Bitmap mBitmap;

	boolean validLmp = false;

	private static final String DIALOG_TAG = "date";
	private static final int REQUEST_DATE = 0;

	public static RegisterFragment newInstance() {
		RegisterFragment fragment = new RegisterFragment();
		return fragment;
	}

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	}

	public void updateDate(Date date) {
		mMenstrualDate = date;
		SimpleDateFormat format = new SimpleDateFormat();
		format.applyPattern("yyyy-MM-dd");
		mDateButton.setText(format.format(date));
	}

	@Override
	@Nullable
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.layout_fragment_register, container, false);

		Constants.is_demo = false;
		mApp = AponjonApplication.getInstance();
		mSession = new SessionManager(getActivity());
		editor= mSession.getEditor();
		pd = new ProgressDialog(getActivity());

		mEditTextName = (BengaliAutoCompleteText) v.findViewById(R.id.reg_edit_text_name);
		//		mEditTextName.addTextChangedListener(new TextWatcher() {
		//
		//			@Override
		//			public void onTextChanged(CharSequence s, int start, int before, int count) {				
		//			}			
		//			@Override
		//			public void beforeTextChanged(CharSequence s, int start, int count,
		//					int after) {				
		//			}			
		//			@Override
		//			public void afterTextChanged(Editable s) {
		//				mName = s.toString();
		//			}
		//		});

		mEditTextEmail = (EditText) v.findViewById(R.id.reg_edit_text_email);
		mEditTextEmail.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {				
			}			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {				
			}			
			@Override
			public void afterTextChanged(Editable s) {
				mEmail = s.toString();
			}
		});

		mEditTextPhone = (EditText) v.findViewById(R.id.reg_edit_text_phone);
		mEditTextPhone.addTextChangedListener(new TextWatcher() {		
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {				
			}			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {				
			}
			@Override
			public void afterTextChanged(Editable s) {
				mPhone = s.toString();
			}
		});

		mEditTextPassword1 = (EditText) v.findViewById(R.id.reg_edit_text_password1);
		mEditTextPassword1.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {				
			}			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {				
			}			
			@Override
			public void afterTextChanged(Editable s) {
				mPassword1 = s.toString();
			}
		});

		mEditTextPassword2 = (EditText) v.findViewById(R.id.reg_edit_text_password2);
		mEditTextPassword2.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {				
			}			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {				
			}			
			@Override
			public void afterTextChanged(Editable s) {
				mPassword2 = s.toString();
			}
		});

		mDateButton = (TextView) v.findViewById(R.id.reg_text_view_date);
		//        updateDate();
		mDateButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {                	            	
				FragmentManager fm = getActivity()
						.getSupportFragmentManager();
				DatePickerFragment dialog = DatePickerFragment
						.newInstance(new Date(System.currentTimeMillis()));
				dialog.setTargetFragment(RegisterFragment.this, REQUEST_DATE);
				dialog.show(fm, DIALOG_TAG);              	
			}
		});


		mRegisterButton = (Button) v.findViewById(R.id.reg_btn_submit);
		mRegisterButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if(mPassword1 != null && mPassword2 != null) {
					if(!mPassword1.equals(mPassword2)) {
						Toast.makeText(getActivity(), "আপনার দেয়া পাসওয়ার্ড ২ টি এক নয়", Toast.LENGTH_SHORT).show();
						return;
					}
				}
				if(mEmail!=null) {
					if(!isEmailValid(mEmail)) {
						Toast.makeText(getActivity(), "একটি সঠিক ই-মেইল প্রদান করুন", Toast.LENGTH_SHORT).show();
						return;
					}
				}

				if(mEditTextName.getText().toString()!=null && mEditTextName.getText().toString().length()<1) {
					Toast.makeText(getActivity(), "সবগুলো ঘরে তথ্য প্রদান করুন", Toast.LENGTH_SHORT).show();
					return;
				}

				if(!validLmp) {
					PromptLmpDateDialog dialog = new PromptLmpDateDialog(getActivity());
					dialog.show();
				}

				if(mEditTextName.getText().toString()!=null && mEmail!=null && mPassword1!=null && mPhone!=null && mMenstrualDate != null && validLmp==true) {
					ExecutorService executor = Executors.newFixedThreadPool(1);
					Runnable worker = new RegisterTask(AppConfig.URL_REGISTER);
					executor.execute(worker);
				} else {
					Toast.makeText(getActivity(), "সবগুলো ঘরে তথ্য প্রদান করুন", Toast.LENGTH_SHORT).show();
					return;
				}
			}
		});

		mImgViewBack = (ImageView) v.findViewById(R.id.back_btn);
		mImgViewBack.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(new Intent(getActivity(), StartActivity.class));
				getActivity().finish();
			}
		});

		mTextTitle = (TextView) v.findViewById(R.id.title_text);
		mTextTitle.setText("নতুন প্রোফাইল");
		mProfileImg = (CircleImageView) v.findViewById(R.id.view_btn_capture_profile_pic);
		mProfileImg.setOnClickListener(new View.OnClickListener() {			
			@Override
			public void onClick(View v) {
				initPhotoCapture();
			}
		});

		return v;
	}

	public void initPhotoCapture() {
		CaptureSrcPrompt srcPrompt = CaptureSrcPrompt.getInstance();

		srcPrompt.setOnClickListener(new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int resultCode) {
				if(resultCode == 0) {
					dispatchTakePictureIntent();									
				} else if (resultCode == 1 ) {
					//					Toast.makeText(getActivity(), "Gallery", Toast.LENGTH_SHORT).show();
					Intent intent = new Intent();
					// Show only images, no videos or anything else				
					intent.setType("image/*");
					intent.setAction(Intent.ACTION_GET_CONTENT);
					// Always show the chooser (if there are multiple options available)
					startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);

				} else if (resultCode == 2 ) {
					dialog.dismiss();
				}
				dialog.dismiss();
			}
		});

		Fragment fragmentByTag = getActivity().getSupportFragmentManager().findFragmentByTag(CaptureSrcPrompt.D_TAG);//avoid opening twice dialog  
		if (fragmentByTag == null)  
			srcPrompt.show(getActivity().getSupportFragmentManager(), CaptureSrcPrompt.D_TAG);
	}

	private void dispatchTakePictureIntent() {
		Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		takePictureIntent.putExtra("android.intent.extras.CAMERA_FACING", 1);
		// Ensure that there's a camera activity to handle the intent
		if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
			// Create the File where the photo should go
			File photoFile = null;
			try {
				photoFile = createImageFile();
			} catch (IOException ex) {
				// Error occurred while creating the File

			}
			// Continue only if the File was successfully created
			if (photoFile != null) {
				takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
						Uri.fromFile(photoFile));
				startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
			}
		}
	}
	private File createImageFile() throws IOException {
		// Create an image file name
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
		String imageFileName = "JPEG_" + timeStamp + "_";
		String storageDir = Environment.getExternalStorageDirectory() + "/picUpload";
		File dir = new File(storageDir);
		if (!dir.exists())
			dir.mkdir();

		File image = new File(storageDir + "/" + imageFileName + ".jpg");

		// Save a file: path for use with ACTION_VIEW intents
		mCurrentPhotoPath = image.getAbsolutePath();
		editor.putString(Constants.UserData.USER_IMG_SD_PATH, mCurrentPhotoPath);
		editor.commit();
		//	    Log.i("EditProfileFragment", "photo path = " + mCurrentPhotoPath);
		return image;
	}

	private void setPic() {
		// Get the dimensions of the View
		int targetW = mProfileImg.getWidth();
		int targetH = mProfileImg.getHeight();

		// Get the dimensions of the bitmap
		BitmapFactory.Options bmOptions = new BitmapFactory.Options();
		bmOptions.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
		int photoW = bmOptions.outWidth;
		int photoH = bmOptions.outHeight;

		// Determine how much to scale down the image
		int scaleFactor = Math.min(photoW/targetW, photoH/targetH);

		// Decode the image file into a Bitmap sized to fill the View
		bmOptions.inJustDecodeBounds = false;
		bmOptions.inSampleSize = scaleFactor << 1;
		bmOptions.inPurgeable = true;

		Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);

		ExifInterface exif = null;
		try {
			exif = new ExifInterface(mCurrentPhotoPath);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  
		String orientString = exif.getAttribute(ExifInterface.TAG_ORIENTATION);
		int orientation = orientString != null ? Integer.parseInt(orientString) :  ExifInterface.ORIENTATION_NORMAL;

		int rotationAngle = 0;
		if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
			rotationAngle = 90;
		}
		if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
			rotationAngle = 180;
		}
		if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
			rotationAngle = 270;
		}
		Matrix mtx = new Matrix();
		mtx.postRotate(rotationAngle);

		// Rotating Bitmap
		Bitmap rotatedBMP = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), mtx, true);

		if (rotatedBMP != bitmap)
			bitmap.recycle();

		mProfileImg.setImageBitmap(rotatedBMP);
		mBitmap = rotatedBMP;

	}
	public boolean isEmailValid(String email)
	{
		String regExpn =
				"^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
						+"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
						+"[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
						+"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
						+"[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
						+"([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";

		CharSequence inputStr = email;

		Pattern pattern = Pattern.compile(regExpn,Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(inputStr);

		if(matcher.matches())
			return true;
		else
			return false;
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode != Activity.RESULT_OK) return;
		if (requestCode == PICK_IMAGE_REQUEST && resultCode == Activity.RESULT_OK && data != null && data.getData() != null) {

			Uri uri = data.getData();

			try {
				Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);

				File file = createImageFile();
				FileOutputStream fos = null;
				try {
					fos = new FileOutputStream(file);
					bitmap.compress(Bitmap.CompressFormat.PNG,70, fos);

					fos.flush();
					fos.close();
					//   MediaStore.Images.Media.insertImage(getContentResolver(), b, "Screen", "screen");
				}catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (Exception e) {
					e.printStackTrace();
				}
				setPic();
				//				mProfileImg.setImageBitmap(bitmap);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} 
		if (requestCode == REQUEST_TAKE_PHOTO && resultCode == Activity.RESULT_OK) {
			setPic();
		}
		if (requestCode == REQUEST_DATE) {
			Date lmpDate = (Date)data.getSerializableExtra(DatePickerFragment.EXTRA_DATE);
			updateDate(lmpDate);
			Date curDate = new Date(System.currentTimeMillis());

			int dayPassed = (int)((curDate.getTime() - lmpDate.getTime()) / (1000*60*60*24l));
			if(dayPassed<35) {
				PromptLmpDateDialog dialog = new PromptLmpDateDialog(getActivity());
				dialog.show();
				validLmp = false;
			} else validLmp = true;
		}
	}

	ProgressDialog pd;
	Handler handler = new Handler();
	public class RegisterTask implements Runnable {
		String mUrlString;

		public RegisterTask(String url) {
			this.mUrlString = url;
		}

		@Override
		public void run() {
			handler.post(new Runnable() {
				@Override
				public void run() {
					pd.show();
				}
			});
			register(mUrlString);
		}
	}

	public void register(String url) {

		AsyncHttpClient client = new AsyncHttpClient();
		// Http Request Params Object
		RequestParams params = new RequestParams();
		// params
		JSONObject reqObj = new JSONObject();
		try {
			/**
			 * {
			 * 		"data":
			 * 			{	
			 * 				"name" : "টেস্ট নাম", 
			 * 				"password" : "test", 
			 * 				"email" : "test2@test.com", 
			 * 				"phone" : "০১৭১৭১০১০১০", 
			 * 				"lmp" : "21-04-2015"
			 * 			}
			 * }
			 */

			JSONObject dataObj = new JSONObject();
			dataObj.put("name", mEditTextName.getText().toString());
			dataObj.put("password", mPassword1);
			dataObj.put("email", mEmail);
			dataObj.put("phone", mPhone);
			dataObj.put("type", 1);
			dataObj.put("lmp", mMenstrualDate);
			reqObj.put("data", dataObj);

			params.put("data" , reqObj.toString());
		} catch (JSONException e) {
			e.printStackTrace();
		}      


		// Make Http call
		client.post(url, params, new AsyncHttpResponseHandler() {
			@Override
			public void onSuccess(String response) {
				processResponse(response);
			}
			// When error occured
			@Override
			public void onFailure(int statusCode, Throwable error, String content) {
				if (statusCode == 404) {
					handler.post(new Runnable() {
						@Override
						public void run() {
							Toast.makeText(getActivity(), "Requested resource not found", Toast.LENGTH_LONG).show();						
						}
					});
				} else if (statusCode == 500) {
					handler.post(new Runnable() {
						@Override
						public void run() {
							Toast.makeText(getActivity(), "Something went wrong at server end", Toast.LENGTH_LONG).show();						
						}
					});
				} else {
					handler.post(new Runnable() {
						@Override
						public void run() {
							Toast.makeText(getActivity(), "Unexpected Error! Device might not be connected to Internet",
									Toast.LENGTH_LONG).show();						
						}
					});
				}
			}
		});
	}

	private void processResponse(String response) {		
		/**
		 * {
				"request": {
								"data": {
											"name": "টেস্ট নাম",
											"password": "test",
											"email": "test2@test.com",
											"phone": "০১৭১৭১০১০১০",
											"type" : 3, // free value = 3paid value= 4
											"lmp": "21-04-2015"
						   				}
						   },
				"result":  {
								"User": {
											"id": "3",
											"name": "টেস্ট নাম",
											"email": "test2@test.com",
											"phone": "০১৭১৭১০১০১০",
											"lmp": "2015-04-21",
											"type": "3",
											"status": "1",
											"token": "o4XTeclLzLpTrySJ6V5v3NSA5O3HH"
										}
						   },
				"message": "Signed in successfully.",
				"status_code": 200
		  }
		 */
		try {
			JSONObject responseObj = new JSONObject(response);

			int statusCode = responseObj.getInt("status_code");
			final String msg = responseObj.getString("message");

			if(statusCode==200) {
				JSONObject resultObj = responseObj.getJSONObject("result");
				//FileWriteBlock
				//				StringBuffer msb1 = new StringBuffer();
				//				msb1.append(resultObj.toString());
				//				try {
				//					File f = new File(Environment.getExternalStorageDirectory().getPath(), "resultUserInfo.txt");
				//					writeToFile(f, msb1);
				//				} catch (IOException e) {
				//					e.printStackTrace();
				//				}
				JSONObject userObj = resultObj.getJSONObject("User");

				String id = userObj.getString("id");
				String name = userObj.getString("name");
				String email = userObj.getString("email");
				String phone = userObj.getString("phone");

				mToken = userObj.getString("token");
				mLmp = userObj.getString("lmp");

				SimpleDateFormat format = new SimpleDateFormat();
				format.applyPattern("yyyy-MM-dd");
				Date lmp = null;
				Date curDate = null;
				int dayPassed = 0;
				int currWeek = 0;
				try {
					lmp = format.parse(userObj.getString("lmp"));
					curDate = new Date(System.currentTimeMillis());

					dayPassed = (int)((curDate.getTime() - lmp.getTime()) / (1000*60*60*24l));
					if(dayPassed%7 == 0) {
						currWeek = dayPassed/7;
					} else currWeek = (dayPassed/7) + 1;					

				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				String token = userObj.getString("token");
				mSession.createLoginSession(id, name, email, phone, mLmp, currWeek, token, null, null, null, null, null);

				//Process userObj to save in database
				DevOpenHelper helper = new DaoMaster.DevOpenHelper(getActivity(), "aponjon-db", null);
				SQLiteDatabase db = helper.getWritableDatabase();
				DaoMaster daomaster = new DaoMaster(db);
				DaoSession session = daomaster.newSession();

				UserDao userDao = session.getUserDao();
				userDao.deleteAll();
				userDao.insert(new User(null, Long.parseLong(id), token, name, email, phone, lmp, null, null));

				CheckRegistered checkReg = new CheckRegistered(getActivity());
				checkReg.checkRegisterd();

				syncData();
				
				//photo upload
				if(mBitmap != null) {
					try {
						sendPhoto(mBitmap);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}

				handler.post(new Runnable() {
					@Override
					public void run() {
						Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();
						if(pd.isShowing())pd.dismiss();
					}
				});

			} else {
				handler.post(new Runnable() {
					@Override
					public void run() {
						Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();
						if(pd.isShowing())pd.dismiss();
					}
				});
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}




	@SuppressLint("NewApi")
	private void syncData() {
		if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
			// only for Honeycomb and newer versions
			new AsyncTask<Void, Void, Void>() {
				@Override
				protected void onPostExecute(Void result) {
					startActivity(new Intent(getActivity(), DashboardActivity.class));
					getActivity().finish();
				}
				@Override
				protected Void doInBackground(Void... params) {
					try {
//						PeriodicContentUpdate periodicTask = new PeriodicContentUpdate(getActivity());
//						periodicTask.updateContent();
						
						Intent startServiceIntent = new Intent(getActivity(), BackgroundService.class);
				        getActivity().startService(startServiceIntent);

						DownloadUserAllDataTask downAllData =new DownloadUserAllDataTask(getActivity());
						downAllData.runTasks();
					} catch (Exception e) {
						e.printStackTrace();
					}
					return null;				
				}
			}.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			new AsyncTask<Void, Void, Void>() {
				@Override
				protected void onPostExecute(Void result) {
					startActivity(new Intent(getActivity(), DashboardActivity.class));
					getActivity().finish();
				}
				@Override
				protected Void doInBackground(Void... params) {
					try {
//						PeriodicContentUpdate periodicTask = new PeriodicContentUpdate(getActivity());
//						periodicTask.updateContent();
						
						Intent startServiceIntent = new Intent(getActivity(), BackgroundService.class);
				        getActivity().startService(startServiceIntent);

						DownloadUserAllDataTask downAllData =new DownloadUserAllDataTask(getActivity());
						downAllData.runTasks();
					} catch (Exception e) {
						e.printStackTrace();
					}
					return null;				
				}
			}.execute();
		}
	}
	
	private void sendPhoto(Bitmap bitmap) {
		new ImageUploadTask(mCurrentPhotoPath, getActivity()).execute(bitmap);
	}

	public static void writeToFile(File pFile, StringBuffer pData) throws IOException {  
		BufferedWriter out = new BufferedWriter(new FileWriter(pFile));  
		out.write(pData.toString());  
		out.flush();  
		out.close();  
	}

}
