package com.humaclab.aponjonshogorbha.fragments;

import com.bd.aponjon.pregnancy.R;
import com.humaclab.aponjonshogorbha.Constants;
import com.humaclab.aponjonshogorbha.activities.AponjonKiKenoActivity;
import com.humaclab.aponjonshogorbha.activities.AponjonKiKenoActivityDemo;
import com.humaclab.aponjonshogorbha.activities.AponjonNibondhonActivityDemo;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class AponjonNibondhonFragment extends Fragment {
	
	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("আপনজনে নিবন্ধন");
	}
	
	@Override
	@Nullable
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		
		View v = inflater.inflate(R.layout.layout_fragment_aponjon_nibondhon, container, false);
		
		v.findViewById(R.id.view_btn_nibondhon).setOnClickListener(new View.OnClickListener() {			
			@Override
			public void onClick(View v) {
				if(Constants.is_demo) {
					startActivity(new Intent(getActivity(), AponjonKiKenoActivityDemo.class));
				} else startActivity(new Intent(getActivity(), AponjonKiKenoActivity.class));
			}
		});
		return v;
	}
}
