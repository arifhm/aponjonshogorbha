package com.humaclab.aponjonshogorbha.fragments;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import bengali.language.support.BengaliUnicodeString;

import com.humaclab.aponjon.Article;
import com.humaclab.aponjon.ArticleDao;
import com.humaclab.aponjon.DaoMaster;
import com.humaclab.aponjon.DaoSession;
import com.humaclab.aponjon.DaoMaster.DevOpenHelper;
import com.humaclab.aponjonshogorbha.Constants;
import com.humaclab.aponjonshogorbha.ImageDownloader;
import com.bd.aponjon.pregnancy.R;
import com.humaclab.aponjonshogorbha.SessionManager;
import com.humaclab.aponjonshogorbha.fragments.dialogs.DemoModeDialog;
import com.squareup.picasso.Picasso;

import android.app.DownloadManager.Query;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class AmarShonamoniFragment extends Fragment {
	
	DaoSession mDaoSession;
	SessionManager mSessionMgr;
	SharedPreferences mPrefs;
	
	TextView mTextMain;
	TextView mPrev;
	TextView mNext;
	TextView mWeek;
	TextView mWeekSub;
	ImageView mImgView;
	Button mBtnShare;
	
	ViewGroup parentIndicator;
	
	int mIndex;
	int mListSize;
	String mLmp;
	String mUserName;
	String mArticleChunk;
	String mCurrWeekString;
	StringBuffer mShareText;
	
	List<Article> articleList;
	String link;
	
	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("আমার সোনামনি");
	}
	
	@Override
	@Nullable
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		
		View v = inflater.inflate(R.layout.layout_fragment_amar_ami_shonamoni, container, false);
		
		mTextMain = (TextView) v.findViewById(R.id.TextView_TextBody);
		mPrev = (TextView) v.findViewById(R.id.prev_btn);
		mNext = (TextView) v.findViewById(R.id.next_btn);
		mWeek = (TextView) v.findViewById(R.id.week_title);
		mWeekSub = (TextView) v.findViewById(R.id.week_sub_title);
		mImgView = (ImageView) v.findViewById(R.id.article_img);
		parentIndicator = (ViewGroup) v.findViewById(R.id.indicator);
		mBtnShare = (Button) v.findViewById(R.id.view_btn_share);
		
		mSessionMgr = new SessionManager(getActivity());
		mPrefs = mSessionMgr.getPrefs();
		mUserName = mPrefs.getString(Constants.UserData.USER_NAME, null);
		mCurrWeekString = BengaliUnicodeString.convertToBengaliText(getActivity(), mPrefs.getInt(Constants.UserData.USER_CURRENT_WEEK, 6)+"");
		
		mWeekSub.setText("এই সপ্তাহে আমার সোনামনি");
		if(Constants.is_demo){
			mTextMain.setText("এই অংশে একজন গর্ভবতী মা তাঁর গর্ভের বয়স অনুযায়ী প্রতি সপ্তাহে তাঁর গর্ভস্থ শিশুর অবস্থা সম্পর্কে জানার পাশাপাশি কিছু সাধারণ স্বাস্থ্যতথ্যও পাবেন।");
		}

		if(!Constants.is_demo) {
			DevOpenHelper helper = new DaoMaster.DevOpenHelper(getActivity(), "aponjon-db", null);
			SQLiteDatabase db = helper.getWritableDatabase();
			DaoMaster daomaster = new DaoMaster(db);
			mDaoSession = daomaster.newSession();

			ArticleDao articleDao = mDaoSession.getArticleDao();
			articleList = articleDao.queryBuilder().where(ArticleDao.Properties.Category.eq(2)).list();
			
			SessionManager session = new SessionManager(getActivity());
		    SharedPreferences pref = session.pref;
		    mLmp = pref.getString(Constants.UserData.USER_LMP, null);
			
		    mIndex = getCurrentWeek();
			mListSize = articleList.size();
			if(mListSize > 0 && (mIndex > 5 && mIndex < 43)) {
				showArticle(v);
			}
		} else {
			
		}
		
	
		mNext.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(Constants.is_demo){
					DemoModeDialog dialog = new DemoModeDialog(getActivity());
					dialog.show();
				} else if(mIndex < 42) {
					mIndex++;
					showArticle(v);
				} 
			}
		});

		mPrev.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if(Constants.is_demo){
					DemoModeDialog dialog = new DemoModeDialog(getActivity());
					dialog.show();
				}else if(mIndex > 6) {
					mIndex--;
					showArticle(v);
				} 
			}
		});
		mBtnShare.setOnClickListener(new View.OnClickListener() {		
			@Override
			public void onClick(View v) {
				if(Constants.is_demo) {
					DemoModeDialog dialog = new DemoModeDialog(getActivity());
					dialog.show();
				} else {
					mShareText.append("অ্যাপ্লিকেশনটি ডাউনলোড করুন এই লিঙ্কে : https://play.google.com/store/apps/details?id=com.bd.aponjon.pregnancy");
					Intent sendIntent = new Intent();
					sendIntent.setAction(Intent.ACTION_SEND);
					sendIntent.putExtra(Intent.EXTRA_TEXT,
					    mShareText.toString());
					sendIntent.setType("text/plain");
					startActivity(sendIntent);
				}
			}
		});
		
		return v;
	}
	
	public void showArticle(View v) {
		Article article = articleList.get(mIndex-6);
		
		Picasso.with(getActivity()).load(article.getImg_link_server()).into(mImgView);
		mTextMain.setText(Html.fromHtml(article.getText_comment()));
		mWeek.setText("সপ্তাহ " + BengaliUnicodeString.convertToBengaliText(getActivity(), String.valueOf(article.getWeek_id())));
		mShareText = new StringBuffer();
		mShareText.append(mUserName + " এখন " + mCurrWeekString + "সপ্তাহে। \n");
		mShareText.append("তিনি সপ্তাহ " + BengaliUnicodeString.convertToBengaliText(getActivity(), String.valueOf(article.getWeek_id())) +
				" এর আর্টিকেলটি পড়েছেন। \n" + Html.fromHtml(article.getText_comment()) + "\n");
		
		int startVal = mIndex - 2;
		if(mIndex<8) {
			startVal = 6;
		} 
		
		if(mIndex==6) {
			
			parentIndicator.getChildAt(1).setVisibility(View.INVISIBLE);
			parentIndicator.getChildAt(2).setVisibility(View.INVISIBLE);
						
			for(int i=3; i<6; i++) {
				TextView view = (TextView) parentIndicator.getChildAt(i);
				view.setText(BengaliUnicodeString.convertToBengaliText(getActivity(), String.valueOf(startVal)));
				startVal++;
			}
		} else if(mIndex == 7) {
			parentIndicator.getChildAt(1).setVisibility(View.INVISIBLE);
			parentIndicator.getChildAt(2).setVisibility(View.VISIBLE);
			for(int i=2; i<6; i++) {
				TextView view = (TextView) parentIndicator.getChildAt(i);
				view.setText(BengaliUnicodeString.convertToBengaliText(getActivity(), String.valueOf(startVal)));
				startVal++;
			}
		} else if (mIndex == 41) {
			parentIndicator.getChildAt(5).setVisibility(View.INVISIBLE);
			parentIndicator.getChildAt(4).setVisibility(View.VISIBLE);
			for(int i=1; i<5; i++) {
				TextView view = (TextView) parentIndicator.getChildAt(i);
				view.setText(BengaliUnicodeString.convertToBengaliText(getActivity(), String.valueOf(startVal)));
				startVal++;
			}
		} else if(mIndex == 42){
			parentIndicator.getChildAt(5).setVisibility(View.INVISIBLE);
			parentIndicator.getChildAt(4).setVisibility(View.INVISIBLE);
			for(int i=1; i<4; i++) {
				TextView view = (TextView) parentIndicator.getChildAt(i);
				view.setText(BengaliUnicodeString.convertToBengaliText(getActivity(), String.valueOf(startVal)));
				startVal++;
			}
		} else {
			parentIndicator.getChildAt(1).setVisibility(View.VISIBLE);
			parentIndicator.getChildAt(2).setVisibility(View.VISIBLE);
			parentIndicator.getChildAt(4).setVisibility(View.VISIBLE);
			parentIndicator.getChildAt(5).setVisibility(View.VISIBLE);
			
			for(int i=1; i<6; i++) {
				TextView view = (TextView) parentIndicator.getChildAt(i);
				view.setText(BengaliUnicodeString.convertToBengaliText(getActivity(), String.valueOf(startVal)));
				startVal++;
			}
		}
	}
	
	public int getCurrentWeek() {
		SimpleDateFormat format = new SimpleDateFormat();
		format.applyPattern("yyyy-MM-dd");
		Date lmp = null;
		Date curDate = null;
		int dayPassed = 0;
		int currWeek = 0;
		try {
			lmp = format.parse(mLmp);
			curDate = new Date(System.currentTimeMillis());
			
			dayPassed = (int)((curDate.getTime() - lmp.getTime()) / (1000*60*60*24l));
			if(dayPassed%7 == 0) {
				currWeek = dayPassed/7;
			} else currWeek = (dayPassed/7) + 1;
			
			
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return currWeek;
	}
}
