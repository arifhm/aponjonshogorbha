package com.humaclab.aponjonshogorbha.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bd.aponjon.pregnancy.R;
import com.dnet.mama.diary.DiaryActivity;
import com.dnet.mama.diary.DiaryActivityDemo;
import com.dnet.mama.movecounter.MoveCounterActivity;
import com.dnet.mama.weight.WeightActivity;
import com.dnet.mama.weight.WeightActivityDemo;
import com.humaclab.aponjonshogorbha.Constants;
import com.humaclab.aponjonshogorbha.fragments.dialogs.DemoModeDialog;

public class AmarJogotFragment extends Fragment implements View.OnClickListener{
	
	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("আমার জগৎ");
	}
	
	@Override
	@Nullable
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		
		View v = inflater.inflate(R.layout.activity_main, container, false);
		
        v.findViewById(R.id.btnDiary).setOnClickListener(this);
        v.findViewById(R.id.btnWeight).setOnClickListener(this);
        v.findViewById(R.id.btnMoveCounter).setOnClickListener(this);
		return v;
	}

	@Override
	public void onClick(View v) {
		if (v.getId()== R.id.btnDiary){
			if(Constants.is_demo){
				 startActivity(new Intent(getActivity(), DiaryActivityDemo.class));
				 getActivity().finish();
			} else startActivity(new Intent(getActivity(), DiaryActivity.class));
        } else if (v.getId()== R.id.btnWeight){
        	if(Constants.is_demo){
        		 startActivity(new Intent(getActivity(), WeightActivityDemo.class));
        		 getActivity().finish();
			} else startActivity(new Intent(getActivity(), WeightActivity.class));

        } else if (v.getId()== R.id.btnMoveCounter){
        	if(Constants.is_demo){
				DemoModeDialog dialog = new DemoModeDialog(getActivity());
				dialog.show();
			} else startActivity(new Intent(getActivity(), MoveCounterActivity.class));
        }
	}
}
