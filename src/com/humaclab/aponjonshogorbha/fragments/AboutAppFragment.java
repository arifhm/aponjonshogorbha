package com.humaclab.aponjonshogorbha.fragments;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;

import android.content.res.AssetManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebSettings.LayoutAlgorithm;
import android.webkit.WebView;

import com.bd.aponjon.pregnancy.R;

public class AboutAppFragment extends Fragment {
	WebView mView;
	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("অ্যাপ সম্পর্কে");
	}

	@Override
	@Nullable
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

		View v = inflater.inflate(R.layout.layout_fragment_about_app, container, false);
		mView = (WebView) v.findViewById(R.id.webView_about_app);
		setupWebView();
		
		return v;
	}

	public void setupWebView(){		
		WebSettings webSetting = mView.getSettings();
		webSetting.setLayoutAlgorithm(LayoutAlgorithm.SINGLE_COLUMN);

		String htmlFilename = "about.html";
		AssetManager mgr = getActivity().getAssets();
		try {
			InputStream in = mgr.open(htmlFilename, AssetManager.ACCESS_BUFFER);
			String htmlContentInStringFormat = StreamToString(in);
			in.close();
			mView.loadDataWithBaseURL(null, htmlContentInStringFormat, "text/html", "utf-8", null);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static String StreamToString(InputStream in) throws IOException {
		if(in == null) {
			return "";
		}
		Writer writer = new StringWriter();
		char[] buffer = new char[1024];
		try {
			Reader reader = new BufferedReader(new InputStreamReader(in, "UTF-8"));
			int n;
			while ((n = reader.read(buffer)) != -1) {
				writer.write(buffer, 0, n);
			}
		} finally {
		}
		return writer.toString();
	}	

}
