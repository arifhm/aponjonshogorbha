package com.humaclab.aponjonshogorbha.fragments;

import com.humaclab.aponjonshogorbha.Constants;
import com.bd.aponjon.pregnancy.R;
import com.humaclab.aponjonshogorbha.SessionManager;
import com.humaclab.aponjonshogorbha.activities.kikhabo.AmarKhaddoTalikaActivity;
import com.humaclab.aponjonshogorbha.activities.kikhabo.HWAInputActivity;
import com.humaclab.aponjonshogorbha.activities.kikhabo.KhaddoTalikaToiriActivity;
import com.humaclab.aponjonshogorbha.activities.kikhabo.NomunaKhaddoActivity;
import com.humaclab.aponjonshogorbha.activities.kikhabo.NomunaKhaddoActivityDemo;
import com.humaclab.aponjonshogorbha.fragments.dialogs.DemoModeDialog;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

public class KiKhaboFragment extends Fragment implements OnClickListener{	
	SessionManager mSessionMgr;
	SharedPreferences mPrefs;
	
	boolean hasHwa;
	
	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("কি খাবো");
	}

	@Override
	@Nullable
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

		View v = inflater.inflate(R.layout.layout_fragment_ki_khabo, container, false);
		
		mSessionMgr = new SessionManager(getActivity());
		mPrefs = mSessionMgr.getPrefs();
		hasHwa = mPrefs.getBoolean(Constants.UserData.HAS_HWA, false);
		
		v.findViewById(R.id.view_btn_1).setOnClickListener(this);
		v.findViewById(R.id.view_btn_2).setOnClickListener(this);
		v.findViewById(R.id.view_btn_3).setOnClickListener(this);

		return v;
	}

	@Override
	public void onClick(View v) {
		
		switch (v.getId()) {
		case R.id.view_btn_1:
			if(Constants.is_demo) {
				startActivity(new Intent(getActivity(), NomunaKhaddoActivityDemo.class));
			} else startActivity(new Intent(getActivity(), NomunaKhaddoActivity.class));
			break;
		case R.id.view_btn_2:
			if(Constants.is_demo){
				DemoModeDialog dialog = new DemoModeDialog(getActivity());
				dialog.show();
			} else if(hasHwa) {
				startActivity(new Intent(getActivity(), KhaddoTalikaToiriActivity.class));
			} else startActivity(new Intent(getActivity(), HWAInputActivity.class)); 

			
			break;
		case R.id.view_btn_3:
			if(Constants.is_demo){
				DemoModeDialog dialog = new DemoModeDialog(getActivity());
				dialog.show();
			} else startActivity(new Intent(getActivity(), AmarKhaddoTalikaActivity.class));
			break;
		default:
			break;
		}

//		if(fragment == null) {
//			fm.beginTransaction().add(R.id.nav_contentframe, fragment).commit();
//		} else {
//			fm.beginTransaction().replace(R.id.nav_contentframe, fragment).commit();
//		}

	}
}
