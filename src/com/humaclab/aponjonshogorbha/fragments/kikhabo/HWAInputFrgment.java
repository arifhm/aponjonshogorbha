package com.humaclab.aponjonshogorbha.fragments.kikhabo;

import java.util.ArrayList;

import bengali.language.support.BengaliUnicodeString;

import com.humaclab.aponjonshogorbha.ProfileUpdateTask;
import com.bd.aponjon.pregnancy.R;
import com.humaclab.aponjonshogorbha.SessionManager;
import com.humaclab.aponjonshogorbha.activities.kikhabo.KhaddoTalikaToiriActivity;
import com.humaclab.aponjonshogorbha.adapters.editprofile.AgeAdapter;
import com.humaclab.aponjonshogorbha.adapters.editprofile.FootAdapter;
import com.humaclab.aponjonshogorbha.adapters.editprofile.InchAdapter;
import com.humaclab.aponjonshogorbha.adapters.editprofile.WeightAdapter;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

public class HWAInputFrgment extends Fragment {

	Spinner mFeet;
	Spinner mInch;
	Spinner mWeight;
	Spinner mAge;

	Button mSubmit;

	SessionManager sessionMgr;
	SharedPreferences prefs;
	Editor editor;

	int _ft;
	int _inch;
	float _height;
	int _weight;
	int _age;

	boolean validHwa = false;

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("উচ্চতা, ওজন ও বয়স");
	}

	@Override
	@Nullable
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.layout_fragment_hwa_ki_khabo, container, false);

		sessionMgr = new SessionManager(getActivity());
		prefs = sessionMgr.getPrefs();
		editor = sessionMgr.getEditor();

		mFeet = (Spinner) v.findViewById(R.id.text_view_picker_ft);
		mInch = (Spinner) v.findViewById(R.id.text_view_picker_inch);
		mWeight = (Spinner) v.findViewById(R.id.text_view_picker_weight);
		mAge = (Spinner) v.findViewById(R.id.text_view_picker_age);

		FootAdapter foot_adapter = new FootAdapter(getActivity(), R.layout.layout_spinner_hwa_dropdown, prepareFeets());
		mFeet.setAdapter(foot_adapter);

		InchAdapter inch_adapter = new InchAdapter(getActivity(), R.layout.layout_spinner_hwa_dropdown, prepareInchs());
		mInch.setAdapter(inch_adapter);

		WeightAdapter weight_adapter = new WeightAdapter(getActivity(), R.layout.layout_spinner_hwa_dropdown, prepareWeights());
		mWeight.setAdapter(weight_adapter);

		AgeAdapter age_adapter = new AgeAdapter(getActivity(), R.layout.layout_spinner_hwa_dropdown, prepareAges());
		mAge.setAdapter(age_adapter);

		mFeet.setOnItemSelectedListener(foot_item_listener);
		mInch.setOnItemSelectedListener(inch_item_listener);
		mWeight.setOnItemSelectedListener(weight_item_listener);
		mAge.setOnItemSelectedListener(age_item_listener);

		mSubmit = (Button) v.findViewById(R.id.edit_profile_btn_submit);
		mSubmit.setOnClickListener(new View.OnClickListener() {			
			@Override
			public void onClick(View v) {				
				if(_ft> 0 || _weight > 0 || _age > 0) {
					if(!(_ft > 0)) {
						Toast.makeText(getActivity(), "আপনার উচ্চতা প্রদান করুন", Toast.LENGTH_SHORT).show();
						return;
					} else if (!(_weight > 0)) {
						Toast.makeText(getActivity(), "আপনার ওজন প্রদান করুন", Toast.LENGTH_SHORT).show();
						return;
					} else if (!(_age > 0)) {
						Toast.makeText(getActivity(), "আপনার বয়স প্রদান করুন", Toast.LENGTH_SHORT).show();
						return;
					} else {
						_height = ((_ft * 12) + _inch) * 2.54f;
						validHwa = true;
					}
				}

				if(validHwa == true) {
					proceedToNext();
				} else {
					Toast.makeText(getActivity(), "আপনি কোনো তথ্য প্রদান করেননি !", Toast.LENGTH_SHORT).show();
					return;
				}		
			}
		});

		return v;
	}

	@SuppressLint("NewApi")
	private void proceedToNext() {
		if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
			// only for Honeycomb and newer versions
			new AsyncTask<Void, Void, Void>() {
				@Override
				protected void onPreExecute() {
				}
				@Override
				protected void onPostExecute(Void result) {
					sessionMgr.setHWAtoPreff(_height, _weight, _age);
					startActivity(new Intent(getActivity(), KhaddoTalikaToiriActivity.class));
					getActivity().finish();
				}
				@Override
				protected Void doInBackground(Void... params) {
					try {
						ProfileUpdateTask update = new ProfileUpdateTask(getActivity(), null, null, null, null, _height, _weight, _age, null);
						update.startProfileUpdate();
					} catch (Exception e) {
						e.printStackTrace();
					}
					return null;				
				}
			}.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			new AsyncTask<Void, Void, Void>() {
				@Override
				protected void onPreExecute() {
				}
				@Override
				protected void onPostExecute(Void result) {
					startActivity(new Intent(getActivity(), KhaddoTalikaToiriActivity.class));
					getActivity().finish();
				}
				@Override
				protected Void doInBackground(Void... params) {
					try {
						ProfileUpdateTask update = new ProfileUpdateTask(getActivity(), null, null, null, null, _height, _weight, _age, null);
						update.startProfileUpdate();
					} catch (Exception e) {
						e.printStackTrace();
					}
					return null;				
				}
			}.execute();
		}
	}

	public android.widget.AdapterView.OnItemSelectedListener foot_item_listener = new android.widget.AdapterView.OnItemSelectedListener() {
		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int position,
				long arg3) {
			if(position == 0) {
				_ft = 0;
			} else _ft = position + 3;
//			Toast.makeText(getActivity(), _ft, Toast.LENGTH_LONG).show();			
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			_ft = 0;
		}
	};

	public android.widget.AdapterView.OnItemSelectedListener inch_item_listener = new android.widget.AdapterView.OnItemSelectedListener() {

		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int position,
				long arg3) {
			if(position == 0) {
				_inch = 0;
			} else _inch = position;
//			Toast.makeText(getActivity(), _inch, Toast.LENGTH_LONG).show();
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			_inch = 0;
		}
	};
	public android.widget.AdapterView.OnItemSelectedListener weight_item_listener = new android.widget.AdapterView.OnItemSelectedListener() {

		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int position,
				long arg3) {
			if(position == 0) {
				_weight = 0;
			} else _weight = position + 39;
//			Toast.makeText(getActivity(), _weight, Toast.LENGTH_LONG).show();
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			_weight = 0;
		}
	};

	public android.widget.AdapterView.OnItemSelectedListener age_item_listener = new android.widget.AdapterView.OnItemSelectedListener() {

		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int position,
				long arg3) {
			if(position == 0) {
				_age = 0;
			} else _age = position + 17;
//			Toast.makeText(getActivity(), _age, Toast.LENGTH_LONG).show();
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			_age = 0;
		}
	};

	ArrayList<String> foots = new ArrayList<String>();
	ArrayList<String> inchs = new ArrayList<String>();
	ArrayList<String> weights = new ArrayList<String>();
	ArrayList<String> ages = new ArrayList<String>();

	public ArrayList<String> prepareFeets(){
		foots = new ArrayList<String>();
		for(int i=3;i<8;i++){
			if(i==3) {
				foots.add("");
			} else {
				foots.add(BengaliUnicodeString.convertToBengaliText(getActivity(), String.valueOf(i))+" ফুট");
			}
		}
		return foots;
	}

	public ArrayList<String> prepareInchs(){
		inchs = new ArrayList<String>();
		for(int i=0;i<=11;i++){
			if(i==0) {
				inchs.add("");
			} else {
				inchs.add(BengaliUnicodeString.convertToBengaliText(getActivity(), String.valueOf(i))+" ইঞ্চি");
			}
		}
		return inchs;
	}

	public ArrayList<String> prepareWeights(){
		weights = new ArrayList<String>();
		for(int i=39;i<121;i++){
			if(i==39) {
				weights.add("");
			} else {
				weights.add(BengaliUnicodeString.convertToBengaliText(getActivity(), String.valueOf(i))+" কেজি");
			}
		}
		return weights;
	}

	public ArrayList<String> prepareAges(){
		ages = new ArrayList<String>();
		for(int i=17;i<56;i++){
			if(i==17) {
				ages.add("");
			} else {
				ages.add(BengaliUnicodeString.convertToBengaliText(getActivity(), String.valueOf(i))+" বছর");
			}
		}
		return ages;
	}
}