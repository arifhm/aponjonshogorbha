package com.humaclab.aponjonshogorbha.fragments.kikhabo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.humaclab.aponjon.DaoSession;
import com.humaclab.aponjon.Food_Category;
import com.humaclab.aponjon.Food_CategoryDao;
import com.humaclab.aponjon.Meal_Food;
import com.humaclab.aponjon.Meal_FoodDao;
import com.humaclab.aponjonshogorbha.AponjonApplication;
import com.bd.aponjon.pregnancy.R;
import com.humaclab.aponjonshogorbha.activities.kikhabo.KhaddoTalikaEditActivity;
import com.humaclab.aponjonshogorbha.adapters.kikhabo.AmarKhaddoTalikaDetailsAdapter;
import com.humaclab.aponjonshogorbha.adapters.kikhabo.AmarKhaddoTalikaAdapter.OnItemClickListener;

import de.greenrobot.dao.query.WhereCondition;

public class AmarKhaddoTalikaDetailsFragment extends Fragment {
	public static final String MEAL_ID = "clicked_meal_id";
	public static final String MEAL_NAME = "clicke_meal_name";
	RecyclerView mRecyclerViewDetails;
	AmarKhaddoTalikaDetailsAdapter mAdapter;
	AponjonApplication mApp;
	Map<String, List<Meal_Food>> mMealMap;
	
	TextView mViewMealTitle;
	ImageView mViewBtnEdit;

	public static Fragment getInstance(long id, String name) {
		Fragment fr = new AmarKhaddoTalikaDetailsFragment();
		Bundle bundle = new Bundle();
		bundle.putSerializable(MEAL_ID, id);
		bundle.putSerializable(MEAL_NAME, name);
		fr.setArguments(bundle);
		
		return fr;
	}
	
	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		((AppCompatActivity) getActivity()).getSupportActionBar().setTitle((String) getArguments().getSerializable(MEAL_NAME));
	}

	@Override
	@Nullable
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.layout_fragment_amar_khddo_talika_details, container, false);
		
		final long id = (Long) getArguments().getSerializable(MEAL_ID);
		final String mealName = (String) getArguments().getSerializable(MEAL_NAME); 
		
		mViewMealTitle = (TextView) v.findViewById(R.id.view_meal_title);
		mViewBtnEdit = (ImageView) v.findViewById(R.id.view_btn_edit_meal);
		mViewBtnEdit.setOnClickListener(new View.OnClickListener() {			
			@Override
			public void onClick(View v) {
				Intent i = new Intent(getActivity(), KhaddoTalikaEditActivity.class);
				i.putExtra(MEAL_ID, id);
				i.putExtra(MEAL_NAME, mealName);
				startActivity(i);
			}
		});
		
		mViewMealTitle.setText(mealName);
		
		mApp = AponjonApplication.getInstance();
		DaoSession daoSession = mApp.getDaoSession();
		
		Food_CategoryDao catFoodDao = daoSession.getFood_CategoryDao();
		List<Food_Category> categories = catFoodDao.queryBuilder().list();
		
		Meal_FoodDao mealFoodDao = daoSession.getMeal_FoodDao();
		mMealMap = new HashMap<>();
		for(Food_Category cat : categories) {
			List<Meal_Food> categoryMealFoods = mealFoodDao.queryBuilder()
					.where(Meal_FoodDao.Properties.Meal_id.eq(id), Meal_FoodDao.Properties.Category_name.eq(cat.getFood_category_name())).list();
			mMealMap.put(cat.getFood_category_name(), categoryMealFoods);
		}
			
		mRecyclerViewDetails = (RecyclerView) v.findViewById(R.id.recyclerView_amar_khaddo_talika_details);
		mAdapter = new AmarKhaddoTalikaDetailsAdapter(getActivity(), categories, mMealMap); 
				
//				Toast.makeText(getActivity(), "Extra ID: " + id, Toast.LENGTH_SHORT).show();
		return v;
	}
	
	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		mRecyclerViewDetails.setAdapter(mAdapter);
		mRecyclerViewDetails.setHasFixedSize(false);
		mRecyclerViewDetails.setLayoutManager(new LinearLayoutManager(getActivity()));
		mRecyclerViewDetails.setItemAnimator(new DefaultItemAnimator());
		
	}
}
