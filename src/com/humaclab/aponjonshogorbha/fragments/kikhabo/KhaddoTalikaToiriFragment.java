package com.humaclab.aponjonshogorbha.fragments.kikhabo;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;
import bengali.language.support.BengaliUnicodeString;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.XAxis.XAxisPosition;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.humaclab.aponjon.DaoMaster;
import com.humaclab.aponjon.DaoMaster.DevOpenHelper;
import com.humaclab.aponjon.DaoSession;
import com.humaclab.aponjon.Food;
import com.humaclab.aponjon.Food_Category;
import com.humaclab.aponjon.Food_CategoryDao;
import com.humaclab.aponjon.Meal;
import com.humaclab.aponjon.MealDao;
import com.humaclab.aponjon.Meal_Food;
import com.humaclab.aponjon.Meal_FoodDao;
import com.humaclab.aponjonshogorbha.CalculateCurrentPER;
import com.humaclab.aponjonshogorbha.Constants;
import com.bd.aponjon.pregnancy.R;
import com.humaclab.aponjonshogorbha.SessionManager;
import com.humaclab.aponjonshogorbha.activities.KiKhaboActivity;
import com.humaclab.aponjonshogorbha.adapters.kikhabo.ExpandableAdapterNotunTalika;
import com.humaclab.aponjonshogorbha.fragments.dialogs.CalorieInadequatePrompt;
import com.humaclab.aponjonshogorbha.fragments.dialogs.CaptureSrcPrompt;
import com.humaclab.aponjonshogorbha.fragments.dialogs.MealSavePrompt;

public class KhaddoTalikaToiriFragment extends Fragment {
	private ArrayList<String> listDataHeader;
	private HashMap<String, List<Food>> listDataChild;
	private ExpandableListView expListView;
	private ExpandableAdapterNotunTalika listAdapter;
	private DaoSession mSession;
	private Map<String, List<Meal_Food>> mMealMap;
	private Meal mMeal;
	
	SessionManager mSessionMgr;
	SharedPreferences mPrefs;
	
	float mMinReqCurb;
	float mMinReqProtien;
	float mMinReqFat;
	float mMinReqTotal;
	float mReqTotal;
	
	float mCurrCurb;
	float mCurrProtien;
	float mCurrFat;
	float mCurrTotal;
	
	BarChart mBarchart;
	TextView mViewTotalSet;
	TextView mViewTotalReq;
	TextView mViewSave;
	String mMealName;
	
	int val= 0;

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("খাদ্য তালিকা তৈরী");
	}
	
	@Override
	@Nullable
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.layout_fragment_talika_toiri, container, false);
		
		mViewTotalSet = (TextView) v.findViewById(R.id.total);
		mViewTotalReq = (TextView) v.findViewById(R.id.min_req);
		
		mBarchart = (BarChart) v.findViewById(R.id.chart);
			
		mSessionMgr = new SessionManager(getActivity());
		mPrefs = mSessionMgr.getPrefs();
		CalculateCurrentPER calPer = new CalculateCurrentPER(getActivity());
		calPer.calculatePer();
		
		mMinReqCurb = mPrefs.getFloat(Constants.UserData.MIN_REQ_CURB, 0f);
		mMinReqProtien = mPrefs.getFloat(Constants.UserData.MIN_REQ_PROTIEN, 0f);
		mMinReqFat = mPrefs.getFloat(Constants.UserData.MIN_REQ_FAT, 0f);
		mMinReqTotal = mPrefs.getFloat(Constants.UserData.MIN_REQ_TOTAL, 0f);
		mReqTotal =mPrefs.getFloat(Constants.UserData.REQ_TOTAL, 0f);
		
		mCurrCurb = 0f;
		mCurrProtien = 0f;
		mCurrFat = 0f;
		mCurrTotal = 0f;
		
		String req = "নুন্যতম সর্বমোট পুষ্টিগুণ: " + BengaliUnicodeString.convertToBengaliText(getActivity(), getFormatDisplayValue(Math.round(mReqTotal))) + " ক্যালরি";
		mViewTotalReq.setText(req);

//		String temp = String.valueOf(mCurrTotal);
//		if(temp.contains("")){
//			rep
//		}
		
		String set = "সর্বমোট পুষ্টিগুণ: <html><body><font color='#f3f3f3'><big><b>" + BengaliUnicodeString.convertToBengaliText(getActivity(), getFormatDisplayValue(Math.round(mCurrTotal))) + "</b></big></font></body></html> ক্যালরি";
		mViewTotalSet.setText(Html.fromHtml(set), TextView.BufferType.SPANNABLE);
//		mViewTotalSet.setText( BengaliUnicodeString.convertToBengaliText(getActivity(), getFormatDisplayValue((int) mCurrTotal)));
		
		mBarchart.setDrawGridBackground(false);
		
		XAxis xAxis = mBarchart.getXAxis();
		xAxis.setPosition(XAxisPosition.BOTTOM);
		xAxis.setTextSize(10f);
		xAxis.setTextColor(Color.WHITE);
		xAxis.setDrawAxisLine(true);
		xAxis.setDrawGridLines(false);
		
		BarData data = new BarData(getXAxisValues(), getDataSet());
		mBarchart.setData(data);
//		mBarchart.animateXY(2000, 2000);
		mBarchart.invalidate();
		
		//Process userObj to save in database
		DevOpenHelper helper = new DaoMaster.DevOpenHelper(getActivity(), "aponjon-db", null);
		SQLiteDatabase db = helper.getWritableDatabase();
		DaoMaster daomaster = new DaoMaster(db);
		mSession = daomaster.newSession();

		Food_CategoryDao categoryDao = mSession.getFood_CategoryDao();
		List<Food_Category> categoryList = categoryDao.queryBuilder().list();
		
				
		// get the listview
		expListView = (ExpandableListView) v.findViewById(R.id.lvExp);

		// prepare mealMap
		prepareMealMap(categoryList);
		
		listAdapter = new ExpandableAdapterNotunTalika(getActivity(), categoryList , mMealMap, KhaddoTalikaToiriFragment.this); 

		// setting list adapter
		expListView.setAdapter(listAdapter);
		
		mViewSave = (TextView) v.findViewById(R.id.save_meal);
		mViewSave.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(mCurrCurb >= mMinReqCurb && mCurrProtien >= mMinReqProtien && mCurrFat >= mMinReqFat && mCurrTotal >= mMinReqTotal) {
					MealSavePrompt promptSave = MealSavePrompt.getInstance("");
					promptSave.setOnClickEditTextResponseCallBack(new MealSavePrompt.onClickEditTextResponseCallBack() {
						
						@Override
						public void onSaveClicked(String txt) {
							mMealName = txt;
						}
					});
					promptSave.setOnClickListener(new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int resultCode) {
							if(resultCode == 1) {
//								Toast.makeText(getActivity(), "Name is " + mMealName, Toast.LENGTH_LONG).show();
								saveMeal(mMealName);
								startActivity(new Intent(getActivity(), KiKhaboActivity.class));
								getActivity().finish();
							}
							if(resultCode == 2) {
								dialog.dismiss();
							}
							dialog.dismiss();
						}
					});
					
					Fragment fragmentByTag = getActivity().getSupportFragmentManager().findFragmentByTag(MealSavePrompt.MEAL_SAVE_TAG);
					if(fragmentByTag == null) {
						promptSave.show(getActivity().getSupportFragmentManager(), MealSavePrompt.MEAL_SAVE_TAG);
					}
				} else {
					CalorieInadequatePrompt prompt = CalorieInadequatePrompt.getInstance();

					prompt.setOnClickListener(new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int resultCode) {							
							if (resultCode == 0 ) {
								dialog.dismiss();
							}
							dialog.dismiss();
						}
					});

					Fragment fragmentByTag = getActivity().getSupportFragmentManager().findFragmentByTag(CalorieInadequatePrompt.CALORIE_INADEQUATE_TAG);//avoid opening twice dialog  
					if (fragmentByTag == null)  
						prompt.show(getActivity().getSupportFragmentManager(), CalorieInadequatePrompt.CALORIE_INADEQUATE_TAG);
				}
			}
		});

		return v;
	}
	
	
	private ArrayList<BarDataSet> getDataSet() {
        ArrayList<BarDataSet> dataSets = null;
 
        ArrayList<BarEntry> valueSet1 = new ArrayList<>();
        BarEntry v1e1 = new BarEntry((float)mMinReqCurb, 0); // Curb
        valueSet1.add(v1e1);
        BarEntry v1e2 = new BarEntry((float)mMinReqProtien, 1); // Protien
        valueSet1.add(v1e2);
        BarEntry v1e3 = new BarEntry((float)mMinReqFat, 2); // Fat
        valueSet1.add(v1e3);
        
        ArrayList<BarEntry> valueSet2 = new ArrayList<>();
        BarEntry v2e1 = new BarEntry((float)mCurrCurb, 0); // Curb
        valueSet2.add(v2e1);
        BarEntry v2e2 = new BarEntry((float)mCurrProtien, 1); // Protien
        valueSet2.add(v2e2);
        BarEntry v2e3 = new BarEntry((float)mCurrFat, 2); // Fat
        valueSet2.add(v2e3);
 
        BarDataSet barDataSet1 = new BarDataSet(valueSet1, "নুন্যতম");
        barDataSet1.setColor(Color.rgb(0, 155, 0));
        BarDataSet barDataSet2 = new BarDataSet(valueSet2, "সর্বমোট গৃহীত");
        barDataSet2.setColors(ColorTemplate.COLORFUL_COLORS);
 
        dataSets = new ArrayList<>();
        dataSets.add(barDataSet1);
        dataSets.add(barDataSet2);
        return dataSets;
    }
 
    private ArrayList<String> getXAxisValues() {
        ArrayList<String> xAxis = new ArrayList<>();
        xAxis.add("শর্করা");
        xAxis.add("প্রোটিন");
        xAxis.add("তেল/চর্বি");
        return xAxis;
    }

	private void prepareMealMap(List<Food_Category> catList) {
		mMealMap = new HashMap<>();
		for(Food_Category cat : catList) {
			List<Food> foods = cat.getCategoryfoods();
			List<Meal_Food> meal_foods = new ArrayList<Meal_Food>();
			for(Food food: foods) {
				Meal_Food mealfood = new Meal_Food();

				mealfood.setFood_data_id(food.getId_server());
				mealfood.setFood_desc(food.getDesc());
				mealfood.setCategory_name(cat.getFood_category_name());
				mealfood.setAbsolute_name(food.getAbsolute_name());
				mealfood.setCal_per_unit_curb(food.getCal_per_unit_curb());
				mealfood.setCal_per_unit_protien(food.getCal_per_unit_protien());
				mealfood.setCal_per_unit_fat(food.getCal_per_unit_fat());
				mealfood.setCal_per_unit_total(food.getCal_per_unit_total());
				mealfood.setFood_unit(food.getUnit());
				mealfood.setIncreament_value(food.getIncreament_value());

				meal_foods.add(mealfood);
			}

			mMealMap.put(cat.getFood_category_name(), meal_foods);
		}
	}
	
	public void reactWithDataSet() {
//		val++;
//		Toast.makeText(getActivity(), "Value now:" + val, Toast.LENGTH_SHORT).show();
		mCurrTotal = 0f; mCurrCurb = 0f;  mCurrProtien = 0f; mCurrFat = 0f;
		
		Set<String> catSets = mMealMap.keySet();
		
		for(String cat : catSets) {
			List<Meal_Food> mealFoods = mMealMap.get(cat);
			
			for(Meal_Food food : mealFoods) {
				if(food.getFood_cal_set_curb() != null) {
					mCurrCurb +=  (float) food.getFood_cal_set_curb();
				}
				if(food.getFood_cal_set_protien() != null) {
					mCurrProtien += (float) food.getFood_cal_set_protien();
				}
				if(food.getFood_cal_set_fat() != null) {
					mCurrFat += (float) food.getFood_cal_set_fat();
				}
				if(food.getFood_cal_set_total() != null) {
					mCurrTotal += (float) food.getFood_cal_set_total();
				}				
				
			}			 
		}
		
		String set = "সর্বমোট পুষ্টিগুণ: <font color='#ffffff'><big><b>" + BengaliUnicodeString.convertToBengaliText(getActivity(), getFormatDisplayValue(Math.round(mCurrTotal))) + "</b></big></font> ক্যালরি";
		mViewTotalSet.setText(Html.fromHtml(set), TextView.BufferType.SPANNABLE);
		
		BarData data = new BarData(getXAxisValues(), getDataSet());
		mBarchart.setData(data);
//		mBarchart.animateXY(2000, 2000);
		mBarchart.invalidate();
		
//		Log.d("CalMeter", "MinReqTotal : " + mMinReqTotal + "CurrTotal : " + mCurrTotal + 
//				"MinReqCurb : " + mMinReqCurb + "CurrCurb : " + mCurrCurb +
//				"MinReqProtien : " + mMinReqProtien + "CurrProtien : " + mCurrProtien +
//				"MinReqFat : " + mMinReqFat + "CurrFat : " + mCurrFat);
		
	}

	public String getFormatDisplayValue(float n) {
		String number = null;
		if(n == (int) n) {
			number = (int) n + "";
		} else number = n + "";

		return BengaliUnicodeString.convertToBengaliText(getActivity(), number);				
	}

	public void saveMeal(String listName) {
		MealDao mealDao = mSession.getMealDao();
		Meal meal = new Meal(null, listName, null, mCurrCurb, mCurrProtien, mCurrFat, mCurrTotal,
				mMinReqCurb, mMinReqProtien, mMinReqFat, mMinReqTotal, new Date(System.currentTimeMillis()), false);
		
		mealDao.insert(meal);
		
		Meal_FoodDao mealFoodDao = mSession.getMeal_FoodDao();
		List<Meal_Food> mealFoods = meal.getMealFoods();
		
		Set<String> keySets  = mMealMap.keySet();
		
		for(String s : keySets) {
			List<Meal_Food> catMealFoods = mMealMap.get(s);
			for(Meal_Food catMealFood : catMealFoods) {
				catMealFood.setMeal_id(meal.getId());
				mealFoods.add(catMealFood);
			}
		}
		for(Meal_Food mealFood : mealFoods) {
			mealFoodDao.insert(mealFood);
		}
		
	}

}