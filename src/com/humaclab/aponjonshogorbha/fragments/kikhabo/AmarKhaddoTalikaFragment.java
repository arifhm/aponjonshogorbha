package com.humaclab.aponjonshogorbha.fragments.kikhabo;

import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.humaclab.aponjon.DaoSession;
import com.humaclab.aponjon.Meal;
import com.humaclab.aponjon.MealDao;
import com.humaclab.aponjonshogorbha.AponjonApplication;
import com.bd.aponjon.pregnancy.R;
import com.humaclab.aponjonshogorbha.activities.kikhabo.AmarKhaddoTalikaDetailsActivity;
import com.humaclab.aponjonshogorbha.adapters.kikhabo.AmarKhaddoTalikaAdapter;
import com.humaclab.aponjonshogorbha.adapters.kikhabo.AmarKhaddoTalikaAdapter.OnItemClickListener;

public class AmarKhaddoTalikaFragment extends Fragment {
	
	RecyclerView mRecyclerView;
	AmarKhaddoTalikaAdapter mAdapter;
	List<Meal> mSavedMeals;
	AponjonApplication mApp;
	TextView mNoListView;
	
	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("আমার খাদ্য তালিকা");
	}
	
	@Override
	@Nullable
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.layout_fragment_amar_khddo_talika, container, false);
		
		mNoListView = (TextView) v.findViewById(R.id.view_no_meal_list);
		
		mApp = AponjonApplication.getInstance();
		DaoSession daoSession = mApp.getDaoSession();
		
		MealDao mealDao = daoSession.getMealDao();
		mSavedMeals = mealDao.queryBuilder().list();
		
		if(mSavedMeals.isEmpty()) {
			mNoListView.setVisibility(View.VISIBLE);
		}
		
		mRecyclerView = (RecyclerView) v.findViewById(R.id.recyclerView_amar_khaddo_talika);
		mAdapter = new AmarKhaddoTalikaAdapter(getActivity(), mSavedMeals); 
		
		return v;
	}
	
	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		mRecyclerView.setAdapter(mAdapter);
		mRecyclerView.setHasFixedSize(true);
		mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
		mRecyclerView.setItemAnimator(new DefaultItemAnimator());
		mAdapter.SetOnItemClickListener(new OnItemClickListener() {
			
			@Override
			public void onItemClick(View view, int position) {
//				Toast.makeText(getActivity(), "Clicked : " + position + ", View ID: " +  view.getId(), Toast.LENGTH_SHORT).show();
				Intent i = new Intent(getActivity(), AmarKhaddoTalikaDetailsActivity.class);
				i.putExtra(AmarKhaddoTalikaDetailsFragment.MEAL_ID, (long) mSavedMeals.get(position).getId());
				i.putExtra(AmarKhaddoTalikaDetailsFragment.MEAL_NAME, mSavedMeals.get(position).getMeal_name());
				startActivity(i);
			}
		});
	}
	
}
