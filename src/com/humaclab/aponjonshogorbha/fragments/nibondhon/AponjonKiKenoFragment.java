package com.humaclab.aponjonshogorbha.fragments.nibondhon;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;

import com.bd.aponjon.pregnancy.R;
import com.humaclab.aponjonshogorbha.AppConfig;
import com.humaclab.aponjonshogorbha.fragments.AponjonNibondhonFragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.AssetManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebSettings.LayoutAlgorithm;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

public class AponjonKiKenoFragment extends Fragment {
	ViewGroup mDetailsBtn;
	TextView mNibondhonBtn;
	CheckBox mCheckAgree;
	
	
	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("আপনজন কি ও কেন");
	}
	
	@Override
	@Nullable
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		
		View v = inflater.inflate(R.layout.layout_fragment_aponjon_ki_keno, container, false);
		
		mDetailsBtn = (ViewGroup) v.findViewById(R.id.view_btn_bistarito);
		mNibondhonBtn = (TextView) v.findViewById(R.id.view_btn_nibondhon);
		mCheckAgree = (CheckBox) v.findViewById(R.id.checkbox_agree1);
		
		mDetailsBtn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				showDetailConfirmDialog();
			}
		});
		
		mNibondhonBtn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(mCheckAgree.isChecked()) {
					Intent httpIntent = new Intent(Intent.ACTION_VIEW);
					httpIntent.setData(Uri.parse(AppConfig.URL_APONJON_NIBONDHON));
					startActivity(httpIntent);   
//					FragmentManager fm = getActivity().getSupportFragmentManager();
//					Fragment fragment = new NibondhonWebViewFragment();
//					fm.beginTransaction().replace(R.id.nav_contentframe, fragment).commit();
				} else {
					Toast.makeText(getActivity(), "আপনি রাজি হলে চেকবক্সটিতে টিক দিন", Toast.LENGTH_SHORT).show();
				}
			}
		});
		
		return v;
	}
	
	
	public void showDetailConfirmDialog(){
		AlertDialog.Builder alert = new AlertDialog.Builder(getActivity()); 
		alert.setTitle("আপনজন কি ও কেন (বিস্তারিত)");

		WebView htmlWebView = new WebView(getActivity());
		
		WebSettings webSetting = htmlWebView.getSettings();
		webSetting.setLayoutAlgorithm(LayoutAlgorithm.SINGLE_COLUMN);
				
        String htmlFilename = "ki_keno.html";
        AssetManager mgr = getActivity().getAssets();
        try {
            InputStream in = mgr.open(htmlFilename, AssetManager.ACCESS_BUFFER);
            String htmlContentInStringFormat = StreamToString(in);
            in.close();
            htmlWebView.loadDataWithBaseURL(null, htmlContentInStringFormat, "text/html", "utf-8", null);
 
        } catch (IOException e) {
            e.printStackTrace();
        }
				
		alert.setView(htmlWebView);
		alert.setPositiveButton("ঠিক আছে", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int id) {
				dialog.dismiss();
			}
		});
		alert.show();
	}
	
	public static String StreamToString(InputStream in) throws IOException {
        if(in == null) {
            return "";
        }
        Writer writer = new StringWriter();
        char[] buffer = new char[1024];
        try {
            Reader reader = new BufferedReader(new InputStreamReader(in, "UTF-8"));
            int n;
            while ((n = reader.read(buffer)) != -1) {
                writer.write(buffer, 0, n);
            }
        } finally {
        }
        return writer.toString();
    }
}
