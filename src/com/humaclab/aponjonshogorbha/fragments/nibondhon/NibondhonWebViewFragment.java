package com.humaclab.aponjonshogorbha.fragments.nibondhon;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;

import com.humaclab.aponjonshogorbha.AppConfig;
import com.bd.aponjon.pregnancy.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebSettings.LayoutAlgorithm;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

public class NibondhonWebViewFragment extends Fragment {
	WebView mView;
	
	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("আপনজনে নিবন্ধন");
	}
	
	@Override
	@Nullable
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		
		View v = inflater.inflate(R.layout.layout_fragment_nibondhon_webview, container, false);
		
		mView = (WebView) v.findViewById(R.id.webView_nibondhon);
		
		setupWebView();
		
		
		return v;
	}
	
	
	public void setupWebView(){		
		WebSettings webSetting = mView.getSettings();
		webSetting.setLayoutAlgorithm(LayoutAlgorithm.SINGLE_COLUMN);
		webSetting.setJavaScriptEnabled(true);
        mView.loadUrl(AppConfig.URL_APONJON_NIBONDHON);
	}
	
}
