package com.humaclab.aponjonshogorbha.fragments;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.humaclab.aponjon.Article;
import com.humaclab.aponjon.ArticleDao;
import com.humaclab.aponjon.DaoMaster;
import com.humaclab.aponjon.DaoMaster.DevOpenHelper;
import com.humaclab.aponjon.services.BackgroundService;
import com.humaclab.aponjonshogorbha.AppConfig;
import com.humaclab.aponjonshogorbha.AponjonApplication;
import com.humaclab.aponjonshogorbha.Constants;
import com.bd.aponjon.pregnancy.R;
import com.humaclab.aponjonshogorbha.SessionManager;
import com.humaclab.aponjonshogorbha.activities.DashboardActivity;
import com.humaclab.aponjonshogorbha.fetch.app.data.ContentFoodUpdate;
import com.humaclab.aponjonshogorbha.fetch.app.data.ContentUpdate;
import com.humaclab.aponjonshogorbha.fetch.app.data.PeriodicContentUpdate;
import com.humaclab.aponjonshogorbha.fetch.app.data.WeekSync;
import com.humaclab.aponjonshogorbha.userdata.download.CheckRegistered;
import com.humaclab.aponjonshogorbha.userdata.download.DownloadUserAllDataTask;

import com.humaclab.aponjonshogorbha.userdata.download.SystemTaskSync;
import com.humaclab.aponjon.DaoSession;
import com.humaclab.aponjon.User;
import com.humaclab.aponjon.UserDao;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class LoginFragment extends Fragment {
	SessionManager mSession;
	AponjonApplication mApp;
	SharedPreferences mPrefs;
	Editor mEditor;

	String mToken;
	String mLmp;

	Button mLoginBtn;
	EditText mEmailEditText;
	EditText mPassEditText;
	ImageView mImgViewBack;
	TextView mTextTitle;

	String mEmail;
	String mPassword;
	@Override
	@Nullable
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.layout_fragment_login, container, false);
		
		Constants.is_demo = false;
		mApp = AponjonApplication.getInstance();
		mPrefs = mApp.getSharedPreferences(Constants.AppData.PREF_NAME, Constants.AppData.PREF_MODE_PRIVATE);
		mEditor = mPrefs.edit();
		mSession = new SessionManager(getActivity());

		//		if(mSession.isLoggedIn()) {
		//			Intent i = new Intent(getActivity(), DrawerFragmentActivity.class);
		//			i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);           
		//            // Add new Flag to start new Activity
		//            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		//            startActivity(i);
		//			getActivity().finish();
		//		}

		mLoginBtn = (Button) v.findViewById(R.id.login_btn_submit);
		mLoginBtn.setOnClickListener(new View.OnClickListener() {			
			@Override
			public void onClick(View v) {
//				mEmail = "turin@gmail.com";
//				mPassword = "dell";
				
				if(mEmail != null && mPassword != null) {
					ExecutorService executor = Executors.newFixedThreadPool(2);
					Runnable worker = new LoginTask(AppConfig.URL_LOGIN);
					executor.execute(worker);
				} else {
					Toast.makeText(getActivity(), "Give all field input properly", Toast.LENGTH_SHORT).show();
					return;
				}
			}	
		});

		mEmailEditText = (EditText) v.findViewById(R.id.login_edit_text_email);
		mEmailEditText.addTextChangedListener(new TextWatcher() {		
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {				
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,	int after) {			
			}

			@Override
			public void afterTextChanged(Editable s) {
				mEmail = s.toString();
			}
		});

		mPassEditText = (EditText) v.findViewById(R.id.loggin_edit_text_password);
		mPassEditText.addTextChangedListener(new TextWatcher() {			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {				
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {				
			}

			@Override
			public void afterTextChanged(Editable s) {
				mPassword = s.toString();
			}
		});

		mImgViewBack = (ImageView) v.findViewById(R.id.back_btn);
		mImgViewBack.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				getActivity().finish();
			}
		});

		mTextTitle = (TextView) v.findViewById(R.id.title_text);
		mTextTitle.setText("পুরনো প্রোফাইলে লগইন ");
		pd = new ProgressDialog(getActivity());
		return v;
	}

	Handler handler = new Handler();
	ProgressDialog pd;
	public class LoginTask implements Runnable {
		String mUrlString;

		public LoginTask(String url) {
			this.mUrlString = url;
		}

		@Override
		public void run() {
			handler.post(new Runnable() {
				@Override
				public void run() {
					pd.show();
				}
			});
			login(mUrlString);
		}
	}

	public void login(String url) {
		AsyncHttpClient client = new AsyncHttpClient();
		// Http Request Params Object
		RequestParams params = new RequestParams();
		// params
		JSONObject reqObj = new JSONObject();
		try {
			/**
			 *	{	
			 *			"data":{
			 *						"email" : "test2@test.com", 
			 *						"password" : "test"
			 *				   }
			 *	}
			 */

			JSONObject reqObjData = new JSONObject();
			reqObjData.put("email", mEmail);
			reqObjData.put("password", mPassword);
			reqObj.put("data", reqObjData);

			params.put("data" , reqObj.toString());
		} catch (JSONException e) {
			e.printStackTrace();
		}      


		// Make Http call
		client.post(url, params, new AsyncHttpResponseHandler() {
			@Override
			public void onSuccess(String response) {
				processLogin(response);
			}
			// When error occured
			@Override
			public void onFailure(int statusCode, Throwable error, String content) {
				if (statusCode == 404) {
					handler.post(new Runnable() {
						@Override
						public void run() {
							Toast.makeText(getActivity(), "Requested resource not found", Toast.LENGTH_LONG).show();						
						}
					});
				} else if (statusCode == 500) {
					handler.post(new Runnable() {
						@Override
						public void run() {
							Toast.makeText(getActivity(), "Something went wrong at server end", Toast.LENGTH_LONG).show();						
						}
					});
				} else {
					handler.post(new Runnable() {
						@Override
						public void run() {
							Toast.makeText(getActivity(), "Unexpected Error! Device might not be connected to Internet]",
									Toast.LENGTH_LONG).show();						
						}
					});
				}
			}
		});
		
//		checkReg();
	}

	private void processLogin(String response) {
		/**
		 * {
				"request": {
								"data": {
											"email": "test2@test.com",
											"password": "test"
										}
						   },
				"result": {
								"User": {
											"id": "3",
											"name": "টেস্ট নাম",
											"email": "test2@test.com",
											"phone": "০১৭১৭১০১০১০",
											"lmp": "2015-04-21",
											"token" : "dfdfjdlfjfjsefeflndlldfjldjflfdlf dlfdl"
											"height_before_pregnancy" : "2.3" //in meter 
											"weight_before_pregnancy" : "45" // Kg
											"age" : ""
											"profile_photo_path" : "" //sd storage path
											"profile_photo" : "" // photo file name 
											"photo_url" : "" // base url  "profile_photo" + "photo_url" = Url of the photo saved in server 
											"type": "3",
											"status": "1",
											"token": "j3f1cDqh56W46knACql4iWogtY292"
										}
						 },
				"message": "Successfully logged in",
				"status_code": 200
		   }	
		 */
		try {
			JSONObject responseObj = new JSONObject(response);
			int statusCode = responseObj.getInt("status_code");
			final String msg = responseObj.getString("message");

			if(statusCode==200) {
				JSONObject resultObj = responseObj.getJSONObject("result");
				StringBuffer msb1 = new StringBuffer();
				msb1.append(resultObj.toString());
				try {
					File f = new File(Environment.getExternalStorageDirectory().getPath(), "resultUserInfo.txt");
					writeToFile(f, msb1);
				} catch (IOException e) {
					e.printStackTrace();
				}
				JSONObject userObj = resultObj.getJSONObject("User");

				String id = userObj.getString("id");
				String name = userObj.getString("name");
				String email = userObj.getString("email");
				String phone = userObj.getString("phone");
				mToken = userObj.getString("token");
				mLmp = userObj.getString("lmp");
				String height = userObj.getString("height_before_pregnancy");
				String weight = userObj.getString("weight_before_pregnancy");
				String age = userObj.getString("age");
				String photoPath = userObj.getString("profile_photo_path");
				String photoFileName = userObj.getString("profile_photo");
				String baseUrl = userObj.getString("photo_url");
				String photoUrl = null;
				if((!photoFileName.equalsIgnoreCase("null") || !photoFileName.equals("")) && (!baseUrl.equalsIgnoreCase("null") || !baseUrl.equals(""))) {
					photoUrl = photoUrl+photoFileName;
				}
								
				if(height.equalsIgnoreCase("") || height.equalsIgnoreCase("null")) height = null;
				if(weight.equalsIgnoreCase("") || weight.equalsIgnoreCase("null")) weight = null;
				if(age.equalsIgnoreCase("") || age.equalsIgnoreCase("null")) age = null;
				if(photoPath.equalsIgnoreCase("") || photoPath.equalsIgnoreCase("null")) photoPath = null; 

				SimpleDateFormat format = new SimpleDateFormat();
				format.applyPattern("yyyy-MM-dd");

				Date lmp = null;
				Date curDate = null;
				int dayPassed = 0;
				int currWeek = 0;
				try {
					lmp = format.parse(userObj.getString("lmp"));
					curDate = new Date(System.currentTimeMillis());

					dayPassed = (int)((curDate.getTime() - lmp.getTime()) / (1000*60*60*24l));
					if(dayPassed%7 == 0) {
						currWeek = dayPassed/7;
					} else currWeek = (dayPassed/7) + 1;

				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				String token = userObj.getString("token");

				mSession.createLoginSession(id, name, email, phone, mLmp, currWeek, token, height, weight, age, photoPath, photoUrl);

				//Process userObj to save in database
				DevOpenHelper helper = new DaoMaster.DevOpenHelper(getActivity(), "aponjon-db", null);
				SQLiteDatabase db = helper.getWritableDatabase();
				DaoMaster daomaster = new DaoMaster(db);
				DaoSession session = daomaster.newSession();

				//	DaoSession session = mApp.getDaoSession();

				UserDao userDao = session.getUserDao();
				userDao.deleteAll();
				userDao.insert(new User(null, Long.parseLong(id), token, name, email, phone, lmp, photoPath, null));
								
								
				CheckRegistered checkReg = new CheckRegistered(getActivity());
				checkReg.checkRegisterd();

				syncData();
				
				handler.post(new Runnable() {
					@Override
					public void run() {
						Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();
						if(pd.isShowing())pd.dismiss();
					}
				});
				
			} else {
				handler.post(new Runnable() {
					@Override
					public void run() {
						Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();
						if(pd.isShowing())pd.dismiss();
					}
				});
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}
	}


	
	
	@SuppressLint("NewApi")
	private void syncData() {
		if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
			// only for Honeycomb and newer versions
			new AsyncTask<Void, Void, Void>() {
				@Override
				protected void onPostExecute(Void result) {
					startActivity(new Intent(getActivity(), DashboardActivity.class));
					getActivity().finish();
				}
				@Override
				protected Void doInBackground(Void... params) {
					try {
//						PeriodicContentUpdate periodicTask = new PeriodicContentUpdate(getActivity());
//						periodicTask.updateContent();
						
						Intent startServiceIntent = new Intent(getActivity(), BackgroundService.class);
				        getActivity().startService(startServiceIntent);
						
						DownloadUserAllDataTask downAllData =new DownloadUserAllDataTask(getActivity());
						downAllData.runTasks();
					} catch (Exception e) {
						e.printStackTrace();
					}
					return null;				
				}
			}.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			new AsyncTask<Void, Void, Void>() {
				@Override
				protected void onPostExecute(Void result) {
					startActivity(new Intent(getActivity(), DashboardActivity.class));
					getActivity().finish();
				}
				@Override
				protected Void doInBackground(Void... params) {
					try {
//						PeriodicContentUpdate periodicTask = new PeriodicContentUpdate(getActivity());
//						periodicTask.updateContent();
						
						Intent startServiceIntent = new Intent(getActivity(), BackgroundService.class);
				        getActivity().startService(startServiceIntent);
						
						DownloadUserAllDataTask downAllData =new DownloadUserAllDataTask(getActivity());
						downAllData.runTasks();
					} catch (Exception e) {
						e.printStackTrace();
					}
					return null;				
				}
			}.execute();
		}
	}
	
	public static void writeToFile(File pFile, StringBuffer pData) throws IOException {  
        BufferedWriter out = new BufferedWriter(new FileWriter(pFile));  
        out.write(pData.toString());  
        out.flush();  
        out.close();  
    }
}
