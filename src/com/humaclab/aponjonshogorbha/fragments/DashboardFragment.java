package com.humaclab.aponjonshogorbha.fragments;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.opengl.Visibility;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidbangladesh.BengaliAutoCompleteText;

import com.dnet.mama.taskmanager.TaskManagerFragment;
import com.humaclab.aponjon.DaoMaster.DevOpenHelper;
import com.humaclab.aponjon.Weight;
import com.humaclab.aponjonshogorbha.AponjonApplication;
import com.humaclab.aponjonshogorbha.Constants;
import com.bd.aponjon.pregnancy.R;
import com.humaclab.aponjonshogorbha.SessionManager;
import com.humaclab.aponjonshogorbha.activities.AmarAmiActivity;
import com.humaclab.aponjonshogorbha.activities.AmarAmiActivityDemo;
import com.humaclab.aponjonshogorbha.activities.AmarJogotActivity;
import com.humaclab.aponjonshogorbha.activities.AmarJogotActivityDemo;
import com.humaclab.aponjonshogorbha.activities.AmarShonamoniActivity;
import com.humaclab.aponjonshogorbha.activities.AmarShonamoniActivityDemo;
import com.humaclab.aponjonshogorbha.activities.AponjonNibondhonActivity;
import com.humaclab.aponjonshogorbha.activities.AponjonNibondhonActivityDemo;
import com.humaclab.aponjonshogorbha.activities.DoctorLineActivity;
import com.humaclab.aponjonshogorbha.activities.DrawerFragmentActivity;
import com.humaclab.aponjonshogorbha.activities.KiKhaboActivity;
import com.humaclab.aponjonshogorbha.activities.KiKhaboActivityDemo;
import com.humaclab.aponjonshogorbha.activities.LoginActivity;
import com.humaclab.aponjonshogorbha.activities.RegisterActivity;
import com.humaclab.aponjonshogorbha.activities.ShishuKobeJonmabeActivity;
import com.humaclab.aponjonshogorbha.activities.ShishuKobeJonmabeActivityDemo;
import com.humaclab.aponjonshogorbha.activities.TaskManagerActivity;
import com.humaclab.aponjonshogorbha.activities.TaskManagerActivityDemo;
import com.humaclab.aponjonshogorbha.userdata.download.DownloadDiary;
import com.humaclab.aponjonshogorbha.userdata.download.DownloadMeal;
import com.humaclab.aponjonshogorbha.userdata.download.DownloadMealFood;
import com.humaclab.aponjonshogorbha.userdata.download.DownloadMove;
import com.humaclab.aponjonshogorbha.userdata.download.DownloadWeight;
import com.humaclab.aponjonshogorbha.userdata.download.SystemTaskSync;
import com.humaclab.aponjonshogorbha.userdata.upload.UploadCustomTask;
import com.humaclab.aponjonshogorbha.userdata.upload.UploadCustomTaskStatus;
import com.humaclab.aponjonshogorbha.userdata.upload.UploadDiary;
import com.humaclab.aponjonshogorbha.userdata.upload.UploadMealFood;
import com.humaclab.aponjonshogorbha.userdata.upload.UploadMove;
import com.humaclab.aponjonshogorbha.userdata.upload.UploadSystemTaskStatus;
import com.humaclab.aponjonshogorbha.userdata.upload.UploadUserAllDataTask;
import com.humaclab.aponjonshogorbha.userdata.upload.UploadWeight;

public class DashboardFragment extends Fragment implements OnClickListener{
	private AponjonApplication mApp;
	private ViewGroup mSignLogin;
	private TextView mLogin;
	private TextView mSignup;
	private SessionManager mSessionMgr;
	private boolean mAponjonReg;
	private ViewGroup mBtnNibondhon;
	private ViewGroup mBtnDoctorLine;
	
	// COMMIT ft_arif_fix branch
	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);		
		((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("আপনজন সগর্ভা");
	}


	@Override
	@Nullable
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.layout_fragment_dashboard, container, false);

		v.findViewById(R.id.db_btn_amar_ami).setOnClickListener(this);
		v.findViewById(R.id.db_btn_amar_shonamoni).setOnClickListener(this);
		v.findViewById(R.id.db_btn_shishu_kobe_jonmabe).setOnClickListener(this);
		v.findViewById(R.id.db_btn_kobe_ki_korbo).setOnClickListener(this);
		v.findViewById(R.id.db_btn_ki_khabo).setOnClickListener(this);
		v.findViewById(R.id.db_btn_amar_jogot).setOnClickListener(this);
		
		mBtnNibondhon = (ViewGroup) v.findViewById(R.id.db_btn_nibondhon);
		mBtnNibondhon.setOnClickListener(this);
		
		mBtnDoctorLine = (ViewGroup) v.findViewById(R.id.db_btn_doctor);
		mBtnDoctorLine.setOnClickListener(this);
		
		mSignLogin = (ViewGroup) v.findViewById(R.id.layout_demo_signup_login);
		mSignup = (TextView) v.findViewById(R.id.demo_view_sign_up);
		mLogin = (TextView) v.findViewById(R.id.demo_view_login);
		
		mApp = AponjonApplication.getInstance();
		mSessionMgr = new SessionManager(getActivity());
		SharedPreferences prefs = mSessionMgr.getPrefs();
		mAponjonReg = prefs.getBoolean(Constants.UserData.IS_APONJON_REG, false);
		
		if(mAponjonReg) {
			mBtnDoctorLine.setVisibility(View.VISIBLE);
			mBtnNibondhon.setVisibility(View.GONE);
		} else {
			mBtnDoctorLine.setVisibility(View.GONE);
			mBtnNibondhon.setVisibility(View.VISIBLE);
		}
		if(Constants.is_demo) {
			mSignLogin.setVisibility(View.VISIBLE);
		}
		
		mSignup.setOnClickListener(new View.OnClickListener() {			
			@Override
			public void onClick(View v) {
				startActivity(new Intent(getActivity(), RegisterActivity.class));
				getActivity().finish();
			}
		});
		
		mLogin.setOnClickListener(new View.OnClickListener() {			
			@Override
			public void onClick(View v) {
				startActivity(new Intent(getActivity(), LoginActivity.class));
				getActivity().finish();
			}
		});
		if(!Constants.is_demo) {
			BengaliAutoCompleteText search = (BengaliAutoCompleteText) v.findViewById(R.id.autoCompleteSearch);
			//		final Spinner searchDropdown = (Spinner)v.findViewById(R.id.autoCompleteSpinner);

			List<String> search_array = Constants.makeSearchArray();
			//		SearchAdapter adapter = new SearchAdapter(getActivity(), R.layout.auto_complete_textview, search_array);
			ArrayAdapter<String> adapter= new ArrayAdapter<String>(getActivity(), 
					android.R.layout.simple_dropdown_item_1line,search_array);
			search.setThreshold(1);
			search.setAdapter(adapter);
			//		searchDropdown.setAdapter(adapter);
//			writeDbToExternalStorage();
			search.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View view, int position,
						long arg3) {
					//				Toast.makeText(getActivity(), search_array.get(position), Toast.LENGTH_LONG).show();
					TextView view1 = (TextView) view.findViewById(android.R.id.text1);
					String str = view1.getText().toString();
					if(autocompleteCallback!=null)
						autocompleteCallback.onItemClicked(str);
				}
			});
		} else {
			((LinearLayout) v.findViewById(R.id.layout_search)).setVisibility(View.GONE);
		}

		return v;
	}

	public void writeDbToExternalStorage(){
		File file = getActivity().getDatabasePath(AponjonApplication.getDatabaseName());
		try {
			File out_file = new File(Environment.getExternalStorageDirectory()+"/aponjon-db");
			if(out_file.exists())
				out_file.delete();
			out_file.createNewFile();
			FileOutputStream out = new FileOutputStream(out_file);
			FileInputStream in = new FileInputStream(file);
			byte [] buffer = new byte[1024];
			int length;
			try {
				while((length=in.read(buffer))>0){
					out.write(buffer, 0, length);
				}
				out.flush();
				out.close();
				in.close();
			} catch (IOException e) {
				e.printStackTrace();
			}finally{
				//Toast.makeText(_context, file.getAbsolutePath(), Toast.LENGTH_LONG).show()
				;	
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			//	Toast.makeText(_context, e.toString(), Toast.LENGTH_LONG).show();
		} catch (IOException e1) {
			e1.printStackTrace();
		}finally{
			File out_file = new File(Environment.getExternalStorageDirectory()+"/aponjon-db");
		}
	}


	@Override
	public void onClick(View v) {
		setScreenView(v.getId());		
	}

	private void setScreenView(int id) {
//		FragmentManager fm = getActivity().getSupportFragmentManager();
//		Fragment fragment = fm.findFragmentById(R.id.nav_contentframe);
		
//		DrawerFragmentActivity.backPressCount = 0;
		switch (id) {
		case R.id.db_btn_amar_ami:
			//            Snackbar.make(mContentFrame, "Item One", Snackbar.LENGTH_SHORT).show();
			//            mCurrentSelectedPosition = 0;
			if(Constants.is_demo) {
				startActivity(new Intent(getActivity(), AmarAmiActivityDemo.class));
			} else startActivity(new Intent(getActivity(), AmarAmiActivity.class));
			
			break;
		case R.id.db_btn_amar_shonamoni:
			if(Constants.is_demo) {
				startActivity(new Intent(getActivity(), AmarShonamoniActivityDemo.class));
			} else startActivity(new Intent(getActivity(), AmarShonamoniActivity.class));
			
			break;
		case R.id.db_btn_shishu_kobe_jonmabe:
			if(Constants.is_demo) {
				startActivity(new Intent(getActivity(), ShishuKobeJonmabeActivityDemo.class));
			} else startActivity(new Intent(getActivity(), ShishuKobeJonmabeActivity.class));
			
			break;
		case R.id.db_btn_kobe_ki_korbo:
			if(Constants.is_demo) {
				startActivity(new Intent(getActivity(), TaskManagerActivityDemo.class));
			} else startActivity(new Intent(getActivity(), TaskManagerActivity.class));
			
			break;
		case R.id.db_btn_ki_khabo:
			if(Constants.is_demo) {
				startActivity(new Intent(getActivity(), KiKhaboActivityDemo.class));
			} else startActivity(new Intent(getActivity(), KiKhaboActivity.class));
			
			break;
		case R.id.db_btn_amar_jogot:
			if(Constants.is_demo) {
				startActivity(new Intent(getActivity(), AmarJogotActivityDemo.class));
			} else startActivity(new Intent(getActivity(), AmarJogotActivity.class));
			
			break;
		case R.id.db_btn_nibondhon:	
			if(Constants.is_demo) {
				startActivity(new Intent(getActivity(), AponjonNibondhonActivityDemo.class));
			} else {
				startActivity(new Intent(getActivity(), AponjonNibondhonActivity.class));
//		    	//FOODS
//				UploadMealFood upMealFood = new UploadMealFood(getActivity());
//				upMealFood.Upload();

			}  
			break;
		case R.id.db_btn_doctor:
			startActivity(new Intent(getActivity(), DoctorLineActivity.class));
		default:
			break;
		}
		
//		if(fragment == null) {
//			fm.beginTransaction().add(R.id.nav_contentframe, fragment).commit();
//		} else {
//			fm.beginTransaction().replace(R.id.nav_contentframe, fragment).commit();
//		}
		
	}
	

	
	public interface AutoCompleteItemClicked{
		public void onItemClicked(String str);
	}
	
	private AutoCompleteItemClicked autocompleteCallback;
	public void setOnAutoCompleteItemClickListener(AutoCompleteItemClicked itemCallback){
		this.autocompleteCallback = itemCallback;
	}
}