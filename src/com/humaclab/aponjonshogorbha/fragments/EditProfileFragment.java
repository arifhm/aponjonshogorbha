package com.humaclab.aponjonshogorbha.fragments;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import bengali.language.support.BengaliUnicodeString;

import com.humaclab.aponjonshogorbha.Constants;
import com.humaclab.aponjonshogorbha.ImageUploadTask;
import com.humaclab.aponjonshogorbha.ProfileUpdateTask;
import com.bd.aponjon.pregnancy.R;
import com.humaclab.aponjonshogorbha.SessionManager;
import com.humaclab.aponjonshogorbha.adapters.editprofile.AgeAdapter;
import com.humaclab.aponjonshogorbha.adapters.editprofile.FootAdapter;
import com.humaclab.aponjonshogorbha.adapters.editprofile.InchAdapter;
import com.humaclab.aponjonshogorbha.adapters.editprofile.WeightAdapter;

import com.humaclab.aponjonshogorbha.fragments.dialogs.CaptureSrcPrompt;
import com.humaclab.aponjonshogorbha.fragments.dialogs.DatePickerFragment;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class EditProfileFragment extends Fragment implements OnClickListener{
	private static final String DIALOG_DATE = "date";
	private int REQUEST_DATE = 0;
	private int REQUEST_TAKE_PHOTO = 1;
	private int PICK_IMAGE_REQUEST = 2;
	
	RelativeLayout mRelativeParent;
	
	EditText mEditEmail;
	ViewGroup mLayoutLabelEmail;
	TextView mViewLabelEmail;
	ImageButton mBtnEditEmail;
	
	EditText mEditPsss1;
	EditText mEditPass2;
	ViewGroup mLayoutPassword;
	Button mBtnEditPass;
	
	EditText mEditPhone;
	ViewGroup mLayoutLabelPhone;
	TextView mViewLabelPhone;
	ImageButton mBtnEditPhone;
	
	TextView mDateButton;
	ViewGroup mLayoutLabelLmp;
	TextView mViewLabelLmp;
	ImageButton mBtnEditLmp;
	
	ViewGroup mLayoutHeight;
	Spinner mFeet;	
	Spinner mInch;
	ViewGroup mLayoutLabelHeight;
	TextView mViewLabelHeight;
	ImageButton mBtnEditHeight;
	
	ViewGroup mHorizontalBorder2;
	
	Spinner mWeight;
	ViewGroup mLayoutLabelWeight;
	TextView mViewLabelWeight;
	ImageButton mBtnEditWeight;
	
	Spinner mAge;
	ViewGroup mLayoutLabelAge;
	TextView mViewLabelAge;
	ImageButton mBtnEditAge;
	
	Button mSubmit;
	SessionManager sessionMgr;
	SharedPreferences prefs;
	Editor editor;
	Bitmap mBitmap;

	CircleImageView mProfileImg;
	TextView mName;
	private File profImgFile;

	String mCurrentPhotoPath;
	String mPhotoLink;
	File photoFile = null;

	String _email;
	String _pass1;
	String _pass2;
	String _phone;
	Date _menstrual_date;
	int _ft;
	int _inch;
	float _height;
	int _weight;
	int _age;
	
	boolean validPass = false;
	boolean validEmail = false;
	boolean validPhone = false;
	boolean validLmp = false;
	boolean validHwa = false;
	boolean validPhotoPath = false;

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("এডিট প্রোফাইল");
	}

	@Override
	@Nullable
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

		View v = inflater.inflate(R.layout.layout_fragment_edit_profile, container, false);
		
		sessionMgr = new SessionManager(getActivity());
		prefs = sessionMgr.getPrefs();
		editor = prefs.edit();
		
		mProfileImg = (CircleImageView) v.findViewById(R.id.profile_image);
		mName = (TextView) v.findViewById(R.id.text_view_title_name);
	
		String name = prefs.getString(Constants.UserData.USER_NAME, "");
		mName.setText(name);
		
		mCurrentPhotoPath = prefs.getString(Constants.UserData.USER_IMG_SD_PATH, null);
		mPhotoLink = prefs.getString(Constants.UserData.USER_IMG_LINK, null);
		if(mCurrentPhotoPath!=null) {
			profImgFile = new File(mCurrentPhotoPath);
		}
		
		if(profImgFile!=null && profImgFile.exists()) {
			Bitmap bitmap = BitmapFactory.decodeFile(profImgFile.getAbsolutePath());
			Matrix mtx = new Matrix();
			mtx.postRotate(270);
			// Rotating Bitmap
			Bitmap rotatedBMP = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), mtx, true);
			mProfileImg.setImageBitmap(rotatedBMP);
		} else if(mPhotoLink!=null) {
//			Bitmap bmp = null;
//			try {				
//				bmp = Picasso.with(getActivity()).load(mPhotoLink).get();
//			} catch (IOException e) {
//				e.printStackTrace();
//			}
////			Picasso.with(getActivity()).load(mPhotoLink).into(mProfileImg);
//			mProfileImg.setImageBitmap(bmp);
		}
		
		v.findViewById(R.id.image_view_camera).setOnClickListener(this);		
				
		mRelativeParent = (RelativeLayout) v.findViewById(R.id.relative_layout_parent);
		mSubmit = (Button) v.findViewById(R.id.edit_profile_btn_submit);
		
		mEditEmail = (EditText) v.findViewById(R.id.edit_profile_email);
		mEditEmail.addTextChangedListener(new TextWatcher() {	
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {				
			}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {				
			}
			@Override
			public void afterTextChanged(Editable s) {
				_email = s.toString();
			}
		});
		String email = prefs.getString(Constants.UserData.USER_EMAIL, null);
		mLayoutLabelEmail = (ViewGroup) v.findViewById(R.id.linear_view_email);
		mViewLabelEmail = (TextView) v.findViewById(R.id.text_view_email);
		mBtnEditEmail = (ImageButton) v.findViewById(R.id.btn_view_edit_email);
		mViewLabelEmail.setText("ইমেইল: " + email);
		mBtnEditEmail.setOnClickListener(new View.OnClickListener() {			
			@Override
			public void onClick(View v) {
				mLayoutLabelEmail.setVisibility(View.INVISIBLE);
				mEditEmail.setVisibility(View.VISIBLE);
				if (android.os.Build.VERSION.SDK_INT > android.os.Build.VERSION_CODES.HONEYCOMB) {
					mEditEmail.setAlpha(0.0f);
					mEditEmail.animate().alpha(1.0f).setDuration(500);
				}
				mSubmit.setVisibility(View.VISIBLE);
			}
		});
		
		mEditPsss1 = (EditText) v.findViewById(R.id.edit_profile_pass1);
		mEditPsss1.addTextChangedListener(new TextWatcher() {	
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {				
			}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {				
			}
			@Override
			public void afterTextChanged(Editable s) {		
				_pass1 = s.toString();
			}
		});
		mEditPass2 = (EditText) v.findViewById(R.id.edit_profile_pass2);
		mEditPass2.addTextChangedListener(new TextWatcher() {	
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {				
			}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {				
			}
			@Override
			public void afterTextChanged(Editable s) {
				_pass2 = s.toString();
			}
		});
		mLayoutPassword = (ViewGroup) v.findViewById(R.id.layout_password);
		mBtnEditPass = (Button) v.findViewById(R.id.btn_view_edit_password);
		mEditPhone = (EditText) v.findViewById(R.id.edit_profile_phone);
		mLayoutLabelPhone = (ViewGroup) v.findViewById(R.id.linear_view_phone);
		mViewLabelPhone = (TextView) v.findViewById(R.id.text_view_phone);
		mBtnEditPhone = (ImageButton) v.findViewById(R.id.btn_view_edit_phone);
		mBtnEditPass.setOnClickListener(new View.OnClickListener() {			
			@Override
			public void onClick(View v) {
				RelativeLayout.LayoutParams editPhoneParams = (RelativeLayout.LayoutParams) mEditPhone.getLayoutParams();
				editPhoneParams.addRule(RelativeLayout.BELOW, R.id.layout_password);
				mEditPhone.setLayoutParams(editPhoneParams);
				RelativeLayout.LayoutParams labelPhoneParams = (RelativeLayout.LayoutParams) mLayoutLabelPhone.getLayoutParams();
				labelPhoneParams.addRule(RelativeLayout.BELOW, R.id.layout_password);
				mLayoutLabelPhone.setLayoutParams(labelPhoneParams);
				mBtnEditPass.setVisibility(View.INVISIBLE);
				mLayoutPassword.setVisibility(View.VISIBLE);
				mRelativeParent.invalidate();
				if (android.os.Build.VERSION.SDK_INT > android.os.Build.VERSION_CODES.HONEYCOMB) {
					mLayoutPassword.setAlpha(0.0f);
					mLayoutPassword.animate().alpha(1.0f).setDuration(500);
				}
				mSubmit.setVisibility(View.VISIBLE);
			}
		});

		mEditPhone.addTextChangedListener(new TextWatcher() {	
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {				
			}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {				
			}
			@Override
			public void afterTextChanged(Editable s) {
				_phone = s.toString();
			}
		});
		String phone = prefs.getString(Constants.UserData.USER_PHONE, null);
		mViewLabelPhone.setText("ফোন: " + phone);
		mBtnEditPhone.setOnClickListener(new View.OnClickListener() {			
			@Override
			public void onClick(View v) {
				mLayoutLabelPhone.setVisibility(View.INVISIBLE);
				mEditPhone.setVisibility(View.VISIBLE);
				mRelativeParent.invalidate();
				if (android.os.Build.VERSION.SDK_INT > android.os.Build.VERSION_CODES.HONEYCOMB) {
					mEditPhone.setAlpha(0.0f);
					mEditPhone.animate().alpha(1.0f).setDuration(500);
				}
				mSubmit.setVisibility(View.VISIBLE);
			}
		});
		
		
		mSubmit.setOnClickListener(new View.OnClickListener() {			
			@Override
			public void onClick(View v) {				
				if(_pass1 != null && _pass2 != null) {
					if(!_pass1.equals(_pass2)) {
						Toast.makeText(getActivity(),"আপনার দেয়া পাসওয়ার্ড ২ টি এক নয়", Toast.LENGTH_SHORT).show();
						return;
					} else validPass = true;
				}
				if(_email!=null) {
					if(!isEmailValid(_email)) {
						Toast.makeText(getActivity(), "একটি সঠিক ই-মেইল প্রদান করুন", Toast.LENGTH_SHORT).show();
						return;
					} else validEmail = true;
				}
				if(_menstrual_date != null) {
					validLmp = true;
				}
				if(_phone!=null) {
					validPhone = true;
				}
				if(_ft> 0 || _weight > 0 || _age > 0) {
					if(!(_ft > 0)) {
						Toast.makeText(getActivity(), "আপনার উচ্চতা প্রদান করুন", Toast.LENGTH_SHORT).show();
						return;
					} else if (!(_weight > 0)) {
						Toast.makeText(getActivity(), "আপনার ওজন প্রদান করুন", Toast.LENGTH_SHORT).show();
						return;
					} else if (!(_age > 0)) {
						Toast.makeText(getActivity(), "আপনার বয়স প্রদান করুন", Toast.LENGTH_SHORT).show();
						return;
					} else {
						_height = ((_ft * 12) + _inch) * 2.54f;
						validHwa = true;
						sessionMgr.setHWAtoPreff(_height, _weight, _age);
					}
				}
				String prefsPhotoPath = prefs.getString(Constants.UserData.USER_IMG_SD_PATH, null);
				if(mCurrentPhotoPath != null && mCurrentPhotoPath.equals("") != true && mCurrentPhotoPath.equals(prefsPhotoPath) == false) {
					validPhotoPath = true;
				}
				
				if(validEmail == true || validPass == true || validPhone == true || validLmp == true || validHwa == true || validPhotoPath == true || mBitmap != null) {
					if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
						// only for Honeycomb and newer versions
						new AsyncTask<Void, Void, Void>() {
							@Override
							protected void onProgressUpdate(Void... values) {
								super.onProgressUpdate(values);
							}
							@Override
							protected void onPostExecute(Void result) {
								Toast.makeText(getActivity(), "Update request executed succesfully", Toast.LENGTH_SHORT).show();
							}
							@Override
							protected Void doInBackground(Void... params) {
								try {
									ProfileUpdateTask update = new ProfileUpdateTask(getActivity(), _email, _pass1, _phone, 
											_menstrual_date, _height, _weight, _age, mCurrentPhotoPath);
									update.startProfileUpdate();
									
									//photo upload
									if(mBitmap != null) {
										try {
											sendPhoto(mBitmap);
										} catch (Exception e) {
											e.printStackTrace();
										}
									}
								} catch (Exception e) {
									e.printStackTrace();
								}
								return null;				
							}
						}.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
					} else {
						new AsyncTask<Void, Void, Void>() {
							protected void onProgressUpdate(Void... values) {
								super.onProgressUpdate(values);
							}
							@Override
							protected void onPostExecute(Void result) {
								Toast.makeText(getActivity(), "Update request executed succesfully", Toast.LENGTH_SHORT).show();
							}
							@Override
							protected Void doInBackground(Void... params) {
								try {
									ProfileUpdateTask update = new ProfileUpdateTask(getActivity(), _email, _pass1, _phone, 
											_menstrual_date, _height, _weight, _age, mCurrentPhotoPath);
									update.startProfileUpdate();
									
									//photo upload
									if(mBitmap != null) {
										try {
											sendPhoto(mBitmap);
										} catch (Exception e) {
											e.printStackTrace();
										}
									}
								} catch (Exception e) {
									e.printStackTrace();
								}
								return null;				
							}
						}.execute();
					}

				} else {
					Toast.makeText(getActivity(), "আপনি কোনো তথ্য প্রদান করেননি !", Toast.LENGTH_SHORT).show();
					return;
				}		
			}
		});
		
		mFeet = (Spinner) v.findViewById(R.id.text_view_picker_ft);
		mInch = (Spinner) v.findViewById(R.id.text_view_picker_inch);
		mWeight = (Spinner) v.findViewById(R.id.text_view_picker_weight);
		mAge = (Spinner) v.findViewById(R.id.text_view_picker_age);
		
		FootAdapter foot_adapter = new FootAdapter(getActivity(), R.layout.layout_spinner_hwa_dropdown, prepareFeets());
		mFeet.setAdapter(foot_adapter);

		InchAdapter inch_adapter = new InchAdapter(getActivity(), R.layout.layout_spinner_hwa_dropdown, prepareInchs());
		mInch.setAdapter(inch_adapter);

		WeightAdapter weight_adapter = new WeightAdapter(getActivity(), R.layout.layout_spinner_hwa_dropdown, prepareWeights());
		mWeight.setAdapter(weight_adapter);

		AgeAdapter age_adapter = new AgeAdapter(getActivity(), R.layout.layout_spinner_hwa_dropdown, prepareAges());
		mAge.setAdapter(age_adapter);

		mFeet.setOnItemSelectedListener(foot_item_listener);
		mInch.setOnItemSelectedListener(inch_item_listener);
		mWeight.setOnItemSelectedListener(weight_item_listener);
		mAge.setOnItemSelectedListener(age_item_listener);
		
		float height = prefs.getFloat(Constants.UserData.USER_HEIGHT, 0f);
		int inch = (int) Math.round((height * 0.393701));
		int feet = (int) Math.floor(inch/12);
		inch = inch % 12;
		
		mLayoutHeight = (ViewGroup) v.findViewById(R.id.layout_height);
		mLayoutLabelHeight = (ViewGroup) v.findViewById(R.id.linear_view_height);
		mViewLabelHeight = (TextView) v.findViewById(R.id.text_view_height);
		mViewLabelHeight.setText((feet>0 ? BengaliUnicodeString.convertToBengaliText(getActivity(), feet+"") + " ফুট " : "") 
				+ (inch>0 ? BengaliUnicodeString.convertToBengaliText(getActivity(), inch+"") +" ইঞ্চি" : ""));
		mBtnEditHeight = (ImageButton) v.findViewById(R.id.btn_view_edit_height);
		mHorizontalBorder2 = (ViewGroup) v.findViewById(R.id.horizontal_border2);
		
		mBtnEditHeight.setOnClickListener(new View.OnClickListener() {			
			@Override
			public void onClick(View v) {
				RelativeLayout.LayoutParams horizontalBorder1Params = (RelativeLayout.LayoutParams) mHorizontalBorder2.getLayoutParams();
				horizontalBorder1Params.addRule(RelativeLayout.BELOW, R.id.layout_height);
				mHorizontalBorder2.setLayoutParams(horizontalBorder1Params);
				mLayoutLabelHeight.setVisibility(View.INVISIBLE);
				mLayoutHeight.setVisibility(View.VISIBLE);
				mRelativeParent.invalidate();
				if (android.os.Build.VERSION.SDK_INT > android.os.Build.VERSION_CODES.HONEYCOMB) {
					mLayoutHeight.setAlpha(0.0f);
					mLayoutHeight.animate().alpha(1.0f).setDuration(500);
				}
				mSubmit.setVisibility(View.VISIBLE);
			}
		});
		String weight = BengaliUnicodeString.convertToBengaliText(getActivity(), prefs.getInt(Constants.UserData.USER_WEIGHT, 0)+"")+ " কেজি";
		mLayoutLabelWeight = (ViewGroup) v.findViewById(R.id.linear_view_weight);
		mViewLabelWeight = (TextView) v.findViewById(R.id.text_view_weight);
		mViewLabelWeight.setText(weight);
		mBtnEditWeight = (ImageButton) v.findViewById(R.id.btn_view_edit_weight);
		mBtnEditWeight.setOnClickListener(new View.OnClickListener() {		
			@Override
			public void onClick(View v) {
				mLayoutLabelWeight.setVisibility(View.INVISIBLE);
				mWeight.setVisibility(View.VISIBLE);
				if (android.os.Build.VERSION.SDK_INT > android.os.Build.VERSION_CODES.HONEYCOMB) {
					mWeight.setAlpha(0.0f);
					mWeight.animate().alpha(1.0f).setDuration(500);
				}
				mSubmit.setVisibility(View.VISIBLE);
			}
		});
		
		String age = BengaliUnicodeString.convertToBengaliText(getActivity(), prefs.getInt(Constants.UserData.USER_AGE, 0)+"")+ " বছর";
		mLayoutLabelAge = (ViewGroup) v.findViewById(R.id.linear_view_age);
		mViewLabelAge = (TextView) v.findViewById(R.id.text_view_age);
		mViewLabelAge.setText(age);
		mBtnEditAge = (ImageButton) v.findViewById(R.id.btn_view_edit_age);
		
		mBtnEditAge.setOnClickListener(new View.OnClickListener() {		
			@Override
			public void onClick(View v) {
				mLayoutLabelAge.setVisibility(View.INVISIBLE);
				mAge.setVisibility(View.VISIBLE);
				if (android.os.Build.VERSION.SDK_INT > android.os.Build.VERSION_CODES.HONEYCOMB) {
					mAge.setAlpha(0.0f);
					mAge.animate().alpha(1.0f).setDuration(500);
				}
				mSubmit.setVisibility(View.VISIBLE);
			}
		});
		
		SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat format2 = new SimpleDateFormat("dd/MM/yyyy");
		String date = prefs.getString(Constants.UserData.USER_LMP, null);
		Date lmpDate = null;
		try {
			lmpDate = format1.parse(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String formatDateString = format2.format(lmpDate);
		mDateButton = (TextView) v.findViewById(R.id.edit_profile_date);
		mLayoutLabelLmp = (ViewGroup) v.findViewById(R.id.linear_view_lmp_date);
		mViewLabelLmp = (TextView) v.findViewById(R.id.text_view_lmp_date);
		mViewLabelLmp.setText("সর্বশেষ মাসিকের তারিখ: " + BengaliUnicodeString.convertToBengaliText(getActivity(), formatDateString));
		mBtnEditLmp = (ImageButton) v.findViewById(R.id.btn_view_edit_lmp_date);
		mBtnEditLmp.setOnClickListener(new View.OnClickListener() {		
			@Override
			public void onClick(View v) {
				mLayoutLabelLmp.setVisibility(View.INVISIBLE);
				mDateButton.setVisibility(View.VISIBLE);
				if (android.os.Build.VERSION.SDK_INT > android.os.Build.VERSION_CODES.HONEYCOMB) {
					mDateButton.setAlpha(0.0f);
					mDateButton.animate().alpha(1.0f).setDuration(500);
				}
				mSubmit.setVisibility(View.VISIBLE);
			}
		});
		//        updateDate();
		mDateButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {                	            	
				FragmentManager fm = getActivity()
						.getSupportFragmentManager();
				DatePickerFragment dialog = DatePickerFragment
						.newInstance(new Date(System.currentTimeMillis()));
				dialog.setTargetFragment(EditProfileFragment.this, REQUEST_DATE);
				dialog.show(fm, DIALOG_DATE);              	
			}
		});
		
		return v;
	}
	
	public boolean isEmailValid(String email)
	{
		String regExpn =
				"^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
						+"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
						+"[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
						+"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
						+"[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
						+"([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";

		CharSequence inputStr = email;

		Pattern pattern = Pattern.compile(regExpn,Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(inputStr);

		if(matcher.matches())
			return true;
		else
			return false;
	}

	public void updateDate(Date date) {
		_menstrual_date = date;
		SimpleDateFormat format = new SimpleDateFormat();
		format.applyPattern("yyyy-MM-dd");
		mDateButton.setText(format.format(date));
	}	

	public android.widget.AdapterView.OnItemSelectedListener foot_item_listener = new android.widget.AdapterView.OnItemSelectedListener() {
		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int position,
				long arg3) {
			if(position == 0) {
				_ft = 0;
			} else _ft = position + 3;
//			Toast.makeText(getActivity(), _ft, Toast.LENGTH_LONG).show();			
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			_ft = 0;
		}
	};

	public android.widget.AdapterView.OnItemSelectedListener inch_item_listener = new android.widget.AdapterView.OnItemSelectedListener() {

		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int position,
				long arg3) {
			if(position == 0) {
				_inch = 0;
			} else _inch = position;
//			Toast.makeText(getActivity(), _inch, Toast.LENGTH_LONG).show();
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			_inch = 0;
		}
	};
	public android.widget.AdapterView.OnItemSelectedListener weight_item_listener = new android.widget.AdapterView.OnItemSelectedListener() {

		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int position,
				long arg3) {
			if(position == 0) {
				_weight = 0;
			} else _weight = position + 39;
//			Toast.makeText(getActivity(), _weight, Toast.LENGTH_LONG).show();
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			_weight = 0;
		}
	};

	public android.widget.AdapterView.OnItemSelectedListener age_item_listener = new android.widget.AdapterView.OnItemSelectedListener() {

		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int position,
				long arg3) {
			if(position == 0) {
				_age = 0;
			} else _age = position + 17;
//			Toast.makeText(getActivity(), _age, Toast.LENGTH_LONG).show();
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			_age = 0;
		}
	};

	ArrayList<String> foots = new ArrayList<String>();
	ArrayList<String> inchs = new ArrayList<String>();
	ArrayList<String> weights = new ArrayList<String>();
	ArrayList<String> ages = new ArrayList<String>();

	public ArrayList<String> prepareFeets(){
		foots = new ArrayList<String>();
		for(int i=3;i<8;i++){
			if(i==3) {
				foots.add("");
			} else {
				foots.add(BengaliUnicodeString.convertToBengaliText(getActivity(), String.valueOf(i))+" ফুট");
			}
		}
		return foots;
	}

	public ArrayList<String> prepareInchs(){
		inchs = new ArrayList<String>();
		for(int i=0;i<=11;i++){
			if(i==0) {
				inchs.add("");
			} else {
				inchs.add(BengaliUnicodeString.convertToBengaliText(getActivity(), String.valueOf(i))+" ইঞ্চি");
			}
		}
		return inchs;
	}

	public ArrayList<String> prepareWeights(){
		weights = new ArrayList<String>();
		for(int i=39;i<121;i++){
			if(i==39) {
				weights.add("");
			} else {
				weights.add(BengaliUnicodeString.convertToBengaliText(getActivity(), String.valueOf(i))+" কেজি");
			}
		}
		return weights;
	}

	public ArrayList<String> prepareAges(){
		ages = new ArrayList<String>();
		for(int i=17;i<56;i++){
			if(i==17) {
				ages.add("");
			} else {
				ages.add(BengaliUnicodeString.convertToBengaliText(getActivity(), String.valueOf(i))+" বছর");
			}
		}
		return ages;
	}



	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.image_view_camera :
			initPhotoCapture();
			break;
		case R.id.reg_text_view_date :
			break;
		default:
			break;
		}
	}


	public void initPhotoCapture() {
		CaptureSrcPrompt srcPrompt = CaptureSrcPrompt.getInstance();

		srcPrompt.setOnClickListener(new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int resultCode) {
				if(resultCode == 0) {
					dispatchTakePictureIntent();									
				} else if (resultCode == 1 ) {
					//					Toast.makeText(getActivity(), "Gallery", Toast.LENGTH_SHORT).show();
					Intent intent = new Intent();
					// Show only images, no videos or anything else				
					intent.setType("image/*");
					intent.setAction(Intent.ACTION_GET_CONTENT);
					// Always show the chooser (if there are multiple options available)
					startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);

				} else if (resultCode == 2 ) {
					dialog.dismiss();
				}
				dialog.dismiss();
			}
		});

		Fragment fragmentByTag = getActivity().getSupportFragmentManager().findFragmentByTag(CaptureSrcPrompt.D_TAG);//avoid opening twice dialog  
		if (fragmentByTag == null)  
			srcPrompt.show(getActivity().getSupportFragmentManager(), CaptureSrcPrompt.D_TAG);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode==PICK_IMAGE_REQUEST && resultCode == Activity.RESULT_OK && data != null && data.getData() != null) {

			Uri uri = data.getData();

			try {
				Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
				
				File file = createImageFile();
				FileOutputStream fos = null;
	             try {
	                 fos = new FileOutputStream(file);
	                 bitmap.compress(Bitmap.CompressFormat.PNG,70, fos);

	                 fos.flush();
	                 fos.close();
	              //   MediaStore.Images.Media.insertImage(getContentResolver(), b, "Screen", "screen");
	             }catch (FileNotFoundException e) {
	                 e.printStackTrace();
	             } catch (Exception e) {
	                 e.printStackTrace();
	             }
	             setPic();
//				mProfileImg.setImageBitmap(bitmap);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else if (requestCode == REQUEST_TAKE_PHOTO && resultCode == Activity.RESULT_OK) {
			setPic();
		} else if(requestCode == REQUEST_DATE) {	    	
			Date date = (Date)data.getSerializableExtra(DatePickerFragment.EXTRA_DATE);
			updateDate(date);			
		}
	}


	private void dispatchTakePictureIntent() {
		Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		takePictureIntent.putExtra("android.intent.extras.CAMERA_FACING", Camera.CameraInfo.CAMERA_FACING_FRONT);
		// Ensure that there's a camera activity to handle the intent
		if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
			// Create the File where the photo should go
			File photoFile = null;
			try {
				photoFile = createImageFile();
			} catch (IOException ex) {
				// Error occurred while creating the File
			}
			// Continue only if the File was successfully created
			if (photoFile != null) {
				takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
						Uri.fromFile(photoFile));
				startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
			}
		}
	}

	private File createImageFile() throws IOException {
		// Create an image file name
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
		String imageFileName = "JPEG_" + timeStamp + "_";
		String storageDir = Environment.getExternalStorageDirectory() + "/picUpload";
		File dir = new File(storageDir);
		if (!dir.exists())
			dir.mkdir();

		File image = new File(storageDir + "/" + imageFileName + ".jpg");

		// Save a file: path for use with ACTION_VIEW intents
		mCurrentPhotoPath = image.getAbsolutePath();
		editor.putString(Constants.UserData.USER_IMG_SD_PATH, mCurrentPhotoPath);
		editor.commit();
		//	    Log.i("EditProfileFragment", "photo path = " + mCurrentPhotoPath);
		return image;
	}

	private void setPic() {
		// Get the dimensions of the View
		int targetW = mProfileImg.getWidth();
		int targetH = mProfileImg.getHeight();

		// Get the dimensions of the bitmap
		BitmapFactory.Options bmOptions = new BitmapFactory.Options();
		bmOptions.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
		int photoW = bmOptions.outWidth;
		int photoH = bmOptions.outHeight;

		// Determine how much to scale down the image
		int scaleFactor = Math.min(photoW/targetW, photoH/targetH);

		// Decode the image file into a Bitmap sized to fill the View
		bmOptions.inJustDecodeBounds = false;
		bmOptions.inSampleSize = scaleFactor << 1;
		bmOptions.inPurgeable = true;

		Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
		
		ExifInterface exif = null;
		try {
			exif = new ExifInterface(mCurrentPhotoPath);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  
		String orientString = exif.getAttribute(ExifInterface.TAG_ORIENTATION);
		int orientation = orientString != null ? Integer.parseInt(orientString) :  ExifInterface.ORIENTATION_NORMAL;

		int rotationAngle = 0;
		if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
			rotationAngle = 90;
		}
		if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
			rotationAngle = 180;
		}
		if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
			rotationAngle = 270;
		}
		Matrix mtx = new Matrix();
		mtx.postRotate(rotationAngle);
		
		// Rotating Bitmap
		Bitmap rotatedBMP = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), mtx, true);

		if (rotatedBMP != bitmap)
			bitmap.recycle();

		mProfileImg.setImageBitmap(rotatedBMP);
		mSubmit.setVisibility(View.VISIBLE);
		mBitmap = rotatedBMP;

	}
	
	private void sendPhoto(Bitmap bitmap) {
		new ImageUploadTask(mCurrentPhotoPath, getActivity()).execute(bitmap);
	}


}
