package com.humaclab.aponjonshogorbha.fragments;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import bengali.language.support.BengaliUnicodeString;

import com.humaclab.aponjonshogorbha.Constants;
import com.bd.aponjon.pregnancy.R;
import com.humaclab.aponjonshogorbha.adapters.shishukobejonmabe.DayAdapter;
import com.humaclab.aponjonshogorbha.adapters.shishukobejonmabe.MonthAdapter;
import com.humaclab.aponjonshogorbha.adapters.shishukobejonmabe.YearAdapter;
import com.humaclab.aponjonshogorbha.fragments.dialogs.CaptureSrcPrompt;
import com.humaclab.aponjonshogorbha.fragments.dialogs.DemoModeDialog;
import com.humaclab.aponjonshogorbha.fragments.dialogs.JekonoShishuKobeJonmabeDialog;

public class ShishuKobeJonmabeFragment extends Fragment {
	TextView mBirthDate;
	Spinner mDate;
	Spinner mMonth;
	Spinner mYear;
	Button mSubmit;
	TextView mTitle;

	int selDate;
	int selMonth;
	int selYear;

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("শিশু কবে জন্মাবে");
	}
	
	@Override
	@Nullable
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

		View v = inflater.inflate(R.layout.layout_fragment_shishu_kobe_jonmabe, container, false);
		
		
		mTitle = (TextView) v.findViewById(R.id.text_view_title1);
		mBirthDate = (TextView) v.findViewById(R.id.text_view_title2);
		mDate = (Spinner) v.findViewById(R.id.text_view_picker_day);
		mMonth = (Spinner) v.findViewById(R.id.text_view_picker_month);
		mYear = (Spinner) v.findViewById(R.id.text_view_picker_year);
		mSubmit = (Button) v.findViewById(R.id.reg_btn_submit);
		
		if(Constants.is_demo) {
			mTitle.setText("আপনার শিশু জন্মাবার তারিখ \n নির্ধারন করতে সর্বশেষ \n মাসিকের তারিখ নিচে লিখুন");
		} else {
			mTitle.setText("আপনার শিশু জন্মাবার তারিখ");
			((TextView) v.findViewById(R.id.text_view_title3)).setText("যেকোনো শিশু জন্মাবার তারিখ নির্ধারন করতে \n সর্বশেষ মাসিকের তারিখ নিচে লিখুন");
		}

		SharedPreferences pref = getActivity().getSharedPreferences(Constants.AppData.PREF_NAME, Constants.AppData.PREF_MODE_PRIVATE);
		String lmp = pref.getString(Constants.UserData.USER_LMP, null);
		String token = pref.getString(Constants.UserData.USER_TOKEN, null);		
		
		if(lmp!=null) {
			SimpleDateFormat format = new SimpleDateFormat();
			format.applyPattern("yyyy-MM-dd");
			Date date = null;
			try {
				date = format.parse(lmp);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			String dob = BengaliUnicodeString.convertToBengaliText(getActivity(), getMonthString(getDateString(date)));
			mBirthDate.setText(dob);
		}
		
		

		DayAdapter day_adapter = new DayAdapter(getActivity(), R.layout.layout_spinner_dropdown, prepareDays());
		mDate.setAdapter(day_adapter);
		MonthAdapter month_adapter = new MonthAdapter(getActivity(), R.layout.layout_spinner_dropdown, prepareMonths());
		mMonth.setAdapter(month_adapter);
		YearAdapter year_adapter = new YearAdapter(getActivity(), R.layout.layout_spinner_dropdown, prepareYears());
		
		mYear.setAdapter(year_adapter);
		mDate.setOnItemSelectedListener(day_item_listener);
		mMonth.setOnItemSelectedListener(month_item_listener);
		mYear.setOnItemSelectedListener(year_item_listener);

		mSubmit.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if(Constants.is_demo) {
					DemoModeDialog dialog = new DemoModeDialog(getActivity());
					dialog.show();
				} else {
					showBirthDate();
				}
				

			}
		});

		return v;
	}

	public android.widget.AdapterView.OnItemSelectedListener day_item_listener = new android.widget.AdapterView.OnItemSelectedListener() {

		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int position,
				long arg3) {
			selDate = position+1;
//			Toast.makeText(getActivity(), days.get(position), Toast.LENGTH_LONG).show();			
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
	
		}
	};

	public android.widget.AdapterView.OnItemSelectedListener month_item_listener = new android.widget.AdapterView.OnItemSelectedListener() {

		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int position,
				long arg3) {
			selMonth = position;
//			Toast.makeText(getActivity(), months.get(position), Toast.LENGTH_LONG).show();

		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			
		}
	};
	
	public android.widget.AdapterView.OnItemSelectedListener year_item_listener = new android.widget.AdapterView.OnItemSelectedListener() {

		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int position,
				long arg3) {
			selYear = position+2014;
//			Toast.makeText(getActivity(), years.get(position), Toast.LENGTH_LONG).show();
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			
		}
	};

	ArrayList<String>months = new ArrayList<String>();
	ArrayList<String>years = new ArrayList<String>();
	ArrayList<String>days = new ArrayList<String>();
	
	public ArrayList<String> prepareYears(){
		years = new ArrayList<String>();
		for(int i=2014;i<=2099;i++){
			years.add(BengaliUnicodeString.convertToBengaliText(getActivity(), String.valueOf(i)));
		}
		return years;
	}
	
	public ArrayList<String> prepareMonths(){
		months = new ArrayList<String>();
		months.add("জানুয়ারী");
		months.add("ফেব্রুয়ারী");
		months.add("মার্চ");
		months.add("এপ্রিল");
		months.add("মে");
		months.add("জুন");
		months.add("জুলাই");
		months.add("অগাস্ট");
		months.add("সেপ্টেম্বর");
		months.add("অক্টোবর");
		months.add("নভেম্বর");
		months.add("ডিসেম্বর");
		return months;
	}
	public ArrayList<String> prepareDays(){
		days = new ArrayList<String>();
		for(int i=1;i<32;i++){
			days.add(BengaliUnicodeString.convertToBengaliText(getActivity(), String.valueOf(i)));
		}
		return days;
	}

	public void showBirthDate() {
		JekonoShishuKobeJonmabeDialog dialogDob = JekonoShishuKobeJonmabeDialog.getInstance(selDate, selMonth, selYear);
		dialogDob.setOnClickListener(new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int resultCode) {
				if(resultCode == 0) {
					dialog.dismiss();
				} 
				dialog.dismiss();
			}
		});

		Fragment fragmentByTag = getActivity().getSupportFragmentManager().findFragmentByTag(JekonoShishuKobeJonmabeDialog.D_TAG);//avoid opening twice dialog  
		if (fragmentByTag == null)  
			dialogDob.show(getActivity().getSupportFragmentManager(), JekonoShishuKobeJonmabeDialog.D_TAG);
	}
	
	public static String getDateString(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, 280); 
        
        DateFormat format = DateFormat.getDateInstance(DateFormat.LONG, Locale.UK);
        String dateString = format.format(cal.getTime());
        
        return dateString;
    }
	
	public String getMonthString(String nonFormatteddate) {
		String formattedString = null;
		String replacableChar = null;
		StringBuilder buffer = new StringBuilder();
		Pattern pattern = Pattern.compile("\\D");
		
		Matcher matcher = pattern.matcher(nonFormatteddate);
		while (matcher.find()) {
			buffer.append(matcher.group());
		}
		replacableChar = buffer.toString().replace(" ", "");
		String replaceChar = getReplaceChar(replacableChar);
		formattedString = nonFormatteddate.replace(replacableChar, replaceChar);
		
		return formattedString;
	}
	
	public String getReplaceChar(String s) {
		List<String> monthsBn = new ArrayList<String>();
		monthsBn.add("জানুয়ারী");
		monthsBn.add("ফেব্রুয়ারী");
		monthsBn.add("মার্চ");
		monthsBn.add("এপ্রিল");
		monthsBn.add("মে");
		monthsBn.add("জুন");
		monthsBn.add("জুলাই");
		monthsBn.add("অগাস্ট");
		monthsBn.add("সেপ্টেম্বর");
		monthsBn.add("অক্টোবর");
		monthsBn.add("নভেম্বর");
		monthsBn.add("ডিসেম্বর");
		
		List<String> monthsEn = new ArrayList<String>();
		
		monthsEn.add("January");
		monthsEn.add("February");
		monthsEn.add("March");
		monthsEn.add("April");
		monthsEn.add("May");
		monthsEn.add("June");
		monthsEn.add("July");
		monthsEn.add("August");
		monthsEn.add("September");
		monthsEn.add("October");
		monthsEn.add("November");
		monthsEn.add("December");
		
		for(int i = 0; i<monthsBn.size(); i++) {
			if(monthsEn.get(i).equalsIgnoreCase(s)) {
				return  monthsBn.get(i);
			}
		}
		return null;
	}

}