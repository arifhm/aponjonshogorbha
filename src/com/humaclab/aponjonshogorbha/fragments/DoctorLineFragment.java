package com.humaclab.aponjonshogorbha.fragments;

import com.bd.aponjon.pregnancy.R;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public class DoctorLineFragment extends Fragment {
	Button mYes;
	Button mNo;

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("ডডক্টর'স লাইন");
	}

	@Override
	@Nullable
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.layout_fragment_doctor_line, container, false);

		mYes = (Button) v.findViewById(R.id.view_btn_yes);
		mYes.setOnClickListener(new View.OnClickListener() {			
			@Override
			public void onClick(View v) {
				call("16227");
			}
		});

		mNo = (Button) v.findViewById(R.id.view_btn_no);
		mNo.setOnClickListener(new View.OnClickListener() {			
			@Override
			public void onClick(View v) {
				getActivity().finish();
			}
		});
		return v;
	}

	private void call(String number) {
		startActivityForResult(
				new Intent("android.intent.action.CALL", Uri.parse("tel:"
						+ number)), 1);
	}
}
