package com.humaclab.aponjonshogorbha.adapters;

import com.bd.aponjon.pregnancy.R;

import de.hdodenhof.circleimageview.CircleImageView;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class NavListAdapter extends Adapter<NavListAdapter.ViewHolder> {
	private static final int TYPE_HEADER = 0;  
	private static final int TYPE_ITEM = 1;

	private String mNavTitles[];
	private int mIcons[];

	private String name;
	private Bitmap profile;
	private int editProf;

	RecycleAdapterCallback mCallback;
	public interface RecycleAdapterCallback{
		public void onRecycleItemClicked(int position);
	}
	public void setOnRecyclerItemClickListener(RecycleAdapterCallback callback){
		this.mCallback = callback;
	}
	public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
		int holderId;       

		TextView txtViewItem;  
		ImageView imgViewIcon;
		int holderPosition;
		CircleImageView imgViewHeaderMain;
//		ImageView imgEditProfileIcon;
		TextView txtViewName;
		ViewHolderCallback mListener;
		
		public static interface ViewHolderCallback {
			public void onClickedItem(View Item);
		}
		public void setHolderPosition(int position){
			this.holderPosition = position;
		}
		public int getHolderPosition(){
			return holderPosition;
		}

		public void setViewHolderCallback(ViewHolderCallback callbacks){
			mListener = callbacks;
		}
		public ViewHolder(View itemView,int ViewType) {   
			super(itemView);
			//			itemView.setClickable(true);
			//			itemView.setOnClickListener(this);
			itemView.setOnClickListener(this);
			if(ViewType == TYPE_ITEM) {
				txtViewItem = (TextView) itemView.findViewById(R.id.nav_list_row_text);
				txtViewItem.setOnClickListener(this);

				imgViewIcon = (ImageView) itemView.findViewById(R.id.nav_list_row_icon);
				imgViewIcon.setOnClickListener(this);

				holderId = 1;                                               
			}
			else{
				txtViewName = (TextView) itemView.findViewById(R.id.nav_header_name_title);
				txtViewName.setOnClickListener(this);
				imgViewHeaderMain = (CircleImageView) itemView.findViewById(R.id.nav_header_round_img);
				imgViewHeaderMain.setOnClickListener(this);
//				imgEditProfileIcon = (ImageView) itemView.findViewById(R.id.img_view_btn_edit_profile);
//				imgEditProfileIcon.setOnClickListener(this);

				holderId = 0;                                                
			}
		}

		@Override
		public void onClick(View v) {
			mListener.onClickedItem(v);
			//			Toast.makeText(contxt,"The Item Clicked is: "+getPosition(),Toast.LENGTH_SHORT).show();
		}

	}



	public NavListAdapter(String titles[],int icons[], String name, Bitmap profile, int editProfile){ 

		this.mNavTitles = titles;                
		this.mIcons = icons;
		this.name = name;
		this.profile = profile;
		this.editProf = editProfile;
	}


	@Override
	public NavListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

		if (viewType == TYPE_ITEM) {
			View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.nav_list_row, parent,false); 

			final ViewHolder vhItem = new ViewHolder(v,viewType);
			vhItem.setViewHolderCallback(new NavListAdapter.ViewHolder.ViewHolderCallback() {
				
				@Override
				public void onClickedItem(View Item) {
					if(mCallback!=null)
					mCallback.onRecycleItemClicked(vhItem.getHolderPosition());
				}
			});
			return vhItem;


		} else if (viewType == TYPE_HEADER) {

			View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_drawer_header,parent,false); 

			final ViewHolder vhHeader = new ViewHolder(v,viewType);
			vhHeader.setViewHolderCallback(new NavListAdapter.ViewHolder.ViewHolderCallback() {
				
				@Override
				public void onClickedItem(View Item) {
					if(mCallback!=null)
					mCallback.onRecycleItemClicked(vhHeader.getHolderPosition());
				}
			});

			return vhHeader; 

		}
		return null;

	}


	@Override
	public void onBindViewHolder(NavListAdapter.ViewHolder holder, int position) {
		if(holder.holderId ==1) {                              
			holder.txtViewItem.setText(mNavTitles[position - 1]); 
			holder.imgViewIcon.setImageResource(mIcons[position - 1]);
		}
		else{
			holder.imgViewHeaderMain.setImageBitmap(profile);
//			holder.imgEditProfileIcon.setImageResource(editProf);
			holder.txtViewName.setText(name);
		}
		holder.holderPosition = position;
	}


	@Override
	public int getItemCount() {
		return mNavTitles.length+1; 
	}


	
	@Override
	public int getItemViewType(int position) {  
		if (isPositionHeader(position))
			return TYPE_HEADER;

		return TYPE_ITEM;
	}

	private boolean isPositionHeader(int position) {
		return position == 0;
	}
}
