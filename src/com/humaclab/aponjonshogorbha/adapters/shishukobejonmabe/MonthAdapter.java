package com.humaclab.aponjonshogorbha.adapters.shishukobejonmabe;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.bd.aponjon.pregnancy.R;

public class MonthAdapter extends ArrayAdapter<String>{

	List<String>days;
	LayoutInflater inflater;
	public MonthAdapter(Context context, int resource, List<String> objects) {
		super(context, resource, objects);
		inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		days = objects;
		tf = Typeface.createFromAsset(context.getAssets(), "solaimanlipinormal.ttf");
		
	}
	Typeface tf;
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		TextView view = (TextView)inflater.inflate(R.layout.layout_spinner_dropdown, null);
		view.setText(days.get(position));
		view.setTypeface(tf);
		return view;
	}
	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		TextView view = (TextView)inflater.inflate(R.layout.layout_spinner_background, null);
		view.setText(days.get(position));
		view.setTypeface(tf);
		return view;
	}
}