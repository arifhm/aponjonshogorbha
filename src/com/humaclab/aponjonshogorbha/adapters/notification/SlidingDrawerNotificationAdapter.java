package com.humaclab.aponjonshogorbha.adapters.notification;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import bengali.language.support.BengaliUnicodeString;

import com.humaclab.aponjon.*;
import com.humaclab.aponjon.DaoMaster.DevOpenHelper;
import com.humaclab.aponjonshogorbha.AponjonApplication;
import com.bd.aponjon.pregnancy.R;
import com.humaclab.aponjonshogorbha.adapters.NavListAdapter.ViewHolder.ViewHolderCallback;


public class SlidingDrawerNotificationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements View.OnClickListener{
	private static final int TYPE_HEADER = 0;
	private static final int TYPE_ITEM = 1;
	private boolean multipleList;
	List<Task> mTaskList;
	List<Task> mDailyTasks;
	List<Task> mWeeklyTasks;
	Context mCtx;
	String mHeaderTextDaily;
	String mHeaderTextWeekly;
	DaoSession mDaoSession;

	public SlidingDrawerNotificationAdapter(Context ctx, List<Task> dailyTasks, List<Task> weeklyTasks, String headerTextDaily, String headerWeekly) {
		mCtx = ctx;
		mTaskList = new ArrayList<>();
		if(dailyTasks != null && weeklyTasks != null) {
			multipleList = true;
			mTaskList.addAll(dailyTasks);
			mTaskList.addAll(weeklyTasks);
		} else if(dailyTasks != null) {
			multipleList = false;
			mTaskList = dailyTasks;
		} else if(weeklyTasks != null) {
			multipleList = false;
			mTaskList = weeklyTasks;
		}
		mHeaderTextDaily = headerTextDaily;
		mHeaderTextWeekly = headerWeekly;
		mDailyTasks = dailyTasks;
		mWeeklyTasks = weeklyTasks;
		
		DevOpenHelper helper = new DaoMaster.DevOpenHelper(mCtx, "aponjon-db", null);
		SQLiteDatabase db = helper.getWritableDatabase();
		DaoMaster daomaster = new DaoMaster(db);
		mDaoSession = daomaster.newSession();
	}

	@Override
	public int getItemCount() {
		if(multipleList) {
			return mTaskList.size() + 2;
		} else {
			return mTaskList.size() + 1;
		}
	}

	@Override
	public int getItemViewType(int position) {
		if (isPositionHeader(position))
			return TYPE_HEADER;
		return TYPE_ITEM;
	}

	private boolean isPositionHeader(int position) {
		if(multipleList) {
			if(position == 0) {
				return true;
			} else if(position == mDailyTasks.size() + 1) {
				return true;
			} else return false;
		} else {
			return position == 0;
		}
	}


	@Override
	public void onBindViewHolder(ViewHolder holder, int position) {		

		if (holder instanceof ViewHolderItem) {
			Task task = null;
			if(multipleList && position > mDailyTasks.size() +1) {
				task = mTaskList.get(position-2);
			} else {
				task = mTaskList.get(position-1);
			}			
			((ViewHolderItem) holder).bindTask(task);
			
		} else if (holder instanceof ViewHolderHeader) {
			if(multipleList) {
				if(position == 0 ) {
					((ViewHolderHeader) holder).bindHeader(mHeaderTextDaily);
				} else {
					((ViewHolderHeader) holder).bindHeader(mHeaderTextWeekly);
				}
			} else if (mDailyTasks != null) {
				((ViewHolderHeader) holder).bindHeader(mHeaderTextDaily);
			} else if (mWeeklyTasks != null) {
				((ViewHolderHeader) holder).bindHeader(mHeaderTextWeekly);
			}
			
		}
	}

	@Override
	public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int position) {
		if (position == TYPE_ITEM) {
			final LayoutInflater mInflater = LayoutInflater.from(parent.getContext());
			final View sView = mInflater.inflate(R.layout.notification_list_row, parent, false);
			return new ViewHolderItem(sView);
		} else if (position == TYPE_HEADER) {
			final LayoutInflater mInflater = LayoutInflater.from(parent.getContext());
			final View sView = mInflater.inflate(R.layout.notification_header_row, parent, false);
			return new ViewHolderHeader(sView);
		}
		return null;
	}

	public class ViewHolderItem extends RecyclerView.ViewHolder {
		Task mTask;
		String mTaskName;
		Date mDate;
		String mTitle;

		TextView mViewTaskName;
		TextView mViewDate;
		TextView mViewTitle;
		ImageView mViewImage;

		public ViewHolderItem(View view) {
			super(view);
			mViewTaskName = (TextView) view.findViewById(R.id.noti_name);
			mViewDate = (TextView) view.findViewById(R.id.noti_date);
			mViewTitle = (TextView) view.findViewById(R.id.noti_title);
			mViewImage = (ImageView) view.findViewById(R.id.noti_icon);
		}

		public void bindTask(Task task) {
			mTask = task;
			SimpleDateFormat format = (SimpleDateFormat) SimpleDateFormat.getInstance();
			format.applyPattern("dd-MM-yyyy");
			TaskTypeDao taskTypeDao = mDaoSession.getTaskTypeDao();
			long id = task.getTypeId();
			TaskType taskType= taskTypeDao.queryBuilder().where(TaskTypeDao.Properties.Id.eq(task.getTypeId())).unique();
			mViewTaskName.setText(taskType.getName());
			Date date = task.getTask_date();                 
			if(date == null) {
				date = new Date(System.currentTimeMillis());
			}
			mViewDate.setText(BengaliUnicodeString.convertToBengaliText(mCtx, format.format(date)));
			mViewTitle.setText(task.getTitle());
			int img;
			switch (taskType.getName()) {
			case "ব্যায়াম":
				img = R.drawable.ic_task_type_excercise;
				break;
			case "ঔষধ":
				img = R.drawable.ic_task_type_medicine;
				break;
			case "টিকা":
				img = R.drawable.ic_task_type_vaccine;
				break;
			default:
				img = R.drawable.ic_task_type_others;
				break;
			}
			mViewImage.setImageDrawable(mCtx.getResources().getDrawable(img));
		}		
	}

	public class ViewHolderHeader extends RecyclerView.ViewHolder {
		String mHeaderTitle;		
		TextView mViewHeaderTitle;

		public ViewHolderHeader(View view) {
			super(view);
			mViewHeaderTitle = (TextView) view.findViewById(R.id.noti_header);

		}

		public void bindHeader(String title) {
			mHeaderTitle = title;
			mViewHeaderTitle.setText(mHeaderTitle);
		}		
	}

	@Override
	public void onClick(View v) {

	}

}
