package com.humaclab.aponjonshogorbha.adapters;

import java.util.HashMap;
import java.util.List;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.humaclab.aponjon.Food;
import com.bd.aponjon.pregnancy.R;

public class ExpandableListAdapter extends BaseExpandableListAdapter {

	private Context _context;
	private List<String> _listDataHeader; // header titles
	// child data in format of header title, child title
	private HashMap<String, List<Food>> _listDataChild;

	public ExpandableListAdapter(Context context, List<String> listDataHeader,
			HashMap<String, List<Food>> listDataChild) {
		this._context = context;
		this._listDataHeader = listDataHeader;
		this._listDataChild = listDataChild;
	}

	@Override
	public Food getChild(int groupPosition, int childPosititon) {
		return this._listDataChild.get(this._listDataHeader.get(groupPosition))
				.get(childPosititon);
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public View getChildView(int groupPosition, final int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {

		final Food child =  getChild(groupPosition, childPosition);

		if (convertView == null) {
			LayoutInflater infalInflater = (LayoutInflater) this._context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInflater.inflate(R.layout.layout_list_row_khaddo_talika, null);
		}

		ImageView view_increse_cal = (ImageView)convertView.findViewById(R.id.view_increse_cal);
		ImageView view_decrese_cal = (ImageView)convertView.findViewById(R.id.view_decrese_cal);
		TextView txtListChild = (TextView) convertView
				.findViewById(R.id.view_food_name);
		final TextView view_display = (TextView)convertView.findViewById(R.id.view_display);
		view_display.setText(child.getIncreament_value()+"");
		//		child.setIncreament_value()
		view_increse_cal.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				child.setIncreament_value(child.getIncreament_value()+1);
				view_display.setText(child.getIncreament_value()+"");
			}
		});
		view_decrese_cal.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if(child.getIncreament_value()>=0){
					child.setIncreament_value(child.getIncreament_value()-1);
					view_display.setText(child.getIncreament_value()+"");
				}
			}
		});

		txtListChild.setText(child.getDesc());
		return convertView;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		return this._listDataChild.get(this._listDataHeader.get(groupPosition))
				.size();
	}

	@Override
	public Object getGroup(int groupPosition) {
		return this._listDataHeader.get(groupPosition);
	}

	@Override
	public int getGroupCount() {
		return this._listDataHeader.size();
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		String headerTitle = (String) getGroup(groupPosition);
		if (convertView == null) {
			LayoutInflater infalInflater = (LayoutInflater) this._context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInflater.inflate(R.layout.list_group, null);
		}

		TextView lblListHeader = (TextView) convertView
				.findViewById(R.id.lblListHeader);
		lblListHeader.setTypeface(null, Typeface.BOLD);
		lblListHeader.setText(headerTitle);

		return convertView;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}
}