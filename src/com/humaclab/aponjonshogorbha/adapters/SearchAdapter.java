package com.humaclab.aponjonshogorbha.adapters;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.bd.aponjon.pregnancy.R;

public class SearchAdapter extends ArrayAdapter<String>{

	LayoutInflater inflater;
	int resource;
	List<String> objects;
	ArrayList<String>suggestions;

	public SearchAdapter(Context context, int resource, List<String> objects) {
		super(context, resource, objects);
		inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.objects = objects;

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		TextView view = (TextView)inflater.inflate(R.layout.auto_complete_textview, null);
		view.setText(objects.get(position));
		//		view.setVisibility(View.INVISIBLE);
		return view;
	}
	@Override
	public void add(String object) {
		super.add(object);
	}


	@Override
	public Filter getFilter() {
		return nameFilter;
	}

	Filter nameFilter = new Filter(){

		public CharSequence convertResultToString(Object resultValue) {

			return (String)resultValue;
		};
		@Override
		protected FilterResults performFiltering(CharSequence constraint) {
			if(constraint != null){
				suggestions.clear();
				for(String ph : objects){
					if(ph.startsWith(constraint.toString()))
					{
						suggestions.add(ph);

					}
				}
				FilterResults filterResults = new FilterResults();
				filterResults.values = suggestions;
				filterResults.count = suggestions.size();
				return filterResults;
			}else{
				return new FilterResults();
			}
			//			return null;
		}

		@Override
		protected void publishResults(CharSequence constraint, FilterResults results) {
//			clear();
//			notifyDataSetChanged();
			ArrayList<String> filteredList = (ArrayList<String>) results.values;
			if(results != null && results.count > 0) {
				clear();
				for (String c : filteredList) {
					//					Log.d("CONTACT",c.getName());
					add(c);
				}
				notifyDataSetChanged();
			}			
		}

	};

}