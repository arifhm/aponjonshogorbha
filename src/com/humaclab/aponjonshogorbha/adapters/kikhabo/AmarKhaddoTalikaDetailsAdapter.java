package com.humaclab.aponjonshogorbha.adapters.kikhabo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import bengali.language.support.BengaliUnicodeString;

import com.humaclab.aponjon.Food_Category;
import com.humaclab.aponjon.Meal;
import com.humaclab.aponjon.Meal_Food;
import com.bd.aponjon.pregnancy.R;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class AmarKhaddoTalikaDetailsAdapter extends Adapter<AmarKhaddoTalikaDetailsAdapter.ViewHolder> {
	
	List<Food_Category> mCategories;
	Map<String, List<Meal_Food>> mMealMap;
	Context mCtx;
	
	public AmarKhaddoTalikaDetailsAdapter(Context ctx, List<Food_Category> categories, Map<String, List<Meal_Food>> map) {
		this.mCategories = categories;
		this.mMealMap = map;
		this.mCtx = ctx;
	}
	
	@Override
	public int getItemCount() {
		return mCategories.size();
	}

	@Override
	public void onBindViewHolder(ViewHolder holder, int position) {
		String keyName = mCategories.get(position).getFood_category_name();
		List<Meal_Food> catMealFoods = mMealMap.get(keyName);
		holder.bindMeal(catMealFoods, keyName);
	}

	@Override
	public ViewHolder onCreateViewHolder(ViewGroup parent, int position) {
		final LayoutInflater mInflater = LayoutInflater.from(parent.getContext());
        final View sView = mInflater.inflate(R.layout.layout_row_amar_khaddo_talika_details, parent, false);
        return new ViewHolder(sView);
	}
	
	public class ViewHolder extends RecyclerView.ViewHolder {
		String mCatName;
		List<Meal_Food> mMealFoods;
		StringBuffer mBuffer;
		boolean showView = false;
		
		ViewGroup mViewGroup;
		TextView mViewCatName;
		TextView mViewFoodsDesc;
		
		public ViewHolder(View view) {
			super(view);
			mViewGroup = (ViewGroup) view.findViewById(R.id.viewgroup_row_khaddo_talika_details);
			mViewCatName = (TextView) view.findViewById(R.id.view_row_cat_title);
			mViewFoodsDesc = (TextView) view.findViewById(R.id.view_row_cat_meals);
		}
		
		public void bindMeal(List<Meal_Food> mealFoods, String categoryName) {
			mMealFoods = mealFoods;
			mViewCatName.setText(categoryName);
			mBuffer = new StringBuffer();
			for(int i = 0; i<mMealFoods.size(); i++) {
				Meal_Food mealFood = mMealFoods.get(i);
				if(mealFood.getFood_cal_set_total() != null) {
					showView = true;
					mBuffer.append(getFormatDisplayValue((float) mealFood.getUnit_amount()) + " "+ mealFood.getFood_unit() + " " + mealFood.getAbsolute_name());
					if(i<mMealFoods.size()-1) {
						mBuffer.append(",");
					}
				}				
			}
			if(showView) {
				mViewFoodsDesc.setText(mBuffer.toString());
			} else {
				mViewGroup.setVisibility(View.GONE);
			}
			
		}		
	}
	
	public String getFormatDisplayValue(float n) {
		String number = null;
		if(n == (int) n) {
			number = (int) n + "";
		} else number = n + "";

		return BengaliUnicodeString.convertToBengaliText(mCtx, number);				
	}
	
}
