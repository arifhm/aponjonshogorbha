package com.humaclab.aponjonshogorbha.adapters.kikhabo;

import java.util.List;

import com.humaclab.aponjon.Meal;
import com.bd.aponjon.pregnancy.R;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class AmarKhaddoTalikaAdapter extends Adapter<AmarKhaddoTalikaAdapter.ViewHolder> {

	List<Meal> mMeals;
	Context mCtx;
	OnItemClickListener mItemClickListener;
	
	public AmarKhaddoTalikaAdapter(Context ctx, List<Meal> meals) {
		this.mMeals = meals;
		this.mCtx = ctx;
	}
	
	@Override
	public int getItemCount() {
		return mMeals.size();
	}

	@Override
	public void onBindViewHolder(ViewHolder holder, int position) {
		Meal meal = mMeals.get(position);
		holder.bindMeal(meal);
	}

	@Override
	public ViewHolder onCreateViewHolder(ViewGroup parent, int position) {
		final LayoutInflater mInflater = LayoutInflater.from(parent.getContext());
        final View sView = mInflater.inflate(R.layout.layout_row_amar_khaddo_talika, parent, false);
        return new ViewHolder(sView);
	}
	
	public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
		TextView mMealName;
		
		public ViewHolder(View view) {
			super(view);			
			mMealName = (TextView) view.findViewById(R.id.view_row_meal_name);
			view.setOnClickListener(this);
		}
				
		public void bindMeal(Meal meal) {
			mMealName.setText(meal.getMeal_name());
		}

		@Override
		public void onClick(View v) {
			if(mItemClickListener != null) {
				mItemClickListener.onItemClick(v, getLayoutPosition());
			}			
		}
		
	}
	
	public interface OnItemClickListener {
        public void onItemClick(View view , int position);
    }

    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }
}
