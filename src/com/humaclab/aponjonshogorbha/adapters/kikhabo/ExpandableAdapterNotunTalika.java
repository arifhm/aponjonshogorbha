package com.humaclab.aponjonshogorbha.adapters.kikhabo;

import java.util.List;
import java.util.Map;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import bengali.language.support.BengaliUnicodeString;

import com.humaclab.aponjon.Food;
import com.humaclab.aponjon.Food_Category;
import com.humaclab.aponjon.Meal_Food;
import com.bd.aponjon.pregnancy.R;
import com.humaclab.aponjonshogorbha.fragments.kikhabo.KhaddoTalikaToiriFragment;


public class ExpandableAdapterNotunTalika extends BaseExpandableListAdapter {
	List<Food_Category> aCatList;
	Context aCtx;
	Map<String, List<Meal_Food>> aMealMap;
	KhaddoTalikaToiriFragment afragment;


	public ExpandableAdapterNotunTalika (Context ctx, List<Food_Category> categoryList, Map<String, List<Meal_Food>> map, KhaddoTalikaToiriFragment fragment) {
		this.aCtx = ctx;
		this.aCatList = categoryList;
		this.aMealMap = map;
		this.afragment = fragment;
	}	

	@Override
	public int getGroupCount() {
		return aCatList.size();
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		return aCatList.get(groupPosition).getCategoryfoods().size();
	}

	@Override
	public Food_Category getGroup(int groupPosition) {
		return aCatList.get(groupPosition);
	}

	@Override
	public Food getChild(int groupPosition, int childPosition) {
		return aCatList.get(groupPosition).getCategoryfoods().get(childPosition);
	}

	@Override
	public long getGroupId(int groupPosition) {
		return aCatList.get(groupPosition).getId();
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return aCatList.get(groupPosition).getCategoryfoods().get(childPosition).getId();
	}

	@Override
	public boolean hasStableIds() {
		return true;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		View v = convertView;

		if(v == null) {
			LayoutInflater inflater = (LayoutInflater) aCtx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = inflater.inflate(R.layout.list_group, parent, false);
		}

		TextView headerText = (TextView) v.findViewById(R.id.lblListHeader);
		headerText.setTypeface(null, Typeface.BOLD);
		headerText.setText(aCatList.get(groupPosition).getFood_category_name());
		return v;
	}

	@Override
	public View getChildView(final int groupPosition, final int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {

		View v = convertView;

		if(v == null) {
			LayoutInflater inflater = (LayoutInflater) aCtx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = inflater.inflate(R.layout.layout_list_row_khaddo_talika, parent, false);
		}

		ImageView view_increse_cal = (ImageView) v.findViewById(R.id.view_increse_cal);
		ImageView view_decrese_cal = (ImageView) v.findViewById(R.id.view_decrese_cal);
		TextView view_food_desc = (TextView) v.findViewById(R.id.view_food_name);
		TextView view_unit_name = (TextView) v.findViewById(R.id.view_unit);
		final TextView view_display = (TextView) v.findViewById(R.id.view_display);
		
		final Food current_child = getChild(groupPosition, childPosition);
		
		String foodName = current_child.getDesc();
		String  foodUnit = current_child.getUnit();

		final Meal_Food mealFoodCurrent = aMealMap.get(getGroup(groupPosition).getFood_category_name()).get(childPosition);
		
//		Log.d("Compare", "Meal_Food Name: " + mealFoodCurrent.getFood_desc() + "Meal_Food Id: " + mealFoodCurrent.getFood_data_id()
//				+ "//// Food Name: " + current_child.getDesc() + "Food Id : " + current_child.getId());
		
		view_food_desc.setText(foodName);
		view_unit_name.setText(foodUnit);
		if(mealFoodCurrent.getUnit_amount() != null) {
			view_display.setText(getFormatDisplayValue((float) mealFoodCurrent.getUnit_amount()));
		} else view_display.setText(getFormatDisplayValue(0f));

		//		child.setIncreament_value()
		view_increse_cal.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				float increamentValue = current_child.getIncreament_value();
				if(mealFoodCurrent.getUnit_amount() == null){
					mealFoodCurrent.setUnit_amount(increamentValue);
					mealFoodCurrent.setFood_cal_set_curb(current_child.getCal_per_unit_curb());
					mealFoodCurrent.setFood_cal_set_protien(current_child.getCal_per_unit_protien());
					mealFoodCurrent.setFood_cal_set_fat(current_child.getCal_per_unit_fat());
					mealFoodCurrent.setFood_cal_set_total(mealFoodCurrent.getFood_cal_set_curb() 
							+ mealFoodCurrent.getFood_cal_set_protien() + mealFoodCurrent.getFood_cal_set_fat());
					
				} else {
					mealFoodCurrent.setUnit_amount(mealFoodCurrent.getUnit_amount()+increamentValue);
					mealFoodCurrent.setFood_cal_set_curb(mealFoodCurrent.getFood_cal_set_curb() + current_child.getCal_per_unit_curb());
					mealFoodCurrent.setFood_cal_set_protien(mealFoodCurrent.getFood_cal_set_protien() + current_child.getCal_per_unit_protien());
					mealFoodCurrent.setFood_cal_set_fat(mealFoodCurrent.getFood_cal_set_fat() + current_child.getCal_per_unit_fat());
					mealFoodCurrent.setFood_cal_set_total(mealFoodCurrent.getFood_cal_set_curb() 
							+ mealFoodCurrent.getFood_cal_set_protien() + mealFoodCurrent.getFood_cal_set_fat());
				}
				view_display.setText(getFormatDisplayValue((float) mealFoodCurrent.getUnit_amount()));
				afragment.reactWithDataSet();
			}
		});
		view_decrese_cal.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				float increamentValue = current_child.getIncreament_value();
				if(mealFoodCurrent.getUnit_amount() != null && ((float) mealFoodCurrent.getUnit_amount()-increamentValue) > -1){
					mealFoodCurrent.setUnit_amount(mealFoodCurrent.getUnit_amount()-increamentValue);
					mealFoodCurrent.setFood_cal_set_curb(mealFoodCurrent.getFood_cal_set_curb() - current_child.getCal_per_unit_curb());
					mealFoodCurrent.setFood_cal_set_protien(mealFoodCurrent.getFood_cal_set_protien() - current_child.getCal_per_unit_protien());
					mealFoodCurrent.setFood_cal_set_fat(mealFoodCurrent.getFood_cal_set_fat() - current_child.getCal_per_unit_fat());
					mealFoodCurrent.setFood_cal_set_total(mealFoodCurrent.getFood_cal_set_curb() 
							+ mealFoodCurrent.getFood_cal_set_protien() + mealFoodCurrent.getFood_cal_set_fat());
					
					view_display.setText(getFormatDisplayValue((float) mealFoodCurrent.getUnit_amount()));
					afragment.reactWithDataSet();
				}
			}
		});


		return v;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}

	public String getFormatDisplayValue(float n) {
		String number = null;
		if(n == (int) n) {
			number = (int) n + "";
		} else number = n + "";

		return BengaliUnicodeString.convertToBengaliText(aCtx, number);				
	}

}
