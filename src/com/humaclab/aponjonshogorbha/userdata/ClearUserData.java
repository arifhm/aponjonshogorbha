package com.humaclab.aponjonshogorbha.userdata;

import com.humaclab.aponjon.DaoSession;
import com.humaclab.aponjon.DiaryDao;
import com.humaclab.aponjon.MealDao;
import com.humaclab.aponjon.Meal_FoodDao;
import com.humaclab.aponjon.MoveDao;
import com.humaclab.aponjon.SyncLogDao;
import com.humaclab.aponjon.TaskDao;
import com.humaclab.aponjon.TaskTypeDao;
import com.humaclab.aponjon.WeightDao;
import com.humaclab.aponjonshogorbha.AponjonApplication;


public class ClearUserData {
	DaoSession mDaoSession;
	
	public ClearUserData() {
		AponjonApplication app = AponjonApplication.getInstance();
		mDaoSession = app.getDaoSession();
	}
	
	public void clearData() {
		MealDao mealDao = mDaoSession.getMealDao();
		mealDao.deleteAll();
		Meal_FoodDao mealFoodDao = mDaoSession.getMeal_FoodDao();
		mealFoodDao.deleteAll();
		WeightDao weightDao = mDaoSession.getWeightDao();
		weightDao.deleteAll();
		TaskTypeDao taskTypeDao = mDaoSession.getTaskTypeDao();
		taskTypeDao.deleteAll();
		TaskDao taskDao = mDaoSession.getTaskDao();
		taskDao.deleteAll();
		DiaryDao diaryDao = mDaoSession.getDiaryDao();
		diaryDao.deleteAll();
		MoveDao moveDao = mDaoSession.getMoveDao();
		moveDao.deleteAll();

	}
}
