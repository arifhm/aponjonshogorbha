package com.humaclab.aponjonshogorbha.userdata.upload;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import android.util.Log;

import com.humaclab.aponjon.DaoMaster;
import com.humaclab.aponjon.DaoMaster.DevOpenHelper;
import com.humaclab.aponjon.DaoSession;
import com.humaclab.aponjon.Task;
import com.humaclab.aponjon.TaskDao;
import com.humaclab.aponjonshogorbha.AponjonRestClient;
import com.humaclab.aponjonshogorbha.AppConfig;
import com.humaclab.aponjonshogorbha.Constants;
import com.humaclab.aponjonshogorbha.SessionManager;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class UploadCustomTask {
	String mToken;
	Context mCtx;
	SessionManager mSessionMgr;
	DaoSession mDaoSession;
	AsyncHttpResponseHandler mHandlerTaskUpload;
	TaskDao taskDao;
	
	public UploadCustomTask(Context ctx) {
		mCtx = ctx;
		mSessionMgr = new SessionManager(mCtx);
		SharedPreferences prefs = mSessionMgr.getPrefs();		
		mToken = prefs.getString(Constants.UserData.USER_TOKEN, null);

		DevOpenHelper helper = new DaoMaster.DevOpenHelper(mCtx, "aponjon-db", null);
		SQLiteDatabase db = helper.getWritableDatabase();
		DaoMaster daomaster = new DaoMaster(db);
		mDaoSession = daomaster.newSession();
	}
	
	public void Upload() {
		taskDao = mDaoSession.getTaskDao();
		List<Task> notSyncedNewCustomTasks = taskDao.queryBuilder()
				.where(TaskDao.Properties.CatId.eq(1), TaskDao.Properties.IsSync.eq(false), TaskDao.Properties.ServerId.isNull()).list();
		if(!notSyncedNewCustomTasks.isEmpty()) {
			
			for(Task task : notSyncedNewCustomTasks) {
				RequestParams params = new RequestParams();
				JSONObject reqObj = new JSONObject();
				try {
					/**
					 * { "token" : "4xAyjtRBPxLgrT1AD3WyXO2oPtz5C", 
					 *   "data": {
					 * 				"id" = ""
					 * 		 		"title" = ""
					 * 				"description" = ""
					 * 				"task_category_id" = ""
					 * 		     }
					 * }
					 */
					reqObj.put("token", mToken);
					JSONObject reqObjTask = new JSONObject();
					reqObjTask.put("app_id", task.getId());
					reqObjTask.put("title", task.getTitle());
					reqObjTask.put("description", task.getDescription());
					reqObjTask.put("task_category_id", task.getTypeId());
					reqObjTask.put("task_date", task.getTask_date());
					reqObj.put("data", reqObjTask);

					params.put("data", reqObj.toString());
					
//					//FileWriteBlock
//					StringBuffer msb1 = new StringBuffer();
//					msb1.append(reqObj.toString());
//					try {
//						File f = new File(Environment.getExternalStorageDirectory().getPath(), "uploadCustomTask.txt");
//						writeToFile(f, msb1);
//					} catch (IOException e) {
//						e.printStackTrace();
//					}
					
				} catch (JSONException e) {
					e.printStackTrace();
				}
				
				mHandlerTaskUpload = new TaskUploadRequestHandler();
				AponjonRestClient.post(AppConfig.URL_UPLOAD_CUSTOM_TASK, params, mHandlerTaskUpload);
			}
			
		}
	}
	
	class TaskUploadRequestHandler extends AsyncHttpResponseHandler {
		@Override
		public void onSuccess(String response) {
			
			try {
				JSONObject responsetObj = new JSONObject(response);
				int statusCode = responsetObj.getInt("status_code");
				if(statusCode == 200) {
					JSONObject resultObj = responsetObj.getJSONObject("result");
					String retServerId = resultObj.getString("last_id");
					long id = resultObj.getLong("app_id");
					
					Task task = taskDao.load(id);
					task.setServerId(retServerId);
					task.setIsSync(true);
					taskDao.update(task);
					
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static void writeToFile(File pFile, StringBuffer pData) throws IOException {  
		BufferedWriter out = new BufferedWriter(new FileWriter(pFile));  
		out.write(pData.toString());  
		out.flush();  
		out.close();  
	}
	
}
