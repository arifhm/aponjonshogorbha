package com.humaclab.aponjonshogorbha.userdata.upload;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import com.humaclab.aponjon.DaoMaster;
import com.humaclab.aponjon.DaoSession;
import com.humaclab.aponjon.Task;
import com.humaclab.aponjon.TaskDao;
import com.humaclab.aponjon.DaoMaster.DevOpenHelper;
import com.humaclab.aponjonshogorbha.AponjonRestClient;
import com.humaclab.aponjonshogorbha.AppConfig;
import com.humaclab.aponjonshogorbha.Constants;
import com.humaclab.aponjonshogorbha.SessionManager;
import com.humaclab.aponjonshogorbha.userdata.upload.UploadCustomTask.TaskUploadRequestHandler;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;

public class UploadSystemTaskStatus {
	String mToken;
	Context mCtx;
	SessionManager mSessionMgr;
	DaoSession mDaoSession;
	AsyncHttpResponseHandler mHandlerStatusTaskUpload;
	TaskDao taskDao;

	public UploadSystemTaskStatus(Context ctx) {
		mCtx = ctx;
		mSessionMgr = new SessionManager(mCtx);
		SharedPreferences prefs = mSessionMgr.getPrefs();		
		mToken = prefs.getString(Constants.UserData.USER_TOKEN, null);

		DevOpenHelper helper = new DaoMaster.DevOpenHelper(mCtx, "aponjon-db", null);
		SQLiteDatabase db = helper.getWritableDatabase();
		DaoMaster daomaster = new DaoMaster(db);
		mDaoSession = daomaster.newSession();
	}

	public void Upload() {
		taskDao = mDaoSession.getTaskDao();
		List<Task> notSyncedNewCustomTasks = taskDao.queryBuilder()
				.where(TaskDao.Properties.CatId.eq(2), TaskDao.Properties.IsSync.eq(false), TaskDao.Properties.ServerId.isNotNull()).list();

		if(!notSyncedNewCustomTasks.isEmpty()) {

			for(Task task : notSyncedNewCustomTasks) {
				RequestParams params = new RequestParams();
				JSONObject reqObj = new JSONObject();
				try {
					/**
					 * { "token" : "4xAyjtRBPxLgrT1AD3WyXO2oPtz5C", 
					 *   "data": {
					 * 				"UserSystemTask" : {
					 * 									"id" : ""
					 * 		 							"is_done" : ""									
					 * 		    					   } 
					 * 			}
					 * }
					 */
					reqObj.put("token", mToken);
					JSONObject reqDataObj = new JSONObject();
					
					JSONObject reqObjStatusSystemTask = new JSONObject();
					reqObjStatusSystemTask.put("id", task.getServerId());
					reqObjStatusSystemTask.put("is_done", task.getIsCompleted() ? 1 : 0);
					
					reqDataObj.put("UserSystemTask", reqObjStatusSystemTask);
					reqObj.put("data", reqDataObj);

					params.put("data", reqObj.toString());

//					//FileWriteBlock
//					StringBuffer msb1 = new StringBuffer();
//					msb1.append(reqObj.toString());
//					try {
//						File f = new File(Environment.getExternalStorageDirectory().getPath(), "uploadSystemTaskStatus.txt");
//						writeToFile(f, msb1);
//					} catch (IOException e) {
//						e.printStackTrace();
//					}
					
				} catch (JSONException e) {
					e.printStackTrace();
				}

				mHandlerStatusTaskUpload = new TaskStatusUploadRequestHandler();
				AponjonRestClient.post(AppConfig.URL_UPLOAD_SYSTEM_TASK_FLAG, params, mHandlerStatusTaskUpload);
			}
		}
	}
	
	class TaskStatusUploadRequestHandler extends AsyncHttpResponseHandler {
		@Override
		public void onSuccess(String response) {
			
			try {
				JSONObject responsetObj = new JSONObject(response);
				int statusCode = responsetObj.getInt("status_code");
				if(statusCode == 200) {
					JSONObject resultObj = responsetObj.getJSONObject("result");
//					String retServerId = resultObj.getString("last_id");
					long id = resultObj.getLong("id");
					
					Task task = taskDao.load(id);
//					task.setServerId(retServerId);
					task.setIsSync(true);
					taskDao.update(task);
					
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static void writeToFile(File pFile, StringBuffer pData) throws IOException {  
		BufferedWriter out = new BufferedWriter(new FileWriter(pFile));  
		out.write(pData.toString());  
		out.flush();  
		out.close();  
	}
}
