package com.humaclab.aponjonshogorbha.userdata.upload;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.humaclab.aponjon.DaoMaster;
import com.humaclab.aponjon.DaoSession;
import com.humaclab.aponjon.DaoMaster.DevOpenHelper;
import com.humaclab.aponjon.Food;
import com.humaclab.aponjon.FoodDao;
import com.humaclab.aponjon.Meal;
import com.humaclab.aponjon.MealDao;
import com.humaclab.aponjon.Meal_Food;
import com.humaclab.aponjon.Meal_FoodDao;
import com.humaclab.aponjonshogorbha.AponjonRestClient;
import com.humaclab.aponjonshogorbha.AppConfig;
import com.humaclab.aponjonshogorbha.Constants;
import com.humaclab.aponjonshogorbha.SessionManager;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import android.util.Log;

public class UploadMealFood {
	String mToken;
	Context mCtx;
	SessionManager mSessionMgr;
	DaoSession mDaoSession;
	AsyncHttpResponseHandler mHandlerMealFoodUpload;
	MealDao mMealDao;
	Meal_FoodDao mMealFoodDao;
	AsyncHttpResponseHandler mHandlerFoodUpload;

	public UploadMealFood(Context ctx) {
		mCtx = ctx;
		mSessionMgr = new SessionManager(mCtx);
		SharedPreferences prefs = mSessionMgr.getPrefs();		
		mToken = prefs.getString(Constants.UserData.USER_TOKEN, null);

		DevOpenHelper helper = new DaoMaster.DevOpenHelper(mCtx, "aponjon-db", null);
		SQLiteDatabase db = helper.getWritableDatabase();
		DaoMaster daomaster = new DaoMaster(db);
		mDaoSession = daomaster.newSession();
	}

	public void Upload() {
		mMealDao = mDaoSession.getMealDao();
		List<Meal> notSyncedMeals = mMealDao.queryBuilder().whereOr(MealDao.Properties.Is_synced.eq(false), MealDao.Properties.Is_synced.isNull()).list();

		if(!notSyncedMeals.isEmpty()) {
			RequestParams params = new RequestParams();
			JSONObject reqObj = new JSONObject();

			try {
				/**
				 * { "token" : "4xAyjtRBPxLgrT1AD3WyXO2oPtz5C", 
				 *   "data": {	
					 				"MealList" : [
					 								{
					 									"app_id" : "meal_id",
					 									"name":"meal_name",
            											"carbohydrate_calorie":30,
            											"protein_calorie":10,
            											"fat_calorie":5.45,
            											"total_calorie":45.45,
            											"min_carbohydrate_calorie":10,
            											"min_protein_calorie":5,
            											"min_fat_calorie":2.25,
            											"min_total_calorie":17.25,
            											"date":"11-08-2015"
					 								},
					 								{

					 								}

					 							 ]
					 				"MealItem" : [
					 								{
					 									"app_id" : "meal_food_id"
					 									"app_list_id":1,
            											"food_id":1,
            											"food_id_server" : 5,
            											"food_carbohydrate_calorie":40,
            											"food_protein_calorie":28,
            											"food_fat_calorie":21,
            											"total_food_calorie":88,
            											"unit":3,
            											"amount":55
					 								},
					 								{	
					 									"app_id" : "",
					 									"app_list_id":1,
            											"food_id":3,
            											"food_id_server" : 7,
            											"food_carbohydrate_calorie":40,
            											"food_protein_calorie":28,
            											"food_fat_calorie":21,
            											"total_food_calorie":88,
            											"unit":3,
            											"amount":55
					 								}
					 							 ]
				 * 		     }
				 * }
				 */

				reqObj.put("token", mToken);
				JSONObject reqObjMealFood = new JSONObject();
				JSONArray mealListArray = new JSONArray();
				JSONArray mealItemArray = new JSONArray();

				mMealFoodDao = mDaoSession.getMeal_FoodDao();
				FoodDao foodDao = mDaoSession.getFoodDao();

				for(Meal meal : notSyncedMeals) {
					JSONObject mealListObj = new JSONObject();

					mealListObj.put("app_id", meal.getId());
					mealListObj.put("name", meal.getMeal_name());
					mealListObj.put("carbohydrate_calorie", meal.getMeal_cal_set_curb());
					mealListObj.put("protein_calorie", meal.getMeal_cal_set_protien());
					mealListObj.put("fat_calorie", meal.getMeal_cal_set_fat());
					mealListObj.put("total_calorie", meal.getMeal_cal_set_total());
					mealListObj.put("min_carbohydrate_calorie", meal.getReq_cal_curb());
					mealListObj.put("min_protein_calorie", meal.getReq_cal_protien());
					mealListObj.put("min_fat_calorie", meal.getReq_cal_fat());
					mealListObj.put("min_total_calorie", meal.getReq_cal_total());
					mealListObj.put("date", meal.getCreation_date());
					
					mealListArray.put(mealListObj);

					List<Meal_Food> mealFoods = mMealFoodDao.queryBuilder().where(Meal_FoodDao.Properties.Meal_id.eq(meal.getId())).list();

					for(Meal_Food mealFood : mealFoods) {
						JSONObject mealItemObj = new JSONObject();

						mealItemObj.put("app_id", mealFood.getId());
						mealItemObj.put("app_list_id", mealFood.getMeal_id());
						mealItemObj.put("food_id", mealFood.getFood_data_id());
						mealItemObj.put("food_id_server", mealFood.getFood_data_id());						
						mealItemObj.put("food_carbohydrate_calorie", mealFood.getFood_cal_set_curb());
						mealItemObj.put("food_protein_calorie", mealFood.getFood_cal_set_protien());
						mealItemObj.put("food_fat_calorie", mealFood.getFood_cal_set_fat());
						mealItemObj.put("total_food_calorie", mealFood.getFood_cal_set_total());
						mealItemObj.put("unit", mealFood.getIncreament_value());
						mealItemObj.put("amount", mealFood.getUnit_amount());
						
						mealItemArray.put(mealItemObj);
					}

				}

				reqObjMealFood.put("MealList", mealListArray);
				reqObjMealFood.put("MealItem", mealItemArray);
				reqObj.put("data", reqObjMealFood);

				params.put("data", reqObj.toString());

				//FileWriteBlock
				StringBuffer msb1 = new StringBuffer();
				msb1.append(reqObj.toString());
				try {
					File f = new File(Environment.getExternalStorageDirectory().getPath(), "requestFoodUpload.txt");
					writeToFile(f, msb1);
				} catch (IOException e) {
					e.printStackTrace();
				}

			} catch (JSONException e) {
				e.printStackTrace();
			}

			mHandlerFoodUpload = new FoodUploadRequestHandler();
			AponjonRestClient.post(AppConfig.URL_UPLOAD_FOOD, params, mHandlerFoodUpload);
		}

	}


	class FoodUploadRequestHandler extends AsyncHttpResponseHandler {
		@Override
		public void onSuccess(String response) {
			
			//FileWriteBlock
			StringBuffer msb1 = new StringBuffer();
			msb1.append(response.toString());
			try {
				File f = new File(Environment.getExternalStorageDirectory().getPath(), "resultFoodUpload.txt");
				writeToFile(f, msb1);
			} catch (IOException e) {
				e.printStackTrace();
			}

			try {
				JSONObject responseObj = new JSONObject(response);
				int statusCode = responseObj.getInt("status_code");
				
				if(statusCode == 200) {
					
					
					JSONObject resultObj = responseObj.getJSONObject("result");
					
					JSONArray serverIdsArray = resultObj.getJSONArray("server_ids");
					JSONArray appIdsArray = resultObj.getJSONArray("app_ids");
					
					MealDao mealDao = mDaoSession.getMealDao();
					for(int i = 0; i<appIdsArray.length(); i++) {
						
						Meal meal = mealDao.load(appIdsArray.getLong(i));
						meal.setId_server(serverIdsArray.getLong(i));
						meal.setIs_synced(true);
						mealDao.update(meal);
						
						List<Meal_Food> mealFoods = meal.getMealFoods();
						
						Meal_FoodDao foodDao = mDaoSession.getMeal_FoodDao();
						for(Meal_Food mealFood : mealFoods) {
							mealFood.setIs_synced(true);
							foodDao.update(mealFood);
							
						}						
						
					}
					
					
					//					JSONObject resultObj = responsetObj.getJSONObject("result");
					//					String retServerId = resultObj.getString("last_id");
					//					long id = resultObj.getLong("app_id");
					//					
					//					Task task = taskDao.load(id);
					//					task.setServerId(retServerId);
					//					task.setIsSync(true);
					//					taskDao.update(task);
					//					
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	}

	public static void writeToFile(File pFile, StringBuffer pData) throws IOException {  
		BufferedWriter out = new BufferedWriter(new FileWriter(pFile));  
		out.write(pData.toString());  
		out.flush();  
		out.close();  
	}
}
