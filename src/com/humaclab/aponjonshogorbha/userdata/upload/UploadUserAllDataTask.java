package com.humaclab.aponjonshogorbha.userdata.upload;

import com.humaclab.aponjonshogorbha.util.DatabaseQueue;

import android.content.Context;

public class UploadUserAllDataTask {
	Context mCtx;
	public UploadUserAllDataTask(Context ctx) {
		this.mCtx = ctx;
	}
	
	public void runTasks() {
		
		//FOODS
		DatabaseQueue.execute(new Runnable() {			
			@Override
			public void run() {
				UploadMealFood upMealFood = new UploadMealFood(mCtx);
				upMealFood.Upload();				
			}
		});
				
		//DIARY
		DatabaseQueue.execute(new Runnable() {			
			@Override
			public void run() {
				UploadDiary upDiary = new UploadDiary(mCtx);
				upDiary.Upload();				
			}
		});
				
		//MOVE
		DatabaseQueue.execute(new Runnable() {			
			@Override
			public void run() {
				UploadMove upMove = new UploadMove(mCtx);
				upMove.Upload();				
			}
		});
		
		//WEIGHT
		DatabaseQueue.execute(new Runnable() {			
			@Override
			public void run() {
				UploadWeight upWeight = new UploadWeight(mCtx);
				upWeight.Upload();				
			}
		});
			
		//TASKS
		DatabaseQueue.execute(new Runnable() {			
			@Override
			public void run() {
				UploadCustomTask upCustomTask = new UploadCustomTask(mCtx);
				upCustomTask.Upload();				
			}
		});
		
		DatabaseQueue.execute(new Runnable() {			
			@Override
			public void run() {
				UploadCustomTaskStatus upCustomTaskStatus = new UploadCustomTaskStatus(mCtx);
				upCustomTaskStatus.Upload();				
			}
		});
				
		DatabaseQueue.execute(new Runnable() {			
			@Override
			public void run() {
				UploadSystemTaskStatus upSystemTaskStatus = new UploadSystemTaskStatus(mCtx);
				upSystemTaskStatus.Upload();				
			}
		});
		
	}
}
