package com.humaclab.aponjonshogorbha.userdata.upload;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import com.humaclab.aponjon.DaoMaster;
import com.humaclab.aponjon.DaoSession;
import com.humaclab.aponjon.DaoMaster.DevOpenHelper;
import com.humaclab.aponjon.Move;
import com.humaclab.aponjon.MoveDao;
import com.humaclab.aponjonshogorbha.AponjonRestClient;
import com.humaclab.aponjonshogorbha.AppConfig;
import com.humaclab.aponjonshogorbha.Constants;
import com.humaclab.aponjonshogorbha.SessionManager;
import com.humaclab.aponjonshogorbha.userdata.upload.UploadWeight.WeightUploadRequestHandler;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;

public class UploadMove {
	String mToken;
	Context mCtx;
	SessionManager mSessionMgr;
	DaoSession mDaoSession;
	AsyncHttpResponseHandler mHandlerMoveUpload;
	MoveDao mMoveDao;
	
	public UploadMove(Context ctx) {
		mCtx = ctx;
		mSessionMgr = new SessionManager(mCtx);
		SharedPreferences prefs = mSessionMgr.getPrefs();		
		mToken = prefs.getString(Constants.UserData.USER_TOKEN, null);

		DevOpenHelper helper = new DaoMaster.DevOpenHelper(mCtx, "aponjon-db", null);
		SQLiteDatabase db = helper.getWritableDatabase();
		DaoMaster daomaster = new DaoMaster(db);
		mDaoSession = daomaster.newSession();
	}
	
	public void Upload() {
		mMoveDao = mDaoSession.getMoveDao();
		List<Move> notSyncedMoves = mMoveDao.queryBuilder().whereOr(MoveDao.Properties.IsSync.eq(false), MoveDao.Properties.IsSync.isNull()).list();

		if(!notSyncedMoves.isEmpty()) {
			
			for(Move move : notSyncedMoves) {
				RequestParams params = new RequestParams();
				JSONObject reqObj = new JSONObject();
				try {
					/**
					 * { "token" : "4xAyjtRBPxLgrT1AD3WyXO2oPtz5C", 
					 *   "data": {
					 * 				"app_id" = "01"
					 * 		 		"counter" = "3"
					 * 				"date" = ""
					 * 		     }
					 * }
					 */
					reqObj.put("token", mToken);
					JSONObject reqObjMove = new JSONObject();
					reqObjMove.put("app_id", move.getId());
					reqObjMove.put("counter", move.getMoveCount());
					reqObjMove.put("date", move.getDate());
					
					reqObj.put("data", reqObjMove);

					params.put("data", reqObj.toString());
					
					//FileWriteBlock
//					StringBuffer msb1 = new StringBuffer();
//					msb1.append(reqObj.toString());
//					try {
//						File f = new File(Environment.getExternalStorageDirectory().getPath(), "requestMoveUpload.txt");
//						writeToFile(f, msb1);
//					} catch (IOException e) {
//						e.printStackTrace();
//					}
					
				} catch (JSONException e) {
					e.printStackTrace();
				}
				
				mHandlerMoveUpload = new MoveUploadRequestHandler();
				AponjonRestClient.post(AppConfig.URL_UPLOAD_MOVE, params, mHandlerMoveUpload);
			}

		}
	}
	
	class MoveUploadRequestHandler extends AsyncHttpResponseHandler {
		@Override
		public void onSuccess(String response) {
			
			//FileWriteBlock
//			StringBuffer msb1 = new StringBuffer();
//			msb1.append(response.toString());
//			try {
//				File f = new File(Environment.getExternalStorageDirectory().getPath(), "resultMoveUpload.txt");
//				writeToFile(f, msb1);
//			} catch (IOException e) {
//				e.printStackTrace();
//			}

			try {
				JSONObject responseObj = new JSONObject(response);
				int statusCode = responseObj.getInt("status_code");
				
				if(statusCode == 200) {
					JSONObject resultObj = responseObj.getJSONObject("result");
					MoveDao moveDao = mDaoSession.getMoveDao();
					long appId = resultObj.getLong("app_id");
//					long serverId = resultObj.getLong("last_id");
					Move move = moveDao.load(appId);
					move.setIsSync(true);
//					move.setId_server(serverId);
					
					moveDao.update(move);
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	}

	public static void writeToFile(File pFile, StringBuffer pData) throws IOException {  
		BufferedWriter out = new BufferedWriter(new FileWriter(pFile));  
		out.write(pData.toString());  
		out.flush();  
		out.close();  
	}
}
