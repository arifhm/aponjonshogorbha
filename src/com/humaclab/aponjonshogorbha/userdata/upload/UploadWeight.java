package com.humaclab.aponjonshogorbha.userdata.upload;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.humaclab.aponjon.DaoMaster;
import com.humaclab.aponjon.DaoSession;
import com.humaclab.aponjon.DaoMaster.DevOpenHelper;
import com.humaclab.aponjon.Diary;
import com.humaclab.aponjon.DiaryDao;
import com.humaclab.aponjon.Meal;
import com.humaclab.aponjon.MealDao;
import com.humaclab.aponjon.Task;
import com.humaclab.aponjon.User;
import com.humaclab.aponjon.UserDao;
import com.humaclab.aponjon.Weight;
import com.humaclab.aponjon.WeightDao;
import com.humaclab.aponjonshogorbha.AponjonRestClient;
import com.humaclab.aponjonshogorbha.AppConfig;
import com.humaclab.aponjonshogorbha.Constants;
import com.humaclab.aponjonshogorbha.SessionManager;
import com.humaclab.aponjonshogorbha.userdata.upload.UploadCustomTask.TaskUploadRequestHandler;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;

public class UploadWeight {
	String mToken;
	Context mCtx;
	SessionManager mSessionMgr;
	DaoSession mDaoSession;
	AsyncHttpResponseHandler mHandlerWeightUpload;
	WeightDao mWeightDao;
	Date mLmpDate;

	public UploadWeight(Context ctx) {
		mCtx = ctx;
		mSessionMgr = new SessionManager(mCtx);
		SharedPreferences prefs = mSessionMgr.getPrefs();		
		mToken = prefs.getString(Constants.UserData.USER_TOKEN, null);
		
		DevOpenHelper helper = new DaoMaster.DevOpenHelper(mCtx, "aponjon-db", null);
		SQLiteDatabase db = helper.getWritableDatabase();
		DaoMaster daomaster = new DaoMaster(db);
		mDaoSession = daomaster.newSession();
	}

	public void Upload() {
		mWeightDao = mDaoSession.getWeightDao();
//		UserDao userDao = mDaoSession.getUserDao();
//		User user = userDao.queryBuilder().where(UserDao.Properties.Token.eq(mToken)).unique();
		List<Weight> notSyncedWeights = mWeightDao.queryBuilder().whereOr(WeightDao.Properties.IsSync.eq(false), WeightDao.Properties.IsSync.isNull()).list();

		if(!notSyncedWeights.isEmpty()) {
			
			for(Weight weight : notSyncedWeights) {
				RequestParams params = new RequestParams();
				JSONObject reqObj = new JSONObject();
				try {
					/**
					 * { "token" : "4xAyjtRBPxLgrT1AD3WyXO2oPtz5C", 
					 *   "data": {
					 * 				"app_id" = ""
					 * 		 		"weight" = ""
					 * 				"weight_gain" = ""
					 * 				"date" = ""
					 * 		     }
					 * }
					 */
					reqObj.put("token", mToken);
					JSONObject reqObjWeight = new JSONObject();
					reqObjWeight.put("app_id", weight.getId());
					reqObjWeight.put("weight", weight.getWeight());
					reqObjWeight.put("weight_gain", weight.getWeightGain());
					
					SimpleDateFormat format = new SimpleDateFormat();
					format.applyPattern("yyyy-MM-dd hh:mm:ss");
					
					String date = format.format(weight.getDate());
					
//					reqObjWeight.put("lmp", user.getLmp());
					reqObjWeight.put("date", date);
					reqObj.put("data", reqObjWeight);

					params.put("data", reqObj.toString());
					
					//FileWriteBlock
//					StringBuffer msb1 = new StringBuffer();
//					msb1.append(reqObj.toString());
//					try {
//						File f = new File(Environment.getExternalStorageDirectory().getPath(), "requestWeightUpload.txt");
//						writeToFile(f, msb1);
//					} catch (IOException e) {
//						e.printStackTrace();
//					}
					
				} catch (JSONException e) {
					e.printStackTrace();
				}
				
				mHandlerWeightUpload = new WeightUploadRequestHandler();
				AponjonRestClient.post(AppConfig.URL_UPLOAD_WEIGHT, params, mHandlerWeightUpload);
			}

		}

	}


	class WeightUploadRequestHandler extends AsyncHttpResponseHandler {
		@Override
		public void onSuccess(String response) {
			
			//FileWriteBlock
//			StringBuffer msb1 = new StringBuffer();
//			msb1.append(response.toString());
//			try {
//				File f = new File(Environment.getExternalStorageDirectory().getPath(), "resultWeightUpload.txt");
//				writeToFile(f, msb1);
//			} catch (IOException e) {
//				e.printStackTrace();
//			}

			try {
				JSONObject responseObj = new JSONObject(response);
				int statusCode = responseObj.getInt("status_code");
				
				if(statusCode == 200) {
					JSONObject resultObj = responseObj.getJSONObject("result");
					
					WeightDao weightDao = mDaoSession.getWeightDao();
					long appId = resultObj.getLong("app_id");
					Weight weight = weightDao.load(appId);
					weight.setIsSync(true);
					
					weightDao.update(weight);
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	}

	public static void writeToFile(File pFile, StringBuffer pData) throws IOException {  
		BufferedWriter out = new BufferedWriter(new FileWriter(pFile));  
		out.write(pData.toString());  
		out.flush();  
		out.close();  
	}
}
