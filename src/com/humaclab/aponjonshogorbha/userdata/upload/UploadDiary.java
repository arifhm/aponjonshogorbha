package com.humaclab.aponjonshogorbha.userdata.upload;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import com.humaclab.aponjon.DaoMaster;
import com.humaclab.aponjon.DaoSession;
import com.humaclab.aponjon.Diary;
import com.humaclab.aponjon.DaoMaster.DevOpenHelper;
import com.humaclab.aponjon.DiaryDao;
import com.humaclab.aponjonshogorbha.AponjonRestClient;
import com.humaclab.aponjonshogorbha.AppConfig;
import com.humaclab.aponjonshogorbha.Constants;
import com.humaclab.aponjonshogorbha.SessionManager;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;

public class UploadDiary {
	String mToken;
	Context mCtx;
	SessionManager mSessionMgr;
	DaoSession mDaoSession;
	AsyncHttpResponseHandler mHandlerDiaryUpload;
	DiaryDao mDiaryDao;

	public UploadDiary(Context ctx) {
		mCtx = ctx;
		mSessionMgr = new SessionManager(mCtx);
		SharedPreferences prefs = mSessionMgr.getPrefs();		
		mToken = prefs.getString(Constants.UserData.USER_TOKEN, null);

		DevOpenHelper helper = new DaoMaster.DevOpenHelper(mCtx, "aponjon-db", null);
		SQLiteDatabase db = helper.getWritableDatabase();
		DaoMaster daomaster = new DaoMaster(db);
		mDaoSession = daomaster.newSession();
	}

	public void Upload() {
		mDiaryDao = mDaoSession.getDiaryDao();
		List<Diary> notSyncedDiarys = mDiaryDao.queryBuilder().whereOr(DiaryDao.Properties.IsSync.eq(false), DiaryDao.Properties.IsSync.isNull()).list();

		if(!notSyncedDiarys.isEmpty()) {

			for(Diary diary : notSyncedDiarys) {
				RequestParams params = new RequestParams();
				JSONObject reqObj = new JSONObject();
				try {
					/**
					 * { "token" : "4xAyjtRBPxLgrT1AD3WyXO2oPtz5C", 
					 *   "data": {
					 * 				"app_id" = ""
					 * 		 		"note" = ""
					 * 				"answers" = ""
					 * 				"image_path" = ""
					 * 				"date" = ""
					 * 		     }
					 * }
					 */
					reqObj.put("token", mToken);
					JSONObject reqObjDiary = new JSONObject();
					reqObjDiary.put("app_id", diary.getId());
					reqObjDiary.put("note", diary.getNote()) ;

					StringBuilder strBuilder = new StringBuilder();
					if(diary.getRg0() == null) {
						strBuilder.append("2,");
					} else if (diary.getRg0() == false) {
						strBuilder.append("0,");
					} else if (diary.getRg0() == true) {
						strBuilder.append("1,");
					}

					if(diary.getRg1() == null) {
						strBuilder.append("2,");
					} else if (diary.getRg1() == false) {
						strBuilder.append("0,");
					} else if (diary.getRg1() == true) {
						strBuilder.append("1,");
					}

					if(diary.getRg2() == null) {
						strBuilder.append("2,");
					} else if (diary.getRg2() == false) {
						strBuilder.append("0,");
					} else if (diary.getRg2() == true){
						strBuilder.append("1,");
					}

					if(diary.getRg3() == null) {
						strBuilder.append("2,");
					} else if (diary.getRg3() == false) {
						strBuilder.append("0,");
					} else if (diary.getRg3() == true){
						strBuilder.append("1,");
					}

					if(diary.getRg4() == null) {
						strBuilder.append("2,");
					} else if (diary.getRg4() == false) {
						strBuilder.append("0,");
					} else if (diary.getRg4() == true){
						strBuilder.append("1,");
					}

					if(diary.getRg5() == null) {
						strBuilder.append("2,");
					} else if (diary.getRg5() == false) {
						strBuilder.append("0,");
					} else if (diary.getRg5() == true) {
						strBuilder.append("1,");
					}

					if(diary.getRg6() == null) {
						strBuilder.append("2,");
					} else if (diary.getRg6() == false) {
						strBuilder.append("0,");
					} else if (diary.getRg6() == true) {
						strBuilder.append("1,");
					}

					if(diary.getRg7() == null) {
						strBuilder.append("2,");
					} else if (diary.getRg7() == false) {
						strBuilder.append("0,");
					} else if (diary.getRg7() == true) {
						strBuilder.append("1,");
					}

					if(diary.getRg8() == null) {
						strBuilder.append("2,");
					} else if (diary.getRg8() == false) {
						strBuilder.append("0,");
					} else if (diary.getRg8() == true){
						strBuilder.append("1,");
					}

					if(diary.getRg9() == null) {
						strBuilder.append("2");
					} else if (diary.getRg9() == false) {
						strBuilder.append("0");
					} else if (diary.getRg9() == true) {
						strBuilder.append("1");
					}

					String s = strBuilder.toString();

					reqObjDiary.put("answers", strBuilder.toString());
					if(diary.getPhotoPath() == null) {
						reqObjDiary.put("image_path", "");
					} else { 
						reqObjDiary.put("image_path", diary.getPhotoPath());
						// Upload Image operation
						
					}
					reqObjDiary.put("date", diary.getDate());

					reqObj.put("data", reqObjDiary);
					params.put("data", reqObj.toString());


					//FileWriteBlock
//					StringBuffer msb1 = new StringBuffer();
//					msb1.append(reqObj.toString());					
//					try {
//						File f = new File(Environment.getExternalStorageDirectory().getPath(), "requestDiaryUpload.txt");
//						writeToFile(f, msb1);
//					} catch (IOException e) {
//						e.printStackTrace();
//					}

				} catch (JSONException e) {
					e.printStackTrace();
				}

				mHandlerDiaryUpload = new DiaryUploadRequestHandler();
				AponjonRestClient.post(AppConfig.URL_UPLOAD_DIARY, params, mHandlerDiaryUpload);
			}

		}
	}

	class DiaryUploadRequestHandler extends AsyncHttpResponseHandler {
		@Override
		public void onSuccess(String response) {

			//FileWriteBlock
//			StringBuffer msb1 = new StringBuffer();
//			msb1.append(response.toString());
//			try {
//				File f = new File(Environment.getExternalStorageDirectory().getPath(), "resultDiaryUpload.txt");
//				writeToFile(f, msb1);
//			} catch (IOException e) {
//				e.printStackTrace();
//			}

			try {
				JSONObject responseObj = new JSONObject(response);
				int statusCode = responseObj.getInt("status_code");

				if(statusCode == 200) {
					JSONObject resultObj = responseObj.getJSONObject("result");
					DiaryDao diaryDao = mDaoSession.getDiaryDao();
					long appId = resultObj.getLong("app_id");
					long serverId = resultObj.getLong("last_id");
					Diary diary = diaryDao.load(appId);
					diary.setIsSync(true);
					diary.setId_server(serverId);
					
					diaryDao.update(diary);
					
					
					
					
					
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	}

	public static void writeToFile(File pFile, StringBuffer pData) throws IOException {  
		BufferedWriter out = new BufferedWriter(new FileWriter(pFile));  
		out.write(pData.toString());  
		out.flush();  
		out.close();  
	}

}
