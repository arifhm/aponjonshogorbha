package com.humaclab.aponjonshogorbha.userdata.download;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Environment;
import android.util.Log;

import com.humaclab.aponjonshogorbha.AponjonRestClient;
import com.humaclab.aponjonshogorbha.Constants;
import com.humaclab.aponjonshogorbha.SessionManager;
import com.humaclab.aponjonshogorbha.Constants.UserData;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class CheckRegistered {
	SessionManager mSessionMgr;
	Context mCtx;

	public CheckRegistered (Context ctx) {
		mCtx = ctx;
		mSessionMgr = new SessionManager(mCtx);
	}

	public void checkRegisterd() {		
		SharedPreferences prefs = mSessionMgr.getPrefs();
		String userNumber = prefs.getString(Constants.UserData.USER_PHONE, "01676000000");
		AponjonRestClient.get("http://api.aponjon.com.bd/is-mobile-exist/"+ userNumber, null, new AsyncHttpResponseHandler(){
						
			@Override
			public void onSuccess(String response) {
				
				try {
					JSONObject responseObj = new JSONObject(response);
					boolean status = responseObj.getBoolean("status");
					Editor editor = mSessionMgr.getEditor();
					if(status) {
						editor.putBoolean(Constants.UserData.IS_APONJON_REG, true);
						editor.commit();
					} else { 
						editor.putBoolean(Constants.UserData.IS_APONJON_REG, false);
						editor.commit();
					}

				} catch (JSONException e) {
					e.printStackTrace();
				}
			}

		});
	}



	public static void writeToFile(File pFile, StringBuffer pData) throws IOException {  
		BufferedWriter out = new BufferedWriter(new FileWriter(pFile));  
		out.write(pData.toString());  
		out.flush();  
		out.close();  
	}
}
