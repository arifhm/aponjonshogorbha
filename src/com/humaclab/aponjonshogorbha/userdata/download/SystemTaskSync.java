package com.humaclab.aponjonshogorbha.userdata.download;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;

import com.humaclab.aponjon.DaoMaster;
import com.humaclab.aponjon.DaoMaster.DevOpenHelper;
import com.humaclab.aponjon.DaoSession;
import com.humaclab.aponjon.Task;
import com.humaclab.aponjon.TaskDao;
import com.humaclab.aponjon.TaskType;
import com.humaclab.aponjon.TaskTypeDao;
import com.humaclab.aponjonshogorbha.AponjonRestClient;
import com.humaclab.aponjonshogorbha.AppConfig;
import com.humaclab.aponjonshogorbha.AponjonApplication;
import com.humaclab.aponjonshogorbha.Constants;
import com.humaclab.aponjonshogorbha.SessionManager;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class SystemTaskSync {
	String mToken;
	Context mCtx;
	SessionManager mSessionMgr;
	DaoSession mDaoSession;
	AsyncHttpResponseHandler mHandlerTaskType;
	AsyncHttpResponseHandler mHandlerTask;
	Date mLastSyncTime;
	AponjonApplication mApp;
//	boolean firstTime = false;


	public SystemTaskSync(Context ctx) {
		this.mCtx = ctx;
		mSessionMgr = new SessionManager(mCtx);
		SharedPreferences prefs = mSessionMgr.getPrefs();		
		mToken = prefs.getString(Constants.UserData.USER_TOKEN, null);
//		mApp = AppController.getInstance();
//		mDaoSession = mApp.getDaoSession();

		DevOpenHelper helper = new DaoMaster.DevOpenHelper(mCtx, "aponjon-db", null);
		SQLiteDatabase db = helper.getWritableDatabase();
		DaoMaster daomaster = new DaoMaster(db);
		mDaoSession = daomaster.newSession();

	}

	public void getTasks () {
//		SyncLogDao syncLogDao = mDaoSession.getSyncLogDao();
//		SyncLog synclog = syncLogDao.queryBuilder().where(SyncLogDao.Properties.Sync_name.eq("SystemTaskDownload")).unique();

		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//		
//		if(synclog != null) {
//			mLastSyncTime = synclog.getLast_sync_server();
//
//		} else {
			Date defaultDate=null;
			try {
				defaultDate = format.parse("1970-01-01 00:00:00");
			} catch (ParseException e) {
				e.printStackTrace();
			}
			mLastSyncTime = defaultDate;

//			firstTime = true;
//		}
		
		
		TaskTypeDao taskTypeDao = mDaoSession.getTaskTypeDao();
		List<TaskType> taskTypes = taskTypeDao.loadAll();
//		if(taskTypes.isEmpty()) {
			RequestParams params = new RequestParams();
			JSONObject reqObj = new JSONObject();
			try {
				/**
				 * { 
				 * 		"token":"4xAyjtRBPxLgrT1AD3WyXO2oPtz5C", 
				 * 		"data": {
				 * 					"last_sync_time": "21-04-2015 12:13:33"
				 * 				} 
				 * }
				 */
				reqObj.put("token", mToken);
				JSONObject reqObjTime = new JSONObject();
				reqObjTime.put("last_sync_time", mLastSyncTime);
				reqObj.put("data", reqObjTime);

				params.put("data", reqObj.toString());
			} catch (JSONException e) {
				e.printStackTrace();
			}
			
			mHandlerTaskType = new TaskTypeRequestHandler();
			AponjonRestClient.post(AppConfig.URL_TASK_CATEGORY_CONTENT, params, mHandlerTaskType);
//		}
	}

	public void getSystemTasks() {
		RequestParams params = new RequestParams();
		JSONObject reqObj = new JSONObject();
		try {
			/**
			 * { "token":"4xAyjtRBPxLgrT1AD3WyXO2oPtz5C", "data": {
			 * "last_sync_time":"21-04-2015 12:13:33" } }
			 */
			reqObj.put("token", mToken);
			JSONObject reqObjTime = new JSONObject();
			reqObjTime.put("last_sync_time", mLastSyncTime);
			reqObj.put("data", reqObjTime);

			params.put("data", reqObj.toString());
		} catch (JSONException e) {
			e.printStackTrace();
		}
		mHandlerTask = new TaskRequestHandler();
		AponjonRestClient.post(AppConfig.URL_SYSTEM_TASK_CONTENT, params, mHandlerTask);
	}
	
	class TaskTypeRequestHandler extends AsyncHttpResponseHandler {
		TaskTypeDao taskTypeDao;
		
		public TaskTypeRequestHandler() {
			taskTypeDao = mDaoSession.getTaskTypeDao();
		}
		
		
		@Override
		public void onSuccess(String response) {

			try {
				JSONObject responsetObj = new JSONObject(response);

				int statusCode = responsetObj.getInt("status_code");
				
				if(statusCode == 200) {
					JSONObject resultObj = responsetObj.getJSONObject("result");
					
//					//FileWriteBlock
//					StringBuffer msb1 = new StringBuffer();
//					msb1.append(resultObj.toString());
//					try {
//						File f = new File(Environment.getExternalStorageDirectory().getPath(), "resultObjSystemTypeTask.txt");
//						writeToFile(f, msb1);
//					} catch (IOException e) {
//						e.printStackTrace();
//					}									
//					//FileWriteBlock
					// insert
					JSONObject insertObj = resultObj.getJSONObject("insert");
					JSONArray taskCatArray = insertObj.getJSONArray("TaskCategory");
					if(!(taskCatArray.isNull(0))) {
						for(int i = 0; i<taskCatArray.length(); i++) {
							JSONObject taskCatObj = taskCatArray.getJSONObject(i);

							long id = taskCatObj.getLong("id");
							int catId = taskCatObj.getInt("type");
							String name = taskCatObj.getString("name");

							taskTypeDao.insert(new TaskType(id, catId, name));
						}									 
					}

					//update
					JSONObject updateObj = resultObj.getJSONObject("update");
					JSONArray taskCatUpdateArray = updateObj.getJSONArray("TaskCategory");
					if(!(taskCatUpdateArray.isNull(0))) {
						for(int i = 0; i<taskCatUpdateArray.length(); i++) {
							JSONObject taskCatObj = taskCatUpdateArray.getJSONObject(i);

							long id = taskCatObj.getLong("id");
							int catId = taskCatObj.getInt("type");
							String name = taskCatObj.getString("name");

							taskTypeDao.update(new TaskType(id, catId, name));
						}									 
					}

					//delete
					JSONObject deleteObj = resultObj.getJSONObject("delete");
					JSONArray taskCatDeleteArray = deleteObj.getJSONArray("TaskCategory");
					if(!(taskCatDeleteArray.isNull(0))) {
						for(int i = 0; i<taskCatDeleteArray.length(); i++) {
							JSONObject taskCatObj = taskCatDeleteArray.getJSONObject(i);

							long id = taskCatObj.getLong("id");

							TaskType taskType = taskTypeDao.queryBuilder().where(TaskTypeDao.Properties.Id.eq(id)).unique();
							taskTypeDao.delete(taskType);
						}									 
					}
					getSystemTasks();
				}
				
			} catch (JSONException e) {
				e.printStackTrace();
			}						
		}
	}
	
	class TaskRequestHandler extends AsyncHttpResponseHandler {
		TaskDao taskDao;
		
		public TaskRequestHandler() {
			taskDao = mDaoSession.getTaskDao();
		}

		@Override
		public void onSuccess(String response) {
			try {
				JSONObject responsetObj = new JSONObject(response);

				int statusCode = responsetObj.getInt("status_code");

				if(statusCode == 200) {
					JSONObject resultObj = responsetObj.getJSONObject("result");
					String new_sync_time = resultObj.getString("last_sync_time");
					
//					//FileWriteBlock
//					StringBuffer msb1 = new StringBuffer();
//					msb1.append(resultObj.toString());
//					try {
//						File f = new File(Environment.getExternalStorageDirectory().getPath(), "resultObjSystemTask.txt");
//						writeToFile(f, msb1);
//					} catch (IOException e) {
//						e.printStackTrace();
//					}									
//					//FileWriteBlock
					
					// insert
					JSONObject insertObj = resultObj.getJSONObject("insert");
					JSONArray taskInsertArray = insertObj.getJSONArray("SystemTask");
					
					
					if(!(taskInsertArray.isNull(0))) {
						for(int i = 0; i<taskInsertArray.length(); i++) {
							JSONObject taskObj = taskInsertArray.getJSONObject(i);
							
							String serverId = taskObj.getString("id");
							int catId = 2;
							String title = taskObj.getString("title");
							String description = taskObj.getString("description");
							int weekNo = taskObj.getInt("week");
							int dayNo = taskObj.getInt("day_no");
							boolean isSync = false;
							boolean isCompleted = false;
							long typeId = taskObj.getLong("task_category_id");
							
							
							Task task = null;
							try {
								task = taskDao.queryBuilder().where(TaskDao.Properties.ServerId.eq(serverId)).unique();
							} catch (Exception e) {
								task = null;
							}
							if(task == null) {
								taskDao.insert(new Task(null, serverId, catId, title, description, null, weekNo, dayNo, isSync, isCompleted, typeId));
							}
							
							
						}									 
					}

					//update
					JSONObject updateObj = resultObj.getJSONObject("update");
					JSONArray taskUpdateArray = updateObj.getJSONArray("SystemTask");
					
					
					if(!(taskUpdateArray.isNull(0))) {
						for(int i = 0; i<taskUpdateArray.length(); i++) {
							JSONObject taskObj = taskUpdateArray.getJSONObject(i);

							String serverId = taskObj.getString("id");
							int catId = 2;
							String title = taskObj.getString("title");
							String description = taskObj.getString("description");
							int weekNo = taskObj.getInt("week");
							int dayNo = taskObj.getInt("day_no");
							boolean isSync = false;
							boolean isCompleted = false;
							long typeId = taskObj.getLong("task_category_id");
							
							Task task = taskDao.queryBuilder().where(TaskDao.Properties.ServerId.eq(serverId)).unique();
							long key = task.getId();
							
							taskDao.update(new Task(key, serverId, catId, title, description, null, weekNo, dayNo, isSync, isCompleted, typeId));

						}									 
					}
					

					//delete
					JSONObject deleteObj = resultObj.getJSONObject("delete");
					JSONArray taskCatDeleteArray = deleteObj.getJSONArray("SystemTask");
					
					
					if(!(taskCatDeleteArray.isNull(0))) {
						for(int i = 0; i<taskCatDeleteArray.length(); i++) {
							JSONObject taskObj = taskCatDeleteArray.getJSONObject(i);

							long id = taskObj.getLong("id");

							Task task = taskDao.queryBuilder().where(TaskDao.Properties.Id.eq(id)).unique();
							taskDao.delete(task);
						}									 
					}
					
//					SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//					Date lastSyncTimeLocal = new Date(System.currentTimeMillis());
//					Date latestServerDate = null;
//					
//					try {
//						latestServerDate = format.parse(new_sync_time);
//					} catch (ParseException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					}
//					
//					SyncLogDao syncLogDao = mDaoSession.getSyncLogDao();
//					if(firstTime) {
//						syncLogDao.insert(new SyncLog(null, "SystemTaskDownload", latestServerDate, lastSyncTimeLocal));
//					} else {
//						SyncLog synclog = syncLogDao.queryBuilder().where(SyncLogDao.Properties.Sync_name.eq("SystemTaskDownload")).unique();
//						long key = synclog.getId();
//						syncLogDao.update(new SyncLog(key, "SystemTaskDownload", latestServerDate, lastSyncTimeLocal));
//					}
					
					CustomTaskSync syncCustomTast = new CustomTaskSync(mCtx);
	        		syncCustomTast.getTasks();
				}

			} catch (JSONException e) {
				e.printStackTrace();
			}						


		}
	}
	
	public static void writeToFile(File pFile, StringBuffer pData) throws IOException {  
		BufferedWriter out = new BufferedWriter(new FileWriter(pFile));  
		out.write(pData.toString());  
		out.flush();  
		out.close();  
	}
}
