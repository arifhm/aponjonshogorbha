package com.humaclab.aponjonshogorbha.userdata.download;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;

import com.humaclab.aponjon.DaoMaster;
import com.humaclab.aponjon.DaoMaster.DevOpenHelper;
import com.humaclab.aponjonshogorbha.AponjonRestClient;
import com.humaclab.aponjonshogorbha.AppConfig;
import com.humaclab.aponjonshogorbha.Constants;
import com.humaclab.aponjonshogorbha.SessionManager;
import com.humaclab.aponjonshogorbha.Constants.UserData;
import com.humaclab.aponjon.DaoSession;
import com.humaclab.aponjon.Food;
import com.humaclab.aponjon.FoodDao;
import com.humaclab.aponjon.Task;
import com.humaclab.aponjon.TaskDao;
import com.humaclab.aponjon.TaskType;
import com.humaclab.aponjon.TaskTypeDao;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class CustomTaskSync {
	String mToken;
	Context mCtx;
	SessionManager mSessionMgr;
	DaoSession mDaoSession;
	AsyncHttpResponseHandler mHandlerTaskCustom;

	public CustomTaskSync(Context ctx) {
		this.mCtx = ctx;
		mSessionMgr = new SessionManager(mCtx);
		SharedPreferences prefs = mSessionMgr.getPrefs();		
		mToken = prefs.getString(Constants.UserData.USER_TOKEN, null);

		DevOpenHelper helper = new DaoMaster.DevOpenHelper(mCtx, "aponjon-db", null);
		SQLiteDatabase db = helper.getWritableDatabase();
		DaoMaster daomaster = new DaoMaster(db);
		mDaoSession = daomaster.newSession();

	}

	public void getTasks () {
		TaskTypeDao taskTypeDao = mDaoSession.getTaskTypeDao();
		List<TaskType> taskTypes = taskTypeDao.queryBuilder().where(TaskTypeDao.Properties.CatId.eq(1)).list();
		if(taskTypes.isEmpty()) {
			//			TaskDao taskDao = mDaoSession.getTaskDao();

			RequestParams params = new RequestParams();
			JSONObject reqObj = new JSONObject();
			try {
				/**
				 * { "token":"4xAyjtRBPxLgrT1AD3WyXO2oPtz5C", "data": {
				 * "last_sync_time":"21-04-2015 12:13:33" } }
				 */
				reqObj.put("token", mToken);
				JSONObject reqObjTime = new JSONObject();
				reqObjTime.put("last_sync_time", "01-01-1970 00:00:00");
				reqObj.put("data", reqObjTime);

				params.put("data", reqObj.toString());
			} catch (JSONException e) {
				e.printStackTrace();
			}
			
			mHandlerTaskCustom = new TaskRequestHandler();
			AponjonRestClient.post(AppConfig.URL_CUSTOM_TASK_CONTENT, params, mHandlerTaskCustom);
		}
	}
	
	class TaskRequestHandler extends AsyncHttpResponseHandler {
		TaskDao taskDao;
		
		public TaskRequestHandler() {
			taskDao = mDaoSession.getTaskDao();
		}

		@Override
		public void onSuccess(String response) {
			try {
				JSONObject responsetObj = new JSONObject(response);

				int statusCode = responsetObj.getInt("status_code");

				if(statusCode == 200) {
					JSONObject resultObj = responsetObj.getJSONObject("result");
					
					//FileWriteBlock
//					StringBuffer msb1 = new StringBuffer();
//					msb1.append(resultObj.toString());
//					try {
//						File f = new File(Environment.getExternalStorageDirectory().getPath(), "resultObjSystemTask.txt");
//						writeToFile(f, msb1);
//					} catch (IOException e) {
//						e.printStackTrace();
//					}									
					//FileWriteBlock
					
					// insert
					JSONObject insertObj = resultObj.getJSONObject("insert");
					JSONArray taskInsertArray = insertObj.getJSONArray("SystemTask");
					
//					//FileWriteBlock
//					StringBuffer msb2 = new StringBuffer();
//					msb2.append(insertObj.toString());
//					try {
//						File f = new File(Environment.getExternalStorageDirectory().getPath(), "insertObjSystemTask.txt");
//						writeToFile(f, msb2);
//					} catch (IOException e) {
//						e.printStackTrace();
//					}
					
					if(!(taskInsertArray.isNull(0))) {
						for(int i = 0; i<taskInsertArray.length(); i++) {
							JSONObject taskObj = taskInsertArray.getJSONObject(i);
							
							String serverId = taskObj.getString("id");
							int catId = 2;
							String title = taskObj.getString("title");
							String description = taskObj.getString("description");
							int weekNo = taskObj.getInt("week");
							int dayNo = taskObj.getInt("day_no");
							boolean isSync = false;
							boolean isCompleted = false;
							long typeId = taskObj.getLong("task_category_id");
							
							
							Task task = null;
							try {
								task = taskDao.queryBuilder().where(TaskDao.Properties.ServerId.eq(serverId)).unique();
							} catch (Exception e) {
								task = null;
							}
							if(task == null) {
								taskDao.insert(new Task(null, serverId, catId, title, description, null, weekNo, dayNo, isSync, isCompleted, typeId));
							}
							
							
						}									 
					}

					//update
					JSONObject updateObj = resultObj.getJSONObject("update");
					JSONArray taskUpdateArray = updateObj.getJSONArray("SystemTask");
					
//					//FileWriteBlock
//					StringBuffer msb3 = new StringBuffer();
//					msb3.append(updateObj.toString());
//					try {
//						File f = new File(Environment.getExternalStorageDirectory().getPath(), "updateObjSystemTask.txt");
//						writeToFile(f, msb3);
//					} catch (IOException e) {
//						e.printStackTrace();
//					}
					
					if(!(taskUpdateArray.isNull(0))) {
						for(int i = 0; i<taskUpdateArray.length(); i++) {
							JSONObject taskObj = taskUpdateArray.getJSONObject(i);

							String serverId = taskObj.getString("id");
							int catId = 2;
							String title = taskObj.getString("title");
							String description = taskObj.getString("description");
							int weekNo = taskObj.getInt("week");
							int dayNo = taskObj.getInt("day_no");
							boolean isSync = false;
							boolean isCompleted = false;
							long typeId = taskObj.getLong("task_category_id");
							
							Task task = taskDao.queryBuilder().where(TaskDao.Properties.ServerId.eq(serverId)).unique();
							long key = task.getId();
							
							taskDao.update(new Task(key, serverId, catId, title, description, null, weekNo, dayNo, isSync, isCompleted, typeId));

						}									 
					}
					

					//delete
					JSONObject deleteObj = resultObj.getJSONObject("delete");
					JSONArray taskCatDeleteArray = deleteObj.getJSONArray("SystemTask");
					

//					//FileWriteBlock
//					StringBuffer msb4 = new StringBuffer();
//					msb4.append(deleteObj.toString());
//					try {
//						File f = new File(Environment.getExternalStorageDirectory().getPath(), "deleteObjSystemTask.txt");
//						writeToFile(f, msb4);
//					} catch (IOException e) {
//						e.printStackTrace();
//					}
					
					if(!(taskCatDeleteArray.isNull(0))) {
						for(int i = 0; i<taskCatDeleteArray.length(); i++) {
							JSONObject taskObj = taskCatDeleteArray.getJSONObject(i);

							long id = taskObj.getLong("id");

							Task task = taskDao.queryBuilder().where(TaskDao.Properties.Id.eq(id)).unique();
							taskDao.delete(task);
						}									 
					}
				}

			} catch (JSONException e) {
				e.printStackTrace();
			}						


		}
	}
	
	public static void writeToFile(File pFile, StringBuffer pData) throws IOException {  
		BufferedWriter out = new BufferedWriter(new FileWriter(pFile));  
		out.write(pData.toString());  
		out.flush();  
		out.close();  
	}
}
