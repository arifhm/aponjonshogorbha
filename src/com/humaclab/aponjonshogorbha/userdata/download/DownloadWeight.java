package com.humaclab.aponjonshogorbha.userdata.download;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.humaclab.aponjon.DaoMaster;
import com.humaclab.aponjon.DaoSession;
import com.humaclab.aponjon.Meal;
import com.humaclab.aponjon.MealDao;
import com.humaclab.aponjon.DaoMaster.DevOpenHelper;
import com.humaclab.aponjon.Move;
import com.humaclab.aponjon.MoveDao;
import com.humaclab.aponjon.Weight;
import com.humaclab.aponjon.WeightDao;
import com.humaclab.aponjonshogorbha.AponjonRestClient;
import com.humaclab.aponjonshogorbha.AppConfig;
import com.humaclab.aponjonshogorbha.Constants;
import com.humaclab.aponjonshogorbha.SessionManager;
import com.humaclab.aponjonshogorbha.userdata.download.DownloadMeal.MealDownloadRequestHandler;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;

public class DownloadWeight {
	String mToken;
	Context mCtx;
	SessionManager mSessionMgr;
	DaoSession mDaoSession;
	AsyncHttpResponseHandler mHandlerWeightDownload;

	public DownloadWeight(Context ctx) {
		this.mCtx = ctx;
		mSessionMgr = new SessionManager(mCtx);
		SharedPreferences prefs = mSessionMgr.getPrefs();		
		mToken = prefs.getString(Constants.UserData.USER_TOKEN, null);

		DevOpenHelper helper = new DaoMaster.DevOpenHelper(mCtx, "aponjon-db", null);
		SQLiteDatabase db = helper.getWritableDatabase();
		DaoMaster daomaster = new DaoMaster(db);
		mDaoSession = daomaster.newSession();
	}

	public void download() {
		WeightDao weightDao = mDaoSession.getWeightDao();
		List<Weight> weights = weightDao.queryBuilder().list();

		if(weights.isEmpty()) {

			RequestParams params = new RequestParams();
			JSONObject reqObj = new JSONObject();
			try {
				/**
				 * { "token":"4xAyjtRBPxLgrT1AD3WyXO2oPtz5C", "data": {
				 * "last_sync_time":"21-04-2015 12:13:33" } }
				 */
				reqObj.put("token", mToken);
				JSONObject reqObjTime = new JSONObject();
				reqObjTime.put("last_sync_time", "01-01-1970 00:00:00");
				reqObj.put("data", reqObjTime);

				params.put("data", reqObj.toString());

				//FileWriteBlock
//				StringBuffer msb1 = new StringBuffer();
//				msb1.append(reqObj.toString());
//				try {
//					File f = new File(Environment.getExternalStorageDirectory().getPath(), "requestDownloadWeight.txt");
//					writeToFile(f, msb1);
//				} catch (IOException e) {
//					e.printStackTrace();
//				}

			} catch (JSONException e) {
				e.printStackTrace();
			}

			mHandlerWeightDownload = new WeightDownloadRequestHandler();
			AponjonRestClient.post(AppConfig.URL_DOWNLOAD_WEIGHT, params, mHandlerWeightDownload);
		}
	}

	class WeightDownloadRequestHandler extends AsyncHttpResponseHandler {

		@Override
		public void onSuccess(String response) {
			try {
				JSONObject responseObj = new JSONObject(response);

				//FileWriteBlock
				StringBuffer msb1 = new StringBuffer();
//				msb1.append(responseObj.toString());
//				try {
//					File f = new File(Environment.getExternalStorageDirectory().getPath(), "resultDownloadWeight.txt");
//					writeToFile(f, msb1);
//				} catch (IOException e) {
//					e.printStackTrace();
//				}									
				//FileWriteBlock
				int statusCode = responseObj.getInt("status_code");

				if(statusCode == 200) {
					JSONObject resultObj = responseObj.getJSONObject("result");
					JSONArray weightListArray = resultObj.getJSONArray("Weight");

					WeightDao weightDao = mDaoSession.getWeightDao();

					SimpleDateFormat format = new SimpleDateFormat();
					format.applyPattern("yyyy-MM-dd hh:mm:ss");

					for(int i=0; i<weightListArray.length(); i++) {
						JSONObject weightObj = weightListArray.getJSONObject(i);

						Weight weight = new Weight();
						weight.setWeight(weightObj.getInt("weight"));
						weight.setWeightGain(weightObj.getDouble("weight_gain"));

						Date date = null;
						try {
							date = format.parse(weightObj.getString("date"));
						} catch (ParseException e) {
							e.printStackTrace();
						}
						weight.setDate(date);
						weight.setIsSync(true);

						weightDao.insert(weight);
					}

				}

			} catch (JSONException e) {
				e.printStackTrace();
			}						


		}
	}

	public static void writeToFile(File pFile, StringBuffer pData) throws IOException {  
		BufferedWriter out = new BufferedWriter(new FileWriter(pFile));  
		out.write(pData.toString());  
		out.flush();  
		out.close();  
	}
}
