package com.humaclab.aponjonshogorbha.userdata.download;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;

import com.humaclab.aponjon.DaoMaster;
import com.humaclab.aponjon.DaoMaster.DevOpenHelper;
import com.humaclab.aponjon.DaoSession;
import com.humaclab.aponjon.Diary;
import com.humaclab.aponjon.DiaryDao;
import com.humaclab.aponjon.Meal;
import com.humaclab.aponjon.WeightDao;
import com.humaclab.aponjonshogorbha.AponjonRestClient;
import com.humaclab.aponjonshogorbha.AppConfig;
import com.humaclab.aponjonshogorbha.Constants;
import com.humaclab.aponjonshogorbha.SessionManager;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class DownloadDiary {
	String mToken;
	Context mCtx;
	SessionManager mSessionMgr;
	DaoSession mDaoSession;
	AsyncHttpResponseHandler mHandlerDiaryDownload;
	
	public DownloadDiary(Context ctx) {
		this.mCtx = ctx;
		mSessionMgr = new SessionManager(mCtx);
		SharedPreferences prefs = mSessionMgr.getPrefs();		
		mToken = prefs.getString(Constants.UserData.USER_TOKEN, null);

		DevOpenHelper helper = new DaoMaster.DevOpenHelper(mCtx, "aponjon-db", null);
		SQLiteDatabase db = helper.getWritableDatabase();
		DaoMaster daomaster = new DaoMaster(db);
		mDaoSession = daomaster.newSession();
	}
	
	public void download() {
		DiaryDao diaryDao = mDaoSession.getDiaryDao();
		List<Diary> diary = diaryDao.queryBuilder().list();
		if(diary.isEmpty()) {
			RequestParams params = new RequestParams();
			JSONObject reqObj = new JSONObject();
			try {
				/**
				 * { "token":"4xAyjtRBPxLgrT1AD3WyXO2oPtz5C", "data": {
				 * "last_sync_time":"21-04-2015 12:13:33" } }
				 */
				reqObj.put("token", mToken);
				JSONObject reqObjTime = new JSONObject();
				reqObjTime.put("last_sync_time", "01-01-1970 00:00:00");
				reqObj.put("data", reqObjTime);

				params.put("data", reqObj.toString());

				//FileWriteBlock
//				StringBuffer msb1 = new StringBuffer();
//				msb1.append(reqObj.toString());
//				try {
//					File f = new File(Environment.getExternalStorageDirectory().getPath(), "requestDownloadDiary.txt");
//					writeToFile(f, msb1);
//				} catch (IOException e) {
//					e.printStackTrace();
//				}
				
			} catch (JSONException e) {
				e.printStackTrace();
			}

			mHandlerDiaryDownload = new DiaryDownloadRequestHandler();
			AponjonRestClient.post(AppConfig.URL_DOWNLOAD_DIARY, params, mHandlerDiaryDownload);
		}
	}
	
	class DiaryDownloadRequestHandler extends AsyncHttpResponseHandler {

		@Override
		public void onSuccess(String response) {
			try {
				JSONObject responseObj = new JSONObject(response);
				
				//FileWriteBlock
//				StringBuffer msb1 = new StringBuffer();
//				msb1.append(responseObj.toString());
//				try {
//					File f = new File(Environment.getExternalStorageDirectory().getPath(), "resultDownloadDiary.txt");
//					writeToFile(f, msb1);
//				} catch (IOException e) {
//					e.printStackTrace();
//				}									
				//FileWriteBlock
				int statusCode = responseObj.getInt("status_code");

				if(statusCode == 200) {
					JSONObject resultObj = responseObj.getJSONObject("result");
					JSONArray notesArray = resultObj.getJSONArray("Note");
					
					DiaryDao diaryDao = mDaoSession.getDiaryDao();
					
					SimpleDateFormat format = new SimpleDateFormat();
					format.applyPattern("yyyy-MM-dd hh:mm:ss");
					
					for(int i=0; i<notesArray.length(); i++) {
						JSONObject noteObj = notesArray.getJSONObject(i);
						
						Diary diary = new Diary();
						diary.setId_server(noteObj.getLong("id"));
						diary.setNote(noteObj.getString("note"));
						String answers = noteObj.getString("answers");
						String[] ansArray = answers.split(",");
						
						if(ansArray[0].equals("1")) {
							diary.setRg0(true);
						} else if (ansArray[0].equals("0")) {
							diary.setRg0(false);
						}						
						if(ansArray[1].equals("1")) {
							diary.setRg1(true);
						} else if (ansArray[1].equals("0")) {
							diary.setRg1(false);
						}
						if(ansArray[2].equals("1")) {
							diary.setRg2(true);
						} else if (ansArray[2].equals("0")) {
							diary.setRg2(false);
						}
						if(ansArray[3].equals("1")) {
							diary.setRg3(true);
						} else if (ansArray[3].equals("0")) {
							diary.setRg3(false);
						}
						if(ansArray[4].equals("1")) {
							diary.setRg4(true);
						} else if (ansArray[4].equals("0")) {
							diary.setRg4(false);
						}
						if(ansArray[5].equals("1")) {
							diary.setRg5(true);
						} else if (ansArray[5].equals("0")) {
							diary.setRg5(false);
						}
						if(ansArray[6].equals("1")) {
							diary.setRg6(true);
						} else if (ansArray[6].equals("0")) {
							diary.setRg6(false);
						}
						if(ansArray[7].equals("1")) {
							diary.setRg7(true);
						} else if (ansArray[7].equals("0")) {
							diary.setRg7(false);
						}
						if(ansArray[8].equals("1")) {
							diary.setRg8(true);
						} else if (ansArray[8].equals("0")) {
							diary.setRg8(false);
						}
						if(ansArray[9].equals("1")) {
							diary.setRg9(true);
						} else if (ansArray[9].equals("0")) {
							diary.setRg9(false);
						}
						
						if(!noteObj.getString("image_path").equalsIgnoreCase("null")) {
							diary.setPhotoPath(noteObj.getString("image_path"));
						}
						
						Date date = null;
						try {
							date = format.parse(noteObj.getString("date")+" 18:30:00");
						} catch (ParseException e) {
							e.printStackTrace();
						}
						diary.setDate(date);
						diary.setIsSync(true);
						
						diaryDao.insert(diary);
					}
					
				}

			} catch (JSONException e) {
				e.printStackTrace();
			}						


		}
	}

	public static void writeToFile(File pFile, StringBuffer pData) throws IOException {  
		BufferedWriter out = new BufferedWriter(new FileWriter(pFile));  
		out.write(pData.toString());  
		out.flush();  
		out.close();  
	}
}
