package com.humaclab.aponjonshogorbha.userdata.download;

import java.util.Timer;
import java.util.TimerTask;

import android.content.Context;

public class DownloadUserAllDataTask {
	Context mCtx;
	public DownloadUserAllDataTask(Context ctx) {
		this.mCtx = ctx;
	}
	
	public void runTasks() {
		//TASKS
		SystemTaskSync syncSystemTask = new SystemTaskSync(mCtx);
		syncSystemTask.getTasks();
		
		//FOODS
		DownloadMeal downMeal = new DownloadMeal(mCtx);
		downMeal.download();
		
		//DIARY
		DownloadDiary downDiary = new DownloadDiary(mCtx);
		downDiary.download();
		
		//WEIGHT
		DownloadWeight downWeight = new DownloadWeight(mCtx);
		downWeight.download();
		
		//MOVE
		DownloadMove downMove = new DownloadMove(mCtx);
		downMove.download();
		
//		Timer timer1 = new Timer();
//		timer1.scheduleAtFixedRate(new TimerTask() {
//		            @Override
//		            public void run() {
//		            	CustomTaskSync syncCustomTast = new CustomTaskSync(mCtx);
//		        		syncCustomTast.getTasks();
//		            }
//		        }, 2000, 2000);
		Timer timer2 = new Timer();
		timer2.scheduleAtFixedRate(new TimerTask() {
		            @Override
		            public void run() {
		                DownloadMealFood downMealFood = new DownloadMealFood(mCtx);
		                downMealFood.download();
		            }
		        }, 5000, 5000);
	}
}
