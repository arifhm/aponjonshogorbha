package com.humaclab.aponjonshogorbha.userdata.download;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.humaclab.aponjon.DaoMaster;
import com.humaclab.aponjon.DaoSession;
import com.humaclab.aponjon.Food;
import com.humaclab.aponjon.FoodDao;
import com.humaclab.aponjon.Food_Category;
import com.humaclab.aponjon.Food_CategoryDao;
import com.humaclab.aponjon.Meal;
import com.humaclab.aponjon.MealDao;
import com.humaclab.aponjon.Meal_Food;
import com.humaclab.aponjon.Meal_FoodDao;
import com.humaclab.aponjon.TaskDao;
import com.humaclab.aponjon.DaoMaster.DevOpenHelper;
import com.humaclab.aponjonshogorbha.AponjonRestClient;
import com.humaclab.aponjonshogorbha.AppConfig;
import com.humaclab.aponjonshogorbha.Constants;
import com.humaclab.aponjonshogorbha.SessionManager;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import android.util.Log;

public class DownloadMealFood {
	String mToken;
	Context mCtx;
	SessionManager mSessionMgr;
	DaoSession mDaoSession;
	AsyncHttpResponseHandler mHandlerMealFood;

	public DownloadMealFood(Context ctx) {		
		this.mCtx = ctx;
		mSessionMgr = new SessionManager(ctx);
		SharedPreferences prefs = mSessionMgr.getPrefs();		
		mToken = prefs.getString(Constants.UserData.USER_TOKEN, null);

		DevOpenHelper helper = new DaoMaster.DevOpenHelper(mCtx, "aponjon-db", null);
		SQLiteDatabase db = helper.getWritableDatabase();
		DaoMaster daomaster = new DaoMaster(db);
		mDaoSession = daomaster.newSession();
	}

	public void download() {
		Meal_FoodDao mealFoodDao = mDaoSession.getMeal_FoodDao();
		List<Meal_Food> mealFoods = mealFoodDao.queryBuilder().list();
		if(mealFoods.isEmpty()) {

			RequestParams params = new RequestParams();
			JSONObject reqObj = new JSONObject();
			try {
				/**
				 * { "token":"4xAyjtRBPxLgrT1AD3WyXO2oPtz5C", "data": {
				 * "last_sync_time":"21-04-2015 12:13:33" } }
				 */
				reqObj.put("token", mToken);
				JSONObject reqObjTime = new JSONObject();
				reqObjTime.put("last_sync_time", "01-01-1970 00:00:00");
				reqObj.put("data", reqObjTime);

				params.put("data", reqObj.toString());

				//FileWriteBlock
//				StringBuffer msb1 = new StringBuffer();
//				msb1.append(reqObj.toString());
//				try {
//					File f = new File(Environment.getExternalStorageDirectory().getPath(), "requestDownloadMealFood.txt");
//					writeToFile(f, msb1);
//				} catch (IOException e) {
//					e.printStackTrace();
//				}

			} catch (JSONException e) {
				e.printStackTrace();
			}

			mHandlerMealFood = new MealFoodDownloadRequestHandler();
			AponjonRestClient.post(AppConfig.URL_DOWNLOAD_MEAL_FOOD, params, mHandlerMealFood);
		}
	}


	class MealFoodDownloadRequestHandler extends AsyncHttpResponseHandler {

		@Override
		public void onSuccess(String response) {
			try {
				JSONObject responsetObj = new JSONObject(response);

				int statusCode = responsetObj.getInt("status_code");


				if(statusCode == 200) {
					JSONObject resultObj = responsetObj.getJSONObject("result");
					JSONArray mealListArray = resultObj.getJSONArray("MealItem");
					
					//FileWriteBlock
//					StringBuffer msb1 = new StringBuffer();
//					msb1.append(mealListArray.toString());
//					try {
//						File f = new File(Environment.getExternalStorageDirectory().getPath(), "resultDownloadMealFood.txt");
//						writeToFile(f, msb1);
//					} catch (IOException e) {
//						e.printStackTrace();
//					}									
					//FileWriteBlock
					
					Food_CategoryDao foodCatDao = mDaoSession.getFood_CategoryDao();
					FoodDao foodDao = mDaoSession.getFoodDao();
					MealDao mealDao = mDaoSession.getMealDao();
					Meal_FoodDao mealFoodDao = mDaoSession.getMeal_FoodDao();

					for(int i=0; i< mealListArray.length(); i++) {
						JSONObject mealFoodObj = mealListArray.getJSONObject(i);


						//						"id":"10",
						//			            "app_list_id":"1",
						//			            "food_id":"57",
						//			            "food_id_server" : "58"
						//			            "meal_list_id":"1",
						//			            "food_carbohydrate_calorie":"193.11",
						//			            "food_protein_calorie":"4.77",
						//			            "food_fat_calorie":"13.29",
						//			            "total_food_calorie":"211.17",
						//			            "unit":"1",
						//			            "amount":"3.0"
						
						
						Meal meal = mealDao.queryBuilder().where(MealDao.Properties.Id_server.eq(mealFoodObj.getLong("meal_list_id"))).unique();
						Food food = foodDao.queryBuilder().where(FoodDao.Properties.Id_server.eq(mealFoodObj.getLong("food_id_server"))).unique();
						Food_Category category = foodCatDao.queryBuilder().where(Food_CategoryDao.Properties.Id.eq(food.getCategory_id())).unique();


						Meal_Food mealFood = new Meal_Food();
						mealFood.setId_server(mealFoodObj.getLong("id"));
						mealFood.setFood_data_id(food.getId());
						mealFood.setMeal_id(meal.getId());
						mealFood.setCategory_name(category.getFood_category_name());
						mealFood.setFood_desc(food.getDesc());
						mealFood.setAbsolute_name(food.getAbsolute_name());
						mealFood.setFood_unit(food.getUnit());
						mealFood.setIncreament_value(food.getIncreament_value());
						mealFood.setCal_per_unit_curb(food.getCal_per_unit_curb());
						mealFood.setCal_per_unit_protien(food.getCal_per_unit_protien());
						mealFood.setCal_per_unit_fat(food.getCal_per_unit_fat());
						mealFood.setCal_per_unit_total(food.getCal_per_unit_total());
						if(!mealFoodObj.getString("food_carbohydrate_calorie").equals("null")) {
							mealFood.setFood_cal_set_curb(Float.parseFloat(mealFoodObj.getString("food_carbohydrate_calorie")));
						}

						if(!mealFoodObj.getString("food_protein_calorie").equals("null")) {
							mealFood.setFood_cal_set_protien(Float.parseFloat(mealFoodObj.getString("food_protein_calorie")));
						}

						if(!mealFoodObj.getString("food_fat_calorie").equals("null")) {
							mealFood.setFood_cal_set_fat(Float.parseFloat(mealFoodObj.getString("food_fat_calorie")));
						}

						if(!mealFoodObj.getString("total_food_calorie").equals("null")) {
							mealFood.setFood_cal_set_total(Float.parseFloat(mealFoodObj.getString("total_food_calorie")));
						}

						if(!mealFoodObj.getString("amount").equals("null")) {
							mealFood.setUnit_amount(Float.parseFloat(mealFoodObj.getString("amount")));
						}

						mealFood.setIs_synced(true);
						mealFoodDao.insert(mealFood);

					}
				}

			} catch (JSONException e) {
				e.printStackTrace();
			}						


		}
	}

	public static void writeToFile(File pFile, StringBuffer pData) throws IOException {  
		BufferedWriter out = new BufferedWriter(new FileWriter(pFile));  
		out.write(pData.toString());  
		out.flush();  
		out.close();  
	}
}
