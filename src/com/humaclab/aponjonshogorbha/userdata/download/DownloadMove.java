package com.humaclab.aponjonshogorbha.userdata.download;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.humaclab.aponjon.DaoMaster;
import com.humaclab.aponjon.DaoSession;
import com.humaclab.aponjon.Meal;
import com.humaclab.aponjon.MealDao;
import com.humaclab.aponjon.DaoMaster.DevOpenHelper;
import com.humaclab.aponjon.Move;
import com.humaclab.aponjon.MoveDao;
import com.humaclab.aponjonshogorbha.AponjonRestClient;
import com.humaclab.aponjonshogorbha.AppConfig;
import com.humaclab.aponjonshogorbha.Constants;
import com.humaclab.aponjonshogorbha.SessionManager;
import com.humaclab.aponjonshogorbha.userdata.download.DownloadMeal.MealDownloadRequestHandler;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;

public class DownloadMove {
	String mToken;
	Context mCtx;
	SessionManager mSessionMgr;
	DaoSession mDaoSession;
	AsyncHttpResponseHandler mHandlerMoveDownload;
	
	public DownloadMove(Context ctx) {
		this.mCtx = ctx;
		mSessionMgr = new SessionManager(mCtx);
		SharedPreferences prefs = mSessionMgr.getPrefs();		
		mToken = prefs.getString(Constants.UserData.USER_TOKEN, null);

		DevOpenHelper helper = new DaoMaster.DevOpenHelper(mCtx, "aponjon-db", null);
		SQLiteDatabase db = helper.getWritableDatabase();
		DaoMaster daomaster = new DaoMaster(db);
		mDaoSession = daomaster.newSession();
	}
	
	public void download() {
		MoveDao moveDao = mDaoSession.getMoveDao();
		List<Move> moves = moveDao.queryBuilder().list();
		if(moves.isEmpty()) {

			RequestParams params = new RequestParams();
			JSONObject reqObj = new JSONObject();
			try {
				/**
				 * { "token":"4xAyjtRBPxLgrT1AD3WyXO2oPtz5C", "data": {
				 * "last_sync_time":"21-04-2015 12:13:33" } }
				 */
				reqObj.put("token", mToken);
				JSONObject reqObjTime = new JSONObject();
				reqObjTime.put("last_sync_time", "01-01-1970 00:00:00");
				reqObj.put("data", reqObjTime);

				params.put("data", reqObj.toString());

				//FileWriteBlock
//				StringBuffer msb1 = new StringBuffer();
//				msb1.append(reqObj.toString());
//				try {
//					File f = new File(Environment.getExternalStorageDirectory().getPath(), "requestDownloadMove.txt");
//					writeToFile(f, msb1);
//				} catch (IOException e) {
//					e.printStackTrace();
//				}
				
			} catch (JSONException e) {
				e.printStackTrace();
			}

			mHandlerMoveDownload = new MoveDownloadRequestHandler();
			AponjonRestClient.post(AppConfig.URL_DOWNLOAD_MOVE, params, mHandlerMoveDownload);
		}
	}
	
	class MoveDownloadRequestHandler extends AsyncHttpResponseHandler {

		@Override
		public void onSuccess(String response) {
			try {
				JSONObject responsetObj = new JSONObject(response);

				int statusCode = responsetObj.getInt("status_code");

				if(statusCode == 200) {
					JSONObject resultObj = responsetObj.getJSONObject("result");
					JSONArray moveListArray = resultObj.getJSONArray("KickCounter");
					
					MoveDao moveDao = mDaoSession.getMoveDao();
					
					SimpleDateFormat format = new SimpleDateFormat();
					format.applyPattern("yyyy-MM-dd hh:mm:ss");
					
					for(int i=0; i<moveListArray.length(); i++) {
						JSONObject moveObj = moveListArray.getJSONObject(i);
						int counter = moveObj.getInt("counter");
						
						Date date = null;
						try {
							date = format.parse(moveObj.getString("date"));
						} catch (ParseException e) {
							e.printStackTrace();
						}
						
						Move move = new Move();
						move.setMoveCount(counter);
						move.setDate(date);
						move.setIsSync(true);
						
						moveDao.insert(move);
					}
					
					

					//FileWriteBlock
//					StringBuffer msb1 = new StringBuffer();
//					msb1.append(resultObj.toString());
//					try {
//						File f = new File(Environment.getExternalStorageDirectory().getPath(), "resultDownloadMove.txt");
//						writeToFile(f, msb1);
//					} catch (IOException e) {
//						e.printStackTrace();
//					}									
					//FileWriteBlock
				}

			} catch (JSONException e) {
				e.printStackTrace();
			}						


		}
	}

	public static void writeToFile(File pFile, StringBuffer pData) throws IOException {  
		BufferedWriter out = new BufferedWriter(new FileWriter(pFile));  
		out.write(pData.toString());  
		out.flush();  
		out.close();  
	}
}
