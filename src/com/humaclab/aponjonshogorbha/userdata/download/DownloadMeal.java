package com.humaclab.aponjonshogorbha.userdata.download;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.humaclab.aponjon.DaoMaster;
import com.humaclab.aponjon.DaoSession;
import com.humaclab.aponjon.Meal;
import com.humaclab.aponjon.MealDao;
import com.humaclab.aponjon.TaskDao;
import com.humaclab.aponjon.DaoMaster.DevOpenHelper;
import com.humaclab.aponjonshogorbha.AponjonRestClient;
import com.humaclab.aponjonshogorbha.AppConfig;
import com.humaclab.aponjonshogorbha.Constants;
import com.humaclab.aponjonshogorbha.SessionManager;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;

public class DownloadMeal {
	String mToken;
	Context mCtx;
	SessionManager mSessionMgr;
	DaoSession mDaoSession;
	AsyncHttpResponseHandler mHandlerMeal;

	public DownloadMeal(Context ctx) {		
		this.mCtx = ctx;
		mSessionMgr = new SessionManager(mCtx);
		SharedPreferences prefs = mSessionMgr.getPrefs();		
		mToken = prefs.getString(Constants.UserData.USER_TOKEN, null);

		DevOpenHelper helper = new DaoMaster.DevOpenHelper(mCtx, "aponjon-db", null);
		SQLiteDatabase db = helper.getWritableDatabase();
		DaoMaster daomaster = new DaoMaster(db);
		mDaoSession = daomaster.newSession();
	}

	public void download() {
		MealDao mealDao = mDaoSession.getMealDao();
		List<Meal> meals = mealDao.queryBuilder().list();
		if(meals.isEmpty()) {

			RequestParams params = new RequestParams();
			JSONObject reqObj = new JSONObject();
			try {
				/**
				 * { "token":"4xAyjtRBPxLgrT1AD3WyXO2oPtz5C", "data": {
				 * "last_sync_time":"21-04-2015 12:13:33" } }
				 */
				reqObj.put("token", mToken);
				JSONObject reqObjTime = new JSONObject();
				reqObjTime.put("last_sync_time", "01-01-1970 00:00:00");
				reqObj.put("data", reqObjTime);

				params.put("data", reqObj.toString());

				//FileWriteBlock
//				StringBuffer msb1 = new StringBuffer();
//				msb1.append(reqObj.toString());
//				try {
//					File f = new File(Environment.getExternalStorageDirectory().getPath(), "requestDownloadMeal.txt");
//					writeToFile(f, msb1);
//				} catch (IOException e) {
//					e.printStackTrace();
//				}
				
			} catch (JSONException e) {
				e.printStackTrace();
			}

			mHandlerMeal = new MealDownloadRequestHandler();
			AponjonRestClient.post(AppConfig.URL_DOWNLOAD_MEAL, params, mHandlerMeal);
		}
	}


	class MealDownloadRequestHandler extends AsyncHttpResponseHandler {

		@Override
		public void onSuccess(String response) {
			try {
				JSONObject responsetObj = new JSONObject(response);

				int statusCode = responsetObj.getInt("status_code");

				if(statusCode == 200) {
					JSONObject resultObj = responsetObj.getJSONObject("result");
					JSONArray mealListArray = resultObj.getJSONArray("MealList");
					
					MealDao mealDao = mDaoSession.getMealDao();
					SimpleDateFormat format = new SimpleDateFormat();
					format.applyPattern("yyyy-MM-dd");
					
					for(int i=0; i< mealListArray.length(); i++) {
						JSONObject mealObj = mealListArray.getJSONObject(i);
										        	 
						Meal meal = new Meal();
						meal.setId_server(mealObj.getLong("id"));
						meal.setMeal_name(mealObj.getString("name"));
						meal.setMeal_cal_set_curb((float) mealObj.getDouble("carbohydrate_calorie"));
						meal.setMeal_cal_set_protien((float) mealObj.getDouble("protein_calorie"));
						meal.setMeal_cal_set_fat((float) mealObj.getDouble("fat_calorie"));
						meal.setMeal_cal_set_total((float) mealObj.getDouble("total_calorie"));
						meal.setReq_cal_curb((float) mealObj.getDouble("min_carbohydrate_calorie"));
						meal.setReq_cal_protien((float) mealObj.getDouble("min_protein_calorie"));
						meal.setReq_cal_fat((float) mealObj.getDouble("min_fat_calorie"));
						meal.setReq_cal_total((float) mealObj.getDouble("min_total_calorie"));
						
						Date date = null;
						try {
							date = format.parse(mealObj.getString("date"));
						} catch (ParseException e) {
							
						}
						meal.setCreation_date(date);
						meal.setIs_synced(true);
						
						mealDao.insert(meal);
					}

					//FileWriteBlock
//					StringBuffer msb1 = new StringBuffer();
//					msb1.append(resultObj.toString());
//					try {
//						File f = new File(Environment.getExternalStorageDirectory().getPath(), "resultDownloadMeal.txt");
//						writeToFile(f, msb1);
//					} catch (IOException e) {
//						e.printStackTrace();
//					}									
					//FileWriteBlock
				}

			} catch (JSONException e) {
				e.printStackTrace();
			}						


		}
	}

	public static void writeToFile(File pFile, StringBuffer pData) throws IOException {  
		BufferedWriter out = new BufferedWriter(new FileWriter(pFile));  
		out.write(pData.toString());  
		out.flush();  
		out.close();  
	}
}
