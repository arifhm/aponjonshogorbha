package com.humaclab.aponjonshogorbha;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class CalculateCurrentPER {
	Context mCtx;
	SessionManager mSessionMgr;
	SharedPreferences mPrefs;
	Editor mEditor;
	int mCurWeek;
	float bmr;
	float height;
	int weight;
	int age;
	

	public CalculateCurrentPER(Context ctx) {
		this.mCtx = ctx;
		mSessionMgr = new SessionManager(mCtx);
		mPrefs = mSessionMgr.getPrefs();
		mEditor = mSessionMgr.getEditor();
	}

	public void calculatePer() {
		float perT=0, perCmin = 0, perPmin = 0, perFmin = 0, perTmin = 0;
		
		mCurWeek = mPrefs.getInt(Constants.UserData.USER_CURRENT_WEEK, 6);
		height =  mPrefs.getFloat(Constants.UserData.USER_HEIGHT, 0f);
		weight = mPrefs.getInt(Constants.UserData.USER_WEIGHT, 0);
		age = mPrefs.getInt(Constants.UserData.USER_AGE, 0);
		
		bmr = (float) ((10*weight) + (6.25*height) - (5*age) - 161);

		if(mCurWeek >0 && mCurWeek < 14) {
			perT = bmr * 1.6f + 85;
			float perC = perT * 0.55f;
			float perP = perT * 0.25f;
			float perF = perT * 0.20f;
			perCmin = (perC * 0.8f);
			perPmin = (perP * 0.8f);
			perFmin = (perF * 0.8f);
			perTmin = (perT * 0.95f);
		} 

		if(mCurWeek >13 && mCurWeek < 28) {
			perT = bmr * 1.6f + 285;
			float perC = perT * 0.55f;
			float perP = perT * 0.25f;
			float perF = perT * 0.20f;
			perCmin = (perC * 0.8f);
			perPmin = (perP * 0.8f);
			perFmin = (perF * 0.8f);
			perTmin = (perT * 0.95f);
		}

		if(mCurWeek >27) {
			perT = bmr * 1.6f + 475;
			float perC = perT * 0.55f;
			float perP = perT * 0.25f;
			float perF = perT * 0.20f;
			perCmin = (perC * 0.8f);
			perPmin = (perP * 0.8f);
			perFmin = (perF * 0.8f);
			perTmin = (perT * 0.95f);
		}
		
		mEditor.putFloat(Constants.UserData.MIN_REQ_CURB, perCmin);
		mEditor.putFloat(Constants.UserData.MIN_REQ_PROTIEN, perPmin);
		mEditor.putFloat(Constants.UserData.MIN_REQ_FAT, perFmin);
		mEditor.putFloat(Constants.UserData.MIN_REQ_TOTAL, perTmin);
		mEditor.putFloat(Constants.UserData.REQ_TOTAL, perT);
		mEditor.commit();
	}
}
