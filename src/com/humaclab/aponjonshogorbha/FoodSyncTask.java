package com.humaclab.aponjonshogorbha;

import java.io.UnsupportedEncodingException;
import java.util.List;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;

import com.humaclab.aponjon.Article;
import com.humaclab.aponjon.DaoMaster;
import com.humaclab.aponjon.DaoSession;
import com.humaclab.aponjon.DaoMaster.DevOpenHelper;
import com.humaclab.aponjon.ArticleDao;
import com.humaclab.aponjon.Food;
import com.humaclab.aponjon.FoodDao;
import com.humaclab.aponjon.Food_Category;
import com.humaclab.aponjon.Food_CategoryDao;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class FoodSyncTask {
	String mToken;
	Context mCtx;
	SessionManager mSessionMgr;
	DaoSession mDaoSession;


	public FoodSyncTask(Context ctx) {
		mCtx = ctx;
		mSessionMgr = new SessionManager(mCtx);
		SharedPreferences prefs = mSessionMgr.getPrefs();		
		mToken = prefs.getString(Constants.UserData.USER_TOKEN, null);

		DevOpenHelper helper = new DaoMaster.DevOpenHelper(mCtx, "aponjon-db", null);
		SQLiteDatabase db = helper.getWritableDatabase();
		DaoMaster daomaster = new DaoMaster(db);
		mDaoSession = daomaster.newSession();
	}
	public void syncFoodData() {
		final Food_CategoryDao categoryDao = mDaoSession.getFood_CategoryDao();
		List<Food_Category> categoryList = categoryDao.queryBuilder().list();
		if(categoryList.isEmpty()) {
			final FoodDao foodDao = mDaoSession.getFoodDao();

			RequestParams params = new RequestParams();
			JSONObject reqObj = new JSONObject();
			try {
				/**
				 * { "token":"4xAyjtRBPxLgrT1AD3WyXO2oPtz5C", "data": {
				 * "last_sync_time":"21-04-2015 12:13:33" } }
				 */
				reqObj.put("token", mToken);
				JSONObject reqObjTime = new JSONObject();
				reqObjTime.put("last_sync_time", "01-01-1970 00:00:00");
				reqObj.put("data", reqObjTime);

				params.put("data", reqObj.toString());
			} catch (JSONException e) {
				e.printStackTrace();
			}
			AponjonRestClient.post(AppConfig.URL_FETCH_CONTENT_FOOD, params, new AsyncHttpResponseHandler() {
				@Override
				public void onSuccess(int arg0, Header[] arg1, byte[] bytes) {
					try {
						String response = new String(bytes, "UTF-8");

						try {
							JSONObject resultObj = new JSONObject(response);

							int statusCode = resultObj.getInt("status_code");

							if(statusCode == 200) {

								//insert foodCategory Manualy
								Food_Category cat1 = new Food_Category(null, Long.parseLong("1"), "শর্করা জাতীয় খাবার", false);
								categoryDao.insert(cat1);
								Food_Category cat2 = new Food_Category(null, Long.parseLong("2"), "আমিষ জাতীয় খাবার", false);
								categoryDao.insert(cat2);
								Food_Category cat3 = new Food_Category(null, Long.parseLong("3"), "তেল ও চর্বি জাতীয় খাবার", false);
								categoryDao.insert(cat3);
								Food_Category cat4 = new Food_Category(null, Long.parseLong("4"), "শাক-সব্জি", false);
								categoryDao.insert(cat4);
								Food_Category cat5 = new Food_Category(null, Long.parseLong("5"), "ফল", false);
								categoryDao.insert(cat5);
								Food_Category cat6 = new Food_Category(null, Long.parseLong("6"), "পানি", false);
								categoryDao.insert(cat6);

								// insert
								JSONObject insertObj = resultObj.getJSONObject("insert");
								JSONArray insertFoodArray = insertObj.getJSONArray("Food"); 
								if (!(insertFoodArray.isNull(0))) {
									for (int i = 0; i < insertFoodArray.length(); i++) {

										JSONObject contentObj = insertFoodArray.getJSONObject(i);

										long id_server = Long.parseLong(contentObj.getString("id"));
										int cat_id_server = Integer.parseInt(contentObj.getString("food_category_id"));
										Food_Category food_cat = new Food_Category();
										switch (cat_id_server) {
										case 1:
											food_cat = cat1;
											break;
										case 2:
											food_cat = cat2;
											break;
										case 3:
											food_cat = cat3;
											break;
										case 4:
											food_cat = cat4; 
											break;
										case 5:
											food_cat = cat5;
											break;										
										case 6:
											food_cat = cat6;
											break;
										default:
											break;
										}
										long category_id = food_cat.getId();
										String desc = contentObj.getString("item_name");
										String absolute_name = contentObj.getString("name_only");
										String unit = contentObj.getString("unit");
										float increament_value = Long.parseLong(contentObj.getString("increamental_value"));
										float cal_per_unit_curb = Long.parseLong(contentObj.getString("per_unit_calorie"));
										float cal_per_unit_protien = Long.parseLong(contentObj.getString("carbohydrate"));
										float cal_per_unit_fat = Long.parseLong(contentObj.getString("protein"));
										float cal_per_unit_total = Long.parseLong(contentObj.getString("fat"));

										foodDao.insert(new Food(null, id_server, category_id, desc, absolute_name, unit, increament_value, cal_per_unit_curb, cal_per_unit_protien, cal_per_unit_fat, cal_per_unit_total));

									}

									// Update Food

									JSONObject updateObj = resultObj.getJSONObject("update");
									JSONArray updateFoodArray = updateObj.getJSONArray("Food"); 
									if (!(updateFoodArray.isNull(0))) {
										for (int i = 0; i < updateFoodArray.length(); i++) {

											JSONObject contentObj = updateFoodArray.getJSONObject(i);

											long id_server = Long.parseLong(contentObj.getString("id"));
											int cat_id_server = Integer.parseInt(contentObj.getString("food_category_id"));
											Food_Category food_cat = new Food_Category();
											switch (cat_id_server) {
											case 1:
												food_cat = cat1;
												break;
											case 2:
												food_cat = cat2;
												break;
											case 3:
												food_cat = cat3;
												break;
											case 4:
												food_cat = cat4; 
												break;
											case 5:
												food_cat = cat5;
												break;										
											case 6:
												food_cat = cat6;
												break;
											default:
												break;
											}
											long category_id = food_cat.getId();
											String desc = contentObj.getString("item_name");
											String absolute_name = contentObj.getString("name_only");
											String unit = contentObj.getString("unit");
											float increament_value = Long.parseLong(contentObj.getString("increamental_value"));
											float cal_per_unit_curb = Long.parseLong(contentObj.getString("per_unit_calorie"));
											float cal_per_unit_protien = Long.parseLong(contentObj.getString("carbohydrate"));
											float cal_per_unit_fat = Long.parseLong(contentObj.getString("protein"));
											float cal_per_unit_total = Long.parseLong(contentObj.getString("fat"));

											Food food = foodDao.queryBuilder().where(FoodDao.Properties.Id_server.eq(id_server)).unique();
											long key = food.getId();

											foodDao.update(new Food(key, id_server, category_id, desc, absolute_name, unit, increament_value, cal_per_unit_curb, cal_per_unit_protien, cal_per_unit_fat, cal_per_unit_total));

										}
									}

									// Delete Food

									JSONObject deleteObj = resultObj.getJSONObject("delete");
									JSONArray deleteFoodArray = deleteObj.getJSONArray("Food");
									if (!(deleteFoodArray.isNull(0))) {
										for (int i = 0; i < deleteFoodArray.length(); i++) {

											JSONObject contentObj = deleteFoodArray.getJSONObject(i);
											long id = Long.parseLong(contentObj.toString());
											Food food = foodDao.queryBuilder().where(ArticleDao.Properties.Ariticle_id.eq(id)).unique();
											foodDao.delete(food);
										}
									}
								}

							}
						} catch (JSONException e) {
							e.printStackTrace();
						}						

					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
					} catch (NullPointerException e) {
						e.printStackTrace();
					}
				}
			});

		}


	}
}
