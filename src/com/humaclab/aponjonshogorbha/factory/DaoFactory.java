package com.humaclab.aponjonshogorbha.factory;

import com.humaclab.aponjon.ArticleDao;
import com.humaclab.aponjon.TaskDao;
import com.humaclab.aponjonshogorbha.AponjonApplication;

public class DaoFactory {
	public static ArticleDao getArticleDao() {
		return AponjonApplication.getDaoSession().getArticleDao();
	}
	
	public static TaskDao getTaskDao() {
		return AponjonApplication.getDaoSession().getTaskDao();
	}
}
