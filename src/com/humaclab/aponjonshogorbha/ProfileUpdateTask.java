package com.humaclab.aponjonshogorbha;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.json.JSONException;
import org.json.JSONObject;


import com.humaclab.aponjon.DaoSession;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

public class ProfileUpdateTask {

	Context mContext;
	SharedPreferences mPreferences;
	SessionManager session;
	Editor editor;

	String mToken;
	String mEmail;
	String mPass;
	String mPhone;
	Date mMenstrulDate;
	float mHeight;
	int mWeight;
	int mAge;
	String mPhotoPath;

	public ProfileUpdateTask(Context ctx, String _email, String _pass1, String _phone, 
			Date _menstrual_date, float _height, int _weight, int _age, String cur_photo_path) {
		mContext = ctx;
		mEmail = _email;
		mPass = _pass1;
		mPhone = _phone;
		mMenstrulDate = _menstrual_date;
		mHeight = _height;
		mWeight = _weight;
		mAge = _age;
		mPhotoPath = cur_photo_path;

		session = new SessionManager(mContext);
		mPreferences = session.getPrefs();
		editor = session.getEditor();
		mToken = mPreferences.getString(Constants.UserData.USER_TOKEN, null);

		//		pd = new ProgressDialog(mContext);
	}

	public void startProfileUpdate() {
		fetchContent(AppConfig.URL_PROFILE_EDIT);
		//		ExecutorService executor = Executors.newFixedThreadPool(1);
		//		Runnable worker = new UpdateProfileTask(AppConfig.URL_PROFILE_EDIT);
		//		executor.execute(worker);
	}

	//	ProgressDialog pd;
	//	Handler handler = new Handler();
	//	public class UpdateProfileTask implements Runnable {
	//		String mUrlString;
	//
	//		public UpdateProfileTask(String url) {
	//			this.mUrlString = url;
	//		}
	//
	//		@Override
	//		public void run() {
	//			fetchContent(mUrlString);
	//		}
	//	}


	public void fetchContent(String url) {
		AsyncHttpClient client = new AsyncHttpClient();

		// Http Request Params Object
		RequestParams params = new RequestParams();


		//		// params
		JSONObject reqObj = new JSONObject();
		try {
			/**
			 *	{
			 *			"token":"4xAyjtRBPxLgrT1AD3WyXO2oPtz5C",
			 *			"data": { 
			 *   						"last_sync_time":"21-04-2015 12:13:33"
			 *   				}
			 *  }
			 *		
			 *
			 */

			reqObj.put("token", mToken);
			JSONObject reqObjTime = new JSONObject();
			reqObjTime.put("email", mEmail);
			reqObjTime.put("phone", mPhone);
			reqObjTime.put("password", mPass);
			reqObjTime.put("height_before_pregnancy", mHeight);
			reqObjTime.put("weight_before_pregnancy", mWeight);
			reqObjTime.put("age", mAge);
			reqObjTime.put("profile_photo_path", mPhotoPath);
			reqObj.put("data", reqObjTime);
			

			params.put("data" , reqObj.toString());
		} catch (JSONException e) {
			e.printStackTrace();
		}      


		// Make Http call
		client.post(url, params, new AsyncHttpResponseHandler() {
			@Override
			public void onSuccess(String response) {
				catchResponse(response);
			}

			// When error occured
			@Override
			public void onFailure(int statusCode, Throwable error, String content) {
				if (statusCode == 404) {
					//					handler.post(new Runnable() {
					//						@Override
					//						public void run() {
					//							Toast.makeText(mContext, "Requested resource not found", Toast.LENGTH_LONG).show();						
					//						}
					//					});
				} else if (statusCode == 500) {
					//					handler.post(new Runnable() {
					//						@Override
					//						public void run() {
					//							Toast.makeText(mContext, "Something went wrong at server end", Toast.LENGTH_LONG).show();						
					//						}
					//					});
				} else {
					//					handler.post(new Runnable() {
					//						@Override
					//						public void run() {
					//							Toast.makeText(mContext, "Unexpected Error! Device might not be connected to Internet]",
					//									Toast.LENGTH_LONG).show();						
					//						}
					//					});
				}
			}
		});
	}

	private void catchResponse(String response) {
		Log.d("ResponseProfile", "profileUpdate : " + response);
		//		
		//		"name":"salma kona",
		//        "email":"kona@gmail.com",
		//        "phone":"01716930306",
		//        "password":"afaa94a8b516c3fe6287e2236f57d1ebb941d58f",
		//        "lmp":"2015-04-13",
		//        "profile_photo":"",
		//        "weight_before_pregnancy":"46.00",
		//        "height_before_pregnancy":"154.94",
		//        "age":"27",


		try {
			JSONObject responseObj = new JSONObject(response.toString());
			JSONObject resultObj = responseObj.getJSONObject("result");
			JSONObject userObj = resultObj.getJSONObject("User");

			String id = userObj.getString("id");
			String name = userObj.getString("name");
			String email = userObj.getString("email");
			String phone = userObj.getString("phone");
			String token = userObj.getString("token");
			String strlmp = userObj.getString("lmp");
			String weight = userObj.getString("weight_before_pregnancy");
			String height = userObj.getString("height_before_pregnancy");
			String age = userObj.getString("age");
			String photoPath = userObj.getString("profile_photo_path");


			SimpleDateFormat format = new SimpleDateFormat();
			format.applyPattern("yyyy-MM-dd");

			Date lmp = null;
			Date curDate = null;
			int dayPassed = 0;
			int currWeek = 0;
			try {
				lmp = format.parse(strlmp);
				curDate = new Date(System.currentTimeMillis());

				dayPassed = (int)((curDate.getTime() - lmp.getTime()) / (1000*60*60*24l));
				if(dayPassed%7 == 0) {
					currWeek = dayPassed/7;
				} else currWeek = (dayPassed/7) + 1;

			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			session.createLoginSession(id, name, email, phone, strlmp, currWeek, token, height, weight, age, photoPath, null);


		} catch (JSONException e) {
			e.printStackTrace();
		}

	}
}
