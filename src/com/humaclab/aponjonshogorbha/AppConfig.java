package com.humaclab.aponjonshogorbha;

public class AppConfig {



	// Server base url
	//	public static String URL_BASE = "http://api.aponjon.com.bd/apps/aponjon_shogorva";

//	// Server base url
//	public static String URL_BASE = "http://www.humaclab.com/development/aponjonshogorva";


//	// Server base url
//	public static String URL_BASE = "http://104.155.218.210/aponjonshogorva";
	
	// Server base url
	public static String URL_BASE = "http://www.shogorbha.aponjon.com.bd";

	// Server user login url
	public static String URL_LOGIN = URL_BASE + "/api/sign_in";
	// Server user register url
	public static String URL_REGISTER = URL_BASE + "/api/sign_up";
	// // Server user register url
	//  public static String URL_FETCH_ARTICLE = URL_BASE + "/api/article";
	// Server aponjon content fetch Url
	public static String URL_FETCH_CONTENT = URL_BASE + "/api/content_sync";

	// Server user content fetch Url
	public static String URL_FETCH_USER_CONTENT = URL_BASE + "/api/user_content_sync";

	// Server aponjon content fetch Url
	public static String URL_FETCH_CONTENT_FOOD = URL_BASE + "/api/food_sync";

	//Aponjon System_Task_Content Url
	public static String URL_SYSTEM_TASK_CONTENT = URL_BASE + "/api/system_task_sync";

	//Aponjon User_System_Task_Flag Url
	public static String URL_CUSTOM_TASK_CONTENT = URL_BASE + "/api/custom_task_sync";

	//Aponjon Task_Category Url
	public static String URL_TASK_CATEGORY_CONTENT = URL_BASE + "/api/task_category_sync";

	/**
	 * Upload_User_Data
	 */

	//Upload Custom_Task
	public static String URL_UPLOAD_CUSTOM_TASK = URL_BASE + "/api/add_custom_task";
	//Upload Custom_Task Status
	public static String URL_UPLOAD_CUSTOM_TASK_FLAG = URL_BASE + "/api/custom_task_done";
	//Upload Custom_Task Status
	public static String URL_UPLOAD_SYSTEM_TASK_FLAG = URL_BASE + "/api/system_task_done";

	//Upload Meal and Meal_Food
	public static String URL_UPLOAD_FOOD = URL_BASE + "/api/add_meal_plan";

	//Upload Note
	public static String URL_UPLOAD_NOTE = URL_BASE + "/api/add_note";

	//Upload Weight
	public static String URL_UPLOAD_WEIGHT = URL_BASE + "/api/add_weight";

	//Upload Diary
	public static String URL_UPLOAD_DIARY = URL_BASE + "/api/add_note";

	//Upload Move
	public static String URL_UPLOAD_MOVE = URL_BASE + "/api/add_kickcount";

	//Aponjon Nibondhon Url    
	public static String URL_APONJON_NIBONDHON = "http://www.aponjon.com.bd/get_reg";

	//Aponjon Picture Upload Url
	public static String URL_PIC_UPLOAD = URL_BASE + "/api/upload_photo";

	//Aponjon Edit Profile Url
	public static String URL_PROFILE_EDIT = URL_BASE + "/api/profile";

	//Aponjon Profile Pic Upload
	public static String URL_PROFILE_PHOTO_UPLOAD = URL_BASE + "/api/upload_photo";

	//Aponjon Profile Pic Upload
	public static String URL_PROFILE_NOTE_PHOTO_UPLOAD = URL_BASE + "/api/upload_notes_photo";


	/**
	 * Download_User_Data
	 */

	//Download Meal
	public static String URL_DOWNLOAD_MEAL = URL_BASE + "/api/meal_plan_sync";

	//Download Meal_Food
	public static String URL_DOWNLOAD_MEAL_FOOD = URL_BASE + "/api/meal_item_sync";

	//Download Meal_Food
	public static String URL_DOWNLOAD_WEIGHT = URL_BASE + "/api/weight_sync";

	//Download Meal_Food
	public static String URL_DOWNLOAD_DIARY = URL_BASE + "/api/note_sync";

	//Download Meal_Food
	public static String URL_DOWNLOAD_MOVE = URL_BASE + "/api/kickcount_sync";







	//	// Server base url
	//	public static String URL_BASE = "http://www.humaclab.com/development/aponjonshogorva";
	//	// Server user login url
	//	public static String URL_LOGIN = URL_BASE + "/api/sign_in";
	//	// Server user register url
	//	public static String URL_REGISTER = URL_BASE + "/api/sign_up";
	//	// // Server user register url
	//	//  public static String URL_FETCH_ARTICLE = URL_BASE + "/development/aponjonshogorva/api/article";
	//	// Server aponjon content fetch Url
	//	public static String URL_FETCH_CONTENT = URL_BASE + "/api/content_sync";
	//
	//	// Server user content fetch Url
	//	public static String URL_FETCH_USER_CONTENT = URL_BASE + "/api/user_content_sync";
	//
	//	// Server aponjon content fetch Url
	//	public static String URL_FETCH_CONTENT_FOOD = URL_BASE + "/api/food_sync";
	//
	//	//Aponjon System_Task_Content Url
	//	public static String URL_SYSTEM_TASK_CONTENT = URL_BASE + "/api/system_task_sync";
	//
	//	//Aponjon User_System_Task_Flag Url
	//	public static String URL_CUSTOM_TASK_CONTENT = URL_BASE + "/api/custom_task_sync";
	//
	//	//Aponjon Task_Category Url
	//	public static String URL_TASK_CATEGORY_CONTENT = URL_BASE + "/api/task_category_sync";
	//
	//	/**
	//	 * Upload_User_Data
	//	 */
	//	//Upload Custom_Task
	//	public static String URL_UPLOAD_CUSTOM_TASK = URL_BASE + "/api/add_custom_task";
	//	//Upload Custom_Task Status
	//	public static String URL_UPLOAD_CUSTOM_TASK_FLAG = URL_BASE + "/api/custom_task_done";
	//	//Upload Custom_Task Status
	//	public static String URL_UPLOAD_SYSTEM_TASK_FLAG = URL_BASE + "/api/system_task_done";
	//
	//	//Upload Meal and Meal_Food
	//	public static String URL_UPLOAD_FOOD = URL_BASE + "/api/add_meal_plan";
	//
	//	//Upload Note
	//	public static String URL_UPLOAD_NOTE = URL_BASE +"/api/add_note";
	//
	//	//Upload Weight
	//	public static String URL_UPLOAD_WEIGHT = URL_BASE +"/api/add_weight";
	//
	//	//Upload Diary
	//	public static String URL_UPLOAD_DIARY = URL_BASE +"/api/add_note";
	//
	//	//Aponjon Nibondhon Url    
	//	public static String URL_APONJON_NIBONDHON = "http://www.aponjon.com.bd/get_reg";
	//
	//	//Aponjon Picture Upload Url
	//	public static String URL_PIC_UPLOAD = URL_BASE + "/api/upload_photo";
	//
	//	//Aponjon Edit Profile Url
	//	public static String URL_PROFILE_EDIT = URL_BASE + "/api/profile";

}
