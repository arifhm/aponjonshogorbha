package com.humaclab.aponjonshogorbha;

import android.app.Application;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.preference.PreferenceManager;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.dnet.mama.diary.CF;
import com.humaclab.aponjon.DaoMaster;
import com.humaclab.aponjon.DaoMaster.DevOpenHelper;
import com.humaclab.aponjon.services.BackgroundService;
import com.humaclab.aponjonshogorbha.util.DatabaseQueue;
import com.humaclab.aponjon.DaoSession;

import de.greenrobot.dao.identityscope.IdentityScopeType;

public class AponjonApplication extends Application {
	public static final String TAG = AponjonApplication.class.getSimpleName();
	private SharedPreferences mPrefferences;

	private RequestQueue mRequestQueue;

	private static AponjonApplication sInstance;
	public static DaoSession mDaoSession;
	public static String mDatabaseName;
	
	public AponjonApplication() {
		sInstance = this;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		
		DatabaseQueue.initialize();
		
		mPrefferences = PreferenceManager.getDefaultSharedPreferences(this);
		CF cf = new CF(getApplicationContext());
		
		Intent startServiceIntent = new Intent(sInstance, BackgroundService.class);
        startService(startServiceIntent);

	}

	public static synchronized AponjonApplication getInstance() {
		return sInstance;
	}
	
	
    public static DaoSession getDaoSession() {
        if(mDaoSession == null) {
            DaoMaster.DevOpenHelper devOpenHelper = new DaoMaster.DevOpenHelper(
                    getInstance().getApplicationContext(), "aponjon-db", null);        
            SQLiteDatabase db = devOpenHelper.getWritableDatabase();
            DaoMaster daomaster = new DaoMaster(db);
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            	mDatabaseName = devOpenHelper.getDatabaseName();
            } else {
            	mDatabaseName = "aponjon-db";
            }
           
            mDaoSession = daomaster.newSession(IdentityScopeType.None);
        }
        return mDaoSession;
    }
    
    public static String getDatabaseName() {
    	return mDatabaseName;
    }

	public RequestQueue getRequestQueue() {
		if (mRequestQueue == null) {
			mRequestQueue = Volley.newRequestQueue(getApplicationContext());
		}

		return mRequestQueue;
	}

	public <T> void addToRequestQueue(Request<T> req, String tag) {
		req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
		getRequestQueue().add(req);
	}

	public <T> void addToRequestQueue(Request<T> req) {
		req.setTag(TAG);
		getRequestQueue().add(req);
	}

	public void cancelPendingRequests(Object tag) {
		if (mRequestQueue != null) {
			mRequestQueue.cancelAll(tag);
		}
	}

	public SharedPreferences getSharedPreferences() {
		return mPrefferences;
	}

//	@Override
//    public String toString() {	
//    	return CF.dateToBanglaTimeDayYear(getDate());
//    }
//	
//	@Override
//    public String toString() {	
//    	return CF.dateToBanglaTimeDayYear(getDate());
//    }
}
