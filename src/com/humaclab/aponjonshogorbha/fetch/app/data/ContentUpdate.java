package com.humaclab.aponjonshogorbha.fetch.app.data;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import junit.framework.Assert;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import android.widget.Toast;

import com.dnet.mama.diary.CF;
import com.humaclab.aponjon.Article;
import com.humaclab.aponjon.ArticleDao;
import com.humaclab.aponjon.DaoMaster;
import com.humaclab.aponjon.DaoMaster.DevOpenHelper;
import com.humaclab.aponjonshogorbha.AppConfig;
import com.humaclab.aponjonshogorbha.AponjonApplication;
import com.humaclab.aponjonshogorbha.Constants;
import com.humaclab.aponjonshogorbha.SessionManager;
import com.humaclab.aponjonshogorbha.Constants.AppData;
import com.humaclab.aponjonshogorbha.Constants.UserData;
import com.humaclab.aponjonshogorbha.factory.DaoFactory;
import com.humaclab.aponjon.DaoSession;
import com.humaclab.aponjon.Diary;
import com.humaclab.aponjon.DiaryDao;
import com.humaclab.aponjon.Move;
import com.humaclab.aponjon.MoveDao;
import com.humaclab.aponjon.SyncLog;
import com.humaclab.aponjon.SyncLogDao;
import com.humaclab.aponjon.Task;
import com.humaclab.aponjon.TaskDao;
import com.humaclab.aponjon.TaskType;
import com.humaclab.aponjon.TaskTypeDao;
import com.humaclab.aponjon.Weight;
import com.humaclab.aponjon.WeightDao;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;


public class ContentUpdate {
	Context mContext;
	String mToken;
	String mImgBaseLink;
	Date mLastSyncTime;
	SharedPreferences mPreferences;
	AponjonApplication mApp;
	SessionManager session;
	DaoSession mDaoSession;
	boolean firstTime = false;

	public ContentUpdate(Context ctx) {
		this.mContext = ctx;
		mApp = (AponjonApplication) ctx.getApplicationContext();
		session = new SessionManager(mContext);
		mPreferences = session.getPrefs();

		mToken = mPreferences.getString(Constants.UserData.USER_TOKEN, null);
		mImgBaseLink = mPreferences.getString(
				Constants.AppData.KEY_BASE_IMG_LINK, null);

		DevOpenHelper helper = new DaoMaster.DevOpenHelper(ctx, "aponjon-db",
				null);
		SQLiteDatabase db = helper.getWritableDatabase();
		DaoMaster daomaster = new DaoMaster(db);
		mDaoSession = daomaster.newSession();
		// mDaoSession = mApp.getDaoSession();
	}

	public void startContentUpdate() {
		SyncLogDao syncLogDao = mDaoSession.getSyncLogDao();
		SyncLog synclog = syncLogDao.queryBuilder().where(SyncLogDao.Properties.Sync_name.eq("ArticleDownload")).unique();

		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		if(synclog != null) {
			mLastSyncTime = synclog.getLast_sync_server();

		} else {
			Date defaultDate=null;
			try {
				defaultDate = format.parse("1970-01-01 00:00:00");
			} catch (ParseException e) {
				e.printStackTrace();
			}
			mLastSyncTime = defaultDate;

			firstTime = true;
		}



		ExecutorService executor = Executors.newFixedThreadPool(1);
		Runnable worker = new FetchContentTask(AppConfig.URL_FETCH_CONTENT);
		executor.execute(worker);

	}

	public class FetchContentTask implements Runnable {
		String mUrlString;

		public FetchContentTask(String url) {
			this.mUrlString = url;
		}

		@Override
		public void run() {
			fetchContent(mUrlString);
		}
	}

	public void fetchContent(String url) {
		AsyncHttpClient client = new AsyncHttpClient();
		// Http Request Params Object
		RequestParams params = new RequestParams();
		// params
		JSONObject reqObj = new JSONObject();
		try {
			/**
			 * { 
			 * 		"token":"4xAyjtRBPxLgrT1AD3WyXO2oPtz5C", 
			 * 		"data": {
			 * 					"last_sync_time":"21-04-2015 12:13:33" 
			 * 				} 
			 * }
			 * 
			 * 
			 */

			reqObj.put("token", mToken);
			JSONObject reqObjTime = new JSONObject();
			reqObjTime.put("last_sync_time", mLastSyncTime);
			reqObj.put("data", reqObjTime);

			params.put("data", reqObj.toString());
		} catch (JSONException e) {
			e.printStackTrace();
		}

		// Make Http call
		client.post(url, params, new AsyncHttpResponseHandler() {
			@Override
			public void onSuccess(String response) {
				processContent(response);
			}

			// When error occured
			@Override
			public void onFailure(int statusCode, Throwable error,
					String content) {
				if (statusCode == 404) {
					new Runnable() {
						@Override
						public void run() {
							Toast.makeText(mContext,
									"Requested resource not found",
									Toast.LENGTH_LONG).show();
						}
					};
				} else if (statusCode == 500) {
					new Runnable() {
						@Override
						public void run() {
							Toast.makeText(mContext,
									"Something went wrong at server end",
									Toast.LENGTH_LONG).show();
						}
					};

				} else {
					new Runnable() {
						@Override
						public void run() {
							Toast.makeText(
									mContext,
									"Unexpected Error occcured! [Most common Error: Device might not be connected to Internet]",
									Toast.LENGTH_LONG).show();
						}
					};
				}
			}
		});
	}

	private void processContent(String response) {

		try {
			ArticleDao articleDao = mDaoSession.getArticleDao();

			JSONObject responseObj = new JSONObject(response);

			//			StringBuffer msb1 = new StringBuffer();
			//			msb1.append(responseObj.toString());
			//			try {
			//				File f = new File(Environment.getExternalStorageDirectory().getPath(), "responseArticle.txt");
			//				writeToFile(f, msb1);
			//			} catch (IOException e) {
			//				e.printStackTrace();
			//			}

			JSONObject resultObj = responseObj.getJSONObject("result");

			String new_sync_time = resultObj.getString("last_sync_time");
			String base_image_url = resultObj.getString("image_url");


			JSONObject insertObj = resultObj.getJSONObject("insert");
			JSONArray insertContentArray = insertObj.getJSONArray("Content"); 
			if (!(insertContentArray.isNull(0))) {
				for (int i = 0; i < insertContentArray.length(); i++) {

					JSONObject contentObj = insertContentArray.getJSONObject(i);


					long id = Long.parseLong(contentObj.getString("id"));
					int week_id = Integer
							.parseInt(contentObj.getString("week"));
					String category = contentObj.getString("type");
					String img_link_server = contentObj.getString("image");
					String text_comment = contentObj.getString("content");

					articleDao.insert(new Article(null, id, category,
							text_comment, base_image_url + img_link_server,
							null, week_id, null, false));

				}
			}

			JSONObject updateObj = resultObj.getJSONObject("update");
			JSONArray updateContentArray = updateObj.getJSONArray("Content"); 
			if (!(updateContentArray.isNull(0))) {
				for (int i = 0; i < updateContentArray.length(); i++) {

					JSONObject contentObj = updateContentArray.getJSONObject(i);

					long id = Long.parseLong(contentObj.getString("id"));
					int week_id = Integer
							.parseInt(contentObj.getString("week"));
					String category = contentObj.getString("type");
					String img_link_server = contentObj.getString("image");
					String text_comment = contentObj.getString("content");

					Article article = articleDao.queryBuilder().where(ArticleDao.Properties.Ariticle_id.eq(id)).unique();
					long key = article.getId();

					articleDao.update(new Article(key, id, category,
							text_comment, base_image_url + img_link_server,
							null, week_id, null, false));

				}
			}

			JSONObject deleteObj = resultObj.getJSONObject("delete");
			JSONArray deleteContentArray = deleteObj.getJSONArray("Content");
			if (!(deleteContentArray.isNull(0))) {
				for (int i = 0; i < deleteContentArray.length(); i++) {

					JSONObject contentObj = deleteContentArray.getJSONObject(i);
					long id = Long.parseLong(contentObj.toString());
					// JSONObject deleteObjs = deleteArray.getJSONObject(i);
					// delete process should be added
					articleDao.queryBuilder().where(ArticleDao.Properties.Ariticle_id.eq(id)).build();
					//					articleDao
					//							.queryBuilder()
					//							.where(com.humaclab.aponjon.ArticleDao.Properties.Ariticle_id
					//									.eq(id).buildDelete()
					//							.executeDeleteWithoutDetachingEntities();

				}
			}

			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date lastSyncTimeLocal = new Date(System.currentTimeMillis());
			Date latestServerDate = null;

			try {
				latestServerDate = format.parse(new_sync_time);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			SyncLogDao syncLogDao = mDaoSession.getSyncLogDao();
			if(firstTime) {
				syncLogDao.insert(new SyncLog(null, "ArticleDownload", latestServerDate, lastSyncTimeLocal));
			} else {
				SyncLog synclog = syncLogDao.queryBuilder().where(SyncLogDao.Properties.Sync_name.eq("ArticleDownload")).unique();
				long key = synclog.getId();
				syncLogDao.update(new SyncLog(key, "ArticleDownload", latestServerDate, lastSyncTimeLocal));
			}



		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void writeToFile(File pFile, StringBuffer pData)
			throws IOException {
		BufferedWriter out = new BufferedWriter(new FileWriter(pFile));
		out.write(pData.toString());
		out.flush();
		out.close();
	}

	public void saveMove(int moveCount, java.util.Date date) {
		MoveDao moveDao = mDaoSession.getMoveDao();
		moveDao.insert(new Move(null, null, moveCount, date, false));
	}

	public List<Move> getMoveCounterList() {
		MoveDao moveDao = mDaoSession.getMoveDao();
		return moveDao.queryBuilder().orderDesc(MoveDao.Properties.Date).list();

	}

	public long saveDiary(String note, Boolean rg0, Boolean rg1, Boolean rg2,
			Boolean rg3, Boolean rg4, Boolean rg5, Boolean rg6, Boolean rg7,
			Boolean rg8, Boolean rg9, String photoPath, String photoLink, java.util.Date date, boolean isSync ) {
		DiaryDao diaryDao = mDaoSession.getDiaryDao();
		long id = diaryDao.insert(new Diary(null, null, null, note, rg0, rg1, rg2, rg3, rg4,
				rg5, rg6, rg7, rg8, rg9, photoPath, photoLink, date, isSync));
		return id;
	}

	public List<Diary> getDiaryList() {
		DiaryDao diaryDao = mDaoSession.getDiaryDao();
		return diaryDao.queryBuilder().orderDesc(DiaryDao.Properties.Date)
				.list();
	}

	public void saveWeight(Integer weight, double weightGain, java.util.Date date) {
		WeightDao weightDao = mDaoSession.getWeightDao();
		weightDao.insert(new Weight(null, null, null, weight, weightGain, date, false));
	}

	public List<Weight> getWeightList() {
		WeightDao weightDao = mDaoSession.getWeightDao();
		return weightDao.queryBuilder().list();
	}

	public void saveTask(String id, String title, String description,
			long typeId, java.util.Date task_date) {

		boolean isSync=false;
		boolean isCompleted=false;
		String apiID="";
		int weekNo=CF.getCurrentWeek(task_date);
		int dayNo=CF.getCurrentDay(task_date);

		TaskDao taskDao = mDaoSession.getTaskDao();
		taskDao.insert(new Task(null, null, 1, title, description, task_date, weekNo, dayNo, isSync, isCompleted, typeId));
	}

	public void updateTask(Task task ) {

		TaskDao taskDao = mDaoSession.getTaskDao();
		taskDao.insertOrReplace(task);
	}

	public List<Task> getTaskList() {
		TaskDao taskDao = mDaoSession.getTaskDao();
		return taskDao.queryBuilder().list();
	}

	public List<Task> getTaskList(long taskTypeId) {

		TaskDao taskDao = mDaoSession.getTaskDao();
		return taskDao.queryBuilder().where(TaskDao.Properties.TypeId.eq(taskTypeId)).list();
	}

	public List<Task> getTaskList(Date date) {
		TaskDao taskDao = mDaoSession.getTaskDao();
		return taskDao.queryBuilder()
				.where(TaskDao.Properties.Task_date.eq(date)).list();
	}



	public List<Task> getThisMonthTaskList(long taskTypeId) {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MONTH, -1);
		cal.set(Calendar.DATE, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
		Date firstDate = cal.getTime();

		Calendar cal2 = Calendar.getInstance();
		cal2.add(Calendar.MONTH, 1);
		cal2.set(Calendar.DATE, 1);
		Date lastDate = cal2.getTime();

		String lmp = mPreferences.getString(Constants.UserData.USER_LMP, null);
		SimpleDateFormat format = (SimpleDateFormat) SimpleDateFormat.getInstance();
		format.applyPattern("yyyy-MM-dd");

		Date lmpDate = null;
		try {
			lmpDate =  format.parse(lmp);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		List<Integer> listOfWeek = new ArrayList<Integer>(); 

		for(int week = 0; week<42; week++)  {
			boolean validWeek = false;
			for(int i=0; i<7; i++) {
				if(lmpDate.after(firstDate) && lmpDate.before(lastDate)) {
					validWeek = true;

					Calendar cal3 = Calendar.getInstance();
					cal3.setTime(lmpDate);
					cal3.add(Calendar.DATE, 7-i);
					lmpDate= cal3.getTime();
					break;
				} else {
					Calendar cal3 = Calendar.getInstance();
					cal3.setTime(lmpDate);
					cal3.add(Calendar.DATE, 1);
					lmpDate= cal3.getTime();
				}
			}
			if(validWeek) listOfWeek.add(week);
		}

		TaskDao taskDao = mDaoSession.getTaskDao();
		return taskDao.queryBuilder().where(TaskDao.Properties.WeekNo.in(listOfWeek), TaskDao.Properties.TypeId.eq(taskTypeId)).list();		

	}

	//get task date for calendar visualize
	public List<Task> getAllTaskDateList() {
		String lmp = null;
		if(Constants.is_demo) {
			lmp = "1970-01-01";
		} else lmp = mPreferences.getString(Constants.UserData.USER_LMP, "1970-01-01");
		SimpleDateFormat format = (SimpleDateFormat) SimpleDateFormat.getInstance();
		format.applyPattern("yyyy-MM-dd");

		Calendar lmpCal = Calendar.getInstance();
		Date lmpDate = null;
		try {
			lmpDate = format.parse(lmp);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		TaskDao taskDao = mDaoSession.getTaskDao();
		List<Task> tasks = taskDao.queryBuilder().list();


		for(Task task : tasks) {
			if(task.getTask_date() == null){
				int day = task.getDayNo();
				Date taskDate = null;
				lmpCal.setTime(lmpDate);
				lmpCal.add(Calendar.DATE, day-1);
				taskDate = lmpCal.getTime();

				task.setTask_date(taskDate);

			}

		}

		return tasks;	
	}


	public List<Task> getThisMonthTaskList(long taskTypeId, Date date) {

		//start date end date calculation
		Calendar startDate = Calendar.getInstance();
		Calendar endDate = Calendar.getInstance();

		// Set time
		startDate.setTime(date);
		startDate.add(Calendar.MONTH, -1);
		endDate.setTime(date);
		endDate.add(Calendar.MONTH, 1);

		startDate.set(Calendar.DAY_OF_MONTH, endDate.getActualMaximum(Calendar.DAY_OF_MONTH));
		startDate.set(Calendar.HOUR_OF_DAY, 23);
		startDate.set(Calendar.MINUTE, 59);
		startDate.set(Calendar.SECOND, 59);
		startDate.set(Calendar.MILLISECOND, 999);

		endDate.set(Calendar.DAY_OF_MONTH, 1);
		endDate.set(Calendar.HOUR_OF_DAY, 0);
		endDate.set(Calendar.MINUTE, 0);
		endDate.set(Calendar.SECOND, 0);
		endDate.set(Calendar.MILLISECOND, 0);

		Date stDate= startDate.getTime();
		Date enDate= endDate.getTime();

		String lmp = mPreferences.getString(Constants.UserData.USER_LMP, null);
		SimpleDateFormat format = (SimpleDateFormat) SimpleDateFormat.getInstance();
		format.applyPattern("yyyy-MM-dd");

		Calendar lmpCal = Calendar.getInstance();
		Date lmpDate = null;
		try {
			lmpDate = format.parse(lmp);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		TaskDao taskDao = mDaoSession.getTaskDao();
		List<Task> tasks = taskDao.queryBuilder().where(TaskDao.Properties.TypeId.eq(taskTypeId)).list();
		List<Task> monthTasks = new ArrayList<>();

		for(Task task : tasks) {
			int day = task.getDayNo();
			Date taskDate = null;
			lmpCal.setTime(lmpDate);
			lmpCal.add(Calendar.DATE, day-1);
			taskDate = lmpCal.getTime();

			if(taskDate.after(stDate) && taskDate.before(enDate)) {
				monthTasks.add(task);
			}
		}

		return monthTasks;	
	}

	public List<Task> getThisDayTaskList(Date date ) {

		String lmp = mPreferences.getString(Constants.UserData.USER_LMP, null);
		SimpleDateFormat format = (SimpleDateFormat) SimpleDateFormat.getInstance();
		format.applyPattern("yyyy-MM-dd");

		Date lmpDate = null;
		try {
			lmpDate =  format.parse(lmp);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		int dayNo = 0;
		if(date.after(lmpDate)) {
			dayNo = (int)((date.getTime() - (lmpDate.getTime()-1000*60*60*24l)) / (1000*60*60*24l));
		}
		

//		Calendar cal = Calendar.getInstance();
//		cal.setTime(lmpDate);
//		cal.add(Calendar.DATE, -1);
//		Date lastDatePrevWeek = cal.getTime();
//		cal.add(Calendar.DATE, 8);
//		Date firstDateNextWeek = cal.getTime();
//
//		int theWeek = 0;
//		for(int week = 1; week<43; week++) {
//			if(date.after(lastDatePrevWeek) && date.before(firstDateNextWeek)) {
//				theWeek = week;
//				break;
//			} else {
//				cal.setTime(lastDatePrevWeek);
//				cal.add(Calendar.DATE, 7);
//				lastDatePrevWeek = cal.getTime();
//
//				cal.add(Calendar.DATE, 8);
//				firstDateNextWeek = cal.getTime();
//			}
//		}

	
		//
		//start date end date calculation
		Calendar startDate = Calendar.getInstance();
		Calendar endDate = Calendar.getInstance();

		// Set time
		startDate.setTime(date);
		endDate.setTime(date);


		startDate.set(Calendar.HOUR_OF_DAY, 0);
		startDate.set(Calendar.MINUTE, 0);
		startDate.set(Calendar.SECOND, 0);
		startDate.set(Calendar.MILLISECOND, 0);

		endDate.set(Calendar.HOUR_OF_DAY, 23);
		endDate.set(Calendar.MINUTE, 59);
		endDate.set(Calendar.SECOND, 59);
		endDate.set(Calendar.MILLISECOND, 999);




		Date stDate= startDate.getTime();
		Date enDate= endDate.getTime();

		// Get customTasks of particular date
		TaskDao taskDao = DaoFactory.getTaskDao();
		List<Task> customTasks = taskDao.queryBuilder().where(TaskDao.Properties.CatId.eq(1), TaskDao.Properties.Task_date.between(stDate, enDate)).list();
		
		List<Task> sytemTasks = taskDao.queryBuilder().where(TaskDao.Properties.CatId.eq(2), TaskDao.Properties.DayNo.eq(dayNo)).list();
		

		List<Task> allTasks = new ArrayList<>();
		allTasks.addAll(sytemTasks);
		allTasks.addAll(customTasks);

		return allTasks; 

	}

	public TaskType getTaskType(String taskTypeid){

		TaskTypeDao taskTypeDao = mDaoSession.getTaskTypeDao();

		return taskTypeDao.queryBuilder().where(TaskTypeDao.Properties.Id.eq(taskTypeid)).unique();
	}

}
