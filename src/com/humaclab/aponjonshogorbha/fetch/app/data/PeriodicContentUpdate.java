package com.humaclab.aponjonshogorbha.fetch.app.data;


import com.humaclab.aponjonshogorbha.util.DatabaseQueue;


import android.content.Context;

public class PeriodicContentUpdate {
	Context mCtx;
	
	public PeriodicContentUpdate(Context ctx) {
		this.mCtx = ctx;
	}
	
	public void updateContent() {		
		DatabaseQueue.execute(new Runnable() {			
			@Override
			public void run() {
				ContentUpdate updateContent = new ContentUpdate(mCtx);
				updateContent.startContentUpdate();
			}
		});
		
		DatabaseQueue.execute(new Runnable() {			
			@Override
			public void run() {
				ContentFoodUpdate updateFoodContent = new ContentFoodUpdate(mCtx);
				updateFoodContent.startContentUpdate();
			}
		});
		
		DatabaseQueue.execute(new Runnable() {			
			@Override
			public void run() {
				WeekSync weekSynk = new WeekSync(mCtx);
				weekSynk.startWeekSynk();
			}
		});
		
//		DatabaseQueue.execute(new Runnable() {			
//			@Override
//			public void run() {
//				SystemTaskSync syncTasks  = new SystemTaskSync(mCtx);
//				syncTasks.getTasks();
//			}
//		});
						
	}
		
}
