package com.humaclab.aponjonshogorbha.fetch.app.data;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

import com.humaclab.aponjon.Article;
import com.humaclab.aponjon.ArticleDao;
import com.humaclab.aponjon.DaoMaster;
import com.humaclab.aponjon.DaoSession;
import com.humaclab.aponjon.DaoMaster.DevOpenHelper;
import com.humaclab.aponjonshogorbha.AppConfig;
import com.humaclab.aponjonshogorbha.AponjonApplication;
import com.humaclab.aponjonshogorbha.Constants;
import com.humaclab.aponjonshogorbha.SessionManager;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;


public class WeekSync {
	Context mContext;
	String mToken;
	String mLmp;
	String mLastSyncTime;
	SharedPreferences mPreferences;
	AponjonApplication mApp;
	SessionManager session;
	Editor mEditor;
	DaoSession mDaoSession;
	
	public WeekSync(Context ctx) {
		this.mContext = ctx;
		mApp = (AponjonApplication) ctx.getApplicationContext();
		session = new SessionManager(mContext);
		mPreferences = session.pref;
		mEditor = mPreferences.edit();

		mToken = mPreferences.getString(Constants.UserData.USER_TOKEN, null);
		mLmp = mPreferences.getString(Constants.UserData.USER_LMP, null);
		
		DevOpenHelper helper = new DaoMaster.DevOpenHelper(ctx, "aponjon-db", null);
		SQLiteDatabase db = helper.getWritableDatabase();
		DaoMaster daomaster = new DaoMaster(db);
		mDaoSession = daomaster.newSession();
		
//		mDaoSession = mApp.getDaoSession();
	}
	
	public void startWeekSynk() {
		ExecutorService executor = Executors.newFixedThreadPool(2);
		Runnable worker = new FetchUserContentTask(AppConfig.URL_FETCH_USER_CONTENT);
		executor.execute(worker);
	}
	
	public class FetchUserContentTask implements Runnable {
		String mUrlString;

		public FetchUserContentTask(String url) {
			this.mUrlString = url;
		}

		@Override
		public void run() {
			fetchUserContent(mUrlString);
		}
	}
	
	public void fetchUserContent(String url) {
		AsyncHttpClient client = new AsyncHttpClient();
		// Http Request Params Object
		RequestParams params = new RequestParams();
		// params
		JSONObject reqObj = new JSONObject();
		try {
			/**
			 *	{
			 *			"token":"4xAyjtRBPxLgrT1AD3WyXO2oPtz5C",
			 *			"data": { 
			 *   						"last_sync_time":"21-04-2015 12:13:33"
			 *   				}
			 *  }
			 *		
			 *
			 */

						
			reqObj.put("token", mToken);
			JSONObject reqObjTime = new JSONObject();
			reqObjTime.put("lmp", mLmp);
			reqObj.put("data", reqObjTime);

			params.put("data" , reqObj.toString());
		} catch (JSONException e) {
			e.printStackTrace();
		}      


		// Make Http call
		client.post(url, params, new AsyncHttpResponseHandler() {
			@Override
			public void onSuccess(String response) {
				processUserContent(response);
			}
			// When error occured
			@Override
			public void onFailure(int statusCode, Throwable error, String content) {
				if (statusCode == 404) {
					new Runnable() {
						@Override
						public void run() {			
							Toast.makeText(mContext, "Requested resource not found", Toast.LENGTH_LONG).show();
						}
					};
				} else if (statusCode == 500) {
					new Runnable() {
						@Override
						public void run() {			
							Toast.makeText(mContext, "Something went wrong at server end", Toast.LENGTH_LONG).show();
						}
					};

				} else {
					new Runnable() {
						@Override
						public void run() {			
							Toast.makeText(mContext, "Unexpected Error occcured! [Most common Error: Device might not be connected to Internet]",
									Toast.LENGTH_LONG).show();
						}
					};
				}
			}
		});
	}
	
	private void processUserContent(String response) {

		try {
			JSONObject responseObj = new JSONObject(response);
			JSONObject resultObj = responseObj.getJSONObject("result");
			
			JSONArray userContentArray = resultObj.getJSONArray("UserContent");
			if(!(userContentArray.isNull(0))) {
				for(int i=0; i<userContentArray.length(); i++) {
					String week = userContentArray.getString(i);
					ArticleDao articleDao = mDaoSession.getArticleDao();
					
					List<Article> articles = articleDao.queryBuilder().where(ArticleDao.Properties.Week_id.eq(week)).list();
					for(Article article : articles) {
						article.setShow_enabled(true);
						articleDao.update(article);
					}
				}
			}
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		
	}
}
