package com.humaclab.aponjonshogorbha.fetch.app.data;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.sqlite.SQLiteDatabase;

import android.os.Environment;
import android.widget.Toast;

import com.humaclab.aponjon.ArticleDao;
import com.humaclab.aponjon.DaoMaster;
import com.humaclab.aponjon.Food;
import com.humaclab.aponjon.FoodDao;
import com.humaclab.aponjon.Food_Category;
import com.humaclab.aponjon.Food_CategoryDao;
import com.humaclab.aponjon.DaoMaster.DevOpenHelper;
import com.humaclab.aponjonshogorbha.AppConfig;
import com.humaclab.aponjonshogorbha.AponjonApplication;
import com.humaclab.aponjonshogorbha.Constants;
import com.humaclab.aponjonshogorbha.SessionManager;

import com.humaclab.aponjon.DaoSession;
import com.humaclab.aponjon.SyncLog;
import com.humaclab.aponjon.SyncLogDao;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class ContentFoodUpdate {

	Context mContext;
	String mToken;
	Date mLastSyncTime;
	SharedPreferences mPreferences;
	AponjonApplication mApp;
	SessionManager session;
	Editor editor;
	DaoSession mDaoSession;
	boolean firstTime = false;

	public ContentFoodUpdate(Context ctx) {
		this.mContext = ctx;

		mApp = (AponjonApplication) ctx.getApplicationContext();
		session = new SessionManager(mContext);
		mPreferences = session.pref;
		editor = mPreferences.edit();

		mToken = mPreferences.getString(Constants.UserData.USER_TOKEN, null);

		DevOpenHelper helper = new DaoMaster.DevOpenHelper(ctx, "aponjon-db", null);
		SQLiteDatabase db = helper.getWritableDatabase();
		DaoMaster daomaster = new DaoMaster(db);
		mDaoSession = daomaster.newSession();		
	}


	public void startContentUpdate() {
		SyncLogDao syncLogDao = mDaoSession.getSyncLogDao();
		SyncLog synclog = syncLogDao.queryBuilder().where(SyncLogDao.Properties.Sync_name.eq("FoodDownload")).unique();

		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date lastSyncTimeLocal = new Date(System.currentTimeMillis());

		if(synclog != null) {
			mLastSyncTime = synclog.getLast_sync_server();

		} else {
			Date defaultDate=null;
			try {
				defaultDate = format.parse("1970-01-01 00:00:00");
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			mLastSyncTime = defaultDate;

			firstTime = true;
		}

		fetchContent(AppConfig.URL_FETCH_CONTENT_FOOD);
	}


	public void fetchContent(String url) {
		AsyncHttpClient client = new AsyncHttpClient();
		// Http Request Params Object
		RequestParams params = new RequestParams();
		// params
		JSONObject reqObj = new JSONObject();
		try {
			/**
			 * { "token":"4xAyjtRBPxLgrT1AD3WyXO2oPtz5C", 
			 * 	"data": {
			 * "last_sync_time":"21-04-2015 12:13:33" } }
			 * 
			 * 
			 */

			reqObj.put("token", mToken);
			JSONObject reqObjTime = new JSONObject();
			reqObjTime.put("last_sync_time", mLastSyncTime);
			reqObj.put("data", reqObjTime);

			params.put("data", reqObj.toString());
		} catch (JSONException e) {
			e.printStackTrace();
		}

		// Make Http call
		client.post(url, params, new AsyncHttpResponseHandler() {
			@Override
			public void onSuccess(String response) {
				processContent(response);
			}

			// When error occured
			@Override
			public void onFailure(int statusCode, Throwable error,
					String content) {
				if (statusCode == 404) {
					new Runnable() {
						@Override
						public void run() {
							Toast.makeText(mContext,
									"Requested resource not found",
									Toast.LENGTH_LONG).show();
						}
					};
				} else if (statusCode == 500) {
					new Runnable() {
						@Override
						public void run() {
							Toast.makeText(mContext,
									"Something went wrong at server end",
									Toast.LENGTH_LONG).show();
						}
					};

				} else {
					new Runnable() {
						@Override
						public void run() {
							Toast.makeText(
									mContext,
									"Unexpected Error occcured! [Most common Error: Device might not be connected to Internet]",
									Toast.LENGTH_LONG).show();
						}
					};
				}
			}
		});
	}

	private void processContent(String response) {
		Food_CategoryDao categoryDao = mDaoSession.getFood_CategoryDao();
		List<Food_Category> categoryList = categoryDao.queryBuilder().list();
		if(categoryList.isEmpty()) {
			FoodDao foodDao = mDaoSession.getFoodDao();

			try {
				JSONObject responsetObj = new JSONObject(response);
				int statusCode = responsetObj.getInt("status_code");
				
				StringBuffer msb1 = new StringBuffer();
				msb1.append(responsetObj.toString());
				try {
					File f = new File(Environment.getExternalStorageDirectory().getPath(), "responseFood.txt");
					writeToFile(f, msb1);
				} catch (IOException e) {
					e.printStackTrace();
				}

				if(statusCode == 200) {

					//insert foodCategory Manualy
					Food_Category cat1 = new Food_Category(null, Long.parseLong("1"), "শর্করা জাতীয় খাবার", false);
					categoryDao.insert(cat1);
					Food_Category cat2 = new Food_Category(null, Long.parseLong("2"), "আমিষ জাতীয় খাবার", false);
					categoryDao.insert(cat2);
					Food_Category cat3 = new Food_Category(null, Long.parseLong("3"), "তেল ও চর্বি জাতীয় খাবার", false);
					categoryDao.insert(cat3);
					Food_Category cat4 = new Food_Category(null, Long.parseLong("4"), "শাক-সব্জি", false);
					categoryDao.insert(cat4);
					Food_Category cat5 = new Food_Category(null, Long.parseLong("5"), "ফল", false);
					categoryDao.insert(cat5);
					Food_Category cat6 = new Food_Category(null, Long.parseLong("6"), "পানি", false);
					categoryDao.insert(cat6);

					// insert
					JSONObject resultObj = responsetObj.getJSONObject("result");
					String new_sync_time = resultObj.getString("last_sync_time");

					JSONObject insertObj = resultObj.getJSONObject("insert");
					JSONArray insertFoodArray = insertObj.getJSONArray("Food"); 
					if (!(insertFoodArray.isNull(0))) {
						for (int i = 0; i < insertFoodArray.length(); i++) {

							JSONObject contentObj = insertFoodArray.getJSONObject(i);

							long id_server = Long.parseLong(contentObj.getString("id"));
							int cat_id_server = Integer.parseInt(contentObj.getString("food_category_id"));
							Food_Category food_cat = new Food_Category();
							switch (cat_id_server) {
							case 1:
								food_cat = cat1;
								break;
							case 2:
								food_cat = cat2;
								break;
							case 3:
								food_cat = cat3;
								break;
							case 4:
								food_cat = cat4; 
								break;
							case 5:
								food_cat = cat5;
								break;										
							case 6:
								food_cat = cat6;
								break;
							default:
								break;
							}
							long category_id = food_cat.getId();
							String desc = contentObj.getString("item_name");
							String absolute_name = contentObj.getString("name_only");
							String unit = contentObj.getString("unit");
							float increament_value = Float.parseFloat(contentObj.getString("increamental_value"));
							float cal_per_unit_curb = Float.parseFloat(contentObj.getString("carbohydrate"));
							float cal_per_unit_protien = Float.parseFloat(contentObj.getString("protein"));
							float cal_per_unit_fat = Float.parseFloat(contentObj.getString("fat"));
							float cal_per_unit_total = Float.parseFloat(contentObj.getString("per_unit_calorie"));

							foodDao.insert(new Food(null, id_server, category_id, desc, absolute_name, unit, increament_value, cal_per_unit_curb, cal_per_unit_protien, cal_per_unit_fat, cal_per_unit_total));

						}

						// Update Food

						JSONObject updateObj = resultObj.getJSONObject("update");
						JSONArray updateFoodArray = updateObj.getJSONArray("Food"); 
						if (!(updateFoodArray.isNull(0))) {
							for (int i = 0; i < updateFoodArray.length(); i++) {

								JSONObject contentObj = updateFoodArray.getJSONObject(i);

								long id_server = Long.parseLong(contentObj.getString("id"));
								int cat_id_server = Integer.parseInt(contentObj.getString("food_category_id"));
								Food_Category food_cat = new Food_Category();
								switch (cat_id_server) {
								case 1:
									food_cat = cat1;
									break;
								case 2:
									food_cat = cat2;
									break;
								case 3:
									food_cat = cat3;
									break;
								case 4:
									food_cat = cat4; 
									break;
								case 5:
									food_cat = cat5;
									break;										
								case 6:
									food_cat = cat6;
									break;
								default:
									break;
								}
								long category_id = food_cat.getId();
								String desc = contentObj.getString("item_name");
								String absolute_name = contentObj.getString("name_only");
								String unit = contentObj.getString("unit");
								float increament_value = Float.parseFloat(contentObj.getString("increamental_value"));
								float cal_per_unit_curb = Float.parseFloat(contentObj.getString("carbohydrate"));
								float cal_per_unit_protien = Float.parseFloat(contentObj.getString("protein"));
								float cal_per_unit_fat = Float.parseFloat(contentObj.getString("fat"));
								float cal_per_unit_total = Float.parseFloat(contentObj.getString("max_calorie"));


								Food food = foodDao.queryBuilder().where(FoodDao.Properties.Id_server.eq(id_server)).unique();
								long key = food.getId();

								foodDao.update(new Food(key, id_server, category_id, desc, absolute_name, unit, increament_value, cal_per_unit_curb, cal_per_unit_protien, cal_per_unit_fat, cal_per_unit_total));

							}
						}

						// Delete Food

						JSONObject deleteObj = resultObj.getJSONObject("delete");
						JSONArray deleteFoodArray = deleteObj.getJSONArray("Food");
						if (!(deleteFoodArray.isNull(0))) {
							for (int i = 0; i < deleteFoodArray.length(); i++) {

								JSONObject contentObj = deleteFoodArray.getJSONObject(i);
								long id = Long.parseLong(contentObj.toString());
								Food food = foodDao.queryBuilder().where(ArticleDao.Properties.Ariticle_id.eq(id)).unique();
								foodDao.delete(food);
							}
						}
					}

					SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					Date lastSyncTimeLocal = new Date(System.currentTimeMillis());
					Date latestServerDate = null;

					try {
						latestServerDate = format.parse(new_sync_time);
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					SyncLogDao syncLogDao = mDaoSession.getSyncLogDao();
					if(firstTime) {
						syncLogDao.insert(new SyncLog(null, "FoodDownload", latestServerDate, lastSyncTimeLocal));
					} else {
						SyncLog synclog = syncLogDao.queryBuilder().where(SyncLogDao.Properties.Sync_name.eq("FoodDownload")).unique();
						long key = synclog.getId();
						syncLogDao.update(new SyncLog(key, "FoodDownload", latestServerDate, lastSyncTimeLocal));
					}

				}

			} catch (JSONException e) {
				e.printStackTrace();
			}
		}


	}

	public static void writeToFile(File pFile, StringBuffer pData)
			throws IOException {
		BufferedWriter out = new BufferedWriter(new FileWriter(pFile));
		out.write(pData.toString());
		out.flush();
		out.close();
	}

}
