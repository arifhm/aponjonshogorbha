package com.humaclab.aponjon.receivers;


import com.bd.aponjon.pregnancy.R;
import com.dnet.mama.taskmanager.TaskManagerFragment;
import com.dnet.mama.taskmanager.TaskManagerTab;
import com.humaclab.aponjon.DaoSession;
import com.humaclab.aponjon.Task;
import com.humaclab.aponjon.TaskDao;
import com.humaclab.aponjonshogorbha.AponjonApplication;
import com.humaclab.aponjonshogorbha.activities.DashboardActivity;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

public class AlarmReceiver extends BroadcastReceiver {
	AponjonApplication mApp;
	DaoSession mDaoSession;
	
	@Override
    public void onReceive(Context context, Intent intent) {
		mApp = AponjonApplication.getInstance();
		mDaoSession = mApp.getDaoSession();
		
		long taskId = intent.getLongExtra("task_id", 0);
		TaskDao taskDao = mDaoSession.getTaskDao();
		Task task = taskDao.load(taskId);
		issueNotification(context, task);
 
    }
	
	 public void issueNotification(Context ctx, Task task) {
	    	NotificationCompat.Builder mNotifBuilder = new NotificationCompat.Builder(ctx)
	        .setSmallIcon(R.drawable.logo_aponjon)
	        .setContentTitle(task.getTitle())
	        .setContentText(task.getDescription());
	    	
	    	Intent resultIntent = new Intent(ctx, TaskManagerTab.class);
	    	resultIntent.putExtra(TaskManagerFragment.FILTER, TaskManagerFragment.FILTER_MONTH);
	    	resultIntent.putExtra("date", task.getTask_date());
	    	
	    	// The stack builder object will contain an artificial back stack for the
	    	// started Activity.
	    	// This ensures that navigating backward from the Activity leads out of
	    	// your application to the Home screen.
	    	TaskStackBuilder stackBuilder = TaskStackBuilder.create(ctx);
	    	// Adds the back stack for the Intent (but not the Intent itself)
	    	stackBuilder.addParentStack(DashboardActivity.class);
	    	// Adds the Intent that starts the Activity to the top of the stack
	    	stackBuilder.addNextIntent(resultIntent);
	    	
	    	// Because clicking the notification opens a new ("special") activity, there's
	    	// no need to create an artificial back stack.
	    	PendingIntent resultPendingIntent =
	    	    PendingIntent.getActivity(
	    	    ctx,
	    	    0,
	    	    resultIntent,
	    	    PendingIntent.FLAG_UPDATE_CURRENT
	    	);
	    	
	    	mNotifBuilder.setContentIntent(resultPendingIntent);
	    	
	    	// Sets an ID for the notification
	    	int mNotificationId = task.getCatId();
	    	// Gets an instance of the NotificationManager service
	    	NotificationManager mNotifyMgr = 
	    	        (NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE);
	    	// Builds the notification and issues it.
	    	mNotifyMgr.notify(mNotificationId, mNotifBuilder.build());
	    }
}
