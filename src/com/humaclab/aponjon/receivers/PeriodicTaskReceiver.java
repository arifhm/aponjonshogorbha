package com.humaclab.aponjon.receivers;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.humaclab.aponjon.DaoSession;
import com.humaclab.aponjon.Task;
import com.humaclab.aponjon.TaskDao;
import com.humaclab.aponjonshogorbha.AponjonApplication;
import com.humaclab.aponjonshogorbha.Constants;
import com.humaclab.aponjonshogorbha.SessionManager;
import com.humaclab.aponjonshogorbha.activities.DashboardActivity;
import com.humaclab.aponjonshogorbha.activities.TaskManagerActivity;
import com.humaclab.aponjonshogorbha.fetch.app.data.PeriodicContentUpdate;
import com.humaclab.aponjonshogorbha.userdata.upload.UploadUserAllDataTask;

import com.bd.aponjon.pregnancy.R;
import com.dnet.mama.taskmanager.TaskManagerFragment;
import com.dnet.mama.taskmanager.TaskManagerTab;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.SystemClock;
import android.support.v4.app.NotificationBuilderWithBuilderAccessor;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;


public class PeriodicTaskReceiver extends BroadcastReceiver {

    private static final String TAG = "PeriodicTaskReceiver";
    private static final String INTENT_ACTION = "com.example.app.PERIODIC_TASK_HEART_BEAT";
    
    private NotificationCompat.Builder mNotifBuilder;
    
    @Override
    public void onReceive(Context context, Intent intent) {
        if (!intent.getAction().isEmpty() && intent.getAction() != null) {
            AponjonApplication myApplication = (AponjonApplication) context.getApplicationContext();
            SharedPreferences sharedPreferences = myApplication.getSharedPreferences();

            if (intent.getAction().equals("android.intent.action.BATTERY_LOW")) {
                sharedPreferences.edit().putBoolean(Constants.BACKGROUND_SERVICE_BATTERY_CONTROL, false).apply();
                stopPeriodicTaskHeartBeat(context);
            } else if (intent.getAction().equals("android.intent.action.BATTERY_OKAY")) {
                sharedPreferences.edit().putBoolean(Constants.BACKGROUND_SERVICE_BATTERY_CONTROL, true).apply();
                restartPeriodicTaskHeartBeat(context, myApplication);
            } else if (intent.getAction().equals(INTENT_ACTION)) {
                doPeriodicTask(context, myApplication);
            }
        }
    }

    private void doPeriodicTask(Context context, AponjonApplication myApplication) {
    	AponjonApplication app = AponjonApplication.getInstance();
    	DaoSession daoSession = app.getDaoSession();
    	
    	UploadUserAllDataTask upUserData = new UploadUserAllDataTask(context);
    	upUserData.runTasks();
    	
    	
    	PeriodicContentUpdate task = new PeriodicContentUpdate(context);
		task.updateContent();
		
		if(isAtStartOfDay()) {
			startSystemCustomTaskNotify(context);
		}
		
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.HOUR_OF_DAY, 1);
		Date anHourLaterOfcurTime = cal.getTime();
		
		cal.add(Calendar.HOUR_OF_DAY, 1);		
		Date twoHourLaterOfcurTime = cal.getTime();
				
		TaskDao taskDao = daoSession.getTaskDao();
		List<Task> setAlarmTasks = taskDao.queryBuilder()
				.where(TaskDao.Properties.CatId.eq(1), TaskDao.Properties.Task_date.between(anHourLaterOfcurTime, twoHourLaterOfcurTime)).list();
		if(!setAlarmTasks.isEmpty()) {
			for(Task alarmTask : setAlarmTasks) {
				Intent braodcast_intent = new Intent(context, AlarmReceiver.class);
				braodcast_intent.putExtra("task_id", alarmTask.getId());
				PendingIntent alarmSender = PendingIntent.getBroadcast(context, 0, braodcast_intent, 0);
				//Set the alarm to 10 seconds from now
		        Calendar calHour = Calendar.getInstance();
		        calHour.setTime(alarmTask.getTask_date());
		        long timeAt = calHour.getTimeInMillis();
		        calHour.add(Calendar.HOUR_OF_DAY, -1);
		        long timeHourBefore = calHour.getTimeInMillis();
		       
		        // Schedule the alarm!
		        AlarmManager am = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
		        am.set(AlarmManager.RTC_WAKEUP, timeHourBefore, alarmSender);
		        am.set(AlarmManager.RTC_WAKEUP, timeAt, alarmSender);
			}
		}
    }

    public void restartPeriodicTaskHeartBeat(Context context, AponjonApplication myApplication) {

        SharedPreferences sharedPreferences = myApplication.getSharedPreferences();
        boolean isBatteryOk = sharedPreferences.getBoolean(Constants.BACKGROUND_SERVICE_BATTERY_CONTROL, true);
        Intent alarmIntent = new Intent(context, PeriodicTaskReceiver.class);
        boolean isAlarmUp = PendingIntent.getBroadcast(context, 0, alarmIntent, PendingIntent.FLAG_NO_CREATE) != null;
        if (isBatteryOk && !isAlarmUp) {
            AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            alarmIntent.setAction(INTENT_ACTION);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, alarmIntent, 0);
            alarmManager.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime(), AlarmManager.INTERVAL_HOUR, pendingIntent);
       }
    }

    public void stopPeriodicTaskHeartBeat(Context context) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent alarmIntent = new Intent(context, PeriodicTaskReceiver.class);
        alarmIntent.setAction(INTENT_ACTION);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, alarmIntent, 0);
        alarmManager.cancel(pendingIntent);
    }
    
	public boolean isAtStartOfDay() {
    	Calendar cal = Calendar.getInstance();
    	Date curTime = cal.getTime();
    	cal.set(Calendar.HOUR_OF_DAY, 0);
    	cal.set(Calendar.MINUTE, 0);
    	cal.set(Calendar.SECOND, 0);
    	Date headTime = cal.getTime();
    	cal.add(Calendar.HOUR_OF_DAY, 1);
    	Date tailTime = cal.getTime();
    	
    	if(curTime.equals(headTime) || (curTime.after(headTime) && curTime.before(tailTime))) {
    		return true;
    	}    	
		return false;
    }
	
	public void startSystemCustomTaskNotify(Context ctx) {
		SessionManager mgr = new SessionManager(ctx);
		AponjonApplication app = AponjonApplication.getInstance();
		
		if(mgr.isLoggedIn()) {
			Calendar cal = Calendar.getInstance();
			Date curDate = cal.getTime();
			cal.add(Calendar.DATE, 2);
			
			int curDay = getUserCurrentDay(ctx);
			int twoDayAfterCurDay = curDay+2;
			
			Date dateAfterTwoDay = cal.getTime();
			
			DaoSession daoSession = app.getDaoSession();
			
			// Generate Notification on day for system task
			TaskDao taskDao = daoSession.getTaskDao();
			List<Task> tasksSystemAfterTwoDay = taskDao.queryBuilder()
					.where(TaskDao.Properties.CatId.eq(2), TaskDao.Properties.DayNo.eq(twoDayAfterCurDay)).list();
			if(!tasksSystemAfterTwoDay.isEmpty()) {
				for(Task task : tasksSystemAfterTwoDay) {
					issueNotification(ctx, task, dateAfterTwoDay);
				}
			}
				
			// Generate Notification on day for custom and system task
			List<Task> tasksSysCustOnDay = taskDao.queryBuilder().where(TaskDao.Properties.DayNo.eq(curDay)).list();
			if(!tasksSysCustOnDay.isEmpty()) {
				for(Task task : tasksSysCustOnDay) {
					issueNotification(ctx, task, curDate);
				}
			}		
		}
		
	}
	
	public void startCustomOnTimeNotificaion() {
		
	}
	
	public int getUserCurrentDay(Context ctx) {
		SessionManager mgr = new SessionManager(ctx);
		SharedPreferences prefs = mgr.getPrefs();
		SimpleDateFormat format = new SimpleDateFormat();
		format.applyPattern("yyyy-MM-dd");
				
		Date lmp = null;
		Date curDate = null;
		int dayPassed = 0;
		try {
			lmp = format.parse(prefs.getString(Constants.UserData.USER_LMP, null));
			curDate = new Date(System.currentTimeMillis());
			dayPassed = (int)((curDate.getTime() - lmp.getTime()) / (1000*60*60*24l));
			
		} catch (ParseException e) {
			e.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
		}
		return dayPassed;
	}
    
    public void issueNotification(Context ctx, Task task, Date date) {
    	NotificationCompat.Builder mNotifBuilder = new NotificationCompat.Builder(ctx)
        .setSmallIcon(R.drawable.logo_aponjon)
        .setContentTitle(task.getTitle())
        .setContentText(task.getDescription());
    	
    	Intent resultIntent = new Intent(ctx, TaskManagerTab.class);
    	resultIntent.putExtra(TaskManagerFragment.FILTER, TaskManagerFragment.FILTER_MONTH);
    	resultIntent.putExtra("date", date);
    	
    	// The stack builder object will contain an artificial back stack for the
    	// started Activity.
    	// This ensures that navigating backward from the Activity leads out of
    	// your application to the Home screen.
    	TaskStackBuilder stackBuilder = TaskStackBuilder.create(ctx);
    	// Adds the back stack for the Intent (but not the Intent itself)
    	stackBuilder.addParentStack(DashboardActivity.class);
    	// Adds the Intent that starts the Activity to the top of the stack
    	stackBuilder.addNextIntent(resultIntent);
    	
    	// Because clicking the notification opens a new ("special") activity, there's
    	// no need to create an artificial back stack.
    	PendingIntent resultPendingIntent =
    	    PendingIntent.getActivity(
    	    ctx,
    	    0,
    	    resultIntent,
    	    PendingIntent.FLAG_UPDATE_CURRENT
    	);
    	
    	mNotifBuilder.setContentIntent(resultPendingIntent);
    	
    	// Sets an ID for the notification
    	int mNotificationId = task.getCatId();
    	// Gets an instance of the NotificationManager service
    	NotificationManager mNotifyMgr = 
    	        (NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE);
    	// Builds the notification and issues it.
    	mNotifyMgr.notify(mNotificationId, mNotifBuilder.build());
    }
    
}