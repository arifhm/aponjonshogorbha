package bengali.language.support;

import android.content.Context;

import com.omicronlab.avro.PhoneticParser;
import com.omicronlab.avro.PhoneticXmlLoader;

public class BengaliUnicodeString {

	
	public static String getBengaliUTF(String text){
		GSUB.text = text.toCharArray();
		GSUB.newlength = text.length();
//		int newlength=count;
		int ll = 0,ret=2;
//		__android_log_print(ANDROID_LOG_VERBOSE, "REORDER", "%s",reorder);
		while ((ret=GSUB.mygsub(GSUB.newlength))==2 &&
				ll < 10) {
		}
		text = new String(GSUB.text,0,GSUB.newlength);
		text = new String(Shape.reorder(text.toCharArray()));
		
		return text;
	}
	
	public static String convertToBengaliText(Context context,String text){
		PhoneticParser avro;
		avro = PhoneticParser.getInstance();    
		avro.setLoader(new PhoneticXmlLoader(context));
		
		return avro.parse(text);
	}
}